/**
 */
package textadventure.util;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

import textadventure.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see textadventure.TextadventurePackage
 * @generated
 */
public class TextadventureSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static TextadventurePackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TextadventureSwitch() {
		if (modelPackage == null) {
			modelPackage = TextadventurePackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @parameter ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case TextadventurePackage.EWORLD: {
				EWorld eWorld = (EWorld)theEObject;
				T result = caseEWorld(eWorld);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TextadventurePackage.ETILE: {
				ETile eTile = (ETile)theEObject;
				T result = caseETile(eTile);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TextadventurePackage.ECHECK_POINT: {
				ECheckPoint eCheckPoint = (ECheckPoint)theEObject;
				T result = caseECheckPoint(eCheckPoint);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TextadventurePackage.EDESTINATION: {
				EDestination eDestination = (EDestination)theEObject;
				T result = caseEDestination(eDestination);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TextadventurePackage.EITEM: {
				EItem eItem = (EItem)theEObject;
				T result = caseEItem(eItem);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TextadventurePackage.EBINDING: {
				EBinding eBinding = (EBinding)theEObject;
				T result = caseEBinding(eBinding);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TextadventurePackage.EEFFECT: {
				EEffect eEffect = (EEffect)theEObject;
				T result = caseEEffect(eEffect);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TextadventurePackage.EWHILE_EFFECT: {
				EWhileEffect eWhileEffect = (EWhileEffect)theEObject;
				T result = caseEWhileEffect(eWhileEffect);
				if (result == null) result = caseEEffect(eWhileEffect);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TextadventurePackage.EACTION: {
				EAction eAction = (EAction)theEObject;
				T result = caseEAction(eAction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TextadventurePackage.EREMOVE_ACTION: {
				ERemoveAction eRemoveAction = (ERemoveAction)theEObject;
				T result = caseERemoveAction(eRemoveAction);
				if (result == null) result = caseEAction(eRemoveAction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TextadventurePackage.ELIST_DESCRIPTION: {
				EListDescription eListDescription = (EListDescription)theEObject;
				T result = caseEListDescription(eListDescription);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TextadventurePackage.EPERSON: {
				EPerson ePerson = (EPerson)theEObject;
				T result = caseEPerson(ePerson);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TextadventurePackage.EBATTLE: {
				EBattle eBattle = (EBattle)theEObject;
				T result = caseEBattle(eBattle);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TextadventurePackage.EBATTLE_CUSTOMIZATION: {
				EBattleCustomization eBattleCustomization = (EBattleCustomization)theEObject;
				T result = caseEBattleCustomization(eBattleCustomization);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TextadventurePackage.EFUNCTION: {
				EFunction eFunction = (EFunction)theEObject;
				T result = caseEFunction(eFunction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TextadventurePackage.EFUNCTION_BODY: {
				EFunctionBody eFunctionBody = (EFunctionBody)theEObject;
				T result = caseEFunctionBody(eFunctionBody);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TextadventurePackage.ESTATEMENT: {
				EStatement eStatement = (EStatement)theEObject;
				T result = caseEStatement(eStatement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TextadventurePackage.EPRINT_STATEMENT: {
				EPrintStatement ePrintStatement = (EPrintStatement)theEObject;
				T result = caseEPrintStatement(ePrintStatement);
				if (result == null) result = caseEStatement(ePrintStatement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TextadventurePackage.EREMOVE_PLAYER_STATEMENT: {
				ERemovePlayerStatement eRemovePlayerStatement = (ERemovePlayerStatement)theEObject;
				T result = caseERemovePlayerStatement(eRemovePlayerStatement);
				if (result == null) result = caseEStatement(eRemovePlayerStatement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TextadventurePackage.EMETHOD_CALL_STATEMENT: {
				EMethodCallStatement eMethodCallStatement = (EMethodCallStatement)theEObject;
				T result = caseEMethodCallStatement(eMethodCallStatement);
				if (result == null) result = caseEStatement(eMethodCallStatement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TextadventurePackage.EABILITY: {
				EAbility eAbility = (EAbility)theEObject;
				T result = caseEAbility(eAbility);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TextadventurePackage.EIGNORE_ITEM_ABILITY: {
				EIgnoreItemAbility eIgnoreItemAbility = (EIgnoreItemAbility)theEObject;
				T result = caseEIgnoreItemAbility(eIgnoreItemAbility);
				if (result == null) result = caseEAbility(eIgnoreItemAbility);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EWorld</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EWorld</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEWorld(EWorld object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ETile</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ETile</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseETile(ETile object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ECheck Point</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ECheck Point</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseECheckPoint(ECheckPoint object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EDestination</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EDestination</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEDestination(EDestination object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EItem</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EItem</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEItem(EItem object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EBinding</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EBinding</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEBinding(EBinding object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EEffect</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EEffect</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEEffect(EEffect object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EWhile Effect</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EWhile Effect</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEWhileEffect(EWhileEffect object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EAction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EAction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEAction(EAction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ERemove Action</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ERemove Action</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseERemoveAction(ERemoveAction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EList Description</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EList Description</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEListDescription(EListDescription object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EPerson</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EPerson</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEPerson(EPerson object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EBattle</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EBattle</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEBattle(EBattle object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EBattle Customization</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EBattle Customization</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEBattleCustomization(EBattleCustomization object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EFunction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EFunction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEFunction(EFunction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EFunction Body</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EFunction Body</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEFunctionBody(EFunctionBody object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EStatement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EStatement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEStatement(EStatement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EPrint Statement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EPrint Statement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEPrintStatement(EPrintStatement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ERemove Player Statement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ERemove Player Statement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseERemovePlayerStatement(ERemovePlayerStatement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EMethod Call Statement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EMethod Call Statement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEMethodCallStatement(EMethodCallStatement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EAbility</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EAbility</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEAbility(EAbility object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EIgnore Item Ability</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EIgnore Item Ability</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEIgnoreItemAbility(EIgnoreItemAbility object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //TextadventureSwitch
