/**
 */
package textadventure.util;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

import textadventure.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see textadventure.TextadventurePackage
 * @generated
 */
public class TextadventureAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static TextadventurePackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TextadventureAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = TextadventurePackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TextadventureSwitch<Adapter> modelSwitch =
		new TextadventureSwitch<Adapter>() {
			@Override
			public Adapter caseEWorld(EWorld object) {
				return createEWorldAdapter();
			}
			@Override
			public Adapter caseETile(ETile object) {
				return createETileAdapter();
			}
			@Override
			public Adapter caseECheckPoint(ECheckPoint object) {
				return createECheckPointAdapter();
			}
			@Override
			public Adapter caseEDestination(EDestination object) {
				return createEDestinationAdapter();
			}
			@Override
			public Adapter caseEItem(EItem object) {
				return createEItemAdapter();
			}
			@Override
			public Adapter caseEBinding(EBinding object) {
				return createEBindingAdapter();
			}
			@Override
			public Adapter caseEEffect(EEffect object) {
				return createEEffectAdapter();
			}
			@Override
			public Adapter caseEWhileEffect(EWhileEffect object) {
				return createEWhileEffectAdapter();
			}
			@Override
			public Adapter caseEAction(EAction object) {
				return createEActionAdapter();
			}
			@Override
			public Adapter caseERemoveAction(ERemoveAction object) {
				return createERemoveActionAdapter();
			}
			@Override
			public Adapter caseEListDescription(EListDescription object) {
				return createEListDescriptionAdapter();
			}
			@Override
			public Adapter caseEPerson(EPerson object) {
				return createEPersonAdapter();
			}
			@Override
			public Adapter caseEBattle(EBattle object) {
				return createEBattleAdapter();
			}
			@Override
			public Adapter caseEBattleCustomization(EBattleCustomization object) {
				return createEBattleCustomizationAdapter();
			}
			@Override
			public Adapter caseEFunction(EFunction object) {
				return createEFunctionAdapter();
			}
			@Override
			public Adapter caseEFunctionBody(EFunctionBody object) {
				return createEFunctionBodyAdapter();
			}
			@Override
			public Adapter caseEStatement(EStatement object) {
				return createEStatementAdapter();
			}
			@Override
			public Adapter caseEPrintStatement(EPrintStatement object) {
				return createEPrintStatementAdapter();
			}
			@Override
			public Adapter caseERemovePlayerStatement(ERemovePlayerStatement object) {
				return createERemovePlayerStatementAdapter();
			}
			@Override
			public Adapter caseEMethodCallStatement(EMethodCallStatement object) {
				return createEMethodCallStatementAdapter();
			}
			@Override
			public Adapter caseEAbility(EAbility object) {
				return createEAbilityAdapter();
			}
			@Override
			public Adapter caseEIgnoreItemAbility(EIgnoreItemAbility object) {
				return createEIgnoreItemAbilityAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link textadventure.EWorld <em>EWorld</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see textadventure.EWorld
	 * @generated
	 */
	public Adapter createEWorldAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link textadventure.ETile <em>ETile</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see textadventure.ETile
	 * @generated
	 */
	public Adapter createETileAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link textadventure.ECheckPoint <em>ECheck Point</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see textadventure.ECheckPoint
	 * @generated
	 */
	public Adapter createECheckPointAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link textadventure.EDestination <em>EDestination</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see textadventure.EDestination
	 * @generated
	 */
	public Adapter createEDestinationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link textadventure.EItem <em>EItem</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see textadventure.EItem
	 * @generated
	 */
	public Adapter createEItemAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link textadventure.EBinding <em>EBinding</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see textadventure.EBinding
	 * @generated
	 */
	public Adapter createEBindingAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link textadventure.EEffect <em>EEffect</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see textadventure.EEffect
	 * @generated
	 */
	public Adapter createEEffectAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link textadventure.EWhileEffect <em>EWhile Effect</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see textadventure.EWhileEffect
	 * @generated
	 */
	public Adapter createEWhileEffectAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link textadventure.EAction <em>EAction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see textadventure.EAction
	 * @generated
	 */
	public Adapter createEActionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link textadventure.ERemoveAction <em>ERemove Action</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see textadventure.ERemoveAction
	 * @generated
	 */
	public Adapter createERemoveActionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link textadventure.EListDescription <em>EList Description</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see textadventure.EListDescription
	 * @generated
	 */
	public Adapter createEListDescriptionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link textadventure.EPerson <em>EPerson</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see textadventure.EPerson
	 * @generated
	 */
	public Adapter createEPersonAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link textadventure.EBattle <em>EBattle</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see textadventure.EBattle
	 * @generated
	 */
	public Adapter createEBattleAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link textadventure.EBattleCustomization <em>EBattle Customization</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see textadventure.EBattleCustomization
	 * @generated
	 */
	public Adapter createEBattleCustomizationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link textadventure.EFunction <em>EFunction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see textadventure.EFunction
	 * @generated
	 */
	public Adapter createEFunctionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link textadventure.EFunctionBody <em>EFunction Body</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see textadventure.EFunctionBody
	 * @generated
	 */
	public Adapter createEFunctionBodyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link textadventure.EStatement <em>EStatement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see textadventure.EStatement
	 * @generated
	 */
	public Adapter createEStatementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link textadventure.EPrintStatement <em>EPrint Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see textadventure.EPrintStatement
	 * @generated
	 */
	public Adapter createEPrintStatementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link textadventure.ERemovePlayerStatement <em>ERemove Player Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see textadventure.ERemovePlayerStatement
	 * @generated
	 */
	public Adapter createERemovePlayerStatementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link textadventure.EMethodCallStatement <em>EMethod Call Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see textadventure.EMethodCallStatement
	 * @generated
	 */
	public Adapter createEMethodCallStatementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link textadventure.EAbility <em>EAbility</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see textadventure.EAbility
	 * @generated
	 */
	public Adapter createEAbilityAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link textadventure.EIgnoreItemAbility <em>EIgnore Item Ability</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see textadventure.EIgnoreItemAbility
	 * @generated
	 */
	public Adapter createEIgnoreItemAbilityAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //TextadventureAdapterFactory
