/**
 */
package textadventure;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>EFunction Body</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link textadventure.EFunctionBody#getStatements <em>Statements</em>}</li>
 * </ul>
 * </p>
 *
 * @see textadventure.TextadventurePackage#getEFunctionBody()
 * @model
 * @generated
 */
public interface EFunctionBody extends EObject {
	/**
	 * Returns the value of the '<em><b>Statements</b></em>' containment reference list.
	 * The list contents are of type {@link textadventure.EStatement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Statements</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Statements</em>' containment reference list.
	 * @see textadventure.TextadventurePackage#getEFunctionBody_Statements()
	 * @model containment="true"
	 * @generated
	 */
	EList<EStatement> getStatements();

} // EFunctionBody
