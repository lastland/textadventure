/**
 */
package textadventure;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>EWorld</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link textadventure.EWorld#getItems <em>Items</em>}</li>
 *   <li>{@link textadventure.EWorld#getPersons <em>Persons</em>}</li>
 *   <li>{@link textadventure.EWorld#getTiles <em>Tiles</em>}</li>
 *   <li>{@link textadventure.EWorld#getCheckpoint <em>Checkpoint</em>}</li>
 *   <li>{@link textadventure.EWorld#getBattle <em>Battle</em>}</li>
 *   <li>{@link textadventure.EWorld#getDestination <em>Destination</em>}</li>
 *   <li>{@link textadventure.EWorld#getPlayerPerson <em>Player Person</em>}</li>
 * </ul>
 * </p>
 *
 * @see textadventure.TextadventurePackage#getEWorld()
 * @model
 * @generated
 */
public interface EWorld extends EObject {
	/**
	 * Returns the value of the '<em><b>Items</b></em>' containment reference list.
	 * The list contents are of type {@link textadventure.EItem}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Items</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Items</em>' containment reference list.
	 * @see textadventure.TextadventurePackage#getEWorld_Items()
	 * @model containment="true"
	 * @generated
	 */
	EList<EItem> getItems();

	/**
	 * Returns the value of the '<em><b>Persons</b></em>' containment reference list.
	 * The list contents are of type {@link textadventure.EPerson}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Persons</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Persons</em>' containment reference list.
	 * @see textadventure.TextadventurePackage#getEWorld_Persons()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<EPerson> getPersons();

	/**
	 * Returns the value of the '<em><b>Tiles</b></em>' containment reference list.
	 * The list contents are of type {@link textadventure.ETile}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Tiles</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tiles</em>' containment reference list.
	 * @see textadventure.TextadventurePackage#getEWorld_Tiles()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<ETile> getTiles();

	/**
	 * Returns the value of the '<em><b>Checkpoint</b></em>' containment reference list.
	 * The list contents are of type {@link textadventure.ECheckPoint}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Checkpoint</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Checkpoint</em>' containment reference list.
	 * @see textadventure.TextadventurePackage#getEWorld_Checkpoint()
	 * @model containment="true"
	 * @generated
	 */
	EList<ECheckPoint> getCheckpoint();

	/**
	 * Returns the value of the '<em><b>Battle</b></em>' containment reference list.
	 * The list contents are of type {@link textadventure.EBattle}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Battle</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Battle</em>' containment reference list.
	 * @see textadventure.TextadventurePackage#getEWorld_Battle()
	 * @model containment="true"
	 * @generated
	 */
	EList<EBattle> getBattle();

	/**
	 * Returns the value of the '<em><b>Destination</b></em>' containment reference list.
	 * The list contents are of type {@link textadventure.EDestination}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Destination</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Destination</em>' containment reference list.
	 * @see textadventure.TextadventurePackage#getEWorld_Destination()
	 * @model containment="true"
	 * @generated
	 */
	EList<EDestination> getDestination();

	/**
	 * Returns the value of the '<em><b>Player Person</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Player Person</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Player Person</em>' reference.
	 * @see #setPlayerPerson(EPerson)
	 * @see textadventure.TextadventurePackage#getEWorld_PlayerPerson()
	 * @model required="true"
	 * @generated
	 */
	EPerson getPlayerPerson();

	/**
	 * Sets the value of the '{@link textadventure.EWorld#getPlayerPerson <em>Player Person</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Player Person</em>' reference.
	 * @see #getPlayerPerson()
	 * @generated
	 */
	void setPlayerPerson(EPerson value);

} // EWorld
