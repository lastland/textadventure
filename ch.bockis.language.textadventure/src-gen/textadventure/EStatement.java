/**
 */
package textadventure;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>EStatement</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see textadventure.TextadventurePackage#getEStatement()
 * @model abstract="true"
 * @generated
 */
public interface EStatement extends EObject {
} // EStatement
