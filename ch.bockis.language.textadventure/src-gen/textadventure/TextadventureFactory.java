/**
 */
package textadventure;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see textadventure.TextadventurePackage
 * @generated
 */
public interface TextadventureFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	TextadventureFactory eINSTANCE = textadventure.impl.TextadventureFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>EWorld</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>EWorld</em>'.
	 * @generated
	 */
	EWorld createEWorld();

	/**
	 * Returns a new object of class '<em>ETile</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>ETile</em>'.
	 * @generated
	 */
	ETile createETile();

	/**
	 * Returns a new object of class '<em>ECheck Point</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>ECheck Point</em>'.
	 * @generated
	 */
	ECheckPoint createECheckPoint();

	/**
	 * Returns a new object of class '<em>EDestination</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>EDestination</em>'.
	 * @generated
	 */
	EDestination createEDestination();

	/**
	 * Returns a new object of class '<em>EItem</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>EItem</em>'.
	 * @generated
	 */
	EItem createEItem();

	/**
	 * Returns a new object of class '<em>EBinding</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>EBinding</em>'.
	 * @generated
	 */
	EBinding createEBinding();

	/**
	 * Returns a new object of class '<em>EWhile Effect</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>EWhile Effect</em>'.
	 * @generated
	 */
	EWhileEffect createEWhileEffect();

	/**
	 * Returns a new object of class '<em>ERemove Action</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>ERemove Action</em>'.
	 * @generated
	 */
	ERemoveAction createERemoveAction();

	/**
	 * Returns a new object of class '<em>EList Description</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>EList Description</em>'.
	 * @generated
	 */
	EListDescription createEListDescription();

	/**
	 * Returns a new object of class '<em>EPerson</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>EPerson</em>'.
	 * @generated
	 */
	EPerson createEPerson();

	/**
	 * Returns a new object of class '<em>EBattle</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>EBattle</em>'.
	 * @generated
	 */
	EBattle createEBattle();

	/**
	 * Returns a new object of class '<em>EBattle Customization</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>EBattle Customization</em>'.
	 * @generated
	 */
	EBattleCustomization createEBattleCustomization();

	/**
	 * Returns a new object of class '<em>EFunction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>EFunction</em>'.
	 * @generated
	 */
	EFunction createEFunction();

	/**
	 * Returns a new object of class '<em>EFunction Body</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>EFunction Body</em>'.
	 * @generated
	 */
	EFunctionBody createEFunctionBody();

	/**
	 * Returns a new object of class '<em>EPrint Statement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>EPrint Statement</em>'.
	 * @generated
	 */
	EPrintStatement createEPrintStatement();

	/**
	 * Returns a new object of class '<em>ERemove Player Statement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>ERemove Player Statement</em>'.
	 * @generated
	 */
	ERemovePlayerStatement createERemovePlayerStatement();

	/**
	 * Returns a new object of class '<em>EMethod Call Statement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>EMethod Call Statement</em>'.
	 * @generated
	 */
	EMethodCallStatement createEMethodCallStatement();

	/**
	 * Returns a new object of class '<em>EIgnore Item Ability</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>EIgnore Item Ability</em>'.
	 * @generated
	 */
	EIgnoreItemAbility createEIgnoreItemAbility();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	TextadventurePackage getTextadventurePackage();

} //TextadventureFactory
