/**
 */
package textadventure;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>EBinding</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link textadventure.EBinding#getParticipant <em>Participant</em>}</li>
 *   <li>{@link textadventure.EBinding#getId <em>Id</em>}</li>
 * </ul>
 * </p>
 *
 * @see textadventure.TextadventurePackage#getEBinding()
 * @model
 * @generated
 */
public interface EBinding extends EObject {
	/**
	 * Returns the value of the '<em><b>Participant</b></em>' attribute.
	 * The literals are from the enumeration {@link textadventure.EParticipant}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Participant</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Participant</em>' attribute.
	 * @see textadventure.EParticipant
	 * @see #setParticipant(EParticipant)
	 * @see textadventure.TextadventurePackage#getEBinding_Participant()
	 * @model required="true"
	 * @generated
	 */
	EParticipant getParticipant();

	/**
	 * Sets the value of the '{@link textadventure.EBinding#getParticipant <em>Participant</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Participant</em>' attribute.
	 * @see textadventure.EParticipant
	 * @see #getParticipant()
	 * @generated
	 */
	void setParticipant(EParticipant value);

	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(String)
	 * @see textadventure.TextadventurePackage#getEBinding_Id()
	 * @model required="true"
	 * @generated
	 */
	String getId();

	/**
	 * Sets the value of the '{@link textadventure.EBinding#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #getId()
	 * @generated
	 */
	void setId(String value);

} // EBinding
