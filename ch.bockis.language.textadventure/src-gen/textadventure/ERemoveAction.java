/**
 */
package textadventure;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ERemove Action</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link textadventure.ERemoveAction#getWhat <em>What</em>}</li>
 *   <li>{@link textadventure.ERemoveAction#getFrom <em>From</em>}</li>
 * </ul>
 * </p>
 *
 * @see textadventure.TextadventurePackage#getERemoveAction()
 * @model
 * @generated
 */
public interface ERemoveAction extends EAction {
	/**
	 * Returns the value of the '<em><b>What</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>What</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>What</em>' reference.
	 * @see #setWhat(EBinding)
	 * @see textadventure.TextadventurePackage#getERemoveAction_What()
	 * @model required="true"
	 * @generated
	 */
	EBinding getWhat();

	/**
	 * Sets the value of the '{@link textadventure.ERemoveAction#getWhat <em>What</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>What</em>' reference.
	 * @see #getWhat()
	 * @generated
	 */
	void setWhat(EBinding value);

	/**
	 * Returns the value of the '<em><b>From</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>From</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>From</em>' containment reference.
	 * @see #setFrom(EListDescription)
	 * @see textadventure.TextadventurePackage#getERemoveAction_From()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EListDescription getFrom();

	/**
	 * Sets the value of the '{@link textadventure.ERemoveAction#getFrom <em>From</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>From</em>' containment reference.
	 * @see #getFrom()
	 * @generated
	 */
	void setFrom(EListDescription value);

} // ERemoveAction
