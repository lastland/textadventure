/**
 */
package textadventure;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>EBattle</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link textadventure.EBattle#getLocation <em>Location</em>}</li>
 *   <li>{@link textadventure.EBattle#getEnemy <em>Enemy</em>}</li>
 *   <li>{@link textadventure.EBattle#getLogics <em>Logics</em>}</li>
 * </ul>
 * </p>
 *
 * @see textadventure.TextadventurePackage#getEBattle()
 * @model
 * @generated
 */
public interface EBattle extends EObject {
	/**
	 * Returns the value of the '<em><b>Location</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Location</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Location</em>' reference.
	 * @see #setLocation(ETile)
	 * @see textadventure.TextadventurePackage#getEBattle_Location()
	 * @model required="true"
	 * @generated
	 */
	ETile getLocation();

	/**
	 * Sets the value of the '{@link textadventure.EBattle#getLocation <em>Location</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Location</em>' reference.
	 * @see #getLocation()
	 * @generated
	 */
	void setLocation(ETile value);

	/**
	 * Returns the value of the '<em><b>Enemy</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Enemy</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Enemy</em>' reference.
	 * @see #setEnemy(EPerson)
	 * @see textadventure.TextadventurePackage#getEBattle_Enemy()
	 * @model required="true"
	 * @generated
	 */
	EPerson getEnemy();

	/**
	 * Sets the value of the '{@link textadventure.EBattle#getEnemy <em>Enemy</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Enemy</em>' reference.
	 * @see #getEnemy()
	 * @generated
	 */
	void setEnemy(EPerson value);

	/**
	 * Returns the value of the '<em><b>Logics</b></em>' containment reference list.
	 * The list contents are of type {@link textadventure.EBattleCustomization}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Logics</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Logics</em>' containment reference list.
	 * @see textadventure.TextadventurePackage#getEBattle_Logics()
	 * @model containment="true"
	 * @generated
	 */
	EList<EBattleCustomization> getLogics();

} // EBattle
