/**
 */
package textadventure;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>EAction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see textadventure.TextadventurePackage#getEAction()
 * @model abstract="true"
 * @generated
 */
public interface EAction extends EObject {
} // EAction
