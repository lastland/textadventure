/**
 */
package textadventure;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>EBattle Customization</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link textadventure.EBattleCustomization#getWhen <em>When</em>}</li>
 *   <li>{@link textadventure.EBattleCustomization#getFunc <em>Func</em>}</li>
 * </ul>
 * </p>
 *
 * @see textadventure.TextadventurePackage#getEBattleCustomization()
 * @model
 * @generated
 */
public interface EBattleCustomization extends EObject {
	/**
	 * Returns the value of the '<em><b>When</b></em>' attribute.
	 * The literals are from the enumeration {@link textadventure.EBattleWhen}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>When</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>When</em>' attribute.
	 * @see textadventure.EBattleWhen
	 * @see #setWhen(EBattleWhen)
	 * @see textadventure.TextadventurePackage#getEBattleCustomization_When()
	 * @model required="true"
	 * @generated
	 */
	EBattleWhen getWhen();

	/**
	 * Sets the value of the '{@link textadventure.EBattleCustomization#getWhen <em>When</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>When</em>' attribute.
	 * @see textadventure.EBattleWhen
	 * @see #getWhen()
	 * @generated
	 */
	void setWhen(EBattleWhen value);

	/**
	 * Returns the value of the '<em><b>Func</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Func</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Func</em>' containment reference.
	 * @see #setFunc(EFunction)
	 * @see textadventure.TextadventurePackage#getEBattleCustomization_Func()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EFunction getFunc();

	/**
	 * Sets the value of the '{@link textadventure.EBattleCustomization#getFunc <em>Func</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Func</em>' containment reference.
	 * @see #getFunc()
	 * @generated
	 */
	void setFunc(EFunction value);

} // EBattleCustomization
