/**
 */
package textadventure;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ERemove Player Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link textadventure.ERemovePlayerStatement#getTarget <em>Target</em>}</li>
 * </ul>
 * </p>
 *
 * @see textadventure.TextadventurePackage#getERemovePlayerStatement()
 * @model
 * @generated
 */
public interface ERemovePlayerStatement extends EStatement {
	/**
	 * Returns the value of the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target</em>' reference.
	 * @see #setTarget(EPerson)
	 * @see textadventure.TextadventurePackage#getERemovePlayerStatement_Target()
	 * @model required="true"
	 * @generated
	 */
	EPerson getTarget();

	/**
	 * Sets the value of the '{@link textadventure.ERemovePlayerStatement#getTarget <em>Target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target</em>' reference.
	 * @see #getTarget()
	 * @generated
	 */
	void setTarget(EPerson value);

} // ERemovePlayerStatement
