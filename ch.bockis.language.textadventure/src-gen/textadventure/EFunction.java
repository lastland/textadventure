/**
 */
package textadventure;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>EFunction</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link textadventure.EFunction#getParams <em>Params</em>}</li>
 *   <li>{@link textadventure.EFunction#getBody <em>Body</em>}</li>
 * </ul>
 * </p>
 *
 * @see textadventure.TextadventurePackage#getEFunction()
 * @model
 * @generated
 */
public interface EFunction extends EObject {
	/**
	 * Returns the value of the '<em><b>Params</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Params</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Params</em>' attribute list.
	 * @see textadventure.TextadventurePackage#getEFunction_Params()
	 * @model
	 * @generated
	 */
	EList<String> getParams();

	/**
	 * Returns the value of the '<em><b>Body</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Body</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Body</em>' containment reference.
	 * @see #setBody(EFunctionBody)
	 * @see textadventure.TextadventurePackage#getEFunction_Body()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EFunctionBody getBody();

	/**
	 * Sets the value of the '{@link textadventure.EFunction#getBody <em>Body</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Body</em>' containment reference.
	 * @see #getBody()
	 * @generated
	 */
	void setBody(EFunctionBody value);

} // EFunction
