/**
 */
package textadventure;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>EWhile Effect</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see textadventure.TextadventurePackage#getEWhileEffect()
 * @model
 * @generated
 */
public interface EWhileEffect extends EEffect {
} // EWhileEffect
