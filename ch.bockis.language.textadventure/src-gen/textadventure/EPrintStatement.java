/**
 */
package textadventure;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>EPrint Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link textadventure.EPrintStatement#getOutput <em>Output</em>}</li>
 * </ul>
 * </p>
 *
 * @see textadventure.TextadventurePackage#getEPrintStatement()
 * @model
 * @generated
 */
public interface EPrintStatement extends EStatement {
	/**
	 * Returns the value of the '<em><b>Output</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Output</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Output</em>' attribute.
	 * @see #setOutput(String)
	 * @see textadventure.TextadventurePackage#getEPrintStatement_Output()
	 * @model required="true"
	 * @generated
	 */
	String getOutput();

	/**
	 * Sets the value of the '{@link textadventure.EPrintStatement#getOutput <em>Output</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Output</em>' attribute.
	 * @see #getOutput()
	 * @generated
	 */
	void setOutput(String value);

} // EPrintStatement
