/**
 */
package textadventure;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>EList Description</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link textadventure.EListDescription#getTarget <em>Target</em>}</li>
 *   <li>{@link textadventure.EListDescription#getProperty <em>Property</em>}</li>
 * </ul>
 * </p>
 *
 * @see textadventure.TextadventurePackage#getEListDescription()
 * @model
 * @generated
 */
public interface EListDescription extends EObject {
	/**
	 * Returns the value of the '<em><b>Target</b></em>' attribute.
	 * The literals are from the enumeration {@link textadventure.ETarget}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target</em>' attribute.
	 * @see textadventure.ETarget
	 * @see #setTarget(ETarget)
	 * @see textadventure.TextadventurePackage#getEListDescription_Target()
	 * @model required="true"
	 * @generated
	 */
	ETarget getTarget();

	/**
	 * Sets the value of the '{@link textadventure.EListDescription#getTarget <em>Target</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target</em>' attribute.
	 * @see textadventure.ETarget
	 * @see #getTarget()
	 * @generated
	 */
	void setTarget(ETarget value);

	/**
	 * Returns the value of the '<em><b>Property</b></em>' attribute.
	 * The literals are from the enumeration {@link textadventure.EProperty}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Property</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Property</em>' attribute.
	 * @see textadventure.EProperty
	 * @see #setProperty(EProperty)
	 * @see textadventure.TextadventurePackage#getEListDescription_Property()
	 * @model required="true"
	 * @generated
	 */
	EProperty getProperty();

	/**
	 * Sets the value of the '{@link textadventure.EListDescription#getProperty <em>Property</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Property</em>' attribute.
	 * @see textadventure.EProperty
	 * @see #getProperty()
	 * @generated
	 */
	void setProperty(EProperty value);

} // EListDescription
