/**
 */
package textadventure;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>EEffect</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link textadventure.EEffect#getCondition <em>Condition</em>}</li>
 *   <li>{@link textadventure.EEffect#getBinding <em>Binding</em>}</li>
 *   <li>{@link textadventure.EEffect#getAction <em>Action</em>}</li>
 * </ul>
 * </p>
 *
 * @see textadventure.TextadventurePackage#getEEffect()
 * @model abstract="true"
 * @generated
 */
public interface EEffect extends EObject {
	/**
	 * Returns the value of the '<em><b>Condition</b></em>' attribute.
	 * The literals are from the enumeration {@link textadventure.ECondition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Condition</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Condition</em>' attribute.
	 * @see textadventure.ECondition
	 * @see #setCondition(ECondition)
	 * @see textadventure.TextadventurePackage#getEEffect_Condition()
	 * @model required="true"
	 * @generated
	 */
	ECondition getCondition();

	/**
	 * Sets the value of the '{@link textadventure.EEffect#getCondition <em>Condition</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Condition</em>' attribute.
	 * @see textadventure.ECondition
	 * @see #getCondition()
	 * @generated
	 */
	void setCondition(ECondition value);

	/**
	 * Returns the value of the '<em><b>Binding</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Binding</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Binding</em>' containment reference.
	 * @see #setBinding(EBinding)
	 * @see textadventure.TextadventurePackage#getEEffect_Binding()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EBinding getBinding();

	/**
	 * Sets the value of the '{@link textadventure.EEffect#getBinding <em>Binding</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Binding</em>' containment reference.
	 * @see #getBinding()
	 * @generated
	 */
	void setBinding(EBinding value);

	/**
	 * Returns the value of the '<em><b>Action</b></em>' containment reference list.
	 * The list contents are of type {@link textadventure.EAction}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Action</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Action</em>' containment reference list.
	 * @see textadventure.TextadventurePackage#getEEffect_Action()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<EAction> getAction();

} // EEffect
