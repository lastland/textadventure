/**
 */
package textadventure;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>EIgnore Item Ability</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link textadventure.EIgnoreItemAbility#getIgnoredItem <em>Ignored Item</em>}</li>
 * </ul>
 * </p>
 *
 * @see textadventure.TextadventurePackage#getEIgnoreItemAbility()
 * @model
 * @generated
 */
public interface EIgnoreItemAbility extends EAbility {
	/**
	 * Returns the value of the '<em><b>Ignored Item</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ignored Item</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ignored Item</em>' reference.
	 * @see #setIgnoredItem(EItem)
	 * @see textadventure.TextadventurePackage#getEIgnoreItemAbility_IgnoredItem()
	 * @model required="true"
	 * @generated
	 */
	EItem getIgnoredItem();

	/**
	 * Sets the value of the '{@link textadventure.EIgnoreItemAbility#getIgnoredItem <em>Ignored Item</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ignored Item</em>' reference.
	 * @see #getIgnoredItem()
	 * @generated
	 */
	void setIgnoredItem(EItem value);

} // EIgnoreItemAbility
