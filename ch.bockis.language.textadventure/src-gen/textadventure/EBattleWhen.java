/**
 */
package textadventure;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>EBattle When</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see textadventure.TextadventurePackage#getEBattleWhen()
 * @model
 * @generated
 */
public enum EBattleWhen implements Enumerator {
	/**
	 * The '<em><b>Before Battle</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #BEFORE_BATTLE_VALUE
	 * @generated
	 * @ordered
	 */
	BEFORE_BATTLE(0, "BeforeBattle", "beforeBattle"),

	/**
	 * The '<em><b>After Battle</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #AFTER_BATTLE_VALUE
	 * @generated
	 * @ordered
	 */
	AFTER_BATTLE(1, "AfterBattle", "afterBattle"),

	/**
	 * The '<em><b>Before Each Turn</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #BEFORE_EACH_TURN_VALUE
	 * @generated
	 * @ordered
	 */
	BEFORE_EACH_TURN(2, "BeforeEachTurn", "onEachTurn");

	/**
	 * The '<em><b>Before Battle</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Before Battle</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #BEFORE_BATTLE
	 * @model name="BeforeBattle" literal="beforeBattle"
	 * @generated
	 * @ordered
	 */
	public static final int BEFORE_BATTLE_VALUE = 0;

	/**
	 * The '<em><b>After Battle</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>After Battle</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #AFTER_BATTLE
	 * @model name="AfterBattle" literal="afterBattle"
	 * @generated
	 * @ordered
	 */
	public static final int AFTER_BATTLE_VALUE = 1;

	/**
	 * The '<em><b>Before Each Turn</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Before Each Turn</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #BEFORE_EACH_TURN
	 * @model name="BeforeEachTurn" literal="onEachTurn"
	 * @generated
	 * @ordered
	 */
	public static final int BEFORE_EACH_TURN_VALUE = 2;

	/**
	 * An array of all the '<em><b>EBattle When</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final EBattleWhen[] VALUES_ARRAY =
		new EBattleWhen[] {
			BEFORE_BATTLE,
			AFTER_BATTLE,
			BEFORE_EACH_TURN,
		};

	/**
	 * A public read-only list of all the '<em><b>EBattle When</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<EBattleWhen> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>EBattle When</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static EBattleWhen get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			EBattleWhen result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>EBattle When</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static EBattleWhen getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			EBattleWhen result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>EBattle When</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static EBattleWhen get(int value) {
		switch (value) {
			case BEFORE_BATTLE_VALUE: return BEFORE_BATTLE;
			case AFTER_BATTLE_VALUE: return AFTER_BATTLE;
			case BEFORE_EACH_TURN_VALUE: return BEFORE_EACH_TURN;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EBattleWhen(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //EBattleWhen
