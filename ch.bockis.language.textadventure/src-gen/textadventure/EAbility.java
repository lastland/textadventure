/**
 */
package textadventure;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>EAbility</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see textadventure.TextadventurePackage#getEAbility()
 * @model abstract="true"
 * @generated
 */
public interface EAbility extends EObject {
} // EAbility
