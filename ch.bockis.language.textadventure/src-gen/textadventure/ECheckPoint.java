/**
 */
package textadventure;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ECheck Point</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link textadventure.ECheckPoint#getPoint <em>Point</em>}</li>
 * </ul>
 * </p>
 *
 * @see textadventure.TextadventurePackage#getECheckPoint()
 * @model
 * @generated
 */
public interface ECheckPoint extends EObject {
	/**
	 * Returns the value of the '<em><b>Point</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Point</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Point</em>' reference.
	 * @see #setPoint(ETile)
	 * @see textadventure.TextadventurePackage#getECheckPoint_Point()
	 * @model required="true"
	 * @generated
	 */
	ETile getPoint();

	/**
	 * Sets the value of the '{@link textadventure.ECheckPoint#getPoint <em>Point</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Point</em>' reference.
	 * @see #getPoint()
	 * @generated
	 */
	void setPoint(ETile value);

} // ECheckPoint
