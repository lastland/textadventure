/**
 */
package textadventure;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see textadventure.TextadventureFactory
 * @model kind="package"
 * @generated
 */
public interface TextadventurePackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "textadventure";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.bockis.ch/language/textadventure";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "textadventure";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	TextadventurePackage eINSTANCE = textadventure.impl.TextadventurePackageImpl.init();

	/**
	 * The meta object id for the '{@link textadventure.impl.EWorldImpl <em>EWorld</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see textadventure.impl.EWorldImpl
	 * @see textadventure.impl.TextadventurePackageImpl#getEWorld()
	 * @generated
	 */
	int EWORLD = 0;

	/**
	 * The feature id for the '<em><b>Items</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EWORLD__ITEMS = 0;

	/**
	 * The feature id for the '<em><b>Persons</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EWORLD__PERSONS = 1;

	/**
	 * The feature id for the '<em><b>Tiles</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EWORLD__TILES = 2;

	/**
	 * The feature id for the '<em><b>Checkpoint</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EWORLD__CHECKPOINT = 3;

	/**
	 * The feature id for the '<em><b>Battle</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EWORLD__BATTLE = 4;

	/**
	 * The feature id for the '<em><b>Destination</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EWORLD__DESTINATION = 5;

	/**
	 * The feature id for the '<em><b>Player Person</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EWORLD__PLAYER_PERSON = 6;

	/**
	 * The number of structural features of the '<em>EWorld</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EWORLD_FEATURE_COUNT = 7;

	/**
	 * The meta object id for the '{@link textadventure.impl.ETileImpl <em>ETile</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see textadventure.impl.ETileImpl
	 * @see textadventure.impl.TextadventurePackageImpl#getETile()
	 * @generated
	 */
	int ETILE = 1;

	/**
	 * The feature id for the '<em><b>X</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ETILE__X = 0;

	/**
	 * The feature id for the '<em><b>Y</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ETILE__Y = 1;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ETILE__DESCRIPTION = 2;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ETILE__ID = 3;

	/**
	 * The number of structural features of the '<em>ETile</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ETILE_FEATURE_COUNT = 4;

	/**
	 * The meta object id for the '{@link textadventure.impl.ECheckPointImpl <em>ECheck Point</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see textadventure.impl.ECheckPointImpl
	 * @see textadventure.impl.TextadventurePackageImpl#getECheckPoint()
	 * @generated
	 */
	int ECHECK_POINT = 2;

	/**
	 * The feature id for the '<em><b>Point</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ECHECK_POINT__POINT = 0;

	/**
	 * The number of structural features of the '<em>ECheck Point</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ECHECK_POINT_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link textadventure.impl.EDestinationImpl <em>EDestination</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see textadventure.impl.EDestinationImpl
	 * @see textadventure.impl.TextadventurePackageImpl#getEDestination()
	 * @generated
	 */
	int EDESTINATION = 3;

	/**
	 * The feature id for the '<em><b>Point</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDESTINATION__POINT = 0;

	/**
	 * The number of structural features of the '<em>EDestination</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDESTINATION_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link textadventure.impl.EItemImpl <em>EItem</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see textadventure.impl.EItemImpl
	 * @see textadventure.impl.TextadventurePackageImpl#getEItem()
	 * @generated
	 */
	int EITEM = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EITEM__NAME = 0;

	/**
	 * The feature id for the '<em><b>Effects</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EITEM__EFFECTS = 1;

	/**
	 * The feature id for the '<em><b>Location</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EITEM__LOCATION = 2;

	/**
	 * The number of structural features of the '<em>EItem</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EITEM_FEATURE_COUNT = 3;

	/**
	 * The meta object id for the '{@link textadventure.impl.EBindingImpl <em>EBinding</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see textadventure.impl.EBindingImpl
	 * @see textadventure.impl.TextadventurePackageImpl#getEBinding()
	 * @generated
	 */
	int EBINDING = 5;

	/**
	 * The feature id for the '<em><b>Participant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EBINDING__PARTICIPANT = 0;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EBINDING__ID = 1;

	/**
	 * The number of structural features of the '<em>EBinding</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EBINDING_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link textadventure.impl.EEffectImpl <em>EEffect</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see textadventure.impl.EEffectImpl
	 * @see textadventure.impl.TextadventurePackageImpl#getEEffect()
	 * @generated
	 */
	int EEFFECT = 6;

	/**
	 * The feature id for the '<em><b>Condition</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EEFFECT__CONDITION = 0;

	/**
	 * The feature id for the '<em><b>Binding</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EEFFECT__BINDING = 1;

	/**
	 * The feature id for the '<em><b>Action</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EEFFECT__ACTION = 2;

	/**
	 * The number of structural features of the '<em>EEffect</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EEFFECT_FEATURE_COUNT = 3;

	/**
	 * The meta object id for the '{@link textadventure.impl.EWhileEffectImpl <em>EWhile Effect</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see textadventure.impl.EWhileEffectImpl
	 * @see textadventure.impl.TextadventurePackageImpl#getEWhileEffect()
	 * @generated
	 */
	int EWHILE_EFFECT = 7;

	/**
	 * The feature id for the '<em><b>Condition</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EWHILE_EFFECT__CONDITION = EEFFECT__CONDITION;

	/**
	 * The feature id for the '<em><b>Binding</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EWHILE_EFFECT__BINDING = EEFFECT__BINDING;

	/**
	 * The feature id for the '<em><b>Action</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EWHILE_EFFECT__ACTION = EEFFECT__ACTION;

	/**
	 * The number of structural features of the '<em>EWhile Effect</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EWHILE_EFFECT_FEATURE_COUNT = EEFFECT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link textadventure.impl.EActionImpl <em>EAction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see textadventure.impl.EActionImpl
	 * @see textadventure.impl.TextadventurePackageImpl#getEAction()
	 * @generated
	 */
	int EACTION = 8;

	/**
	 * The number of structural features of the '<em>EAction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EACTION_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link textadventure.impl.ERemoveActionImpl <em>ERemove Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see textadventure.impl.ERemoveActionImpl
	 * @see textadventure.impl.TextadventurePackageImpl#getERemoveAction()
	 * @generated
	 */
	int EREMOVE_ACTION = 9;

	/**
	 * The feature id for the '<em><b>What</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EREMOVE_ACTION__WHAT = EACTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>From</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EREMOVE_ACTION__FROM = EACTION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>ERemove Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EREMOVE_ACTION_FEATURE_COUNT = EACTION_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link textadventure.impl.EListDescriptionImpl <em>EList Description</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see textadventure.impl.EListDescriptionImpl
	 * @see textadventure.impl.TextadventurePackageImpl#getEListDescription()
	 * @generated
	 */
	int ELIST_DESCRIPTION = 10;

	/**
	 * The feature id for the '<em><b>Target</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELIST_DESCRIPTION__TARGET = 0;

	/**
	 * The feature id for the '<em><b>Property</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELIST_DESCRIPTION__PROPERTY = 1;

	/**
	 * The number of structural features of the '<em>EList Description</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELIST_DESCRIPTION_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link textadventure.impl.EPersonImpl <em>EPerson</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see textadventure.impl.EPersonImpl
	 * @see textadventure.impl.TextadventurePackageImpl#getEPerson()
	 * @generated
	 */
	int EPERSON = 11;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EPERSON__NAME = 0;

	/**
	 * The feature id for the '<em><b>Abilities</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EPERSON__ABILITIES = 1;

	/**
	 * The feature id for the '<em><b>Location</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EPERSON__LOCATION = 2;

	/**
	 * The feature id for the '<em><b>Hp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EPERSON__HP = 3;

	/**
	 * The feature id for the '<em><b>Defense</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EPERSON__DEFENSE = 4;

	/**
	 * The feature id for the '<em><b>Attack</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EPERSON__ATTACK = 5;

	/**
	 * The number of structural features of the '<em>EPerson</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EPERSON_FEATURE_COUNT = 6;

	/**
	 * The meta object id for the '{@link textadventure.impl.EBattleImpl <em>EBattle</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see textadventure.impl.EBattleImpl
	 * @see textadventure.impl.TextadventurePackageImpl#getEBattle()
	 * @generated
	 */
	int EBATTLE = 12;

	/**
	 * The feature id for the '<em><b>Location</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EBATTLE__LOCATION = 0;

	/**
	 * The feature id for the '<em><b>Enemy</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EBATTLE__ENEMY = 1;

	/**
	 * The feature id for the '<em><b>Logics</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EBATTLE__LOGICS = 2;

	/**
	 * The number of structural features of the '<em>EBattle</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EBATTLE_FEATURE_COUNT = 3;

	/**
	 * The meta object id for the '{@link textadventure.impl.EBattleCustomizationImpl <em>EBattle Customization</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see textadventure.impl.EBattleCustomizationImpl
	 * @see textadventure.impl.TextadventurePackageImpl#getEBattleCustomization()
	 * @generated
	 */
	int EBATTLE_CUSTOMIZATION = 13;

	/**
	 * The feature id for the '<em><b>When</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EBATTLE_CUSTOMIZATION__WHEN = 0;

	/**
	 * The feature id for the '<em><b>Func</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EBATTLE_CUSTOMIZATION__FUNC = 1;

	/**
	 * The number of structural features of the '<em>EBattle Customization</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EBATTLE_CUSTOMIZATION_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link textadventure.impl.EFunctionImpl <em>EFunction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see textadventure.impl.EFunctionImpl
	 * @see textadventure.impl.TextadventurePackageImpl#getEFunction()
	 * @generated
	 */
	int EFUNCTION = 14;

	/**
	 * The feature id for the '<em><b>Params</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EFUNCTION__PARAMS = 0;

	/**
	 * The feature id for the '<em><b>Body</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EFUNCTION__BODY = 1;

	/**
	 * The number of structural features of the '<em>EFunction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EFUNCTION_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link textadventure.impl.EFunctionBodyImpl <em>EFunction Body</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see textadventure.impl.EFunctionBodyImpl
	 * @see textadventure.impl.TextadventurePackageImpl#getEFunctionBody()
	 * @generated
	 */
	int EFUNCTION_BODY = 15;

	/**
	 * The feature id for the '<em><b>Statements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EFUNCTION_BODY__STATEMENTS = 0;

	/**
	 * The number of structural features of the '<em>EFunction Body</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EFUNCTION_BODY_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link textadventure.impl.EStatementImpl <em>EStatement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see textadventure.impl.EStatementImpl
	 * @see textadventure.impl.TextadventurePackageImpl#getEStatement()
	 * @generated
	 */
	int ESTATEMENT = 16;

	/**
	 * The number of structural features of the '<em>EStatement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ESTATEMENT_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link textadventure.impl.EPrintStatementImpl <em>EPrint Statement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see textadventure.impl.EPrintStatementImpl
	 * @see textadventure.impl.TextadventurePackageImpl#getEPrintStatement()
	 * @generated
	 */
	int EPRINT_STATEMENT = 17;

	/**
	 * The feature id for the '<em><b>Output</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EPRINT_STATEMENT__OUTPUT = ESTATEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>EPrint Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EPRINT_STATEMENT_FEATURE_COUNT = ESTATEMENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link textadventure.impl.ERemovePlayerStatementImpl <em>ERemove Player Statement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see textadventure.impl.ERemovePlayerStatementImpl
	 * @see textadventure.impl.TextadventurePackageImpl#getERemovePlayerStatement()
	 * @generated
	 */
	int EREMOVE_PLAYER_STATEMENT = 18;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EREMOVE_PLAYER_STATEMENT__TARGET = ESTATEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>ERemove Player Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EREMOVE_PLAYER_STATEMENT_FEATURE_COUNT = ESTATEMENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link textadventure.impl.EMethodCallStatementImpl <em>EMethod Call Statement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see textadventure.impl.EMethodCallStatementImpl
	 * @see textadventure.impl.TextadventurePackageImpl#getEMethodCallStatement()
	 * @generated
	 */
	int EMETHOD_CALL_STATEMENT = 19;

	/**
	 * The feature id for the '<em><b>Object</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EMETHOD_CALL_STATEMENT__OBJECT = ESTATEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Method</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EMETHOD_CALL_STATEMENT__METHOD = ESTATEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Params</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EMETHOD_CALL_STATEMENT__PARAMS = ESTATEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>EMethod Call Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EMETHOD_CALL_STATEMENT_FEATURE_COUNT = ESTATEMENT_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link textadventure.impl.EAbilityImpl <em>EAbility</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see textadventure.impl.EAbilityImpl
	 * @see textadventure.impl.TextadventurePackageImpl#getEAbility()
	 * @generated
	 */
	int EABILITY = 20;

	/**
	 * The number of structural features of the '<em>EAbility</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EABILITY_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link textadventure.impl.EIgnoreItemAbilityImpl <em>EIgnore Item Ability</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see textadventure.impl.EIgnoreItemAbilityImpl
	 * @see textadventure.impl.TextadventurePackageImpl#getEIgnoreItemAbility()
	 * @generated
	 */
	int EIGNORE_ITEM_ABILITY = 21;

	/**
	 * The feature id for the '<em><b>Ignored Item</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EIGNORE_ITEM_ABILITY__IGNORED_ITEM = EABILITY_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>EIgnore Item Ability</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EIGNORE_ITEM_ABILITY_FEATURE_COUNT = EABILITY_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link textadventure.ECondition <em>ECondition</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see textadventure.ECondition
	 * @see textadventure.impl.TextadventurePackageImpl#getECondition()
	 * @generated
	 */
	int ECONDITION = 22;

	/**
	 * The meta object id for the '{@link textadventure.EParticipant <em>EParticipant</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see textadventure.EParticipant
	 * @see textadventure.impl.TextadventurePackageImpl#getEParticipant()
	 * @generated
	 */
	int EPARTICIPANT = 23;

	/**
	 * The meta object id for the '{@link textadventure.ETarget <em>ETarget</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see textadventure.ETarget
	 * @see textadventure.impl.TextadventurePackageImpl#getETarget()
	 * @generated
	 */
	int ETARGET = 24;

	/**
	 * The meta object id for the '{@link textadventure.EProperty <em>EProperty</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see textadventure.EProperty
	 * @see textadventure.impl.TextadventurePackageImpl#getEProperty()
	 * @generated
	 */
	int EPROPERTY = 25;


	/**
	 * The meta object id for the '{@link textadventure.EBattleWhen <em>EBattle When</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see textadventure.EBattleWhen
	 * @see textadventure.impl.TextadventurePackageImpl#getEBattleWhen()
	 * @generated
	 */
	int EBATTLE_WHEN = 26;


	/**
	 * Returns the meta object for class '{@link textadventure.EWorld <em>EWorld</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>EWorld</em>'.
	 * @see textadventure.EWorld
	 * @generated
	 */
	EClass getEWorld();

	/**
	 * Returns the meta object for the containment reference list '{@link textadventure.EWorld#getItems <em>Items</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Items</em>'.
	 * @see textadventure.EWorld#getItems()
	 * @see #getEWorld()
	 * @generated
	 */
	EReference getEWorld_Items();

	/**
	 * Returns the meta object for the containment reference list '{@link textadventure.EWorld#getPersons <em>Persons</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Persons</em>'.
	 * @see textadventure.EWorld#getPersons()
	 * @see #getEWorld()
	 * @generated
	 */
	EReference getEWorld_Persons();

	/**
	 * Returns the meta object for the containment reference list '{@link textadventure.EWorld#getTiles <em>Tiles</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Tiles</em>'.
	 * @see textadventure.EWorld#getTiles()
	 * @see #getEWorld()
	 * @generated
	 */
	EReference getEWorld_Tiles();

	/**
	 * Returns the meta object for the containment reference list '{@link textadventure.EWorld#getCheckpoint <em>Checkpoint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Checkpoint</em>'.
	 * @see textadventure.EWorld#getCheckpoint()
	 * @see #getEWorld()
	 * @generated
	 */
	EReference getEWorld_Checkpoint();

	/**
	 * Returns the meta object for the containment reference list '{@link textadventure.EWorld#getBattle <em>Battle</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Battle</em>'.
	 * @see textadventure.EWorld#getBattle()
	 * @see #getEWorld()
	 * @generated
	 */
	EReference getEWorld_Battle();

	/**
	 * Returns the meta object for the containment reference list '{@link textadventure.EWorld#getDestination <em>Destination</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Destination</em>'.
	 * @see textadventure.EWorld#getDestination()
	 * @see #getEWorld()
	 * @generated
	 */
	EReference getEWorld_Destination();

	/**
	 * Returns the meta object for the reference '{@link textadventure.EWorld#getPlayerPerson <em>Player Person</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Player Person</em>'.
	 * @see textadventure.EWorld#getPlayerPerson()
	 * @see #getEWorld()
	 * @generated
	 */
	EReference getEWorld_PlayerPerson();

	/**
	 * Returns the meta object for class '{@link textadventure.ETile <em>ETile</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ETile</em>'.
	 * @see textadventure.ETile
	 * @generated
	 */
	EClass getETile();

	/**
	 * Returns the meta object for the attribute '{@link textadventure.ETile#getX <em>X</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>X</em>'.
	 * @see textadventure.ETile#getX()
	 * @see #getETile()
	 * @generated
	 */
	EAttribute getETile_X();

	/**
	 * Returns the meta object for the attribute '{@link textadventure.ETile#getY <em>Y</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Y</em>'.
	 * @see textadventure.ETile#getY()
	 * @see #getETile()
	 * @generated
	 */
	EAttribute getETile_Y();

	/**
	 * Returns the meta object for the attribute '{@link textadventure.ETile#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see textadventure.ETile#getDescription()
	 * @see #getETile()
	 * @generated
	 */
	EAttribute getETile_Description();

	/**
	 * Returns the meta object for the attribute '{@link textadventure.ETile#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see textadventure.ETile#getId()
	 * @see #getETile()
	 * @generated
	 */
	EAttribute getETile_Id();

	/**
	 * Returns the meta object for class '{@link textadventure.ECheckPoint <em>ECheck Point</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ECheck Point</em>'.
	 * @see textadventure.ECheckPoint
	 * @generated
	 */
	EClass getECheckPoint();

	/**
	 * Returns the meta object for the reference '{@link textadventure.ECheckPoint#getPoint <em>Point</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Point</em>'.
	 * @see textadventure.ECheckPoint#getPoint()
	 * @see #getECheckPoint()
	 * @generated
	 */
	EReference getECheckPoint_Point();

	/**
	 * Returns the meta object for class '{@link textadventure.EDestination <em>EDestination</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>EDestination</em>'.
	 * @see textadventure.EDestination
	 * @generated
	 */
	EClass getEDestination();

	/**
	 * Returns the meta object for the reference '{@link textadventure.EDestination#getPoint <em>Point</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Point</em>'.
	 * @see textadventure.EDestination#getPoint()
	 * @see #getEDestination()
	 * @generated
	 */
	EReference getEDestination_Point();

	/**
	 * Returns the meta object for class '{@link textadventure.EItem <em>EItem</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>EItem</em>'.
	 * @see textadventure.EItem
	 * @generated
	 */
	EClass getEItem();

	/**
	 * Returns the meta object for the attribute '{@link textadventure.EItem#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see textadventure.EItem#getName()
	 * @see #getEItem()
	 * @generated
	 */
	EAttribute getEItem_Name();

	/**
	 * Returns the meta object for the containment reference list '{@link textadventure.EItem#getEffects <em>Effects</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Effects</em>'.
	 * @see textadventure.EItem#getEffects()
	 * @see #getEItem()
	 * @generated
	 */
	EReference getEItem_Effects();

	/**
	 * Returns the meta object for the reference '{@link textadventure.EItem#getLocation <em>Location</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Location</em>'.
	 * @see textadventure.EItem#getLocation()
	 * @see #getEItem()
	 * @generated
	 */
	EReference getEItem_Location();

	/**
	 * Returns the meta object for class '{@link textadventure.EBinding <em>EBinding</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>EBinding</em>'.
	 * @see textadventure.EBinding
	 * @generated
	 */
	EClass getEBinding();

	/**
	 * Returns the meta object for the attribute '{@link textadventure.EBinding#getParticipant <em>Participant</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Participant</em>'.
	 * @see textadventure.EBinding#getParticipant()
	 * @see #getEBinding()
	 * @generated
	 */
	EAttribute getEBinding_Participant();

	/**
	 * Returns the meta object for the attribute '{@link textadventure.EBinding#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see textadventure.EBinding#getId()
	 * @see #getEBinding()
	 * @generated
	 */
	EAttribute getEBinding_Id();

	/**
	 * Returns the meta object for class '{@link textadventure.EEffect <em>EEffect</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>EEffect</em>'.
	 * @see textadventure.EEffect
	 * @generated
	 */
	EClass getEEffect();

	/**
	 * Returns the meta object for the attribute '{@link textadventure.EEffect#getCondition <em>Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Condition</em>'.
	 * @see textadventure.EEffect#getCondition()
	 * @see #getEEffect()
	 * @generated
	 */
	EAttribute getEEffect_Condition();

	/**
	 * Returns the meta object for the containment reference '{@link textadventure.EEffect#getBinding <em>Binding</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Binding</em>'.
	 * @see textadventure.EEffect#getBinding()
	 * @see #getEEffect()
	 * @generated
	 */
	EReference getEEffect_Binding();

	/**
	 * Returns the meta object for the containment reference list '{@link textadventure.EEffect#getAction <em>Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Action</em>'.
	 * @see textadventure.EEffect#getAction()
	 * @see #getEEffect()
	 * @generated
	 */
	EReference getEEffect_Action();

	/**
	 * Returns the meta object for class '{@link textadventure.EWhileEffect <em>EWhile Effect</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>EWhile Effect</em>'.
	 * @see textadventure.EWhileEffect
	 * @generated
	 */
	EClass getEWhileEffect();

	/**
	 * Returns the meta object for class '{@link textadventure.EAction <em>EAction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>EAction</em>'.
	 * @see textadventure.EAction
	 * @generated
	 */
	EClass getEAction();

	/**
	 * Returns the meta object for class '{@link textadventure.ERemoveAction <em>ERemove Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ERemove Action</em>'.
	 * @see textadventure.ERemoveAction
	 * @generated
	 */
	EClass getERemoveAction();

	/**
	 * Returns the meta object for the reference '{@link textadventure.ERemoveAction#getWhat <em>What</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>What</em>'.
	 * @see textadventure.ERemoveAction#getWhat()
	 * @see #getERemoveAction()
	 * @generated
	 */
	EReference getERemoveAction_What();

	/**
	 * Returns the meta object for the containment reference '{@link textadventure.ERemoveAction#getFrom <em>From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>From</em>'.
	 * @see textadventure.ERemoveAction#getFrom()
	 * @see #getERemoveAction()
	 * @generated
	 */
	EReference getERemoveAction_From();

	/**
	 * Returns the meta object for class '{@link textadventure.EListDescription <em>EList Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>EList Description</em>'.
	 * @see textadventure.EListDescription
	 * @generated
	 */
	EClass getEListDescription();

	/**
	 * Returns the meta object for the attribute '{@link textadventure.EListDescription#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Target</em>'.
	 * @see textadventure.EListDescription#getTarget()
	 * @see #getEListDescription()
	 * @generated
	 */
	EAttribute getEListDescription_Target();

	/**
	 * Returns the meta object for the attribute '{@link textadventure.EListDescription#getProperty <em>Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Property</em>'.
	 * @see textadventure.EListDescription#getProperty()
	 * @see #getEListDescription()
	 * @generated
	 */
	EAttribute getEListDescription_Property();

	/**
	 * Returns the meta object for class '{@link textadventure.EPerson <em>EPerson</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>EPerson</em>'.
	 * @see textadventure.EPerson
	 * @generated
	 */
	EClass getEPerson();

	/**
	 * Returns the meta object for the attribute '{@link textadventure.EPerson#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see textadventure.EPerson#getName()
	 * @see #getEPerson()
	 * @generated
	 */
	EAttribute getEPerson_Name();

	/**
	 * Returns the meta object for the containment reference list '{@link textadventure.EPerson#getAbilities <em>Abilities</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Abilities</em>'.
	 * @see textadventure.EPerson#getAbilities()
	 * @see #getEPerson()
	 * @generated
	 */
	EReference getEPerson_Abilities();

	/**
	 * Returns the meta object for the reference '{@link textadventure.EPerson#getLocation <em>Location</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Location</em>'.
	 * @see textadventure.EPerson#getLocation()
	 * @see #getEPerson()
	 * @generated
	 */
	EReference getEPerson_Location();

	/**
	 * Returns the meta object for the attribute '{@link textadventure.EPerson#getHp <em>Hp</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Hp</em>'.
	 * @see textadventure.EPerson#getHp()
	 * @see #getEPerson()
	 * @generated
	 */
	EAttribute getEPerson_Hp();

	/**
	 * Returns the meta object for the attribute '{@link textadventure.EPerson#getDefense <em>Defense</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Defense</em>'.
	 * @see textadventure.EPerson#getDefense()
	 * @see #getEPerson()
	 * @generated
	 */
	EAttribute getEPerson_Defense();

	/**
	 * Returns the meta object for the attribute '{@link textadventure.EPerson#getAttack <em>Attack</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Attack</em>'.
	 * @see textadventure.EPerson#getAttack()
	 * @see #getEPerson()
	 * @generated
	 */
	EAttribute getEPerson_Attack();

	/**
	 * Returns the meta object for class '{@link textadventure.EBattle <em>EBattle</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>EBattle</em>'.
	 * @see textadventure.EBattle
	 * @generated
	 */
	EClass getEBattle();

	/**
	 * Returns the meta object for the reference '{@link textadventure.EBattle#getLocation <em>Location</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Location</em>'.
	 * @see textadventure.EBattle#getLocation()
	 * @see #getEBattle()
	 * @generated
	 */
	EReference getEBattle_Location();

	/**
	 * Returns the meta object for the reference '{@link textadventure.EBattle#getEnemy <em>Enemy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Enemy</em>'.
	 * @see textadventure.EBattle#getEnemy()
	 * @see #getEBattle()
	 * @generated
	 */
	EReference getEBattle_Enemy();

	/**
	 * Returns the meta object for the containment reference list '{@link textadventure.EBattle#getLogics <em>Logics</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Logics</em>'.
	 * @see textadventure.EBattle#getLogics()
	 * @see #getEBattle()
	 * @generated
	 */
	EReference getEBattle_Logics();

	/**
	 * Returns the meta object for class '{@link textadventure.EBattleCustomization <em>EBattle Customization</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>EBattle Customization</em>'.
	 * @see textadventure.EBattleCustomization
	 * @generated
	 */
	EClass getEBattleCustomization();

	/**
	 * Returns the meta object for the attribute '{@link textadventure.EBattleCustomization#getWhen <em>When</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>When</em>'.
	 * @see textadventure.EBattleCustomization#getWhen()
	 * @see #getEBattleCustomization()
	 * @generated
	 */
	EAttribute getEBattleCustomization_When();

	/**
	 * Returns the meta object for the containment reference '{@link textadventure.EBattleCustomization#getFunc <em>Func</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Func</em>'.
	 * @see textadventure.EBattleCustomization#getFunc()
	 * @see #getEBattleCustomization()
	 * @generated
	 */
	EReference getEBattleCustomization_Func();

	/**
	 * Returns the meta object for class '{@link textadventure.EFunction <em>EFunction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>EFunction</em>'.
	 * @see textadventure.EFunction
	 * @generated
	 */
	EClass getEFunction();

	/**
	 * Returns the meta object for the attribute list '{@link textadventure.EFunction#getParams <em>Params</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Params</em>'.
	 * @see textadventure.EFunction#getParams()
	 * @see #getEFunction()
	 * @generated
	 */
	EAttribute getEFunction_Params();

	/**
	 * Returns the meta object for the containment reference '{@link textadventure.EFunction#getBody <em>Body</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Body</em>'.
	 * @see textadventure.EFunction#getBody()
	 * @see #getEFunction()
	 * @generated
	 */
	EReference getEFunction_Body();

	/**
	 * Returns the meta object for class '{@link textadventure.EFunctionBody <em>EFunction Body</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>EFunction Body</em>'.
	 * @see textadventure.EFunctionBody
	 * @generated
	 */
	EClass getEFunctionBody();

	/**
	 * Returns the meta object for the containment reference list '{@link textadventure.EFunctionBody#getStatements <em>Statements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Statements</em>'.
	 * @see textadventure.EFunctionBody#getStatements()
	 * @see #getEFunctionBody()
	 * @generated
	 */
	EReference getEFunctionBody_Statements();

	/**
	 * Returns the meta object for class '{@link textadventure.EStatement <em>EStatement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>EStatement</em>'.
	 * @see textadventure.EStatement
	 * @generated
	 */
	EClass getEStatement();

	/**
	 * Returns the meta object for class '{@link textadventure.EPrintStatement <em>EPrint Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>EPrint Statement</em>'.
	 * @see textadventure.EPrintStatement
	 * @generated
	 */
	EClass getEPrintStatement();

	/**
	 * Returns the meta object for the attribute '{@link textadventure.EPrintStatement#getOutput <em>Output</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Output</em>'.
	 * @see textadventure.EPrintStatement#getOutput()
	 * @see #getEPrintStatement()
	 * @generated
	 */
	EAttribute getEPrintStatement_Output();

	/**
	 * Returns the meta object for class '{@link textadventure.ERemovePlayerStatement <em>ERemove Player Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ERemove Player Statement</em>'.
	 * @see textadventure.ERemovePlayerStatement
	 * @generated
	 */
	EClass getERemovePlayerStatement();

	/**
	 * Returns the meta object for the reference '{@link textadventure.ERemovePlayerStatement#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Target</em>'.
	 * @see textadventure.ERemovePlayerStatement#getTarget()
	 * @see #getERemovePlayerStatement()
	 * @generated
	 */
	EReference getERemovePlayerStatement_Target();

	/**
	 * Returns the meta object for class '{@link textadventure.EMethodCallStatement <em>EMethod Call Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>EMethod Call Statement</em>'.
	 * @see textadventure.EMethodCallStatement
	 * @generated
	 */
	EClass getEMethodCallStatement();

	/**
	 * Returns the meta object for the attribute '{@link textadventure.EMethodCallStatement#getObject <em>Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Object</em>'.
	 * @see textadventure.EMethodCallStatement#getObject()
	 * @see #getEMethodCallStatement()
	 * @generated
	 */
	EAttribute getEMethodCallStatement_Object();

	/**
	 * Returns the meta object for the attribute '{@link textadventure.EMethodCallStatement#getMethod <em>Method</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Method</em>'.
	 * @see textadventure.EMethodCallStatement#getMethod()
	 * @see #getEMethodCallStatement()
	 * @generated
	 */
	EAttribute getEMethodCallStatement_Method();

	/**
	 * Returns the meta object for the attribute list '{@link textadventure.EMethodCallStatement#getParams <em>Params</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Params</em>'.
	 * @see textadventure.EMethodCallStatement#getParams()
	 * @see #getEMethodCallStatement()
	 * @generated
	 */
	EAttribute getEMethodCallStatement_Params();

	/**
	 * Returns the meta object for class '{@link textadventure.EAbility <em>EAbility</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>EAbility</em>'.
	 * @see textadventure.EAbility
	 * @generated
	 */
	EClass getEAbility();

	/**
	 * Returns the meta object for class '{@link textadventure.EIgnoreItemAbility <em>EIgnore Item Ability</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>EIgnore Item Ability</em>'.
	 * @see textadventure.EIgnoreItemAbility
	 * @generated
	 */
	EClass getEIgnoreItemAbility();

	/**
	 * Returns the meta object for the reference '{@link textadventure.EIgnoreItemAbility#getIgnoredItem <em>Ignored Item</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Ignored Item</em>'.
	 * @see textadventure.EIgnoreItemAbility#getIgnoredItem()
	 * @see #getEIgnoreItemAbility()
	 * @generated
	 */
	EReference getEIgnoreItemAbility_IgnoredItem();

	/**
	 * Returns the meta object for enum '{@link textadventure.ECondition <em>ECondition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>ECondition</em>'.
	 * @see textadventure.ECondition
	 * @generated
	 */
	EEnum getECondition();

	/**
	 * Returns the meta object for enum '{@link textadventure.EParticipant <em>EParticipant</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>EParticipant</em>'.
	 * @see textadventure.EParticipant
	 * @generated
	 */
	EEnum getEParticipant();

	/**
	 * Returns the meta object for enum '{@link textadventure.ETarget <em>ETarget</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>ETarget</em>'.
	 * @see textadventure.ETarget
	 * @generated
	 */
	EEnum getETarget();

	/**
	 * Returns the meta object for enum '{@link textadventure.EProperty <em>EProperty</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>EProperty</em>'.
	 * @see textadventure.EProperty
	 * @generated
	 */
	EEnum getEProperty();

	/**
	 * Returns the meta object for enum '{@link textadventure.EBattleWhen <em>EBattle When</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>EBattle When</em>'.
	 * @see textadventure.EBattleWhen
	 * @generated
	 */
	EEnum getEBattleWhen();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	TextadventureFactory getTextadventureFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link textadventure.impl.EWorldImpl <em>EWorld</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see textadventure.impl.EWorldImpl
		 * @see textadventure.impl.TextadventurePackageImpl#getEWorld()
		 * @generated
		 */
		EClass EWORLD = eINSTANCE.getEWorld();

		/**
		 * The meta object literal for the '<em><b>Items</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EWORLD__ITEMS = eINSTANCE.getEWorld_Items();

		/**
		 * The meta object literal for the '<em><b>Persons</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EWORLD__PERSONS = eINSTANCE.getEWorld_Persons();

		/**
		 * The meta object literal for the '<em><b>Tiles</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EWORLD__TILES = eINSTANCE.getEWorld_Tiles();

		/**
		 * The meta object literal for the '<em><b>Checkpoint</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EWORLD__CHECKPOINT = eINSTANCE.getEWorld_Checkpoint();

		/**
		 * The meta object literal for the '<em><b>Battle</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EWORLD__BATTLE = eINSTANCE.getEWorld_Battle();

		/**
		 * The meta object literal for the '<em><b>Destination</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EWORLD__DESTINATION = eINSTANCE.getEWorld_Destination();

		/**
		 * The meta object literal for the '<em><b>Player Person</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EWORLD__PLAYER_PERSON = eINSTANCE.getEWorld_PlayerPerson();

		/**
		 * The meta object literal for the '{@link textadventure.impl.ETileImpl <em>ETile</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see textadventure.impl.ETileImpl
		 * @see textadventure.impl.TextadventurePackageImpl#getETile()
		 * @generated
		 */
		EClass ETILE = eINSTANCE.getETile();

		/**
		 * The meta object literal for the '<em><b>X</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ETILE__X = eINSTANCE.getETile_X();

		/**
		 * The meta object literal for the '<em><b>Y</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ETILE__Y = eINSTANCE.getETile_Y();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ETILE__DESCRIPTION = eINSTANCE.getETile_Description();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ETILE__ID = eINSTANCE.getETile_Id();

		/**
		 * The meta object literal for the '{@link textadventure.impl.ECheckPointImpl <em>ECheck Point</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see textadventure.impl.ECheckPointImpl
		 * @see textadventure.impl.TextadventurePackageImpl#getECheckPoint()
		 * @generated
		 */
		EClass ECHECK_POINT = eINSTANCE.getECheckPoint();

		/**
		 * The meta object literal for the '<em><b>Point</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ECHECK_POINT__POINT = eINSTANCE.getECheckPoint_Point();

		/**
		 * The meta object literal for the '{@link textadventure.impl.EDestinationImpl <em>EDestination</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see textadventure.impl.EDestinationImpl
		 * @see textadventure.impl.TextadventurePackageImpl#getEDestination()
		 * @generated
		 */
		EClass EDESTINATION = eINSTANCE.getEDestination();

		/**
		 * The meta object literal for the '<em><b>Point</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EDESTINATION__POINT = eINSTANCE.getEDestination_Point();

		/**
		 * The meta object literal for the '{@link textadventure.impl.EItemImpl <em>EItem</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see textadventure.impl.EItemImpl
		 * @see textadventure.impl.TextadventurePackageImpl#getEItem()
		 * @generated
		 */
		EClass EITEM = eINSTANCE.getEItem();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EITEM__NAME = eINSTANCE.getEItem_Name();

		/**
		 * The meta object literal for the '<em><b>Effects</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EITEM__EFFECTS = eINSTANCE.getEItem_Effects();

		/**
		 * The meta object literal for the '<em><b>Location</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EITEM__LOCATION = eINSTANCE.getEItem_Location();

		/**
		 * The meta object literal for the '{@link textadventure.impl.EBindingImpl <em>EBinding</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see textadventure.impl.EBindingImpl
		 * @see textadventure.impl.TextadventurePackageImpl#getEBinding()
		 * @generated
		 */
		EClass EBINDING = eINSTANCE.getEBinding();

		/**
		 * The meta object literal for the '<em><b>Participant</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EBINDING__PARTICIPANT = eINSTANCE.getEBinding_Participant();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EBINDING__ID = eINSTANCE.getEBinding_Id();

		/**
		 * The meta object literal for the '{@link textadventure.impl.EEffectImpl <em>EEffect</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see textadventure.impl.EEffectImpl
		 * @see textadventure.impl.TextadventurePackageImpl#getEEffect()
		 * @generated
		 */
		EClass EEFFECT = eINSTANCE.getEEffect();

		/**
		 * The meta object literal for the '<em><b>Condition</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EEFFECT__CONDITION = eINSTANCE.getEEffect_Condition();

		/**
		 * The meta object literal for the '<em><b>Binding</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EEFFECT__BINDING = eINSTANCE.getEEffect_Binding();

		/**
		 * The meta object literal for the '<em><b>Action</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EEFFECT__ACTION = eINSTANCE.getEEffect_Action();

		/**
		 * The meta object literal for the '{@link textadventure.impl.EWhileEffectImpl <em>EWhile Effect</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see textadventure.impl.EWhileEffectImpl
		 * @see textadventure.impl.TextadventurePackageImpl#getEWhileEffect()
		 * @generated
		 */
		EClass EWHILE_EFFECT = eINSTANCE.getEWhileEffect();

		/**
		 * The meta object literal for the '{@link textadventure.impl.EActionImpl <em>EAction</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see textadventure.impl.EActionImpl
		 * @see textadventure.impl.TextadventurePackageImpl#getEAction()
		 * @generated
		 */
		EClass EACTION = eINSTANCE.getEAction();

		/**
		 * The meta object literal for the '{@link textadventure.impl.ERemoveActionImpl <em>ERemove Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see textadventure.impl.ERemoveActionImpl
		 * @see textadventure.impl.TextadventurePackageImpl#getERemoveAction()
		 * @generated
		 */
		EClass EREMOVE_ACTION = eINSTANCE.getERemoveAction();

		/**
		 * The meta object literal for the '<em><b>What</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EREMOVE_ACTION__WHAT = eINSTANCE.getERemoveAction_What();

		/**
		 * The meta object literal for the '<em><b>From</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EREMOVE_ACTION__FROM = eINSTANCE.getERemoveAction_From();

		/**
		 * The meta object literal for the '{@link textadventure.impl.EListDescriptionImpl <em>EList Description</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see textadventure.impl.EListDescriptionImpl
		 * @see textadventure.impl.TextadventurePackageImpl#getEListDescription()
		 * @generated
		 */
		EClass ELIST_DESCRIPTION = eINSTANCE.getEListDescription();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ELIST_DESCRIPTION__TARGET = eINSTANCE.getEListDescription_Target();

		/**
		 * The meta object literal for the '<em><b>Property</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ELIST_DESCRIPTION__PROPERTY = eINSTANCE.getEListDescription_Property();

		/**
		 * The meta object literal for the '{@link textadventure.impl.EPersonImpl <em>EPerson</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see textadventure.impl.EPersonImpl
		 * @see textadventure.impl.TextadventurePackageImpl#getEPerson()
		 * @generated
		 */
		EClass EPERSON = eINSTANCE.getEPerson();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EPERSON__NAME = eINSTANCE.getEPerson_Name();

		/**
		 * The meta object literal for the '<em><b>Abilities</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EPERSON__ABILITIES = eINSTANCE.getEPerson_Abilities();

		/**
		 * The meta object literal for the '<em><b>Location</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EPERSON__LOCATION = eINSTANCE.getEPerson_Location();

		/**
		 * The meta object literal for the '<em><b>Hp</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EPERSON__HP = eINSTANCE.getEPerson_Hp();

		/**
		 * The meta object literal for the '<em><b>Defense</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EPERSON__DEFENSE = eINSTANCE.getEPerson_Defense();

		/**
		 * The meta object literal for the '<em><b>Attack</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EPERSON__ATTACK = eINSTANCE.getEPerson_Attack();

		/**
		 * The meta object literal for the '{@link textadventure.impl.EBattleImpl <em>EBattle</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see textadventure.impl.EBattleImpl
		 * @see textadventure.impl.TextadventurePackageImpl#getEBattle()
		 * @generated
		 */
		EClass EBATTLE = eINSTANCE.getEBattle();

		/**
		 * The meta object literal for the '<em><b>Location</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EBATTLE__LOCATION = eINSTANCE.getEBattle_Location();

		/**
		 * The meta object literal for the '<em><b>Enemy</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EBATTLE__ENEMY = eINSTANCE.getEBattle_Enemy();

		/**
		 * The meta object literal for the '<em><b>Logics</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EBATTLE__LOGICS = eINSTANCE.getEBattle_Logics();

		/**
		 * The meta object literal for the '{@link textadventure.impl.EBattleCustomizationImpl <em>EBattle Customization</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see textadventure.impl.EBattleCustomizationImpl
		 * @see textadventure.impl.TextadventurePackageImpl#getEBattleCustomization()
		 * @generated
		 */
		EClass EBATTLE_CUSTOMIZATION = eINSTANCE.getEBattleCustomization();

		/**
		 * The meta object literal for the '<em><b>When</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EBATTLE_CUSTOMIZATION__WHEN = eINSTANCE.getEBattleCustomization_When();

		/**
		 * The meta object literal for the '<em><b>Func</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EBATTLE_CUSTOMIZATION__FUNC = eINSTANCE.getEBattleCustomization_Func();

		/**
		 * The meta object literal for the '{@link textadventure.impl.EFunctionImpl <em>EFunction</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see textadventure.impl.EFunctionImpl
		 * @see textadventure.impl.TextadventurePackageImpl#getEFunction()
		 * @generated
		 */
		EClass EFUNCTION = eINSTANCE.getEFunction();

		/**
		 * The meta object literal for the '<em><b>Params</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EFUNCTION__PARAMS = eINSTANCE.getEFunction_Params();

		/**
		 * The meta object literal for the '<em><b>Body</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EFUNCTION__BODY = eINSTANCE.getEFunction_Body();

		/**
		 * The meta object literal for the '{@link textadventure.impl.EFunctionBodyImpl <em>EFunction Body</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see textadventure.impl.EFunctionBodyImpl
		 * @see textadventure.impl.TextadventurePackageImpl#getEFunctionBody()
		 * @generated
		 */
		EClass EFUNCTION_BODY = eINSTANCE.getEFunctionBody();

		/**
		 * The meta object literal for the '<em><b>Statements</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EFUNCTION_BODY__STATEMENTS = eINSTANCE.getEFunctionBody_Statements();

		/**
		 * The meta object literal for the '{@link textadventure.impl.EStatementImpl <em>EStatement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see textadventure.impl.EStatementImpl
		 * @see textadventure.impl.TextadventurePackageImpl#getEStatement()
		 * @generated
		 */
		EClass ESTATEMENT = eINSTANCE.getEStatement();

		/**
		 * The meta object literal for the '{@link textadventure.impl.EPrintStatementImpl <em>EPrint Statement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see textadventure.impl.EPrintStatementImpl
		 * @see textadventure.impl.TextadventurePackageImpl#getEPrintStatement()
		 * @generated
		 */
		EClass EPRINT_STATEMENT = eINSTANCE.getEPrintStatement();

		/**
		 * The meta object literal for the '<em><b>Output</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EPRINT_STATEMENT__OUTPUT = eINSTANCE.getEPrintStatement_Output();

		/**
		 * The meta object literal for the '{@link textadventure.impl.ERemovePlayerStatementImpl <em>ERemove Player Statement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see textadventure.impl.ERemovePlayerStatementImpl
		 * @see textadventure.impl.TextadventurePackageImpl#getERemovePlayerStatement()
		 * @generated
		 */
		EClass EREMOVE_PLAYER_STATEMENT = eINSTANCE.getERemovePlayerStatement();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EREMOVE_PLAYER_STATEMENT__TARGET = eINSTANCE.getERemovePlayerStatement_Target();

		/**
		 * The meta object literal for the '{@link textadventure.impl.EMethodCallStatementImpl <em>EMethod Call Statement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see textadventure.impl.EMethodCallStatementImpl
		 * @see textadventure.impl.TextadventurePackageImpl#getEMethodCallStatement()
		 * @generated
		 */
		EClass EMETHOD_CALL_STATEMENT = eINSTANCE.getEMethodCallStatement();

		/**
		 * The meta object literal for the '<em><b>Object</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EMETHOD_CALL_STATEMENT__OBJECT = eINSTANCE.getEMethodCallStatement_Object();

		/**
		 * The meta object literal for the '<em><b>Method</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EMETHOD_CALL_STATEMENT__METHOD = eINSTANCE.getEMethodCallStatement_Method();

		/**
		 * The meta object literal for the '<em><b>Params</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EMETHOD_CALL_STATEMENT__PARAMS = eINSTANCE.getEMethodCallStatement_Params();

		/**
		 * The meta object literal for the '{@link textadventure.impl.EAbilityImpl <em>EAbility</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see textadventure.impl.EAbilityImpl
		 * @see textadventure.impl.TextadventurePackageImpl#getEAbility()
		 * @generated
		 */
		EClass EABILITY = eINSTANCE.getEAbility();

		/**
		 * The meta object literal for the '{@link textadventure.impl.EIgnoreItemAbilityImpl <em>EIgnore Item Ability</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see textadventure.impl.EIgnoreItemAbilityImpl
		 * @see textadventure.impl.TextadventurePackageImpl#getEIgnoreItemAbility()
		 * @generated
		 */
		EClass EIGNORE_ITEM_ABILITY = eINSTANCE.getEIgnoreItemAbility();

		/**
		 * The meta object literal for the '<em><b>Ignored Item</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EIGNORE_ITEM_ABILITY__IGNORED_ITEM = eINSTANCE.getEIgnoreItemAbility_IgnoredItem();

		/**
		 * The meta object literal for the '{@link textadventure.ECondition <em>ECondition</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see textadventure.ECondition
		 * @see textadventure.impl.TextadventurePackageImpl#getECondition()
		 * @generated
		 */
		EEnum ECONDITION = eINSTANCE.getECondition();

		/**
		 * The meta object literal for the '{@link textadventure.EParticipant <em>EParticipant</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see textadventure.EParticipant
		 * @see textadventure.impl.TextadventurePackageImpl#getEParticipant()
		 * @generated
		 */
		EEnum EPARTICIPANT = eINSTANCE.getEParticipant();

		/**
		 * The meta object literal for the '{@link textadventure.ETarget <em>ETarget</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see textadventure.ETarget
		 * @see textadventure.impl.TextadventurePackageImpl#getETarget()
		 * @generated
		 */
		EEnum ETARGET = eINSTANCE.getETarget();

		/**
		 * The meta object literal for the '{@link textadventure.EProperty <em>EProperty</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see textadventure.EProperty
		 * @see textadventure.impl.TextadventurePackageImpl#getEProperty()
		 * @generated
		 */
		EEnum EPROPERTY = eINSTANCE.getEProperty();

		/**
		 * The meta object literal for the '{@link textadventure.EBattleWhen <em>EBattle When</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see textadventure.EBattleWhen
		 * @see textadventure.impl.TextadventurePackageImpl#getEBattleWhen()
		 * @generated
		 */
		EEnum EBATTLE_WHEN = eINSTANCE.getEBattleWhen();

	}

} //TextadventurePackage
