/**
 */
package textadventure.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

import textadventure.EBattle;
import textadventure.ECheckPoint;
import textadventure.EDestination;
import textadventure.EItem;
import textadventure.EPerson;
import textadventure.ETile;
import textadventure.EWorld;
import textadventure.TextadventurePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>EWorld</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link textadventure.impl.EWorldImpl#getItems <em>Items</em>}</li>
 *   <li>{@link textadventure.impl.EWorldImpl#getPersons <em>Persons</em>}</li>
 *   <li>{@link textadventure.impl.EWorldImpl#getTiles <em>Tiles</em>}</li>
 *   <li>{@link textadventure.impl.EWorldImpl#getCheckpoint <em>Checkpoint</em>}</li>
 *   <li>{@link textadventure.impl.EWorldImpl#getBattle <em>Battle</em>}</li>
 *   <li>{@link textadventure.impl.EWorldImpl#getDestination <em>Destination</em>}</li>
 *   <li>{@link textadventure.impl.EWorldImpl#getPlayerPerson <em>Player Person</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class EWorldImpl extends EObjectImpl implements EWorld {
	/**
	 * The cached value of the '{@link #getItems() <em>Items</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getItems()
	 * @generated
	 * @ordered
	 */
	protected EList<EItem> items;

	/**
	 * The cached value of the '{@link #getPersons() <em>Persons</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPersons()
	 * @generated
	 * @ordered
	 */
	protected EList<EPerson> persons;

	/**
	 * The cached value of the '{@link #getTiles() <em>Tiles</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTiles()
	 * @generated
	 * @ordered
	 */
	protected EList<ETile> tiles;

	/**
	 * The cached value of the '{@link #getCheckpoint() <em>Checkpoint</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCheckpoint()
	 * @generated
	 * @ordered
	 */
	protected EList<ECheckPoint> checkpoint;

	/**
	 * The cached value of the '{@link #getBattle() <em>Battle</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBattle()
	 * @generated
	 * @ordered
	 */
	protected EList<EBattle> battle;

	/**
	 * The cached value of the '{@link #getDestination() <em>Destination</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDestination()
	 * @generated
	 * @ordered
	 */
	protected EList<EDestination> destination;

	/**
	 * The cached value of the '{@link #getPlayerPerson() <em>Player Person</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPlayerPerson()
	 * @generated
	 * @ordered
	 */
	protected EPerson playerPerson;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EWorldImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TextadventurePackage.Literals.EWORLD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EItem> getItems() {
		if (items == null) {
			items = new EObjectContainmentEList<EItem>(EItem.class, this, TextadventurePackage.EWORLD__ITEMS);
		}
		return items;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EPerson> getPersons() {
		if (persons == null) {
			persons = new EObjectContainmentEList<EPerson>(EPerson.class, this, TextadventurePackage.EWORLD__PERSONS);
		}
		return persons;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ETile> getTiles() {
		if (tiles == null) {
			tiles = new EObjectContainmentEList<ETile>(ETile.class, this, TextadventurePackage.EWORLD__TILES);
		}
		return tiles;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ECheckPoint> getCheckpoint() {
		if (checkpoint == null) {
			checkpoint = new EObjectContainmentEList<ECheckPoint>(ECheckPoint.class, this, TextadventurePackage.EWORLD__CHECKPOINT);
		}
		return checkpoint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EBattle> getBattle() {
		if (battle == null) {
			battle = new EObjectContainmentEList<EBattle>(EBattle.class, this, TextadventurePackage.EWORLD__BATTLE);
		}
		return battle;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EDestination> getDestination() {
		if (destination == null) {
			destination = new EObjectContainmentEList<EDestination>(EDestination.class, this, TextadventurePackage.EWORLD__DESTINATION);
		}
		return destination;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EPerson getPlayerPerson() {
		if (playerPerson != null && playerPerson.eIsProxy()) {
			InternalEObject oldPlayerPerson = (InternalEObject)playerPerson;
			playerPerson = (EPerson)eResolveProxy(oldPlayerPerson);
			if (playerPerson != oldPlayerPerson) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TextadventurePackage.EWORLD__PLAYER_PERSON, oldPlayerPerson, playerPerson));
			}
		}
		return playerPerson;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EPerson basicGetPlayerPerson() {
		return playerPerson;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPlayerPerson(EPerson newPlayerPerson) {
		EPerson oldPlayerPerson = playerPerson;
		playerPerson = newPlayerPerson;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TextadventurePackage.EWORLD__PLAYER_PERSON, oldPlayerPerson, playerPerson));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case TextadventurePackage.EWORLD__ITEMS:
				return ((InternalEList<?>)getItems()).basicRemove(otherEnd, msgs);
			case TextadventurePackage.EWORLD__PERSONS:
				return ((InternalEList<?>)getPersons()).basicRemove(otherEnd, msgs);
			case TextadventurePackage.EWORLD__TILES:
				return ((InternalEList<?>)getTiles()).basicRemove(otherEnd, msgs);
			case TextadventurePackage.EWORLD__CHECKPOINT:
				return ((InternalEList<?>)getCheckpoint()).basicRemove(otherEnd, msgs);
			case TextadventurePackage.EWORLD__BATTLE:
				return ((InternalEList<?>)getBattle()).basicRemove(otherEnd, msgs);
			case TextadventurePackage.EWORLD__DESTINATION:
				return ((InternalEList<?>)getDestination()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TextadventurePackage.EWORLD__ITEMS:
				return getItems();
			case TextadventurePackage.EWORLD__PERSONS:
				return getPersons();
			case TextadventurePackage.EWORLD__TILES:
				return getTiles();
			case TextadventurePackage.EWORLD__CHECKPOINT:
				return getCheckpoint();
			case TextadventurePackage.EWORLD__BATTLE:
				return getBattle();
			case TextadventurePackage.EWORLD__DESTINATION:
				return getDestination();
			case TextadventurePackage.EWORLD__PLAYER_PERSON:
				if (resolve) return getPlayerPerson();
				return basicGetPlayerPerson();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TextadventurePackage.EWORLD__ITEMS:
				getItems().clear();
				getItems().addAll((Collection<? extends EItem>)newValue);
				return;
			case TextadventurePackage.EWORLD__PERSONS:
				getPersons().clear();
				getPersons().addAll((Collection<? extends EPerson>)newValue);
				return;
			case TextadventurePackage.EWORLD__TILES:
				getTiles().clear();
				getTiles().addAll((Collection<? extends ETile>)newValue);
				return;
			case TextadventurePackage.EWORLD__CHECKPOINT:
				getCheckpoint().clear();
				getCheckpoint().addAll((Collection<? extends ECheckPoint>)newValue);
				return;
			case TextadventurePackage.EWORLD__BATTLE:
				getBattle().clear();
				getBattle().addAll((Collection<? extends EBattle>)newValue);
				return;
			case TextadventurePackage.EWORLD__DESTINATION:
				getDestination().clear();
				getDestination().addAll((Collection<? extends EDestination>)newValue);
				return;
			case TextadventurePackage.EWORLD__PLAYER_PERSON:
				setPlayerPerson((EPerson)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TextadventurePackage.EWORLD__ITEMS:
				getItems().clear();
				return;
			case TextadventurePackage.EWORLD__PERSONS:
				getPersons().clear();
				return;
			case TextadventurePackage.EWORLD__TILES:
				getTiles().clear();
				return;
			case TextadventurePackage.EWORLD__CHECKPOINT:
				getCheckpoint().clear();
				return;
			case TextadventurePackage.EWORLD__BATTLE:
				getBattle().clear();
				return;
			case TextadventurePackage.EWORLD__DESTINATION:
				getDestination().clear();
				return;
			case TextadventurePackage.EWORLD__PLAYER_PERSON:
				setPlayerPerson((EPerson)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TextadventurePackage.EWORLD__ITEMS:
				return items != null && !items.isEmpty();
			case TextadventurePackage.EWORLD__PERSONS:
				return persons != null && !persons.isEmpty();
			case TextadventurePackage.EWORLD__TILES:
				return tiles != null && !tiles.isEmpty();
			case TextadventurePackage.EWORLD__CHECKPOINT:
				return checkpoint != null && !checkpoint.isEmpty();
			case TextadventurePackage.EWORLD__BATTLE:
				return battle != null && !battle.isEmpty();
			case TextadventurePackage.EWORLD__DESTINATION:
				return destination != null && !destination.isEmpty();
			case TextadventurePackage.EWORLD__PLAYER_PERSON:
				return playerPerson != null;
		}
		return super.eIsSet(featureID);
	}

} //EWorldImpl
