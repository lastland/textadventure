/**
 */
package textadventure.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import textadventure.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class TextadventureFactoryImpl extends EFactoryImpl implements TextadventureFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static TextadventureFactory init() {
		try {
			TextadventureFactory theTextadventureFactory = (TextadventureFactory)EPackage.Registry.INSTANCE.getEFactory(TextadventurePackage.eNS_URI);
			if (theTextadventureFactory != null) {
				return theTextadventureFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new TextadventureFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TextadventureFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case TextadventurePackage.EWORLD: return createEWorld();
			case TextadventurePackage.ETILE: return createETile();
			case TextadventurePackage.ECHECK_POINT: return createECheckPoint();
			case TextadventurePackage.EDESTINATION: return createEDestination();
			case TextadventurePackage.EITEM: return createEItem();
			case TextadventurePackage.EBINDING: return createEBinding();
			case TextadventurePackage.EWHILE_EFFECT: return createEWhileEffect();
			case TextadventurePackage.EREMOVE_ACTION: return createERemoveAction();
			case TextadventurePackage.ELIST_DESCRIPTION: return createEListDescription();
			case TextadventurePackage.EPERSON: return createEPerson();
			case TextadventurePackage.EBATTLE: return createEBattle();
			case TextadventurePackage.EBATTLE_CUSTOMIZATION: return createEBattleCustomization();
			case TextadventurePackage.EFUNCTION: return createEFunction();
			case TextadventurePackage.EFUNCTION_BODY: return createEFunctionBody();
			case TextadventurePackage.EPRINT_STATEMENT: return createEPrintStatement();
			case TextadventurePackage.EREMOVE_PLAYER_STATEMENT: return createERemovePlayerStatement();
			case TextadventurePackage.EMETHOD_CALL_STATEMENT: return createEMethodCallStatement();
			case TextadventurePackage.EIGNORE_ITEM_ABILITY: return createEIgnoreItemAbility();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case TextadventurePackage.ECONDITION:
				return createEConditionFromString(eDataType, initialValue);
			case TextadventurePackage.EPARTICIPANT:
				return createEParticipantFromString(eDataType, initialValue);
			case TextadventurePackage.ETARGET:
				return createETargetFromString(eDataType, initialValue);
			case TextadventurePackage.EPROPERTY:
				return createEPropertyFromString(eDataType, initialValue);
			case TextadventurePackage.EBATTLE_WHEN:
				return createEBattleWhenFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case TextadventurePackage.ECONDITION:
				return convertEConditionToString(eDataType, instanceValue);
			case TextadventurePackage.EPARTICIPANT:
				return convertEParticipantToString(eDataType, instanceValue);
			case TextadventurePackage.ETARGET:
				return convertETargetToString(eDataType, instanceValue);
			case TextadventurePackage.EPROPERTY:
				return convertEPropertyToString(eDataType, instanceValue);
			case TextadventurePackage.EBATTLE_WHEN:
				return convertEBattleWhenToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EWorld createEWorld() {
		EWorldImpl eWorld = new EWorldImpl();
		return eWorld;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ETile createETile() {
		ETileImpl eTile = new ETileImpl();
		return eTile;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ECheckPoint createECheckPoint() {
		ECheckPointImpl eCheckPoint = new ECheckPointImpl();
		return eCheckPoint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDestination createEDestination() {
		EDestinationImpl eDestination = new EDestinationImpl();
		return eDestination;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EItem createEItem() {
		EItemImpl eItem = new EItemImpl();
		return eItem;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EBinding createEBinding() {
		EBindingImpl eBinding = new EBindingImpl();
		return eBinding;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EWhileEffect createEWhileEffect() {
		EWhileEffectImpl eWhileEffect = new EWhileEffectImpl();
		return eWhileEffect;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ERemoveAction createERemoveAction() {
		ERemoveActionImpl eRemoveAction = new ERemoveActionImpl();
		return eRemoveAction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EListDescription createEListDescription() {
		EListDescriptionImpl eListDescription = new EListDescriptionImpl();
		return eListDescription;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EPerson createEPerson() {
		EPersonImpl ePerson = new EPersonImpl();
		return ePerson;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EBattle createEBattle() {
		EBattleImpl eBattle = new EBattleImpl();
		return eBattle;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EBattleCustomization createEBattleCustomization() {
		EBattleCustomizationImpl eBattleCustomization = new EBattleCustomizationImpl();
		return eBattleCustomization;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EFunction createEFunction() {
		EFunctionImpl eFunction = new EFunctionImpl();
		return eFunction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EFunctionBody createEFunctionBody() {
		EFunctionBodyImpl eFunctionBody = new EFunctionBodyImpl();
		return eFunctionBody;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EPrintStatement createEPrintStatement() {
		EPrintStatementImpl ePrintStatement = new EPrintStatementImpl();
		return ePrintStatement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ERemovePlayerStatement createERemovePlayerStatement() {
		ERemovePlayerStatementImpl eRemovePlayerStatement = new ERemovePlayerStatementImpl();
		return eRemovePlayerStatement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMethodCallStatement createEMethodCallStatement() {
		EMethodCallStatementImpl eMethodCallStatement = new EMethodCallStatementImpl();
		return eMethodCallStatement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EIgnoreItemAbility createEIgnoreItemAbility() {
		EIgnoreItemAbilityImpl eIgnoreItemAbility = new EIgnoreItemAbilityImpl();
		return eIgnoreItemAbility;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ECondition createEConditionFromString(EDataType eDataType, String initialValue) {
		ECondition result = ECondition.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertEConditionToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EParticipant createEParticipantFromString(EDataType eDataType, String initialValue) {
		EParticipant result = EParticipant.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertEParticipantToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ETarget createETargetFromString(EDataType eDataType, String initialValue) {
		ETarget result = ETarget.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertETargetToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EProperty createEPropertyFromString(EDataType eDataType, String initialValue) {
		EProperty result = EProperty.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertEPropertyToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EBattleWhen createEBattleWhenFromString(EDataType eDataType, String initialValue) {
		EBattleWhen result = EBattleWhen.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertEBattleWhenToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TextadventurePackage getTextadventurePackage() {
		return (TextadventurePackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static TextadventurePackage getPackage() {
		return TextadventurePackage.eINSTANCE;
	}

} //TextadventureFactoryImpl
