/**
 */
package textadventure.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import textadventure.EAbility;
import textadventure.EPerson;
import textadventure.ETile;
import textadventure.TextadventurePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>EPerson</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link textadventure.impl.EPersonImpl#getName <em>Name</em>}</li>
 *   <li>{@link textadventure.impl.EPersonImpl#getAbilities <em>Abilities</em>}</li>
 *   <li>{@link textadventure.impl.EPersonImpl#getLocation <em>Location</em>}</li>
 *   <li>{@link textadventure.impl.EPersonImpl#getHp <em>Hp</em>}</li>
 *   <li>{@link textadventure.impl.EPersonImpl#getDefense <em>Defense</em>}</li>
 *   <li>{@link textadventure.impl.EPersonImpl#getAttack <em>Attack</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class EPersonImpl extends EObjectImpl implements EPerson {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getAbilities() <em>Abilities</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAbilities()
	 * @generated
	 * @ordered
	 */
	protected EList<EAbility> abilities;

	/**
	 * The cached value of the '{@link #getLocation() <em>Location</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocation()
	 * @generated
	 * @ordered
	 */
	protected ETile location;

	/**
	 * The default value of the '{@link #getHp() <em>Hp</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHp()
	 * @generated
	 * @ordered
	 */
	protected static final int HP_EDEFAULT = 100;

	/**
	 * The cached value of the '{@link #getHp() <em>Hp</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHp()
	 * @generated
	 * @ordered
	 */
	protected int hp = HP_EDEFAULT;

	/**
	 * The default value of the '{@link #getDefense() <em>Defense</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDefense()
	 * @generated
	 * @ordered
	 */
	protected static final int DEFENSE_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getDefense() <em>Defense</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDefense()
	 * @generated
	 * @ordered
	 */
	protected int defense = DEFENSE_EDEFAULT;

	/**
	 * The default value of the '{@link #getAttack() <em>Attack</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAttack()
	 * @generated
	 * @ordered
	 */
	protected static final int ATTACK_EDEFAULT = 1;

	/**
	 * The cached value of the '{@link #getAttack() <em>Attack</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAttack()
	 * @generated
	 * @ordered
	 */
	protected int attack = ATTACK_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EPersonImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TextadventurePackage.Literals.EPERSON;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TextadventurePackage.EPERSON__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EAbility> getAbilities() {
		if (abilities == null) {
			abilities = new EObjectContainmentEList<EAbility>(EAbility.class, this, TextadventurePackage.EPERSON__ABILITIES);
		}
		return abilities;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ETile getLocation() {
		if (location != null && location.eIsProxy()) {
			InternalEObject oldLocation = (InternalEObject)location;
			location = (ETile)eResolveProxy(oldLocation);
			if (location != oldLocation) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TextadventurePackage.EPERSON__LOCATION, oldLocation, location));
			}
		}
		return location;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ETile basicGetLocation() {
		return location;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLocation(ETile newLocation) {
		ETile oldLocation = location;
		location = newLocation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TextadventurePackage.EPERSON__LOCATION, oldLocation, location));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getHp() {
		return hp;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHp(int newHp) {
		int oldHp = hp;
		hp = newHp;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TextadventurePackage.EPERSON__HP, oldHp, hp));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getDefense() {
		return defense;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDefense(int newDefense) {
		int oldDefense = defense;
		defense = newDefense;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TextadventurePackage.EPERSON__DEFENSE, oldDefense, defense));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getAttack() {
		return attack;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAttack(int newAttack) {
		int oldAttack = attack;
		attack = newAttack;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TextadventurePackage.EPERSON__ATTACK, oldAttack, attack));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case TextadventurePackage.EPERSON__ABILITIES:
				return ((InternalEList<?>)getAbilities()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TextadventurePackage.EPERSON__NAME:
				return getName();
			case TextadventurePackage.EPERSON__ABILITIES:
				return getAbilities();
			case TextadventurePackage.EPERSON__LOCATION:
				if (resolve) return getLocation();
				return basicGetLocation();
			case TextadventurePackage.EPERSON__HP:
				return getHp();
			case TextadventurePackage.EPERSON__DEFENSE:
				return getDefense();
			case TextadventurePackage.EPERSON__ATTACK:
				return getAttack();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TextadventurePackage.EPERSON__NAME:
				setName((String)newValue);
				return;
			case TextadventurePackage.EPERSON__ABILITIES:
				getAbilities().clear();
				getAbilities().addAll((Collection<? extends EAbility>)newValue);
				return;
			case TextadventurePackage.EPERSON__LOCATION:
				setLocation((ETile)newValue);
				return;
			case TextadventurePackage.EPERSON__HP:
				setHp((Integer)newValue);
				return;
			case TextadventurePackage.EPERSON__DEFENSE:
				setDefense((Integer)newValue);
				return;
			case TextadventurePackage.EPERSON__ATTACK:
				setAttack((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TextadventurePackage.EPERSON__NAME:
				setName(NAME_EDEFAULT);
				return;
			case TextadventurePackage.EPERSON__ABILITIES:
				getAbilities().clear();
				return;
			case TextadventurePackage.EPERSON__LOCATION:
				setLocation((ETile)null);
				return;
			case TextadventurePackage.EPERSON__HP:
				setHp(HP_EDEFAULT);
				return;
			case TextadventurePackage.EPERSON__DEFENSE:
				setDefense(DEFENSE_EDEFAULT);
				return;
			case TextadventurePackage.EPERSON__ATTACK:
				setAttack(ATTACK_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TextadventurePackage.EPERSON__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case TextadventurePackage.EPERSON__ABILITIES:
				return abilities != null && !abilities.isEmpty();
			case TextadventurePackage.EPERSON__LOCATION:
				return location != null;
			case TextadventurePackage.EPERSON__HP:
				return hp != HP_EDEFAULT;
			case TextadventurePackage.EPERSON__DEFENSE:
				return defense != DEFENSE_EDEFAULT;
			case TextadventurePackage.EPERSON__ATTACK:
				return attack != ATTACK_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", hp: ");
		result.append(hp);
		result.append(", defense: ");
		result.append(defense);
		result.append(", attack: ");
		result.append(attack);
		result.append(')');
		return result.toString();
	}

} //EPersonImpl
