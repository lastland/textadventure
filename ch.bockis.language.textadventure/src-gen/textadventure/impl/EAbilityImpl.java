/**
 */
package textadventure.impl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.EObjectImpl;

import textadventure.EAbility;
import textadventure.TextadventurePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>EAbility</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public abstract class EAbilityImpl extends EObjectImpl implements EAbility {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EAbilityImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TextadventurePackage.Literals.EABILITY;
	}

} //EAbilityImpl
