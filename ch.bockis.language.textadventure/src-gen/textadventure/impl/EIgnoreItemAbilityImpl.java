/**
 */
package textadventure.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import textadventure.EIgnoreItemAbility;
import textadventure.EItem;
import textadventure.TextadventurePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>EIgnore Item Ability</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link textadventure.impl.EIgnoreItemAbilityImpl#getIgnoredItem <em>Ignored Item</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class EIgnoreItemAbilityImpl extends EAbilityImpl implements EIgnoreItemAbility {
	/**
	 * The cached value of the '{@link #getIgnoredItem() <em>Ignored Item</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIgnoredItem()
	 * @generated
	 * @ordered
	 */
	protected EItem ignoredItem;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EIgnoreItemAbilityImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TextadventurePackage.Literals.EIGNORE_ITEM_ABILITY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EItem getIgnoredItem() {
		if (ignoredItem != null && ignoredItem.eIsProxy()) {
			InternalEObject oldIgnoredItem = (InternalEObject)ignoredItem;
			ignoredItem = (EItem)eResolveProxy(oldIgnoredItem);
			if (ignoredItem != oldIgnoredItem) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TextadventurePackage.EIGNORE_ITEM_ABILITY__IGNORED_ITEM, oldIgnoredItem, ignoredItem));
			}
		}
		return ignoredItem;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EItem basicGetIgnoredItem() {
		return ignoredItem;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIgnoredItem(EItem newIgnoredItem) {
		EItem oldIgnoredItem = ignoredItem;
		ignoredItem = newIgnoredItem;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TextadventurePackage.EIGNORE_ITEM_ABILITY__IGNORED_ITEM, oldIgnoredItem, ignoredItem));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TextadventurePackage.EIGNORE_ITEM_ABILITY__IGNORED_ITEM:
				if (resolve) return getIgnoredItem();
				return basicGetIgnoredItem();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TextadventurePackage.EIGNORE_ITEM_ABILITY__IGNORED_ITEM:
				setIgnoredItem((EItem)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TextadventurePackage.EIGNORE_ITEM_ABILITY__IGNORED_ITEM:
				setIgnoredItem((EItem)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TextadventurePackage.EIGNORE_ITEM_ABILITY__IGNORED_ITEM:
				return ignoredItem != null;
		}
		return super.eIsSet(featureID);
	}

} //EIgnoreItemAbilityImpl
