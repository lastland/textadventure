/**
 */
package textadventure.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import textadventure.EBattleCustomization;
import textadventure.EBattleWhen;
import textadventure.EFunction;
import textadventure.TextadventurePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>EBattle Customization</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link textadventure.impl.EBattleCustomizationImpl#getWhen <em>When</em>}</li>
 *   <li>{@link textadventure.impl.EBattleCustomizationImpl#getFunc <em>Func</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class EBattleCustomizationImpl extends EObjectImpl implements EBattleCustomization {
	/**
	 * The default value of the '{@link #getWhen() <em>When</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWhen()
	 * @generated
	 * @ordered
	 */
	protected static final EBattleWhen WHEN_EDEFAULT = EBattleWhen.BEFORE_BATTLE;

	/**
	 * The cached value of the '{@link #getWhen() <em>When</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWhen()
	 * @generated
	 * @ordered
	 */
	protected EBattleWhen when = WHEN_EDEFAULT;

	/**
	 * The cached value of the '{@link #getFunc() <em>Func</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFunc()
	 * @generated
	 * @ordered
	 */
	protected EFunction func;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EBattleCustomizationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TextadventurePackage.Literals.EBATTLE_CUSTOMIZATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EBattleWhen getWhen() {
		return when;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setWhen(EBattleWhen newWhen) {
		EBattleWhen oldWhen = when;
		when = newWhen == null ? WHEN_EDEFAULT : newWhen;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TextadventurePackage.EBATTLE_CUSTOMIZATION__WHEN, oldWhen, when));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EFunction getFunc() {
		return func;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFunc(EFunction newFunc, NotificationChain msgs) {
		EFunction oldFunc = func;
		func = newFunc;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TextadventurePackage.EBATTLE_CUSTOMIZATION__FUNC, oldFunc, newFunc);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFunc(EFunction newFunc) {
		if (newFunc != func) {
			NotificationChain msgs = null;
			if (func != null)
				msgs = ((InternalEObject)func).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TextadventurePackage.EBATTLE_CUSTOMIZATION__FUNC, null, msgs);
			if (newFunc != null)
				msgs = ((InternalEObject)newFunc).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TextadventurePackage.EBATTLE_CUSTOMIZATION__FUNC, null, msgs);
			msgs = basicSetFunc(newFunc, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TextadventurePackage.EBATTLE_CUSTOMIZATION__FUNC, newFunc, newFunc));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case TextadventurePackage.EBATTLE_CUSTOMIZATION__FUNC:
				return basicSetFunc(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TextadventurePackage.EBATTLE_CUSTOMIZATION__WHEN:
				return getWhen();
			case TextadventurePackage.EBATTLE_CUSTOMIZATION__FUNC:
				return getFunc();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TextadventurePackage.EBATTLE_CUSTOMIZATION__WHEN:
				setWhen((EBattleWhen)newValue);
				return;
			case TextadventurePackage.EBATTLE_CUSTOMIZATION__FUNC:
				setFunc((EFunction)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TextadventurePackage.EBATTLE_CUSTOMIZATION__WHEN:
				setWhen(WHEN_EDEFAULT);
				return;
			case TextadventurePackage.EBATTLE_CUSTOMIZATION__FUNC:
				setFunc((EFunction)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TextadventurePackage.EBATTLE_CUSTOMIZATION__WHEN:
				return when != WHEN_EDEFAULT;
			case TextadventurePackage.EBATTLE_CUSTOMIZATION__FUNC:
				return func != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (when: ");
		result.append(when);
		result.append(')');
		return result.toString();
	}

} //EBattleCustomizationImpl
