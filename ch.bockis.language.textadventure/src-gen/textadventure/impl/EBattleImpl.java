/**
 */
package textadventure.impl;

import java.util.Collection;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import textadventure.EBattle;
import textadventure.EBattleCustomization;
import textadventure.EPerson;
import textadventure.ETile;
import textadventure.TextadventurePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>EBattle</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link textadventure.impl.EBattleImpl#getLocation <em>Location</em>}</li>
 *   <li>{@link textadventure.impl.EBattleImpl#getEnemy <em>Enemy</em>}</li>
 *   <li>{@link textadventure.impl.EBattleImpl#getLogics <em>Logics</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class EBattleImpl extends EObjectImpl implements EBattle {
	/**
	 * The cached value of the '{@link #getLocation() <em>Location</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocation()
	 * @generated
	 * @ordered
	 */
	protected ETile location;
	/**
	 * The cached value of the '{@link #getEnemy() <em>Enemy</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEnemy()
	 * @generated
	 * @ordered
	 */
	protected EPerson enemy;

	/**
	 * The cached value of the '{@link #getLogics() <em>Logics</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLogics()
	 * @generated
	 * @ordered
	 */
	protected EList<EBattleCustomization> logics;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EBattleImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TextadventurePackage.Literals.EBATTLE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ETile getLocation() {
		if (location != null && location.eIsProxy()) {
			InternalEObject oldLocation = (InternalEObject)location;
			location = (ETile)eResolveProxy(oldLocation);
			if (location != oldLocation) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TextadventurePackage.EBATTLE__LOCATION, oldLocation, location));
			}
		}
		return location;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ETile basicGetLocation() {
		return location;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLocation(ETile newLocation) {
		ETile oldLocation = location;
		location = newLocation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TextadventurePackage.EBATTLE__LOCATION, oldLocation, location));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EPerson getEnemy() {
		if (enemy != null && enemy.eIsProxy()) {
			InternalEObject oldEnemy = (InternalEObject)enemy;
			enemy = (EPerson)eResolveProxy(oldEnemy);
			if (enemy != oldEnemy) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TextadventurePackage.EBATTLE__ENEMY, oldEnemy, enemy));
			}
		}
		return enemy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EPerson basicGetEnemy() {
		return enemy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEnemy(EPerson newEnemy) {
		EPerson oldEnemy = enemy;
		enemy = newEnemy;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TextadventurePackage.EBATTLE__ENEMY, oldEnemy, enemy));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EBattleCustomization> getLogics() {
		if (logics == null) {
			logics = new EObjectContainmentEList<EBattleCustomization>(EBattleCustomization.class, this, TextadventurePackage.EBATTLE__LOGICS);
		}
		return logics;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case TextadventurePackage.EBATTLE__LOGICS:
				return ((InternalEList<?>)getLogics()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TextadventurePackage.EBATTLE__LOCATION:
				if (resolve) return getLocation();
				return basicGetLocation();
			case TextadventurePackage.EBATTLE__ENEMY:
				if (resolve) return getEnemy();
				return basicGetEnemy();
			case TextadventurePackage.EBATTLE__LOGICS:
				return getLogics();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TextadventurePackage.EBATTLE__LOCATION:
				setLocation((ETile)newValue);
				return;
			case TextadventurePackage.EBATTLE__ENEMY:
				setEnemy((EPerson)newValue);
				return;
			case TextadventurePackage.EBATTLE__LOGICS:
				getLogics().clear();
				getLogics().addAll((Collection<? extends EBattleCustomization>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TextadventurePackage.EBATTLE__LOCATION:
				setLocation((ETile)null);
				return;
			case TextadventurePackage.EBATTLE__ENEMY:
				setEnemy((EPerson)null);
				return;
			case TextadventurePackage.EBATTLE__LOGICS:
				getLogics().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TextadventurePackage.EBATTLE__LOCATION:
				return location != null;
			case TextadventurePackage.EBATTLE__ENEMY:
				return enemy != null;
			case TextadventurePackage.EBATTLE__LOGICS:
				return logics != null && !logics.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //EBattleImpl
