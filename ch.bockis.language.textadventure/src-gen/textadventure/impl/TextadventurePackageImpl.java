/**
 */
package textadventure.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import textadventure.EAbility;
import textadventure.EAction;
import textadventure.EBattle;
import textadventure.EBattleCustomization;
import textadventure.EBattleWhen;
import textadventure.EBinding;
import textadventure.ECheckPoint;
import textadventure.ECondition;
import textadventure.EDestination;
import textadventure.EEffect;
import textadventure.EFunction;
import textadventure.EFunctionBody;
import textadventure.EIgnoreItemAbility;
import textadventure.EItem;
import textadventure.EListDescription;
import textadventure.EMethodCallStatement;
import textadventure.EParticipant;
import textadventure.EPerson;
import textadventure.EPosition;
import textadventure.EPrintStatement;
import textadventure.EProperty;
import textadventure.ERemoveAction;
import textadventure.ERemovePlayerStatement;
import textadventure.EStatement;
import textadventure.ETarget;
import textadventure.ETile;
import textadventure.EWhileEffect;
import textadventure.EWorld;
import textadventure.TextadventureFactory;
import textadventure.TextadventurePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class TextadventurePackageImpl extends EPackageImpl implements TextadventurePackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass eWorldEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass eTileEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass eCheckPointEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass eDestinationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass eItemEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass eBindingEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass eEffectEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass eWhileEffectEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass eActionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass eRemoveActionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass eListDescriptionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ePersonEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass eBattleEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass eBattleCustomizationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass eFunctionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass eFunctionBodyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass eStatementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ePrintStatementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass eRemovePlayerStatementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass eMethodCallStatementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass eAbilityEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass eIgnoreItemAbilityEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum eConditionEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum eParticipantEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum eTargetEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum ePropertyEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum eBattleWhenEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see textadventure.TextadventurePackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private TextadventurePackageImpl() {
		super(eNS_URI, TextadventureFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link TextadventurePackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static TextadventurePackage init() {
		if (isInited) return (TextadventurePackage)EPackage.Registry.INSTANCE.getEPackage(TextadventurePackage.eNS_URI);

		// Obtain or create and register package
		TextadventurePackageImpl theTextadventurePackage = (TextadventurePackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof TextadventurePackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new TextadventurePackageImpl());

		isInited = true;

		// Create package meta-data objects
		theTextadventurePackage.createPackageContents();

		// Initialize created meta-data
		theTextadventurePackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theTextadventurePackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(TextadventurePackage.eNS_URI, theTextadventurePackage);
		return theTextadventurePackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEWorld() {
		return eWorldEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEWorld_Items() {
		return (EReference)eWorldEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEWorld_Persons() {
		return (EReference)eWorldEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEWorld_Tiles() {
		return (EReference)eWorldEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEWorld_Checkpoint() {
		return (EReference)eWorldEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEWorld_Battle() {
		return (EReference)eWorldEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEWorld_Destination() {
		return (EReference)eWorldEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEWorld_PlayerPerson() {
		return (EReference)eWorldEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getETile() {
		return eTileEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getETile_X() {
		return (EAttribute)eTileEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getETile_Y() {
		return (EAttribute)eTileEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getETile_Description() {
		return (EAttribute)eTileEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getETile_Id() {
		return (EAttribute)eTileEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getECheckPoint() {
		return eCheckPointEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getECheckPoint_Point() {
		return (EReference)eCheckPointEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEDestination() {
		return eDestinationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEDestination_Point() {
		return (EReference)eDestinationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEItem() {
		return eItemEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEItem_Name() {
		return (EAttribute)eItemEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEItem_Effects() {
		return (EReference)eItemEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEItem_Location() {
		return (EReference)eItemEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEBinding() {
		return eBindingEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEBinding_Participant() {
		return (EAttribute)eBindingEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEBinding_Id() {
		return (EAttribute)eBindingEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEEffect() {
		return eEffectEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEEffect_Condition() {
		return (EAttribute)eEffectEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEEffect_Binding() {
		return (EReference)eEffectEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEEffect_Action() {
		return (EReference)eEffectEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEWhileEffect() {
		return eWhileEffectEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEAction() {
		return eActionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getERemoveAction() {
		return eRemoveActionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getERemoveAction_What() {
		return (EReference)eRemoveActionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getERemoveAction_From() {
		return (EReference)eRemoveActionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEListDescription() {
		return eListDescriptionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEListDescription_Target() {
		return (EAttribute)eListDescriptionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEListDescription_Property() {
		return (EAttribute)eListDescriptionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEPerson() {
		return ePersonEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEPerson_Name() {
		return (EAttribute)ePersonEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEPerson_Abilities() {
		return (EReference)ePersonEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEPerson_Location() {
		return (EReference)ePersonEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEPerson_Hp() {
		return (EAttribute)ePersonEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEPerson_Defense() {
		return (EAttribute)ePersonEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEPerson_Attack() {
		return (EAttribute)ePersonEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEBattle() {
		return eBattleEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEBattle_Location() {
		return (EReference)eBattleEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEBattle_Enemy() {
		return (EReference)eBattleEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEBattle_Logics() {
		return (EReference)eBattleEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEBattleCustomization() {
		return eBattleCustomizationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEBattleCustomization_When() {
		return (EAttribute)eBattleCustomizationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEBattleCustomization_Func() {
		return (EReference)eBattleCustomizationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEFunction() {
		return eFunctionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEFunction_Params() {
		return (EAttribute)eFunctionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEFunction_Body() {
		return (EReference)eFunctionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEFunctionBody() {
		return eFunctionBodyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEFunctionBody_Statements() {
		return (EReference)eFunctionBodyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEStatement() {
		return eStatementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEPrintStatement() {
		return ePrintStatementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEPrintStatement_Output() {
		return (EAttribute)ePrintStatementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getERemovePlayerStatement() {
		return eRemovePlayerStatementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getERemovePlayerStatement_Target() {
		return (EReference)eRemovePlayerStatementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEMethodCallStatement() {
		return eMethodCallStatementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEMethodCallStatement_Object() {
		return (EAttribute)eMethodCallStatementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEMethodCallStatement_Method() {
		return (EAttribute)eMethodCallStatementEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEMethodCallStatement_Params() {
		return (EAttribute)eMethodCallStatementEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEAbility() {
		return eAbilityEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEIgnoreItemAbility() {
		return eIgnoreItemAbilityEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEIgnoreItemAbility_IgnoredItem() {
		return (EReference)eIgnoreItemAbilityEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getECondition() {
		return eConditionEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getEParticipant() {
		return eParticipantEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getETarget() {
		return eTargetEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getEProperty() {
		return ePropertyEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getEBattleWhen() {
		return eBattleWhenEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TextadventureFactory getTextadventureFactory() {
		return (TextadventureFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		eWorldEClass = createEClass(EWORLD);
		createEReference(eWorldEClass, EWORLD__ITEMS);
		createEReference(eWorldEClass, EWORLD__PERSONS);
		createEReference(eWorldEClass, EWORLD__TILES);
		createEReference(eWorldEClass, EWORLD__CHECKPOINT);
		createEReference(eWorldEClass, EWORLD__BATTLE);
		createEReference(eWorldEClass, EWORLD__DESTINATION);
		createEReference(eWorldEClass, EWORLD__PLAYER_PERSON);

		eTileEClass = createEClass(ETILE);
		createEAttribute(eTileEClass, ETILE__X);
		createEAttribute(eTileEClass, ETILE__Y);
		createEAttribute(eTileEClass, ETILE__DESCRIPTION);
		createEAttribute(eTileEClass, ETILE__ID);

		eCheckPointEClass = createEClass(ECHECK_POINT);
		createEReference(eCheckPointEClass, ECHECK_POINT__POINT);

		eDestinationEClass = createEClass(EDESTINATION);
		createEReference(eDestinationEClass, EDESTINATION__POINT);

		eItemEClass = createEClass(EITEM);
		createEAttribute(eItemEClass, EITEM__NAME);
		createEReference(eItemEClass, EITEM__EFFECTS);
		createEReference(eItemEClass, EITEM__LOCATION);

		eBindingEClass = createEClass(EBINDING);
		createEAttribute(eBindingEClass, EBINDING__PARTICIPANT);
		createEAttribute(eBindingEClass, EBINDING__ID);

		eEffectEClass = createEClass(EEFFECT);
		createEAttribute(eEffectEClass, EEFFECT__CONDITION);
		createEReference(eEffectEClass, EEFFECT__BINDING);
		createEReference(eEffectEClass, EEFFECT__ACTION);

		eWhileEffectEClass = createEClass(EWHILE_EFFECT);

		eActionEClass = createEClass(EACTION);

		eRemoveActionEClass = createEClass(EREMOVE_ACTION);
		createEReference(eRemoveActionEClass, EREMOVE_ACTION__WHAT);
		createEReference(eRemoveActionEClass, EREMOVE_ACTION__FROM);

		eListDescriptionEClass = createEClass(ELIST_DESCRIPTION);
		createEAttribute(eListDescriptionEClass, ELIST_DESCRIPTION__TARGET);
		createEAttribute(eListDescriptionEClass, ELIST_DESCRIPTION__PROPERTY);

		ePersonEClass = createEClass(EPERSON);
		createEAttribute(ePersonEClass, EPERSON__NAME);
		createEReference(ePersonEClass, EPERSON__ABILITIES);
		createEReference(ePersonEClass, EPERSON__LOCATION);
		createEAttribute(ePersonEClass, EPERSON__HP);
		createEAttribute(ePersonEClass, EPERSON__DEFENSE);
		createEAttribute(ePersonEClass, EPERSON__ATTACK);

		eBattleEClass = createEClass(EBATTLE);
		createEReference(eBattleEClass, EBATTLE__LOCATION);
		createEReference(eBattleEClass, EBATTLE__ENEMY);
		createEReference(eBattleEClass, EBATTLE__LOGICS);

		eBattleCustomizationEClass = createEClass(EBATTLE_CUSTOMIZATION);
		createEAttribute(eBattleCustomizationEClass, EBATTLE_CUSTOMIZATION__WHEN);
		createEReference(eBattleCustomizationEClass, EBATTLE_CUSTOMIZATION__FUNC);

		eFunctionEClass = createEClass(EFUNCTION);
		createEAttribute(eFunctionEClass, EFUNCTION__PARAMS);
		createEReference(eFunctionEClass, EFUNCTION__BODY);

		eFunctionBodyEClass = createEClass(EFUNCTION_BODY);
		createEReference(eFunctionBodyEClass, EFUNCTION_BODY__STATEMENTS);

		eStatementEClass = createEClass(ESTATEMENT);

		ePrintStatementEClass = createEClass(EPRINT_STATEMENT);
		createEAttribute(ePrintStatementEClass, EPRINT_STATEMENT__OUTPUT);

		eRemovePlayerStatementEClass = createEClass(EREMOVE_PLAYER_STATEMENT);
		createEReference(eRemovePlayerStatementEClass, EREMOVE_PLAYER_STATEMENT__TARGET);

		eMethodCallStatementEClass = createEClass(EMETHOD_CALL_STATEMENT);
		createEAttribute(eMethodCallStatementEClass, EMETHOD_CALL_STATEMENT__OBJECT);
		createEAttribute(eMethodCallStatementEClass, EMETHOD_CALL_STATEMENT__METHOD);
		createEAttribute(eMethodCallStatementEClass, EMETHOD_CALL_STATEMENT__PARAMS);

		eAbilityEClass = createEClass(EABILITY);

		eIgnoreItemAbilityEClass = createEClass(EIGNORE_ITEM_ABILITY);
		createEReference(eIgnoreItemAbilityEClass, EIGNORE_ITEM_ABILITY__IGNORED_ITEM);

		// Create enums
		eConditionEEnum = createEEnum(ECONDITION);
		eParticipantEEnum = createEEnum(EPARTICIPANT);
		eTargetEEnum = createEEnum(ETARGET);
		ePropertyEEnum = createEEnum(EPROPERTY);
		eBattleWhenEEnum = createEEnum(EBATTLE_WHEN);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		eWhileEffectEClass.getESuperTypes().add(this.getEEffect());
		eRemoveActionEClass.getESuperTypes().add(this.getEAction());
		ePrintStatementEClass.getESuperTypes().add(this.getEStatement());
		eRemovePlayerStatementEClass.getESuperTypes().add(this.getEStatement());
		eMethodCallStatementEClass.getESuperTypes().add(this.getEStatement());
		eIgnoreItemAbilityEClass.getESuperTypes().add(this.getEAbility());

		// Initialize classes and features; add operations and parameters
		initEClass(eWorldEClass, EWorld.class, "EWorld", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEWorld_Items(), this.getEItem(), null, "items", null, 0, -1, EWorld.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEWorld_Persons(), this.getEPerson(), null, "persons", null, 1, -1, EWorld.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEWorld_Tiles(), this.getETile(), null, "tiles", null, 1, -1, EWorld.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEWorld_Checkpoint(), this.getECheckPoint(), null, "checkpoint", null, 0, -1, EWorld.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEWorld_Battle(), this.getEBattle(), null, "battle", null, 0, -1, EWorld.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEWorld_Destination(), this.getEDestination(), null, "destination", null, 0, -1, EWorld.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEWorld_PlayerPerson(), this.getEPerson(), null, "playerPerson", null, 1, 1, EWorld.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(eTileEClass, ETile.class, "ETile", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getETile_X(), ecorePackage.getEInt(), "x", null, 1, 1, ETile.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getETile_Y(), ecorePackage.getEInt(), "y", null, 1, 1, ETile.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getETile_Description(), ecorePackage.getEString(), "description", null, 1, 1, ETile.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getETile_Id(), ecorePackage.getEString(), "id", null, 1, 1, ETile.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(eCheckPointEClass, ECheckPoint.class, "ECheckPoint", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getECheckPoint_Point(), this.getETile(), null, "point", null, 1, 1, ECheckPoint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(eDestinationEClass, EDestination.class, "EDestination", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEDestination_Point(), this.getETile(), null, "point", null, 1, 1, EDestination.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(eItemEClass, EItem.class, "EItem", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getEItem_Name(), ecorePackage.getEString(), "name", null, 1, 1, EItem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEItem_Effects(), this.getEEffect(), null, "effects", null, 0, -1, EItem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEItem_Location(), this.getETile(), null, "location", null, 0, 1, EItem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(eBindingEClass, EBinding.class, "EBinding", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getEBinding_Participant(), this.getEParticipant(), "participant", null, 1, 1, EBinding.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEBinding_Id(), ecorePackage.getEString(), "id", null, 1, 1, EBinding.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(eEffectEClass, EEffect.class, "EEffect", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getEEffect_Condition(), this.getECondition(), "condition", null, 1, 1, EEffect.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEEffect_Binding(), this.getEBinding(), null, "binding", null, 1, 1, EEffect.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEEffect_Action(), this.getEAction(), null, "action", null, 1, -1, EEffect.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(eWhileEffectEClass, EWhileEffect.class, "EWhileEffect", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(eActionEClass, EAction.class, "EAction", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(eRemoveActionEClass, ERemoveAction.class, "ERemoveAction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getERemoveAction_What(), this.getEBinding(), null, "what", null, 1, 1, ERemoveAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getERemoveAction_From(), this.getEListDescription(), null, "from", null, 1, 1, ERemoveAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(eListDescriptionEClass, EListDescription.class, "EListDescription", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getEListDescription_Target(), this.getETarget(), "target", null, 1, 1, EListDescription.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEListDescription_Property(), this.getEProperty(), "property", null, 1, 1, EListDescription.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(ePersonEClass, EPerson.class, "EPerson", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getEPerson_Name(), ecorePackage.getEString(), "name", null, 1, 1, EPerson.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEPerson_Abilities(), this.getEAbility(), null, "abilities", null, 0, -1, EPerson.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEPerson_Location(), this.getETile(), null, "location", null, 1, 1, EPerson.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEPerson_Hp(), ecorePackage.getEInt(), "hp", null, 0, 1, EPerson.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEPerson_Defense(), ecorePackage.getEInt(), "defense", null, 0, 1, EPerson.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEPerson_Attack(), ecorePackage.getEInt(), "attack", null, 0, 1, EPerson.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(eBattleEClass, EBattle.class, "EBattle", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEBattle_Location(), this.getETile(), null, "location", null, 1, 1, EBattle.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEBattle_Enemy(), this.getEPerson(), null, "enemy", null, 1, 1, EBattle.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEBattle_Logics(), this.getEBattleCustomization(), null, "logics", null, 0, -1, EBattle.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(eBattleCustomizationEClass, EBattleCustomization.class, "EBattleCustomization", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getEBattleCustomization_When(), this.getEBattleWhen(), "when", null, 1, 1, EBattleCustomization.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEBattleCustomization_Func(), this.getEFunction(), null, "func", null, 1, 1, EBattleCustomization.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(eFunctionEClass, EFunction.class, "EFunction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getEFunction_Params(), ecorePackage.getEString(), "params", null, 0, -1, EFunction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEFunction_Body(), this.getEFunctionBody(), null, "body", null, 1, 1, EFunction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(eFunctionBodyEClass, EFunctionBody.class, "EFunctionBody", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEFunctionBody_Statements(), this.getEStatement(), null, "statements", null, 0, -1, EFunctionBody.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(eStatementEClass, EStatement.class, "EStatement", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(ePrintStatementEClass, EPrintStatement.class, "EPrintStatement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getEPrintStatement_Output(), ecorePackage.getEString(), "output", null, 1, 1, EPrintStatement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(eRemovePlayerStatementEClass, ERemovePlayerStatement.class, "ERemovePlayerStatement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getERemovePlayerStatement_Target(), this.getEPerson(), null, "target", null, 1, 1, ERemovePlayerStatement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(eMethodCallStatementEClass, EMethodCallStatement.class, "EMethodCallStatement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getEMethodCallStatement_Object(), ecorePackage.getEString(), "object", null, 1, 1, EMethodCallStatement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEMethodCallStatement_Method(), ecorePackage.getEString(), "method", null, 1, 1, EMethodCallStatement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEMethodCallStatement_Params(), ecorePackage.getEString(), "params", null, 0, -1, EMethodCallStatement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(eAbilityEClass, EAbility.class, "EAbility", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(eIgnoreItemAbilityEClass, EIgnoreItemAbility.class, "EIgnoreItemAbility", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEIgnoreItemAbility_IgnoredItem(), this.getEItem(), null, "ignoredItem", null, 1, 1, EIgnoreItemAbility.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(eConditionEEnum, ECondition.class, "ECondition");
		addEEnumLiteral(eConditionEEnum, ECondition.USED);

		initEEnum(eParticipantEEnum, EParticipant.class, "EParticipant");
		addEEnumLiteral(eParticipantEEnum, EParticipant.ON);

		initEEnum(eTargetEEnum, ETarget.class, "ETarget");
		addEEnumLiteral(eTargetEEnum, ETarget.TILE);

		initEEnum(ePropertyEEnum, EProperty.class, "EProperty");
		addEEnumLiteral(ePropertyEEnum, EProperty.PERSONS);
		addEEnumLiteral(ePropertyEEnum, EProperty.ITEMS);

		initEEnum(eBattleWhenEEnum, EBattleWhen.class, "EBattleWhen");
		addEEnumLiteral(eBattleWhenEEnum, EBattleWhen.BEFORE_BATTLE);
		addEEnumLiteral(eBattleWhenEEnum, EBattleWhen.AFTER_BATTLE);
		addEEnumLiteral(eBattleWhenEEnum, EBattleWhen.BEFORE_EACH_TURN);

		// Create resource
		createResource(eNS_URI);
	}

} //TextadventurePackageImpl
