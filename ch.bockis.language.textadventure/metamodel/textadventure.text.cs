SYNTAXDEF textadventure
FOR <http://www.bockis.ch/language/textadventure>

START EWorld

OPTIONS {
	overrideLaunchConfigurationHelper = "false";
	additionalDependencies = "org.eclipse.ui.console,org.eclipse.jface";	
}

TOKENS {
	DEFINE COMMENT $'//'(~('\n'|'\r'|'\uffff'))*$;
	DEFINE INTEGER $('-')?('1'..'9')('0'..'9')*|'0'$;
	DEFINE FLOAT $('-')?(('1'..'9') ('0'..'9')* | '0') '.' ('0'..'9')+ $;
}

TOKENSTYLES {
	"tile" COLOR #7F0055, BOLD;
	"item" COLOR #7F0055, BOLD;
	"while" COLOR #7F0055, BOLD;
	"remove" COLOR #7F0055, BOLD;
	"person" COLOR #7F0055, BOLD;
	"used" COLOR #7F0055, BOLD;
	"Persons" COLOR #7F0055, BOLD;
	"Items" COLOR #7F0055, BOLD;
	"ignore" COLOR #7F0055, BOLD;
	"Tile" COLOR #7F0055, BOLD;
	"x" COLOR #7F0055, BOLD;
	"y" COLOR #7F0055, BOLD;
	"description" COLOR #7F0055, BOLD;
	"from" COLOR #7F0055, BOLD;
	"hp" COLOR #7F0055, BOLD;
	"defense" COLOR #7F0055, BOLD;
	"attack" COLOR #7F0055, BOLD;
	"battle" COLOR #7F0055, BOLD;
	"checkpoint" COLOR #7F0055, BOLD;
	"destination" COLOR #7F0055, BOLD;
	"@" COLOR #00557F, BOLD;
}


RULES {
	EWorld ::= "player" "=" playerPerson[] tiles+ items* persons+ battle* checkpoint* destination*;
	ETile ::= "tile" id[] "{" "x" "=" x[INTEGER] "y" "=" y[INTEGER] "description" "=" description['"','"'] "}";
	EItem ::= "item" name[] ("@" location[])? "{" (effects)* "}";
	ECheckPoint ::= "checkpoint" "@" point[];
	EDestination ::= "destination" "@" point[];
	EBinding ::= participant[ON:"on"] id[];
	EWhileEffect ::= "while" condition[USED:"used"] binding "{" action "}";
	ERemoveAction ::= "remove" what[] "from" from;
	EListDescription ::= target[TILE:"Tile"] property[PERSONS:"Persons", ITEMS:"Items"];
	EPerson ::= "person" name[] "@" location[] "{" ("hp" "=" hp[INTEGER] | "defense" "=" defense[INTEGER] | "attack" "=" attack[INTEGER])* abilities* "}";
	EBattle ::= "battle" "@" location[] "{" "enemy" "=" enemy[] logics* "}";
	EBattleCustomization ::= when[] "=" func;
	EFunction ::= "function(" (params[] ("," params[])*)? ")" "{" body "}";
	EFunctionBody ::= (statements ";")*;
	EPrintStatement ::= "print" output['"', '"'];
	ERemovePlayerStatement ::= "remove" target[];
	EMethodCallStatement ::= object[] method[] params['"','"']*;
	EIgnoreItemAbility ::= "ignore" ignoredItem[];
}