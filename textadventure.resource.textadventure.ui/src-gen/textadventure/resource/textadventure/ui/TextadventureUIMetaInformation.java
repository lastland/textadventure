/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package textadventure.resource.textadventure.ui;

public class TextadventureUIMetaInformation extends textadventure.resource.textadventure.mopp.TextadventureMetaInformation {
	
	public textadventure.resource.textadventure.ITextadventureHoverTextProvider getHoverTextProvider() {
		return new textadventure.resource.textadventure.ui.TextadventureHoverTextProvider();
	}
	
	public textadventure.resource.textadventure.ui.TextadventureImageProvider getImageProvider() {
		return textadventure.resource.textadventure.ui.TextadventureImageProvider.INSTANCE;
	}
	
	public textadventure.resource.textadventure.ui.TextadventureColorManager createColorManager() {
		return new textadventure.resource.textadventure.ui.TextadventureColorManager();
	}
	
	/**
	 * @deprecated this method is only provided to preserve API compatibility. Use
	 * createTokenScanner(textadventure.resource.textadventure.ITextadventureTextResour
	 * ce, textadventure.resource.textadventure.ui.TextadventureColorManager) instead.
	 */
	public textadventure.resource.textadventure.ui.TextadventureTokenScanner createTokenScanner(textadventure.resource.textadventure.ui.TextadventureColorManager colorManager) {
		return createTokenScanner(null, colorManager);
	}
	
	public textadventure.resource.textadventure.ui.TextadventureTokenScanner createTokenScanner(textadventure.resource.textadventure.ITextadventureTextResource resource, textadventure.resource.textadventure.ui.TextadventureColorManager colorManager) {
		return new textadventure.resource.textadventure.ui.TextadventureTokenScanner(resource, colorManager);
	}
	
	public textadventure.resource.textadventure.ui.TextadventureCodeCompletionHelper createCodeCompletionHelper() {
		return new textadventure.resource.textadventure.ui.TextadventureCodeCompletionHelper();
	}
	
}
