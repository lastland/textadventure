/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package textadventure.resource.textadventure.ui;

/**
 * This class is only generated for backwards compatiblity. The original contents
 * of this class have been moved to class
 * textadventure.resource.textadventure.mopp.TextadventureAntlrTokenHelper.
 */
public class TextadventureAntlrTokenHelper {
	// This class is intentionally left empty.
}
