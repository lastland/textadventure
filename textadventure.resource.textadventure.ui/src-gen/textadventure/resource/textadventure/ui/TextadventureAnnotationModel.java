/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package textadventure.resource.textadventure.ui;

public class TextadventureAnnotationModel extends org.eclipse.ui.texteditor.ResourceMarkerAnnotationModel {
	
	public TextadventureAnnotationModel(org.eclipse.core.resources.IResource resource) {
		super(resource);
	}
	
	protected org.eclipse.ui.texteditor.MarkerAnnotation createMarkerAnnotation(org.eclipse.core.resources.IMarker marker) {
		return new textadventure.resource.textadventure.ui.TextadventureMarkerAnnotation(marker);
	}
	
}
