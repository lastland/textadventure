/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package textadventure.resource.textadventure.ui;

public abstract class AbstractTextadventureOutlinePageAction extends org.eclipse.jface.action.Action {
	
	private String preferenceKey = this.getClass().getSimpleName() + ".isChecked";
	
	private textadventure.resource.textadventure.ui.TextadventureOutlinePageTreeViewer treeViewer;
	
	public AbstractTextadventureOutlinePageAction(textadventure.resource.textadventure.ui.TextadventureOutlinePageTreeViewer treeViewer, String text, int style) {
		super(text, style);
		this.treeViewer = treeViewer;
	}
	
	public void initialize(String imagePath) {
		org.eclipse.jface.resource.ImageDescriptor descriptor = textadventure.resource.textadventure.ui.TextadventureImageProvider.INSTANCE.getImageDescriptor(imagePath);
		setDisabledImageDescriptor(descriptor);
		setImageDescriptor(descriptor);
		setHoverImageDescriptor(descriptor);
		boolean checked = textadventure.resource.textadventure.ui.TextadventureUIPlugin.getDefault().getPreferenceStore().getBoolean(preferenceKey);
		valueChanged(checked, false);
	}
	
	@Override	
	public void run() {
		if (keepState()) {
			valueChanged(isChecked(), true);
		} else {
			runBusy(true);
		}
	}
	
	public void runBusy(final boolean on) {
		org.eclipse.swt.custom.BusyIndicator.showWhile(org.eclipse.swt.widgets.Display.getCurrent(), new Runnable() {
			public void run() {
				runInternal(on);
			}
		});
	}
	
	public abstract void runInternal(boolean on);
	
	private void valueChanged(boolean on, boolean store) {
		setChecked(on);
		runBusy(on);
		if (store) {
			textadventure.resource.textadventure.ui.TextadventureUIPlugin.getDefault().getPreferenceStore().setValue(preferenceKey, on);
		}
	}
	
	public boolean keepState() {
		return true;
	}
	
	public textadventure.resource.textadventure.ui.TextadventureOutlinePageTreeViewer getTreeViewer() {
		return treeViewer;
	}
	
	public textadventure.resource.textadventure.ui.TextadventureOutlinePageTreeViewerComparator getTreeViewerComparator() {
		return (textadventure.resource.textadventure.ui.TextadventureOutlinePageTreeViewerComparator) treeViewer.getComparator();
	}
	
}
