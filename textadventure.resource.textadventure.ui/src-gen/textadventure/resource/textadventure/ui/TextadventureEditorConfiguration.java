/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package textadventure.resource.textadventure.ui;

/**
 * This class is deprecated and not used as of EMFText 1.4.1. The original
 * contents of this class have been moved to
 * textadventure.resource.textadventure.ui.TextadventureSourceViewerConfiguration.
 * This class is only generated to avoid compile errors with existing versions of
 * this class.
 */
@Deprecated
public class TextadventureEditorConfiguration {
	
}
