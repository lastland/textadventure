/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package textadventure.resource.textadventure.ui;

/**
 * A provider for BracketHandler objects.
 */
public interface ITextadventureBracketHandlerProvider {
	
	/**
	 * Returns the bracket handler.
	 */
	public textadventure.resource.textadventure.ui.ITextadventureBracketHandler getBracketHandler();
	
}
