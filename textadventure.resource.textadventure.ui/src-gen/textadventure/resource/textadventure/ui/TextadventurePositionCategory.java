/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package textadventure.resource.textadventure.ui;

/**
 * An enumeration of all position categories.
 */
public enum TextadventurePositionCategory {
	BRACKET, DEFINTION, PROXY;
}
