/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package textadventure.resource.textadventure.ui;

/**
 * A class which can be overridden to customize code completion proposals.
 */
public class TextadventureProposalPostProcessor {
	
	public java.util.List<textadventure.resource.textadventure.ui.TextadventureCompletionProposal> process(java.util.List<textadventure.resource.textadventure.ui.TextadventureCompletionProposal> proposals) {
		// the default implementation does returns the proposals as they are
		return proposals;
	}
	
}
