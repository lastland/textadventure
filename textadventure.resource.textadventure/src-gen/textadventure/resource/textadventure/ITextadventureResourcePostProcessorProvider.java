/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package textadventure.resource.textadventure;

/**
 * Implementors of this interface can provide a post-processor for text resources.
 */
public interface ITextadventureResourcePostProcessorProvider {
	
	/**
	 * Returns the processor that shall be called after text resource are successfully
	 * parsed.
	 */
	public textadventure.resource.textadventure.ITextadventureResourcePostProcessor getResourcePostProcessor();
	
}
