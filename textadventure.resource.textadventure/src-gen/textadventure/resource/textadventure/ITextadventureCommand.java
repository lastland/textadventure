/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package textadventure.resource.textadventure;

/**
 * A simple interface for commands that can be executed and that return
 * information about the success of their execution.
 */
public interface ITextadventureCommand<ContextType> {
	
	public boolean execute(ContextType context);
}
