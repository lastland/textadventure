/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package textadventure.resource.textadventure;

/**
 * Implementors of this interface provide an EMF resource.
 */
public interface ITextadventureResourceProvider {
	
	/**
	 * Returns the resource.
	 */
	public textadventure.resource.textadventure.ITextadventureTextResource getResource();
	
}
