/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package textadventure.resource.textadventure;

public interface ITextadventureInterpreterListener {
	
	public void handleInterpreteObject(org.eclipse.emf.ecore.EObject element);
}
