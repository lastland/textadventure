// $ANTLR 3.4

	package textadventure.resource.textadventure.mopp;


import org.antlr.runtime3_4_0.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

@SuppressWarnings({"all", "warnings", "unchecked"})
public class TextadventureParser extends TextadventureANTLRParserBase {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "COMMENT", "FLOAT", "INTEGER", "LINEBREAK", "QUOTED_34_34", "TEXT", "WHITESPACE", "')'", "','", "';'", "'='", "'@'", "'Items'", "'Persons'", "'Tile'", "'attack'", "'battle'", "'checkpoint'", "'defense'", "'description'", "'destination'", "'enemy'", "'from'", "'function('", "'hp'", "'ignore'", "'item'", "'on'", "'person'", "'player'", "'print'", "'remove'", "'tile'", "'used'", "'while'", "'x'", "'y'", "'{'", "'}'"
    };

    public static final int EOF=-1;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__19=19;
    public static final int T__20=20;
    public static final int T__21=21;
    public static final int T__22=22;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int T__29=29;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__42=42;
    public static final int COMMENT=4;
    public static final int FLOAT=5;
    public static final int INTEGER=6;
    public static final int LINEBREAK=7;
    public static final int QUOTED_34_34=8;
    public static final int TEXT=9;
    public static final int WHITESPACE=10;

    // delegates
    public TextadventureANTLRParserBase[] getDelegates() {
        return new TextadventureANTLRParserBase[] {};
    }

    // delegators


    public TextadventureParser(TokenStream input) {
        this(input, new RecognizerSharedState());
    }
    public TextadventureParser(TokenStream input, RecognizerSharedState state) {
        super(input, state);
        this.state.initializeRuleMemo(43 + 1);
         

    }

    public String[] getTokenNames() { return TextadventureParser.tokenNames; }
    public String getGrammarFileName() { return "Textadventure.g"; }


    	private textadventure.resource.textadventure.ITextadventureTokenResolverFactory tokenResolverFactory = new textadventure.resource.textadventure.mopp.TextadventureTokenResolverFactory();
    	
    	/**
    	 * the index of the last token that was handled by collectHiddenTokens()
    	 */
    	private int lastPosition;
    	
    	/**
    	 * A flag that indicates whether the parser should remember all expected elements.
    	 * This flag is set to true when using the parse for code completion. Otherwise it
    	 * is set to false.
    	 */
    	private boolean rememberExpectedElements = false;
    	
    	private Object parseToIndexTypeObject;
    	private int lastTokenIndex = 0;
    	
    	/**
    	 * A list of expected elements the were collected while parsing the input stream.
    	 * This list is only filled if <code>rememberExpectedElements</code> is set to
    	 * true.
    	 */
    	private java.util.List<textadventure.resource.textadventure.mopp.TextadventureExpectedTerminal> expectedElements = new java.util.ArrayList<textadventure.resource.textadventure.mopp.TextadventureExpectedTerminal>();
    	
    	private int mismatchedTokenRecoveryTries = 0;
    	/**
    	 * A helper list to allow a lexer to pass errors to its parser
    	 */
    	protected java.util.List<org.antlr.runtime3_4_0.RecognitionException> lexerExceptions = java.util.Collections.synchronizedList(new java.util.ArrayList<org.antlr.runtime3_4_0.RecognitionException>());
    	
    	/**
    	 * Another helper list to allow a lexer to pass positions of errors to its parser
    	 */
    	protected java.util.List<Integer> lexerExceptionsPosition = java.util.Collections.synchronizedList(new java.util.ArrayList<Integer>());
    	
    	/**
    	 * A stack for incomplete objects. This stack is used filled when the parser is
    	 * used for code completion. Whenever the parser starts to read an object it is
    	 * pushed on the stack. Once the element was parser completely it is popped from
    	 * the stack.
    	 */
    	java.util.List<org.eclipse.emf.ecore.EObject> incompleteObjects = new java.util.ArrayList<org.eclipse.emf.ecore.EObject>();
    	
    	private int stopIncludingHiddenTokens;
    	private int stopExcludingHiddenTokens;
    	private int tokenIndexOfLastCompleteElement;
    	
    	private int expectedElementsIndexOfLastCompleteElement;
    	
    	/**
    	 * The offset indicating the cursor position when the parser is used for code
    	 * completion by calling parseToExpectedElements().
    	 */
    	private int cursorOffset;
    	
    	/**
    	 * The offset of the first hidden token of the last expected element. This offset
    	 * is used to discard expected elements, which are not needed for code completion.
    	 */
    	private int lastStartIncludingHidden;
    	
    	protected void addErrorToResource(final String errorMessage, final int column, final int line, final int startIndex, final int stopIndex) {
    		postParseCommands.add(new textadventure.resource.textadventure.ITextadventureCommand<textadventure.resource.textadventure.ITextadventureTextResource>() {
    			public boolean execute(textadventure.resource.textadventure.ITextadventureTextResource resource) {
    				if (resource == null) {
    					// the resource can be null if the parser is used for code completion
    					return true;
    				}
    				resource.addProblem(new textadventure.resource.textadventure.ITextadventureProblem() {
    					public textadventure.resource.textadventure.TextadventureEProblemSeverity getSeverity() {
    						return textadventure.resource.textadventure.TextadventureEProblemSeverity.ERROR;
    					}
    					public textadventure.resource.textadventure.TextadventureEProblemType getType() {
    						return textadventure.resource.textadventure.TextadventureEProblemType.SYNTAX_ERROR;
    					}
    					public String getMessage() {
    						return errorMessage;
    					}
    					public java.util.Collection<textadventure.resource.textadventure.ITextadventureQuickFix> getQuickFixes() {
    						return null;
    					}
    				}, column, line, startIndex, stopIndex);
    				return true;
    			}
    		});
    	}
    	
    	public void addExpectedElement(org.eclipse.emf.ecore.EClass eClass, int[] ids) {
    		if (!this.rememberExpectedElements) {
    			return;
    		}
    		int terminalID = ids[0];
    		int followSetID = ids[1];
    		textadventure.resource.textadventure.ITextadventureExpectedElement terminal = textadventure.resource.textadventure.grammar.TextadventureFollowSetProvider.TERMINALS[terminalID];
    		textadventure.resource.textadventure.mopp.TextadventureContainedFeature[] containmentFeatures = new textadventure.resource.textadventure.mopp.TextadventureContainedFeature[ids.length - 2];
    		for (int i = 2; i < ids.length; i++) {
    			containmentFeatures[i - 2] = textadventure.resource.textadventure.grammar.TextadventureFollowSetProvider.LINKS[ids[i]];
    		}
    		textadventure.resource.textadventure.grammar.TextadventureContainmentTrace containmentTrace = new textadventure.resource.textadventure.grammar.TextadventureContainmentTrace(eClass, containmentFeatures);
    		org.eclipse.emf.ecore.EObject container = getLastIncompleteElement();
    		textadventure.resource.textadventure.mopp.TextadventureExpectedTerminal expectedElement = new textadventure.resource.textadventure.mopp.TextadventureExpectedTerminal(container, terminal, followSetID, containmentTrace);
    		setPosition(expectedElement, input.index());
    		int startIncludingHiddenTokens = expectedElement.getStartIncludingHiddenTokens();
    		if (lastStartIncludingHidden >= 0 && lastStartIncludingHidden < startIncludingHiddenTokens && cursorOffset > startIncludingHiddenTokens) {
    			// clear list of expected elements
    			this.expectedElements.clear();
    			this.expectedElementsIndexOfLastCompleteElement = 0;
    		}
    		lastStartIncludingHidden = startIncludingHiddenTokens;
    		this.expectedElements.add(expectedElement);
    	}
    	
    	protected void collectHiddenTokens(org.eclipse.emf.ecore.EObject element) {
    	}
    	
    	protected void copyLocalizationInfos(final org.eclipse.emf.ecore.EObject source, final org.eclipse.emf.ecore.EObject target) {
    		if (disableLocationMap) {
    			return;
    		}
    		postParseCommands.add(new textadventure.resource.textadventure.ITextadventureCommand<textadventure.resource.textadventure.ITextadventureTextResource>() {
    			public boolean execute(textadventure.resource.textadventure.ITextadventureTextResource resource) {
    				textadventure.resource.textadventure.ITextadventureLocationMap locationMap = resource.getLocationMap();
    				if (locationMap == null) {
    					// the locationMap can be null if the parser is used for code completion
    					return true;
    				}
    				locationMap.setCharStart(target, locationMap.getCharStart(source));
    				locationMap.setCharEnd(target, locationMap.getCharEnd(source));
    				locationMap.setColumn(target, locationMap.getColumn(source));
    				locationMap.setLine(target, locationMap.getLine(source));
    				return true;
    			}
    		});
    	}
    	
    	protected void copyLocalizationInfos(final org.antlr.runtime3_4_0.CommonToken source, final org.eclipse.emf.ecore.EObject target) {
    		if (disableLocationMap) {
    			return;
    		}
    		postParseCommands.add(new textadventure.resource.textadventure.ITextadventureCommand<textadventure.resource.textadventure.ITextadventureTextResource>() {
    			public boolean execute(textadventure.resource.textadventure.ITextadventureTextResource resource) {
    				textadventure.resource.textadventure.ITextadventureLocationMap locationMap = resource.getLocationMap();
    				if (locationMap == null) {
    					// the locationMap can be null if the parser is used for code completion
    					return true;
    				}
    				if (source == null) {
    					return true;
    				}
    				locationMap.setCharStart(target, source.getStartIndex());
    				locationMap.setCharEnd(target, source.getStopIndex());
    				locationMap.setColumn(target, source.getCharPositionInLine());
    				locationMap.setLine(target, source.getLine());
    				return true;
    			}
    		});
    	}
    	
    	/**
    	 * Sets the end character index and the last line for the given object in the
    	 * location map.
    	 */
    	protected void setLocalizationEnd(java.util.Collection<textadventure.resource.textadventure.ITextadventureCommand<textadventure.resource.textadventure.ITextadventureTextResource>> postParseCommands , final org.eclipse.emf.ecore.EObject object, final int endChar, final int endLine) {
    		if (disableLocationMap) {
    			return;
    		}
    		postParseCommands.add(new textadventure.resource.textadventure.ITextadventureCommand<textadventure.resource.textadventure.ITextadventureTextResource>() {
    			public boolean execute(textadventure.resource.textadventure.ITextadventureTextResource resource) {
    				textadventure.resource.textadventure.ITextadventureLocationMap locationMap = resource.getLocationMap();
    				if (locationMap == null) {
    					// the locationMap can be null if the parser is used for code completion
    					return true;
    				}
    				locationMap.setCharEnd(object, endChar);
    				locationMap.setLine(object, endLine);
    				return true;
    			}
    		});
    	}
    	
    	public textadventure.resource.textadventure.ITextadventureTextParser createInstance(java.io.InputStream actualInputStream, String encoding) {
    		try {
    			if (encoding == null) {
    				return new TextadventureParser(new org.antlr.runtime3_4_0.CommonTokenStream(new TextadventureLexer(new org.antlr.runtime3_4_0.ANTLRInputStream(actualInputStream))));
    			} else {
    				return new TextadventureParser(new org.antlr.runtime3_4_0.CommonTokenStream(new TextadventureLexer(new org.antlr.runtime3_4_0.ANTLRInputStream(actualInputStream, encoding))));
    			}
    		} catch (java.io.IOException e) {
    			new textadventure.resource.textadventure.util.TextadventureRuntimeUtil().logError("Error while creating parser.", e);
    			return null;
    		}
    	}
    	
    	/**
    	 * This default constructor is only used to call createInstance() on it.
    	 */
    	public TextadventureParser() {
    		super(null);
    	}
    	
    	protected org.eclipse.emf.ecore.EObject doParse() throws org.antlr.runtime3_4_0.RecognitionException {
    		this.lastPosition = 0;
    		// required because the lexer class can not be subclassed
    		((TextadventureLexer) getTokenStream().getTokenSource()).lexerExceptions = lexerExceptions;
    		((TextadventureLexer) getTokenStream().getTokenSource()).lexerExceptionsPosition = lexerExceptionsPosition;
    		Object typeObject = getTypeObject();
    		if (typeObject == null) {
    			return start();
    		} else if (typeObject instanceof org.eclipse.emf.ecore.EClass) {
    			org.eclipse.emf.ecore.EClass type = (org.eclipse.emf.ecore.EClass) typeObject;
    			if (type.getInstanceClass() == textadventure.EWorld.class) {
    				return parse_textadventure_EWorld();
    			}
    			if (type.getInstanceClass() == textadventure.ETile.class) {
    				return parse_textadventure_ETile();
    			}
    			if (type.getInstanceClass() == textadventure.EItem.class) {
    				return parse_textadventure_EItem();
    			}
    			if (type.getInstanceClass() == textadventure.ECheckPoint.class) {
    				return parse_textadventure_ECheckPoint();
    			}
    			if (type.getInstanceClass() == textadventure.EDestination.class) {
    				return parse_textadventure_EDestination();
    			}
    			if (type.getInstanceClass() == textadventure.EBinding.class) {
    				return parse_textadventure_EBinding();
    			}
    			if (type.getInstanceClass() == textadventure.EWhileEffect.class) {
    				return parse_textadventure_EWhileEffect();
    			}
    			if (type.getInstanceClass() == textadventure.ERemoveAction.class) {
    				return parse_textadventure_ERemoveAction();
    			}
    			if (type.getInstanceClass() == textadventure.EListDescription.class) {
    				return parse_textadventure_EListDescription();
    			}
    			if (type.getInstanceClass() == textadventure.EPerson.class) {
    				return parse_textadventure_EPerson();
    			}
    			if (type.getInstanceClass() == textadventure.EBattle.class) {
    				return parse_textadventure_EBattle();
    			}
    			if (type.getInstanceClass() == textadventure.EBattleCustomization.class) {
    				return parse_textadventure_EBattleCustomization();
    			}
    			if (type.getInstanceClass() == textadventure.EFunction.class) {
    				return parse_textadventure_EFunction();
    			}
    			if (type.getInstanceClass() == textadventure.EFunctionBody.class) {
    				return parse_textadventure_EFunctionBody();
    			}
    			if (type.getInstanceClass() == textadventure.EPrintStatement.class) {
    				return parse_textadventure_EPrintStatement();
    			}
    			if (type.getInstanceClass() == textadventure.ERemovePlayerStatement.class) {
    				return parse_textadventure_ERemovePlayerStatement();
    			}
    			if (type.getInstanceClass() == textadventure.EMethodCallStatement.class) {
    				return parse_textadventure_EMethodCallStatement();
    			}
    			if (type.getInstanceClass() == textadventure.EIgnoreItemAbility.class) {
    				return parse_textadventure_EIgnoreItemAbility();
    			}
    		}
    		throw new textadventure.resource.textadventure.mopp.TextadventureUnexpectedContentTypeException(typeObject);
    	}
    	
    	public int getMismatchedTokenRecoveryTries() {
    		return mismatchedTokenRecoveryTries;
    	}
    	
    	public Object getMissingSymbol(org.antlr.runtime3_4_0.IntStream arg0, org.antlr.runtime3_4_0.RecognitionException arg1, int arg2, org.antlr.runtime3_4_0.BitSet arg3) {
    		mismatchedTokenRecoveryTries++;
    		return super.getMissingSymbol(arg0, arg1, arg2, arg3);
    	}
    	
    	public Object getParseToIndexTypeObject() {
    		return parseToIndexTypeObject;
    	}
    	
    	protected Object getTypeObject() {
    		Object typeObject = getParseToIndexTypeObject();
    		if (typeObject != null) {
    			return typeObject;
    		}
    		java.util.Map<?,?> options = getOptions();
    		if (options != null) {
    			typeObject = options.get(textadventure.resource.textadventure.ITextadventureOptions.RESOURCE_CONTENT_TYPE);
    		}
    		return typeObject;
    	}
    	
    	/**
    	 * Implementation that calls {@link #doParse()} and handles the thrown
    	 * RecognitionExceptions.
    	 */
    	public textadventure.resource.textadventure.ITextadventureParseResult parse() {
    		terminateParsing = false;
    		postParseCommands = new java.util.ArrayList<textadventure.resource.textadventure.ITextadventureCommand<textadventure.resource.textadventure.ITextadventureTextResource>>();
    		textadventure.resource.textadventure.mopp.TextadventureParseResult parseResult = new textadventure.resource.textadventure.mopp.TextadventureParseResult();
    		try {
    			org.eclipse.emf.ecore.EObject result =  doParse();
    			if (lexerExceptions.isEmpty()) {
    				parseResult.setRoot(result);
    			}
    		} catch (org.antlr.runtime3_4_0.RecognitionException re) {
    			reportError(re);
    		} catch (java.lang.IllegalArgumentException iae) {
    			if ("The 'no null' constraint is violated".equals(iae.getMessage())) {
    				// can be caused if a null is set on EMF models where not allowed. this will just
    				// happen if other errors occurred before
    			} else {
    				iae.printStackTrace();
    			}
    		}
    		for (org.antlr.runtime3_4_0.RecognitionException re : lexerExceptions) {
    			reportLexicalError(re);
    		}
    		parseResult.getPostParseCommands().addAll(postParseCommands);
    		return parseResult;
    	}
    	
    	public java.util.List<textadventure.resource.textadventure.mopp.TextadventureExpectedTerminal> parseToExpectedElements(org.eclipse.emf.ecore.EClass type, textadventure.resource.textadventure.ITextadventureTextResource dummyResource, int cursorOffset) {
    		this.rememberExpectedElements = true;
    		this.parseToIndexTypeObject = type;
    		this.cursorOffset = cursorOffset;
    		this.lastStartIncludingHidden = -1;
    		final org.antlr.runtime3_4_0.CommonTokenStream tokenStream = (org.antlr.runtime3_4_0.CommonTokenStream) getTokenStream();
    		textadventure.resource.textadventure.ITextadventureParseResult result = parse();
    		for (org.eclipse.emf.ecore.EObject incompleteObject : incompleteObjects) {
    			org.antlr.runtime3_4_0.Lexer lexer = (org.antlr.runtime3_4_0.Lexer) tokenStream.getTokenSource();
    			int endChar = lexer.getCharIndex();
    			int endLine = lexer.getLine();
    			setLocalizationEnd(result.getPostParseCommands(), incompleteObject, endChar, endLine);
    		}
    		if (result != null) {
    			org.eclipse.emf.ecore.EObject root = result.getRoot();
    			if (root != null) {
    				dummyResource.getContentsInternal().add(root);
    			}
    			for (textadventure.resource.textadventure.ITextadventureCommand<textadventure.resource.textadventure.ITextadventureTextResource> command : result.getPostParseCommands()) {
    				command.execute(dummyResource);
    			}
    		}
    		// remove all expected elements that were added after the last complete element
    		expectedElements = expectedElements.subList(0, expectedElementsIndexOfLastCompleteElement + 1);
    		int lastFollowSetID = expectedElements.get(expectedElementsIndexOfLastCompleteElement).getFollowSetID();
    		java.util.Set<textadventure.resource.textadventure.mopp.TextadventureExpectedTerminal> currentFollowSet = new java.util.LinkedHashSet<textadventure.resource.textadventure.mopp.TextadventureExpectedTerminal>();
    		java.util.List<textadventure.resource.textadventure.mopp.TextadventureExpectedTerminal> newFollowSet = new java.util.ArrayList<textadventure.resource.textadventure.mopp.TextadventureExpectedTerminal>();
    		for (int i = expectedElementsIndexOfLastCompleteElement; i >= 0; i--) {
    			textadventure.resource.textadventure.mopp.TextadventureExpectedTerminal expectedElementI = expectedElements.get(i);
    			if (expectedElementI.getFollowSetID() == lastFollowSetID) {
    				currentFollowSet.add(expectedElementI);
    			} else {
    				break;
    			}
    		}
    		int followSetID = 103;
    		int i;
    		for (i = tokenIndexOfLastCompleteElement; i < tokenStream.size(); i++) {
    			org.antlr.runtime3_4_0.CommonToken nextToken = (org.antlr.runtime3_4_0.CommonToken) tokenStream.get(i);
    			if (nextToken.getType() < 0) {
    				break;
    			}
    			if (nextToken.getChannel() == 99) {
    				// hidden tokens do not reduce the follow set
    			} else {
    				// now that we have found the next visible token the position for that expected
    				// terminals can be set
    				for (textadventure.resource.textadventure.mopp.TextadventureExpectedTerminal nextFollow : newFollowSet) {
    					lastTokenIndex = 0;
    					setPosition(nextFollow, i);
    				}
    				newFollowSet.clear();
    				// normal tokens do reduce the follow set - only elements that match the token are
    				// kept
    				for (textadventure.resource.textadventure.mopp.TextadventureExpectedTerminal nextFollow : currentFollowSet) {
    					if (nextFollow.getTerminal().getTokenNames().contains(getTokenNames()[nextToken.getType()])) {
    						// keep this one - it matches
    						java.util.Collection<textadventure.resource.textadventure.util.TextadventurePair<textadventure.resource.textadventure.ITextadventureExpectedElement, textadventure.resource.textadventure.mopp.TextadventureContainedFeature[]>> newFollowers = nextFollow.getTerminal().getFollowers();
    						for (textadventure.resource.textadventure.util.TextadventurePair<textadventure.resource.textadventure.ITextadventureExpectedElement, textadventure.resource.textadventure.mopp.TextadventureContainedFeature[]> newFollowerPair : newFollowers) {
    							textadventure.resource.textadventure.ITextadventureExpectedElement newFollower = newFollowerPair.getLeft();
    							org.eclipse.emf.ecore.EObject container = getLastIncompleteElement();
    							textadventure.resource.textadventure.grammar.TextadventureContainmentTrace containmentTrace = new textadventure.resource.textadventure.grammar.TextadventureContainmentTrace(null, newFollowerPair.getRight());
    							textadventure.resource.textadventure.mopp.TextadventureExpectedTerminal newFollowTerminal = new textadventure.resource.textadventure.mopp.TextadventureExpectedTerminal(container, newFollower, followSetID, containmentTrace);
    							newFollowSet.add(newFollowTerminal);
    							expectedElements.add(newFollowTerminal);
    						}
    					}
    				}
    				currentFollowSet.clear();
    				currentFollowSet.addAll(newFollowSet);
    			}
    			followSetID++;
    		}
    		// after the last token in the stream we must set the position for the elements
    		// that were added during the last iteration of the loop
    		for (textadventure.resource.textadventure.mopp.TextadventureExpectedTerminal nextFollow : newFollowSet) {
    			lastTokenIndex = 0;
    			setPosition(nextFollow, i);
    		}
    		return this.expectedElements;
    	}
    	
    	public void setPosition(textadventure.resource.textadventure.mopp.TextadventureExpectedTerminal expectedElement, int tokenIndex) {
    		int currentIndex = Math.max(0, tokenIndex);
    		for (int index = lastTokenIndex; index < currentIndex; index++) {
    			if (index >= input.size()) {
    				break;
    			}
    			org.antlr.runtime3_4_0.CommonToken tokenAtIndex = (org.antlr.runtime3_4_0.CommonToken) input.get(index);
    			stopIncludingHiddenTokens = tokenAtIndex.getStopIndex() + 1;
    			if (tokenAtIndex.getChannel() != 99 && !anonymousTokens.contains(tokenAtIndex)) {
    				stopExcludingHiddenTokens = tokenAtIndex.getStopIndex() + 1;
    			}
    		}
    		lastTokenIndex = Math.max(0, currentIndex);
    		expectedElement.setPosition(stopExcludingHiddenTokens, stopIncludingHiddenTokens);
    	}
    	
    	public Object recoverFromMismatchedToken(org.antlr.runtime3_4_0.IntStream input, int ttype, org.antlr.runtime3_4_0.BitSet follow) throws org.antlr.runtime3_4_0.RecognitionException {
    		if (!rememberExpectedElements) {
    			return super.recoverFromMismatchedToken(input, ttype, follow);
    		} else {
    			return null;
    		}
    	}
    	
    	/**
    	 * Translates errors thrown by the parser into human readable messages.
    	 */
    	public void reportError(final org.antlr.runtime3_4_0.RecognitionException e)  {
    		String message = e.getMessage();
    		if (e instanceof org.antlr.runtime3_4_0.MismatchedTokenException) {
    			org.antlr.runtime3_4_0.MismatchedTokenException mte = (org.antlr.runtime3_4_0.MismatchedTokenException) e;
    			String expectedTokenName = formatTokenName(mte.expecting);
    			String actualTokenName = formatTokenName(e.token.getType());
    			message = "Syntax error on token \"" + e.token.getText() + " (" + actualTokenName + ")\", \"" + expectedTokenName + "\" expected";
    		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedTreeNodeException) {
    			org.antlr.runtime3_4_0.MismatchedTreeNodeException mtne = (org.antlr.runtime3_4_0.MismatchedTreeNodeException) e;
    			String expectedTokenName = formatTokenName(mtne.expecting);
    			message = "mismatched tree node: " + "xxx" + "; tokenName " + expectedTokenName;
    		} else if (e instanceof org.antlr.runtime3_4_0.NoViableAltException) {
    			message = "Syntax error on token \"" + e.token.getText() + "\", check following tokens";
    		} else if (e instanceof org.antlr.runtime3_4_0.EarlyExitException) {
    			message = "Syntax error on token \"" + e.token.getText() + "\", delete this token";
    		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedSetException) {
    			org.antlr.runtime3_4_0.MismatchedSetException mse = (org.antlr.runtime3_4_0.MismatchedSetException) e;
    			message = "mismatched token: " + e.token + "; expecting set " + mse.expecting;
    		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedNotSetException) {
    			org.antlr.runtime3_4_0.MismatchedNotSetException mse = (org.antlr.runtime3_4_0.MismatchedNotSetException) e;
    			message = "mismatched token: " +  e.token + "; expecting set " + mse.expecting;
    		} else if (e instanceof org.antlr.runtime3_4_0.FailedPredicateException) {
    			org.antlr.runtime3_4_0.FailedPredicateException fpe = (org.antlr.runtime3_4_0.FailedPredicateException) e;
    			message = "rule " + fpe.ruleName + " failed predicate: {" +  fpe.predicateText + "}?";
    		}
    		// the resource may be null if the parser is used for code completion
    		final String finalMessage = message;
    		if (e.token instanceof org.antlr.runtime3_4_0.CommonToken) {
    			final org.antlr.runtime3_4_0.CommonToken ct = (org.antlr.runtime3_4_0.CommonToken) e.token;
    			addErrorToResource(finalMessage, ct.getCharPositionInLine(), ct.getLine(), ct.getStartIndex(), ct.getStopIndex());
    		} else {
    			addErrorToResource(finalMessage, e.token.getCharPositionInLine(), e.token.getLine(), 1, 5);
    		}
    	}
    	
    	/**
    	 * Translates errors thrown by the lexer into human readable messages.
    	 */
    	public void reportLexicalError(final org.antlr.runtime3_4_0.RecognitionException e)  {
    		String message = "";
    		if (e instanceof org.antlr.runtime3_4_0.MismatchedTokenException) {
    			org.antlr.runtime3_4_0.MismatchedTokenException mte = (org.antlr.runtime3_4_0.MismatchedTokenException) e;
    			message = "Syntax error on token \"" + ((char) e.c) + "\", \"" + (char) mte.expecting + "\" expected";
    		} else if (e instanceof org.antlr.runtime3_4_0.NoViableAltException) {
    			message = "Syntax error on token \"" + ((char) e.c) + "\", delete this token";
    		} else if (e instanceof org.antlr.runtime3_4_0.EarlyExitException) {
    			org.antlr.runtime3_4_0.EarlyExitException eee = (org.antlr.runtime3_4_0.EarlyExitException) e;
    			message = "required (...)+ loop (decision=" + eee.decisionNumber + ") did not match anything; on line " + e.line + ":" + e.charPositionInLine + " char=" + ((char) e.c) + "'";
    		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedSetException) {
    			org.antlr.runtime3_4_0.MismatchedSetException mse = (org.antlr.runtime3_4_0.MismatchedSetException) e;
    			message = "mismatched char: '" + ((char) e.c) + "' on line " + e.line + ":" + e.charPositionInLine + "; expecting set " + mse.expecting;
    		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedNotSetException) {
    			org.antlr.runtime3_4_0.MismatchedNotSetException mse = (org.antlr.runtime3_4_0.MismatchedNotSetException) e;
    			message = "mismatched char: '" + ((char) e.c) + "' on line " + e.line + ":" + e.charPositionInLine + "; expecting set " + mse.expecting;
    		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedRangeException) {
    			org.antlr.runtime3_4_0.MismatchedRangeException mre = (org.antlr.runtime3_4_0.MismatchedRangeException) e;
    			message = "mismatched char: '" + ((char) e.c) + "' on line " + e.line + ":" + e.charPositionInLine + "; expecting set '" + (char) mre.a + "'..'" + (char) mre.b + "'";
    		} else if (e instanceof org.antlr.runtime3_4_0.FailedPredicateException) {
    			org.antlr.runtime3_4_0.FailedPredicateException fpe = (org.antlr.runtime3_4_0.FailedPredicateException) e;
    			message = "rule " + fpe.ruleName + " failed predicate: {" + fpe.predicateText + "}?";
    		}
    		addErrorToResource(message, e.charPositionInLine, e.line, lexerExceptionsPosition.get(lexerExceptions.indexOf(e)), lexerExceptionsPosition.get(lexerExceptions.indexOf(e)));
    	}
    	
    	private void startIncompleteElement(Object object) {
    		if (object instanceof org.eclipse.emf.ecore.EObject) {
    			this.incompleteObjects.add((org.eclipse.emf.ecore.EObject) object);
    		}
    	}
    	
    	private void completedElement(Object object, boolean isContainment) {
    		if (isContainment && !this.incompleteObjects.isEmpty()) {
    			boolean exists = this.incompleteObjects.remove(object);
    			if (!exists) {
    			}
    		}
    		if (object instanceof org.eclipse.emf.ecore.EObject) {
    			this.tokenIndexOfLastCompleteElement = getTokenStream().index();
    			this.expectedElementsIndexOfLastCompleteElement = expectedElements.size() - 1;
    		}
    	}
    	
    	private org.eclipse.emf.ecore.EObject getLastIncompleteElement() {
    		if (incompleteObjects.isEmpty()) {
    			return null;
    		}
    		return incompleteObjects.get(incompleteObjects.size() - 1);
    	}
    	



    // $ANTLR start "start"
    // Textadventure.g:550:1: start returns [ org.eclipse.emf.ecore.EObject element = null] : (c0= parse_textadventure_EWorld ) EOF ;
    public final org.eclipse.emf.ecore.EObject start() throws RecognitionException {
        org.eclipse.emf.ecore.EObject element =  null;

        int start_StartIndex = input.index();

        textadventure.EWorld c0 =null;


        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 1) ) { return element; }

            // Textadventure.g:551:2: ( (c0= parse_textadventure_EWorld ) EOF )
            // Textadventure.g:552:2: (c0= parse_textadventure_EWorld ) EOF
            {
            if ( state.backtracking==0 ) {
            		// follow set for start rule(s)
            		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[0]);
            		expectedElementsIndexOfLastCompleteElement = 0;
            	}

            // Textadventure.g:557:2: (c0= parse_textadventure_EWorld )
            // Textadventure.g:558:3: c0= parse_textadventure_EWorld
            {
            pushFollow(FOLLOW_parse_textadventure_EWorld_in_start82);
            c0=parse_textadventure_EWorld();

            state._fsp--;
            if (state.failed) return element;

            if ( state.backtracking==0 ) { element = c0; }

            }


            match(input,EOF,FOLLOW_EOF_in_start89); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		retrieveLayoutInformation(element, null, null, false);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 1, start_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "start"



    // $ANTLR start "parse_textadventure_EWorld"
    // Textadventure.g:566:1: parse_textadventure_EWorld returns [textadventure.EWorld element = null] : a0= 'player' a1= '=' (a2= TEXT ) ( (a3_0= parse_textadventure_ETile ) )+ ( (a4_0= parse_textadventure_EItem ) )* ( (a5_0= parse_textadventure_EPerson ) )+ ( (a6_0= parse_textadventure_EBattle ) )* ( (a7_0= parse_textadventure_ECheckPoint ) )* ( (a8_0= parse_textadventure_EDestination ) )* ;
    public final textadventure.EWorld parse_textadventure_EWorld() throws RecognitionException {
        textadventure.EWorld element =  null;

        int parse_textadventure_EWorld_StartIndex = input.index();

        Token a0=null;
        Token a1=null;
        Token a2=null;
        textadventure.ETile a3_0 =null;

        textadventure.EItem a4_0 =null;

        textadventure.EPerson a5_0 =null;

        textadventure.EBattle a6_0 =null;

        textadventure.ECheckPoint a7_0 =null;

        textadventure.EDestination a8_0 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 2) ) { return element; }

            // Textadventure.g:569:2: (a0= 'player' a1= '=' (a2= TEXT ) ( (a3_0= parse_textadventure_ETile ) )+ ( (a4_0= parse_textadventure_EItem ) )* ( (a5_0= parse_textadventure_EPerson ) )+ ( (a6_0= parse_textadventure_EBattle ) )* ( (a7_0= parse_textadventure_ECheckPoint ) )* ( (a8_0= parse_textadventure_EDestination ) )* )
            // Textadventure.g:570:2: a0= 'player' a1= '=' (a2= TEXT ) ( (a3_0= parse_textadventure_ETile ) )+ ( (a4_0= parse_textadventure_EItem ) )* ( (a5_0= parse_textadventure_EPerson ) )+ ( (a6_0= parse_textadventure_EBattle ) )* ( (a7_0= parse_textadventure_ECheckPoint ) )* ( (a8_0= parse_textadventure_EDestination ) )*
            {
            a0=(Token)match(input,33,FOLLOW_33_in_parse_textadventure_EWorld115); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = textadventure.TextadventureFactory.eINSTANCE.createEWorld();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_0_0_0_0, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[1]);
            	}

            a1=(Token)match(input,14,FOLLOW_14_in_parse_textadventure_EWorld129); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = textadventure.TextadventureFactory.eINSTANCE.createEWorld();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_0_0_0_1, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[2]);
            	}

            // Textadventure.g:598:2: (a2= TEXT )
            // Textadventure.g:599:3: a2= TEXT
            {
            a2=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_textadventure_EWorld147); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new textadventure.resource.textadventure.mopp.TextadventureTerminateParsingException();
            			}
            			if (element == null) {
            				element = textadventure.TextadventureFactory.eINSTANCE.createEWorld();
            				startIncompleteElement(element);
            			}
            			if (a2 != null) {
            				textadventure.resource.textadventure.ITextadventureTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
            				tokenResolver.setOptions(getOptions());
            				textadventure.resource.textadventure.ITextadventureTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a2.getText(), element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EWORLD__PLAYER_PERSON), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a2).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStopIndex());
            				}
            				String resolved = (String) resolvedObject;
            				textadventure.EPerson proxy = textadventure.TextadventureFactory.eINSTANCE.createEPerson();
            				collectHiddenTokens(element);
            				registerContextDependentProxy(new textadventure.resource.textadventure.mopp.TextadventureContextDependentURIFragmentFactory<textadventure.EWorld, textadventure.EPerson>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getEWorldPlayerPersonReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EWORLD__PLAYER_PERSON), resolved, proxy);
            				if (proxy != null) {
            					Object value = proxy;
            					element.eSet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EWORLD__PLAYER_PERSON), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_0_0_0_2, proxy, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, element);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, proxy);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEWorld(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[3]);
            	}

            // Textadventure.g:638:2: ( (a3_0= parse_textadventure_ETile ) )+
            int cnt1=0;
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==36) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // Textadventure.g:639:3: (a3_0= parse_textadventure_ETile )
            	    {
            	    // Textadventure.g:639:3: (a3_0= parse_textadventure_ETile )
            	    // Textadventure.g:640:4: a3_0= parse_textadventure_ETile
            	    {
            	    pushFollow(FOLLOW_parse_textadventure_ETile_in_parse_textadventure_EWorld177);
            	    a3_0=parse_textadventure_ETile();

            	    state._fsp--;
            	    if (state.failed) return element;

            	    if ( state.backtracking==0 ) {
            	    				if (terminateParsing) {
            	    					throw new textadventure.resource.textadventure.mopp.TextadventureTerminateParsingException();
            	    				}
            	    				if (element == null) {
            	    					element = textadventure.TextadventureFactory.eINSTANCE.createEWorld();
            	    					startIncompleteElement(element);
            	    				}
            	    				if (a3_0 != null) {
            	    					if (a3_0 != null) {
            	    						Object value = a3_0;
            	    						addObjectToList(element, textadventure.TextadventurePackage.EWORLD__TILES, value);
            	    						completedElement(value, true);
            	    					}
            	    					collectHiddenTokens(element);
            	    					retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_0_0_0_3, a3_0, true);
            	    					copyLocalizationInfos(a3_0, element);
            	    				}
            	    			}

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt1 >= 1 ) break loop1;
            	    if (state.backtracking>0) {state.failed=true; return element;}
                        EarlyExitException eee =
                            new EarlyExitException(1, input);
                        throw eee;
                }
                cnt1++;
            } while (true);


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEWorld(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[4]);
            		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEWorld(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[5]);
            		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEWorld(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[6]);
            	}

            // Textadventure.g:668:2: ( (a4_0= parse_textadventure_EItem ) )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==30) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // Textadventure.g:669:3: (a4_0= parse_textadventure_EItem )
            	    {
            	    // Textadventure.g:669:3: (a4_0= parse_textadventure_EItem )
            	    // Textadventure.g:670:4: a4_0= parse_textadventure_EItem
            	    {
            	    pushFollow(FOLLOW_parse_textadventure_EItem_in_parse_textadventure_EWorld212);
            	    a4_0=parse_textadventure_EItem();

            	    state._fsp--;
            	    if (state.failed) return element;

            	    if ( state.backtracking==0 ) {
            	    				if (terminateParsing) {
            	    					throw new textadventure.resource.textadventure.mopp.TextadventureTerminateParsingException();
            	    				}
            	    				if (element == null) {
            	    					element = textadventure.TextadventureFactory.eINSTANCE.createEWorld();
            	    					startIncompleteElement(element);
            	    				}
            	    				if (a4_0 != null) {
            	    					if (a4_0 != null) {
            	    						Object value = a4_0;
            	    						addObjectToList(element, textadventure.TextadventurePackage.EWORLD__ITEMS, value);
            	    						completedElement(value, true);
            	    					}
            	    					collectHiddenTokens(element);
            	    					retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_0_0_0_4, a4_0, true);
            	    					copyLocalizationInfos(a4_0, element);
            	    				}
            	    			}

            	    }


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEWorld(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[7]);
            		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEWorld(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[8]);
            	}

            // Textadventure.g:697:2: ( (a5_0= parse_textadventure_EPerson ) )+
            int cnt3=0;
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==32) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // Textadventure.g:698:3: (a5_0= parse_textadventure_EPerson )
            	    {
            	    // Textadventure.g:698:3: (a5_0= parse_textadventure_EPerson )
            	    // Textadventure.g:699:4: a5_0= parse_textadventure_EPerson
            	    {
            	    pushFollow(FOLLOW_parse_textadventure_EPerson_in_parse_textadventure_EWorld247);
            	    a5_0=parse_textadventure_EPerson();

            	    state._fsp--;
            	    if (state.failed) return element;

            	    if ( state.backtracking==0 ) {
            	    				if (terminateParsing) {
            	    					throw new textadventure.resource.textadventure.mopp.TextadventureTerminateParsingException();
            	    				}
            	    				if (element == null) {
            	    					element = textadventure.TextadventureFactory.eINSTANCE.createEWorld();
            	    					startIncompleteElement(element);
            	    				}
            	    				if (a5_0 != null) {
            	    					if (a5_0 != null) {
            	    						Object value = a5_0;
            	    						addObjectToList(element, textadventure.TextadventurePackage.EWORLD__PERSONS, value);
            	    						completedElement(value, true);
            	    					}
            	    					collectHiddenTokens(element);
            	    					retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_0_0_0_5, a5_0, true);
            	    					copyLocalizationInfos(a5_0, element);
            	    				}
            	    			}

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt3 >= 1 ) break loop3;
            	    if (state.backtracking>0) {state.failed=true; return element;}
                        EarlyExitException eee =
                            new EarlyExitException(3, input);
                        throw eee;
                }
                cnt3++;
            } while (true);


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEWorld(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[9]);
            		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEWorld(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[10]);
            		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEWorld(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[11]);
            		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEWorld(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[12]);
            	}

            // Textadventure.g:728:2: ( (a6_0= parse_textadventure_EBattle ) )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==20) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // Textadventure.g:729:3: (a6_0= parse_textadventure_EBattle )
            	    {
            	    // Textadventure.g:729:3: (a6_0= parse_textadventure_EBattle )
            	    // Textadventure.g:730:4: a6_0= parse_textadventure_EBattle
            	    {
            	    pushFollow(FOLLOW_parse_textadventure_EBattle_in_parse_textadventure_EWorld282);
            	    a6_0=parse_textadventure_EBattle();

            	    state._fsp--;
            	    if (state.failed) return element;

            	    if ( state.backtracking==0 ) {
            	    				if (terminateParsing) {
            	    					throw new textadventure.resource.textadventure.mopp.TextadventureTerminateParsingException();
            	    				}
            	    				if (element == null) {
            	    					element = textadventure.TextadventureFactory.eINSTANCE.createEWorld();
            	    					startIncompleteElement(element);
            	    				}
            	    				if (a6_0 != null) {
            	    					if (a6_0 != null) {
            	    						Object value = a6_0;
            	    						addObjectToList(element, textadventure.TextadventurePackage.EWORLD__BATTLE, value);
            	    						completedElement(value, true);
            	    					}
            	    					collectHiddenTokens(element);
            	    					retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_0_0_0_6, a6_0, true);
            	    					copyLocalizationInfos(a6_0, element);
            	    				}
            	    			}

            	    }


            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEWorld(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[13]);
            		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEWorld(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[14]);
            		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEWorld(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[15]);
            	}

            // Textadventure.g:758:2: ( (a7_0= parse_textadventure_ECheckPoint ) )*
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( (LA5_0==21) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // Textadventure.g:759:3: (a7_0= parse_textadventure_ECheckPoint )
            	    {
            	    // Textadventure.g:759:3: (a7_0= parse_textadventure_ECheckPoint )
            	    // Textadventure.g:760:4: a7_0= parse_textadventure_ECheckPoint
            	    {
            	    pushFollow(FOLLOW_parse_textadventure_ECheckPoint_in_parse_textadventure_EWorld317);
            	    a7_0=parse_textadventure_ECheckPoint();

            	    state._fsp--;
            	    if (state.failed) return element;

            	    if ( state.backtracking==0 ) {
            	    				if (terminateParsing) {
            	    					throw new textadventure.resource.textadventure.mopp.TextadventureTerminateParsingException();
            	    				}
            	    				if (element == null) {
            	    					element = textadventure.TextadventureFactory.eINSTANCE.createEWorld();
            	    					startIncompleteElement(element);
            	    				}
            	    				if (a7_0 != null) {
            	    					if (a7_0 != null) {
            	    						Object value = a7_0;
            	    						addObjectToList(element, textadventure.TextadventurePackage.EWORLD__CHECKPOINT, value);
            	    						completedElement(value, true);
            	    					}
            	    					collectHiddenTokens(element);
            	    					retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_0_0_0_7, a7_0, true);
            	    					copyLocalizationInfos(a7_0, element);
            	    				}
            	    			}

            	    }


            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEWorld(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[16]);
            		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEWorld(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[17]);
            	}

            // Textadventure.g:787:2: ( (a8_0= parse_textadventure_EDestination ) )*
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( (LA6_0==24) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // Textadventure.g:788:3: (a8_0= parse_textadventure_EDestination )
            	    {
            	    // Textadventure.g:788:3: (a8_0= parse_textadventure_EDestination )
            	    // Textadventure.g:789:4: a8_0= parse_textadventure_EDestination
            	    {
            	    pushFollow(FOLLOW_parse_textadventure_EDestination_in_parse_textadventure_EWorld352);
            	    a8_0=parse_textadventure_EDestination();

            	    state._fsp--;
            	    if (state.failed) return element;

            	    if ( state.backtracking==0 ) {
            	    				if (terminateParsing) {
            	    					throw new textadventure.resource.textadventure.mopp.TextadventureTerminateParsingException();
            	    				}
            	    				if (element == null) {
            	    					element = textadventure.TextadventureFactory.eINSTANCE.createEWorld();
            	    					startIncompleteElement(element);
            	    				}
            	    				if (a8_0 != null) {
            	    					if (a8_0 != null) {
            	    						Object value = a8_0;
            	    						addObjectToList(element, textadventure.TextadventurePackage.EWORLD__DESTINATION, value);
            	    						completedElement(value, true);
            	    					}
            	    					collectHiddenTokens(element);
            	    					retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_0_0_0_8, a8_0, true);
            	    					copyLocalizationInfos(a8_0, element);
            	    				}
            	    			}

            	    }


            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEWorld(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[18]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 2, parse_textadventure_EWorld_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_textadventure_EWorld"



    // $ANTLR start "parse_textadventure_ETile"
    // Textadventure.g:817:1: parse_textadventure_ETile returns [textadventure.ETile element = null] : a0= 'tile' (a1= TEXT ) a2= '{' a3= 'x' a4= '=' (a5= INTEGER ) a6= 'y' a7= '=' (a8= INTEGER ) a9= 'description' a10= '=' (a11= QUOTED_34_34 ) a12= '}' ;
    public final textadventure.ETile parse_textadventure_ETile() throws RecognitionException {
        textadventure.ETile element =  null;

        int parse_textadventure_ETile_StartIndex = input.index();

        Token a0=null;
        Token a1=null;
        Token a2=null;
        Token a3=null;
        Token a4=null;
        Token a5=null;
        Token a6=null;
        Token a7=null;
        Token a8=null;
        Token a9=null;
        Token a10=null;
        Token a11=null;
        Token a12=null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 3) ) { return element; }

            // Textadventure.g:820:2: (a0= 'tile' (a1= TEXT ) a2= '{' a3= 'x' a4= '=' (a5= INTEGER ) a6= 'y' a7= '=' (a8= INTEGER ) a9= 'description' a10= '=' (a11= QUOTED_34_34 ) a12= '}' )
            // Textadventure.g:821:2: a0= 'tile' (a1= TEXT ) a2= '{' a3= 'x' a4= '=' (a5= INTEGER ) a6= 'y' a7= '=' (a8= INTEGER ) a9= 'description' a10= '=' (a11= QUOTED_34_34 ) a12= '}'
            {
            a0=(Token)match(input,36,FOLLOW_36_in_parse_textadventure_ETile393); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = textadventure.TextadventureFactory.eINSTANCE.createETile();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_1_0_0_0, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[19]);
            	}

            // Textadventure.g:835:2: (a1= TEXT )
            // Textadventure.g:836:3: a1= TEXT
            {
            a1=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_textadventure_ETile411); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new textadventure.resource.textadventure.mopp.TextadventureTerminateParsingException();
            			}
            			if (element == null) {
            				element = textadventure.TextadventureFactory.eINSTANCE.createETile();
            				startIncompleteElement(element);
            			}
            			if (a1 != null) {
            				textadventure.resource.textadventure.ITextadventureTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
            				tokenResolver.setOptions(getOptions());
            				textadventure.resource.textadventure.ITextadventureTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.ETILE__ID), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a1).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStopIndex());
            				}
            				java.lang.String resolved = (java.lang.String) resolvedObject;
            				if (resolved != null) {
            					Object value = resolved;
            					element.eSet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.ETILE__ID), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_1_0_0_1, resolved, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a1, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[20]);
            	}

            a2=(Token)match(input,41,FOLLOW_41_in_parse_textadventure_ETile432); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = textadventure.TextadventureFactory.eINSTANCE.createETile();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_1_0_0_2, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[21]);
            	}

            a3=(Token)match(input,39,FOLLOW_39_in_parse_textadventure_ETile446); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = textadventure.TextadventureFactory.eINSTANCE.createETile();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_1_0_0_3, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a3, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[22]);
            	}

            a4=(Token)match(input,14,FOLLOW_14_in_parse_textadventure_ETile460); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = textadventure.TextadventureFactory.eINSTANCE.createETile();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_1_0_0_4, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a4, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[23]);
            	}

            // Textadventure.g:913:2: (a5= INTEGER )
            // Textadventure.g:914:3: a5= INTEGER
            {
            a5=(Token)match(input,INTEGER,FOLLOW_INTEGER_in_parse_textadventure_ETile478); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new textadventure.resource.textadventure.mopp.TextadventureTerminateParsingException();
            			}
            			if (element == null) {
            				element = textadventure.TextadventureFactory.eINSTANCE.createETile();
            				startIncompleteElement(element);
            			}
            			if (a5 != null) {
            				textadventure.resource.textadventure.ITextadventureTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("INTEGER");
            				tokenResolver.setOptions(getOptions());
            				textadventure.resource.textadventure.ITextadventureTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a5.getText(), element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.ETILE__X), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a5).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a5).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a5).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a5).getStopIndex());
            				}
            				java.lang.Integer resolved = (java.lang.Integer) resolvedObject;
            				if (resolved != null) {
            					Object value = resolved;
            					element.eSet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.ETILE__X), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_1_0_0_5, resolved, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a5, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[24]);
            	}

            a6=(Token)match(input,40,FOLLOW_40_in_parse_textadventure_ETile499); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = textadventure.TextadventureFactory.eINSTANCE.createETile();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_1_0_0_6, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a6, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[25]);
            	}

            a7=(Token)match(input,14,FOLLOW_14_in_parse_textadventure_ETile513); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = textadventure.TextadventureFactory.eINSTANCE.createETile();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_1_0_0_7, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a7, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[26]);
            	}

            // Textadventure.g:977:2: (a8= INTEGER )
            // Textadventure.g:978:3: a8= INTEGER
            {
            a8=(Token)match(input,INTEGER,FOLLOW_INTEGER_in_parse_textadventure_ETile531); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new textadventure.resource.textadventure.mopp.TextadventureTerminateParsingException();
            			}
            			if (element == null) {
            				element = textadventure.TextadventureFactory.eINSTANCE.createETile();
            				startIncompleteElement(element);
            			}
            			if (a8 != null) {
            				textadventure.resource.textadventure.ITextadventureTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("INTEGER");
            				tokenResolver.setOptions(getOptions());
            				textadventure.resource.textadventure.ITextadventureTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a8.getText(), element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.ETILE__Y), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a8).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a8).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a8).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a8).getStopIndex());
            				}
            				java.lang.Integer resolved = (java.lang.Integer) resolvedObject;
            				if (resolved != null) {
            					Object value = resolved;
            					element.eSet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.ETILE__Y), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_1_0_0_8, resolved, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a8, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[27]);
            	}

            a9=(Token)match(input,23,FOLLOW_23_in_parse_textadventure_ETile552); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = textadventure.TextadventureFactory.eINSTANCE.createETile();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_1_0_0_9, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a9, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[28]);
            	}

            a10=(Token)match(input,14,FOLLOW_14_in_parse_textadventure_ETile566); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = textadventure.TextadventureFactory.eINSTANCE.createETile();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_1_0_0_10, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a10, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[29]);
            	}

            // Textadventure.g:1041:2: (a11= QUOTED_34_34 )
            // Textadventure.g:1042:3: a11= QUOTED_34_34
            {
            a11=(Token)match(input,QUOTED_34_34,FOLLOW_QUOTED_34_34_in_parse_textadventure_ETile584); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new textadventure.resource.textadventure.mopp.TextadventureTerminateParsingException();
            			}
            			if (element == null) {
            				element = textadventure.TextadventureFactory.eINSTANCE.createETile();
            				startIncompleteElement(element);
            			}
            			if (a11 != null) {
            				textadventure.resource.textadventure.ITextadventureTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34");
            				tokenResolver.setOptions(getOptions());
            				textadventure.resource.textadventure.ITextadventureTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a11.getText(), element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.ETILE__DESCRIPTION), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a11).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a11).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a11).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a11).getStopIndex());
            				}
            				java.lang.String resolved = (java.lang.String) resolvedObject;
            				if (resolved != null) {
            					Object value = resolved;
            					element.eSet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.ETILE__DESCRIPTION), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_1_0_0_11, resolved, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a11, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[30]);
            	}

            a12=(Token)match(input,42,FOLLOW_42_in_parse_textadventure_ETile605); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = textadventure.TextadventureFactory.eINSTANCE.createETile();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_1_0_0_12, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a12, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEWorld(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[31]);
            		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEWorld(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[32]);
            		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEWorld(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[33]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 3, parse_textadventure_ETile_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_textadventure_ETile"



    // $ANTLR start "parse_textadventure_EItem"
    // Textadventure.g:1095:1: parse_textadventure_EItem returns [textadventure.EItem element = null] : a0= 'item' (a1= TEXT ) ( (a2= '@' (a3= TEXT ) ) )? a4= '{' ( ( (a5_0= parse_textadventure_EEffect ) ) )* a6= '}' ;
    public final textadventure.EItem parse_textadventure_EItem() throws RecognitionException {
        textadventure.EItem element =  null;

        int parse_textadventure_EItem_StartIndex = input.index();

        Token a0=null;
        Token a1=null;
        Token a2=null;
        Token a3=null;
        Token a4=null;
        Token a6=null;
        textadventure.EEffect a5_0 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 4) ) { return element; }

            // Textadventure.g:1098:2: (a0= 'item' (a1= TEXT ) ( (a2= '@' (a3= TEXT ) ) )? a4= '{' ( ( (a5_0= parse_textadventure_EEffect ) ) )* a6= '}' )
            // Textadventure.g:1099:2: a0= 'item' (a1= TEXT ) ( (a2= '@' (a3= TEXT ) ) )? a4= '{' ( ( (a5_0= parse_textadventure_EEffect ) ) )* a6= '}'
            {
            a0=(Token)match(input,30,FOLLOW_30_in_parse_textadventure_EItem634); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = textadventure.TextadventureFactory.eINSTANCE.createEItem();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_2_0_0_0, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[34]);
            	}

            // Textadventure.g:1113:2: (a1= TEXT )
            // Textadventure.g:1114:3: a1= TEXT
            {
            a1=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_textadventure_EItem652); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new textadventure.resource.textadventure.mopp.TextadventureTerminateParsingException();
            			}
            			if (element == null) {
            				element = textadventure.TextadventureFactory.eINSTANCE.createEItem();
            				startIncompleteElement(element);
            			}
            			if (a1 != null) {
            				textadventure.resource.textadventure.ITextadventureTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
            				tokenResolver.setOptions(getOptions());
            				textadventure.resource.textadventure.ITextadventureTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EITEM__NAME), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a1).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStopIndex());
            				}
            				java.lang.String resolved = (java.lang.String) resolvedObject;
            				if (resolved != null) {
            					Object value = resolved;
            					element.eSet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EITEM__NAME), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_2_0_0_1, resolved, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a1, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[35]);
            		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[36]);
            	}

            // Textadventure.g:1150:2: ( (a2= '@' (a3= TEXT ) ) )?
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==15) ) {
                alt7=1;
            }
            switch (alt7) {
                case 1 :
                    // Textadventure.g:1151:3: (a2= '@' (a3= TEXT ) )
                    {
                    // Textadventure.g:1151:3: (a2= '@' (a3= TEXT ) )
                    // Textadventure.g:1152:4: a2= '@' (a3= TEXT )
                    {
                    a2=(Token)match(input,15,FOLLOW_15_in_parse_textadventure_EItem682); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    				if (element == null) {
                    					element = textadventure.TextadventureFactory.eINSTANCE.createEItem();
                    					startIncompleteElement(element);
                    				}
                    				collectHiddenTokens(element);
                    				retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_2_0_0_2_0_0_0, null, true);
                    				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
                    			}

                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[37]);
                    			}

                    // Textadventure.g:1166:4: (a3= TEXT )
                    // Textadventure.g:1167:5: a3= TEXT
                    {
                    a3=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_textadventure_EItem708); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    					if (terminateParsing) {
                    						throw new textadventure.resource.textadventure.mopp.TextadventureTerminateParsingException();
                    					}
                    					if (element == null) {
                    						element = textadventure.TextadventureFactory.eINSTANCE.createEItem();
                    						startIncompleteElement(element);
                    					}
                    					if (a3 != null) {
                    						textadventure.resource.textadventure.ITextadventureTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
                    						tokenResolver.setOptions(getOptions());
                    						textadventure.resource.textadventure.ITextadventureTokenResolveResult result = getFreshTokenResolveResult();
                    						tokenResolver.resolve(a3.getText(), element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EITEM__LOCATION), result);
                    						Object resolvedObject = result.getResolvedToken();
                    						if (resolvedObject == null) {
                    							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a3).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a3).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a3).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a3).getStopIndex());
                    						}
                    						String resolved = (String) resolvedObject;
                    						textadventure.ETile proxy = textadventure.TextadventureFactory.eINSTANCE.createETile();
                    						collectHiddenTokens(element);
                    						registerContextDependentProxy(new textadventure.resource.textadventure.mopp.TextadventureContextDependentURIFragmentFactory<textadventure.EItem, textadventure.ETile>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getEItemLocationReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EITEM__LOCATION), resolved, proxy);
                    						if (proxy != null) {
                    							Object value = proxy;
                    							element.eSet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EITEM__LOCATION), value);
                    							completedElement(value, false);
                    						}
                    						collectHiddenTokens(element);
                    						retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_2_0_0_2_0_0_1, proxy, true);
                    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a3, element);
                    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a3, proxy);
                    					}
                    				}

                    }


                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[38]);
                    			}

                    }


                    }
                    break;

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[39]);
            	}

            a4=(Token)match(input,41,FOLLOW_41_in_parse_textadventure_EItem754); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = textadventure.TextadventureFactory.eINSTANCE.createEItem();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_2_0_0_3, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a4, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEItem(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[40]);
            		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[41]);
            	}

            // Textadventure.g:1228:2: ( ( (a5_0= parse_textadventure_EEffect ) ) )*
            loop8:
            do {
                int alt8=2;
                int LA8_0 = input.LA(1);

                if ( (LA8_0==38) ) {
                    alt8=1;
                }


                switch (alt8) {
            	case 1 :
            	    // Textadventure.g:1229:3: ( (a5_0= parse_textadventure_EEffect ) )
            	    {
            	    // Textadventure.g:1229:3: ( (a5_0= parse_textadventure_EEffect ) )
            	    // Textadventure.g:1230:4: (a5_0= parse_textadventure_EEffect )
            	    {
            	    // Textadventure.g:1230:4: (a5_0= parse_textadventure_EEffect )
            	    // Textadventure.g:1231:5: a5_0= parse_textadventure_EEffect
            	    {
            	    pushFollow(FOLLOW_parse_textadventure_EEffect_in_parse_textadventure_EItem783);
            	    a5_0=parse_textadventure_EEffect();

            	    state._fsp--;
            	    if (state.failed) return element;

            	    if ( state.backtracking==0 ) {
            	    					if (terminateParsing) {
            	    						throw new textadventure.resource.textadventure.mopp.TextadventureTerminateParsingException();
            	    					}
            	    					if (element == null) {
            	    						element = textadventure.TextadventureFactory.eINSTANCE.createEItem();
            	    						startIncompleteElement(element);
            	    					}
            	    					if (a5_0 != null) {
            	    						if (a5_0 != null) {
            	    							Object value = a5_0;
            	    							addObjectToList(element, textadventure.TextadventurePackage.EITEM__EFFECTS, value);
            	    							completedElement(value, true);
            	    						}
            	    						collectHiddenTokens(element);
            	    						retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_2_0_0_4_0_0_0, a5_0, true);
            	    						copyLocalizationInfos(a5_0, element);
            	    					}
            	    				}

            	    }


            	    if ( state.backtracking==0 ) {
            	    				// expected elements (follow set)
            	    				addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEItem(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[42]);
            	    				addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[43]);
            	    			}

            	    }


            	    }
            	    break;

            	default :
            	    break loop8;
                }
            } while (true);


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEItem(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[44]);
            		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[45]);
            	}

            a6=(Token)match(input,42,FOLLOW_42_in_parse_textadventure_EItem824); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = textadventure.TextadventureFactory.eINSTANCE.createEItem();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_2_0_0_5, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a6, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEWorld(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[46]);
            		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEWorld(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[47]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 4, parse_textadventure_EItem_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_textadventure_EItem"



    // $ANTLR start "parse_textadventure_ECheckPoint"
    // Textadventure.g:1282:1: parse_textadventure_ECheckPoint returns [textadventure.ECheckPoint element = null] : a0= 'checkpoint' a1= '@' (a2= TEXT ) ;
    public final textadventure.ECheckPoint parse_textadventure_ECheckPoint() throws RecognitionException {
        textadventure.ECheckPoint element =  null;

        int parse_textadventure_ECheckPoint_StartIndex = input.index();

        Token a0=null;
        Token a1=null;
        Token a2=null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 5) ) { return element; }

            // Textadventure.g:1285:2: (a0= 'checkpoint' a1= '@' (a2= TEXT ) )
            // Textadventure.g:1286:2: a0= 'checkpoint' a1= '@' (a2= TEXT )
            {
            a0=(Token)match(input,21,FOLLOW_21_in_parse_textadventure_ECheckPoint853); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = textadventure.TextadventureFactory.eINSTANCE.createECheckPoint();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_3_0_0_0, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[48]);
            	}

            a1=(Token)match(input,15,FOLLOW_15_in_parse_textadventure_ECheckPoint867); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = textadventure.TextadventureFactory.eINSTANCE.createECheckPoint();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_3_0_0_1, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[49]);
            	}

            // Textadventure.g:1314:2: (a2= TEXT )
            // Textadventure.g:1315:3: a2= TEXT
            {
            a2=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_textadventure_ECheckPoint885); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new textadventure.resource.textadventure.mopp.TextadventureTerminateParsingException();
            			}
            			if (element == null) {
            				element = textadventure.TextadventureFactory.eINSTANCE.createECheckPoint();
            				startIncompleteElement(element);
            			}
            			if (a2 != null) {
            				textadventure.resource.textadventure.ITextadventureTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
            				tokenResolver.setOptions(getOptions());
            				textadventure.resource.textadventure.ITextadventureTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a2.getText(), element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.ECHECK_POINT__POINT), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a2).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStopIndex());
            				}
            				String resolved = (String) resolvedObject;
            				textadventure.ETile proxy = textadventure.TextadventureFactory.eINSTANCE.createETile();
            				collectHiddenTokens(element);
            				registerContextDependentProxy(new textadventure.resource.textadventure.mopp.TextadventureContextDependentURIFragmentFactory<textadventure.ECheckPoint, textadventure.ETile>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getECheckPointPointReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.ECHECK_POINT__POINT), resolved, proxy);
            				if (proxy != null) {
            					Object value = proxy;
            					element.eSet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.ECHECK_POINT__POINT), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_3_0_0_2, proxy, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, element);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, proxy);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEWorld(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[50]);
            		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEWorld(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[51]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 5, parse_textadventure_ECheckPoint_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_textadventure_ECheckPoint"



    // $ANTLR start "parse_textadventure_EDestination"
    // Textadventure.g:1357:1: parse_textadventure_EDestination returns [textadventure.EDestination element = null] : a0= 'destination' a1= '@' (a2= TEXT ) ;
    public final textadventure.EDestination parse_textadventure_EDestination() throws RecognitionException {
        textadventure.EDestination element =  null;

        int parse_textadventure_EDestination_StartIndex = input.index();

        Token a0=null;
        Token a1=null;
        Token a2=null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 6) ) { return element; }

            // Textadventure.g:1360:2: (a0= 'destination' a1= '@' (a2= TEXT ) )
            // Textadventure.g:1361:2: a0= 'destination' a1= '@' (a2= TEXT )
            {
            a0=(Token)match(input,24,FOLLOW_24_in_parse_textadventure_EDestination921); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = textadventure.TextadventureFactory.eINSTANCE.createEDestination();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_4_0_0_0, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[52]);
            	}

            a1=(Token)match(input,15,FOLLOW_15_in_parse_textadventure_EDestination935); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = textadventure.TextadventureFactory.eINSTANCE.createEDestination();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_4_0_0_1, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[53]);
            	}

            // Textadventure.g:1389:2: (a2= TEXT )
            // Textadventure.g:1390:3: a2= TEXT
            {
            a2=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_textadventure_EDestination953); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new textadventure.resource.textadventure.mopp.TextadventureTerminateParsingException();
            			}
            			if (element == null) {
            				element = textadventure.TextadventureFactory.eINSTANCE.createEDestination();
            				startIncompleteElement(element);
            			}
            			if (a2 != null) {
            				textadventure.resource.textadventure.ITextadventureTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
            				tokenResolver.setOptions(getOptions());
            				textadventure.resource.textadventure.ITextadventureTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a2.getText(), element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EDESTINATION__POINT), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a2).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStopIndex());
            				}
            				String resolved = (String) resolvedObject;
            				textadventure.ETile proxy = textadventure.TextadventureFactory.eINSTANCE.createETile();
            				collectHiddenTokens(element);
            				registerContextDependentProxy(new textadventure.resource.textadventure.mopp.TextadventureContextDependentURIFragmentFactory<textadventure.EDestination, textadventure.ETile>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getEDestinationPointReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EDESTINATION__POINT), resolved, proxy);
            				if (proxy != null) {
            					Object value = proxy;
            					element.eSet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EDESTINATION__POINT), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_4_0_0_2, proxy, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, element);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, proxy);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEWorld(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[54]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 6, parse_textadventure_EDestination_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_textadventure_EDestination"



    // $ANTLR start "parse_textadventure_EBinding"
    // Textadventure.g:1431:1: parse_textadventure_EBinding returns [textadventure.EBinding element = null] : ( (a0= 'on' ) ) (a3= TEXT ) ;
    public final textadventure.EBinding parse_textadventure_EBinding() throws RecognitionException {
        textadventure.EBinding element =  null;

        int parse_textadventure_EBinding_StartIndex = input.index();

        Token a0=null;
        Token a3=null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 7) ) { return element; }

            // Textadventure.g:1434:2: ( ( (a0= 'on' ) ) (a3= TEXT ) )
            // Textadventure.g:1435:2: ( (a0= 'on' ) ) (a3= TEXT )
            {
            // Textadventure.g:1435:2: ( (a0= 'on' ) )
            // Textadventure.g:1436:3: (a0= 'on' )
            {
            // Textadventure.g:1436:3: (a0= 'on' )
            // Textadventure.g:1437:4: a0= 'on'
            {
            a0=(Token)match(input,31,FOLLOW_31_in_parse_textadventure_EBinding998); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            				if (element == null) {
            					element = textadventure.TextadventureFactory.eINSTANCE.createEBinding();
            					startIncompleteElement(element);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_5_0_0_0, null, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
            				// set value of enumeration attribute
            				Object value = textadventure.TextadventurePackage.eINSTANCE.getEParticipant().getEEnumLiteral(textadventure.EParticipant.ON_VALUE).getInstance();
            				element.eSet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EBINDING__PARTICIPANT), value);
            				completedElement(value, false);
            			}

            }


            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[55]);
            	}

            // Textadventure.g:1457:2: (a3= TEXT )
            // Textadventure.g:1458:3: a3= TEXT
            {
            a3=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_textadventure_EBinding1023); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new textadventure.resource.textadventure.mopp.TextadventureTerminateParsingException();
            			}
            			if (element == null) {
            				element = textadventure.TextadventureFactory.eINSTANCE.createEBinding();
            				startIncompleteElement(element);
            			}
            			if (a3 != null) {
            				textadventure.resource.textadventure.ITextadventureTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
            				tokenResolver.setOptions(getOptions());
            				textadventure.resource.textadventure.ITextadventureTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a3.getText(), element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EBINDING__ID), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a3).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a3).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a3).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a3).getStopIndex());
            				}
            				java.lang.String resolved = (java.lang.String) resolvedObject;
            				if (resolved != null) {
            					Object value = resolved;
            					element.eSet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EBINDING__ID), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_5_0_0_1, resolved, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a3, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[56]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 7, parse_textadventure_EBinding_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_textadventure_EBinding"



    // $ANTLR start "parse_textadventure_EWhileEffect"
    // Textadventure.g:1495:1: parse_textadventure_EWhileEffect returns [textadventure.EWhileEffect element = null] : a0= 'while' ( (a1= 'used' ) ) (a4_0= parse_textadventure_EBinding ) a5= '{' (a6_0= parse_textadventure_EAction ) a7= '}' ;
    public final textadventure.EWhileEffect parse_textadventure_EWhileEffect() throws RecognitionException {
        textadventure.EWhileEffect element =  null;

        int parse_textadventure_EWhileEffect_StartIndex = input.index();

        Token a0=null;
        Token a1=null;
        Token a5=null;
        Token a7=null;
        textadventure.EBinding a4_0 =null;

        textadventure.EAction a6_0 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 8) ) { return element; }

            // Textadventure.g:1498:2: (a0= 'while' ( (a1= 'used' ) ) (a4_0= parse_textadventure_EBinding ) a5= '{' (a6_0= parse_textadventure_EAction ) a7= '}' )
            // Textadventure.g:1499:2: a0= 'while' ( (a1= 'used' ) ) (a4_0= parse_textadventure_EBinding ) a5= '{' (a6_0= parse_textadventure_EAction ) a7= '}'
            {
            a0=(Token)match(input,38,FOLLOW_38_in_parse_textadventure_EWhileEffect1059); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = textadventure.TextadventureFactory.eINSTANCE.createEWhileEffect();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_6_0_0_0, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[57]);
            	}

            // Textadventure.g:1513:2: ( (a1= 'used' ) )
            // Textadventure.g:1514:3: (a1= 'used' )
            {
            // Textadventure.g:1514:3: (a1= 'used' )
            // Textadventure.g:1515:4: a1= 'used'
            {
            a1=(Token)match(input,37,FOLLOW_37_in_parse_textadventure_EWhileEffect1082); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            				if (element == null) {
            					element = textadventure.TextadventureFactory.eINSTANCE.createEWhileEffect();
            					startIncompleteElement(element);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_6_0_0_1, null, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
            				// set value of enumeration attribute
            				Object value = textadventure.TextadventurePackage.eINSTANCE.getECondition().getEEnumLiteral(textadventure.ECondition.USED_VALUE).getInstance();
            				element.eSet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EWHILE_EFFECT__CONDITION), value);
            				completedElement(value, false);
            			}

            }


            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEWhileEffect(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[58]);
            	}

            // Textadventure.g:1535:2: (a4_0= parse_textadventure_EBinding )
            // Textadventure.g:1536:3: a4_0= parse_textadventure_EBinding
            {
            pushFollow(FOLLOW_parse_textadventure_EBinding_in_parse_textadventure_EWhileEffect1107);
            a4_0=parse_textadventure_EBinding();

            state._fsp--;
            if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new textadventure.resource.textadventure.mopp.TextadventureTerminateParsingException();
            			}
            			if (element == null) {
            				element = textadventure.TextadventureFactory.eINSTANCE.createEWhileEffect();
            				startIncompleteElement(element);
            			}
            			if (a4_0 != null) {
            				if (a4_0 != null) {
            					Object value = a4_0;
            					element.eSet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EWHILE_EFFECT__BINDING), value);
            					completedElement(value, true);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_6_0_0_2, a4_0, true);
            				copyLocalizationInfos(a4_0, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[59]);
            	}

            a5=(Token)match(input,41,FOLLOW_41_in_parse_textadventure_EWhileEffect1125); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = textadventure.TextadventureFactory.eINSTANCE.createEWhileEffect();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_6_0_0_3, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a5, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEWhileEffect(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[60]);
            	}

            // Textadventure.g:1575:2: (a6_0= parse_textadventure_EAction )
            // Textadventure.g:1576:3: a6_0= parse_textadventure_EAction
            {
            pushFollow(FOLLOW_parse_textadventure_EAction_in_parse_textadventure_EWhileEffect1143);
            a6_0=parse_textadventure_EAction();

            state._fsp--;
            if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new textadventure.resource.textadventure.mopp.TextadventureTerminateParsingException();
            			}
            			if (element == null) {
            				element = textadventure.TextadventureFactory.eINSTANCE.createEWhileEffect();
            				startIncompleteElement(element);
            			}
            			if (a6_0 != null) {
            				if (a6_0 != null) {
            					Object value = a6_0;
            					addObjectToList(element, textadventure.TextadventurePackage.EWHILE_EFFECT__ACTION, value);
            					completedElement(value, true);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_6_0_0_4, a6_0, true);
            				copyLocalizationInfos(a6_0, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[61]);
            	}

            a7=(Token)match(input,42,FOLLOW_42_in_parse_textadventure_EWhileEffect1161); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = textadventure.TextadventureFactory.eINSTANCE.createEWhileEffect();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_6_0_0_5, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a7, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEItem(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[62]);
            		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[63]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 8, parse_textadventure_EWhileEffect_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_textadventure_EWhileEffect"



    // $ANTLR start "parse_textadventure_ERemoveAction"
    // Textadventure.g:1618:1: parse_textadventure_ERemoveAction returns [textadventure.ERemoveAction element = null] : a0= 'remove' (a1= TEXT ) a2= 'from' (a3_0= parse_textadventure_EListDescription ) ;
    public final textadventure.ERemoveAction parse_textadventure_ERemoveAction() throws RecognitionException {
        textadventure.ERemoveAction element =  null;

        int parse_textadventure_ERemoveAction_StartIndex = input.index();

        Token a0=null;
        Token a1=null;
        Token a2=null;
        textadventure.EListDescription a3_0 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 9) ) { return element; }

            // Textadventure.g:1621:2: (a0= 'remove' (a1= TEXT ) a2= 'from' (a3_0= parse_textadventure_EListDescription ) )
            // Textadventure.g:1622:2: a0= 'remove' (a1= TEXT ) a2= 'from' (a3_0= parse_textadventure_EListDescription )
            {
            a0=(Token)match(input,35,FOLLOW_35_in_parse_textadventure_ERemoveAction1190); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = textadventure.TextadventureFactory.eINSTANCE.createERemoveAction();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_7_0_0_0, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[64]);
            	}

            // Textadventure.g:1636:2: (a1= TEXT )
            // Textadventure.g:1637:3: a1= TEXT
            {
            a1=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_textadventure_ERemoveAction1208); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new textadventure.resource.textadventure.mopp.TextadventureTerminateParsingException();
            			}
            			if (element == null) {
            				element = textadventure.TextadventureFactory.eINSTANCE.createERemoveAction();
            				startIncompleteElement(element);
            			}
            			if (a1 != null) {
            				textadventure.resource.textadventure.ITextadventureTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
            				tokenResolver.setOptions(getOptions());
            				textadventure.resource.textadventure.ITextadventureTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EREMOVE_ACTION__WHAT), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a1).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStopIndex());
            				}
            				String resolved = (String) resolvedObject;
            				textadventure.EBinding proxy = textadventure.TextadventureFactory.eINSTANCE.createEBinding();
            				collectHiddenTokens(element);
            				registerContextDependentProxy(new textadventure.resource.textadventure.mopp.TextadventureContextDependentURIFragmentFactory<textadventure.ERemoveAction, textadventure.EBinding>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getERemoveActionWhatReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EREMOVE_ACTION__WHAT), resolved, proxy);
            				if (proxy != null) {
            					Object value = proxy;
            					element.eSet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EREMOVE_ACTION__WHAT), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_7_0_0_1, proxy, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a1, element);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a1, proxy);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[65]);
            	}

            a2=(Token)match(input,26,FOLLOW_26_in_parse_textadventure_ERemoveAction1229); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = textadventure.TextadventureFactory.eINSTANCE.createERemoveAction();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_7_0_0_2, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getERemoveAction(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[66]);
            	}

            // Textadventure.g:1690:2: (a3_0= parse_textadventure_EListDescription )
            // Textadventure.g:1691:3: a3_0= parse_textadventure_EListDescription
            {
            pushFollow(FOLLOW_parse_textadventure_EListDescription_in_parse_textadventure_ERemoveAction1247);
            a3_0=parse_textadventure_EListDescription();

            state._fsp--;
            if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new textadventure.resource.textadventure.mopp.TextadventureTerminateParsingException();
            			}
            			if (element == null) {
            				element = textadventure.TextadventureFactory.eINSTANCE.createERemoveAction();
            				startIncompleteElement(element);
            			}
            			if (a3_0 != null) {
            				if (a3_0 != null) {
            					Object value = a3_0;
            					element.eSet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EREMOVE_ACTION__FROM), value);
            					completedElement(value, true);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_7_0_0_3, a3_0, true);
            				copyLocalizationInfos(a3_0, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[67]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 9, parse_textadventure_ERemoveAction_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_textadventure_ERemoveAction"



    // $ANTLR start "parse_textadventure_EListDescription"
    // Textadventure.g:1718:1: parse_textadventure_EListDescription returns [textadventure.EListDescription element = null] : ( (a0= 'Tile' ) ) ( (a3= 'Persons' |a4= 'Items' ) ) ;
    public final textadventure.EListDescription parse_textadventure_EListDescription() throws RecognitionException {
        textadventure.EListDescription element =  null;

        int parse_textadventure_EListDescription_StartIndex = input.index();

        Token a0=null;
        Token a3=null;
        Token a4=null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 10) ) { return element; }

            // Textadventure.g:1721:2: ( ( (a0= 'Tile' ) ) ( (a3= 'Persons' |a4= 'Items' ) ) )
            // Textadventure.g:1722:2: ( (a0= 'Tile' ) ) ( (a3= 'Persons' |a4= 'Items' ) )
            {
            // Textadventure.g:1722:2: ( (a0= 'Tile' ) )
            // Textadventure.g:1723:3: (a0= 'Tile' )
            {
            // Textadventure.g:1723:3: (a0= 'Tile' )
            // Textadventure.g:1724:4: a0= 'Tile'
            {
            a0=(Token)match(input,18,FOLLOW_18_in_parse_textadventure_EListDescription1289); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            				if (element == null) {
            					element = textadventure.TextadventureFactory.eINSTANCE.createEListDescription();
            					startIncompleteElement(element);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_8_0_0_0, null, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
            				// set value of enumeration attribute
            				Object value = textadventure.TextadventurePackage.eINSTANCE.getETarget().getEEnumLiteral(textadventure.ETarget.TILE_VALUE).getInstance();
            				element.eSet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.ELIST_DESCRIPTION__TARGET), value);
            				completedElement(value, false);
            			}

            }


            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[68]);
            	}

            // Textadventure.g:1744:2: ( (a3= 'Persons' |a4= 'Items' ) )
            // Textadventure.g:1745:3: (a3= 'Persons' |a4= 'Items' )
            {
            // Textadventure.g:1745:3: (a3= 'Persons' |a4= 'Items' )
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==17) ) {
                alt9=1;
            }
            else if ( (LA9_0==16) ) {
                alt9=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 9, 0, input);

                throw nvae;

            }
            switch (alt9) {
                case 1 :
                    // Textadventure.g:1746:4: a3= 'Persons'
                    {
                    a3=(Token)match(input,17,FOLLOW_17_in_parse_textadventure_EListDescription1319); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    				if (element == null) {
                    					element = textadventure.TextadventureFactory.eINSTANCE.createEListDescription();
                    					startIncompleteElement(element);
                    				}
                    				collectHiddenTokens(element);
                    				retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_8_0_0_1, null, true);
                    				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a3, element);
                    				// set value of enumeration attribute
                    				Object value = textadventure.TextadventurePackage.eINSTANCE.getEProperty().getEEnumLiteral(textadventure.EProperty.PERSONS_VALUE).getInstance();
                    				element.eSet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.ELIST_DESCRIPTION__PROPERTY), value);
                    				completedElement(value, false);
                    			}

                    }
                    break;
                case 2 :
                    // Textadventure.g:1759:8: a4= 'Items'
                    {
                    a4=(Token)match(input,16,FOLLOW_16_in_parse_textadventure_EListDescription1334); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    				if (element == null) {
                    					element = textadventure.TextadventureFactory.eINSTANCE.createEListDescription();
                    					startIncompleteElement(element);
                    				}
                    				collectHiddenTokens(element);
                    				retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_8_0_0_1, null, true);
                    				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a4, element);
                    				// set value of enumeration attribute
                    				Object value = textadventure.TextadventurePackage.eINSTANCE.getEProperty().getEEnumLiteral(textadventure.EProperty.ITEMS_VALUE).getInstance();
                    				element.eSet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.ELIST_DESCRIPTION__PROPERTY), value);
                    				completedElement(value, false);
                    			}

                    }
                    break;

            }


            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[69]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 10, parse_textadventure_EListDescription_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_textadventure_EListDescription"



    // $ANTLR start "parse_textadventure_EPerson"
    // Textadventure.g:1781:1: parse_textadventure_EPerson returns [textadventure.EPerson element = null] : a0= 'person' (a1= TEXT ) a2= '@' (a3= TEXT ) a4= '{' ( (a5= 'hp' a6= '=' (a7= INTEGER ) |a8= 'defense' a9= '=' (a10= INTEGER ) |a11= 'attack' a12= '=' (a13= INTEGER ) ) )* ( (a14_0= parse_textadventure_EAbility ) )* a15= '}' ;
    public final textadventure.EPerson parse_textadventure_EPerson() throws RecognitionException {
        textadventure.EPerson element =  null;

        int parse_textadventure_EPerson_StartIndex = input.index();

        Token a0=null;
        Token a1=null;
        Token a2=null;
        Token a3=null;
        Token a4=null;
        Token a5=null;
        Token a6=null;
        Token a7=null;
        Token a8=null;
        Token a9=null;
        Token a10=null;
        Token a11=null;
        Token a12=null;
        Token a13=null;
        Token a15=null;
        textadventure.EAbility a14_0 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 11) ) { return element; }

            // Textadventure.g:1784:2: (a0= 'person' (a1= TEXT ) a2= '@' (a3= TEXT ) a4= '{' ( (a5= 'hp' a6= '=' (a7= INTEGER ) |a8= 'defense' a9= '=' (a10= INTEGER ) |a11= 'attack' a12= '=' (a13= INTEGER ) ) )* ( (a14_0= parse_textadventure_EAbility ) )* a15= '}' )
            // Textadventure.g:1785:2: a0= 'person' (a1= TEXT ) a2= '@' (a3= TEXT ) a4= '{' ( (a5= 'hp' a6= '=' (a7= INTEGER ) |a8= 'defense' a9= '=' (a10= INTEGER ) |a11= 'attack' a12= '=' (a13= INTEGER ) ) )* ( (a14_0= parse_textadventure_EAbility ) )* a15= '}'
            {
            a0=(Token)match(input,32,FOLLOW_32_in_parse_textadventure_EPerson1370); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = textadventure.TextadventureFactory.eINSTANCE.createEPerson();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_9_0_0_0, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[70]);
            	}

            // Textadventure.g:1799:2: (a1= TEXT )
            // Textadventure.g:1800:3: a1= TEXT
            {
            a1=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_textadventure_EPerson1388); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new textadventure.resource.textadventure.mopp.TextadventureTerminateParsingException();
            			}
            			if (element == null) {
            				element = textadventure.TextadventureFactory.eINSTANCE.createEPerson();
            				startIncompleteElement(element);
            			}
            			if (a1 != null) {
            				textadventure.resource.textadventure.ITextadventureTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
            				tokenResolver.setOptions(getOptions());
            				textadventure.resource.textadventure.ITextadventureTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EPERSON__NAME), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a1).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStopIndex());
            				}
            				java.lang.String resolved = (java.lang.String) resolvedObject;
            				if (resolved != null) {
            					Object value = resolved;
            					element.eSet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EPERSON__NAME), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_9_0_0_1, resolved, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a1, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[71]);
            	}

            a2=(Token)match(input,15,FOLLOW_15_in_parse_textadventure_EPerson1409); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = textadventure.TextadventureFactory.eINSTANCE.createEPerson();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_9_0_0_2, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[72]);
            	}

            // Textadventure.g:1849:2: (a3= TEXT )
            // Textadventure.g:1850:3: a3= TEXT
            {
            a3=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_textadventure_EPerson1427); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new textadventure.resource.textadventure.mopp.TextadventureTerminateParsingException();
            			}
            			if (element == null) {
            				element = textadventure.TextadventureFactory.eINSTANCE.createEPerson();
            				startIncompleteElement(element);
            			}
            			if (a3 != null) {
            				textadventure.resource.textadventure.ITextadventureTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
            				tokenResolver.setOptions(getOptions());
            				textadventure.resource.textadventure.ITextadventureTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a3.getText(), element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EPERSON__LOCATION), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a3).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a3).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a3).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a3).getStopIndex());
            				}
            				String resolved = (String) resolvedObject;
            				textadventure.ETile proxy = textadventure.TextadventureFactory.eINSTANCE.createETile();
            				collectHiddenTokens(element);
            				registerContextDependentProxy(new textadventure.resource.textadventure.mopp.TextadventureContextDependentURIFragmentFactory<textadventure.EPerson, textadventure.ETile>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getEPersonLocationReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EPERSON__LOCATION), resolved, proxy);
            				if (proxy != null) {
            					Object value = proxy;
            					element.eSet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EPERSON__LOCATION), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_9_0_0_3, proxy, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a3, element);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a3, proxy);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[73]);
            	}

            a4=(Token)match(input,41,FOLLOW_41_in_parse_textadventure_EPerson1448); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = textadventure.TextadventureFactory.eINSTANCE.createEPerson();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_9_0_0_4, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a4, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[74]);
            		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[75]);
            		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[76]);
            		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEPerson(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[77]);
            		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[78]);
            	}

            // Textadventure.g:1907:2: ( (a5= 'hp' a6= '=' (a7= INTEGER ) |a8= 'defense' a9= '=' (a10= INTEGER ) |a11= 'attack' a12= '=' (a13= INTEGER ) ) )*
            loop11:
            do {
                int alt11=2;
                int LA11_0 = input.LA(1);

                if ( (LA11_0==19||LA11_0==22||LA11_0==28) ) {
                    alt11=1;
                }


                switch (alt11) {
            	case 1 :
            	    // Textadventure.g:1908:3: (a5= 'hp' a6= '=' (a7= INTEGER ) |a8= 'defense' a9= '=' (a10= INTEGER ) |a11= 'attack' a12= '=' (a13= INTEGER ) )
            	    {
            	    // Textadventure.g:1908:3: (a5= 'hp' a6= '=' (a7= INTEGER ) |a8= 'defense' a9= '=' (a10= INTEGER ) |a11= 'attack' a12= '=' (a13= INTEGER ) )
            	    int alt10=3;
            	    switch ( input.LA(1) ) {
            	    case 28:
            	        {
            	        alt10=1;
            	        }
            	        break;
            	    case 22:
            	        {
            	        alt10=2;
            	        }
            	        break;
            	    case 19:
            	        {
            	        alt10=3;
            	        }
            	        break;
            	    default:
            	        if (state.backtracking>0) {state.failed=true; return element;}
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 10, 0, input);

            	        throw nvae;

            	    }

            	    switch (alt10) {
            	        case 1 :
            	            // Textadventure.g:1909:4: a5= 'hp' a6= '=' (a7= INTEGER )
            	            {
            	            a5=(Token)match(input,28,FOLLOW_28_in_parse_textadventure_EPerson1471); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            				if (element == null) {
            	            					element = textadventure.TextadventureFactory.eINSTANCE.createEPerson();
            	            					startIncompleteElement(element);
            	            				}
            	            				collectHiddenTokens(element);
            	            				retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_9_0_0_5_0_0_0, null, true);
            	            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a5, element);
            	            			}

            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[79]);
            	            			}

            	            a6=(Token)match(input,14,FOLLOW_14_in_parse_textadventure_EPerson1491); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            				if (element == null) {
            	            					element = textadventure.TextadventureFactory.eINSTANCE.createEPerson();
            	            					startIncompleteElement(element);
            	            				}
            	            				collectHiddenTokens(element);
            	            				retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_9_0_0_5_0_0_1, null, true);
            	            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a6, element);
            	            			}

            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[80]);
            	            			}

            	            // Textadventure.g:1937:4: (a7= INTEGER )
            	            // Textadventure.g:1938:5: a7= INTEGER
            	            {
            	            a7=(Token)match(input,INTEGER,FOLLOW_INTEGER_in_parse_textadventure_EPerson1517); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            					if (terminateParsing) {
            	            						throw new textadventure.resource.textadventure.mopp.TextadventureTerminateParsingException();
            	            					}
            	            					if (element == null) {
            	            						element = textadventure.TextadventureFactory.eINSTANCE.createEPerson();
            	            						startIncompleteElement(element);
            	            					}
            	            					if (a7 != null) {
            	            						textadventure.resource.textadventure.ITextadventureTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("INTEGER");
            	            						tokenResolver.setOptions(getOptions());
            	            						textadventure.resource.textadventure.ITextadventureTokenResolveResult result = getFreshTokenResolveResult();
            	            						tokenResolver.resolve(a7.getText(), element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EPERSON__HP), result);
            	            						Object resolvedObject = result.getResolvedToken();
            	            						if (resolvedObject == null) {
            	            							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a7).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a7).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a7).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a7).getStopIndex());
            	            						}
            	            						java.lang.Integer resolved = (java.lang.Integer) resolvedObject;
            	            						if (resolved != null) {
            	            							Object value = resolved;
            	            							element.eSet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EPERSON__HP), value);
            	            							completedElement(value, false);
            	            						}
            	            						collectHiddenTokens(element);
            	            						retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_9_0_0_5_0_0_2, resolved, true);
            	            						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a7, element);
            	            					}
            	            				}

            	            }


            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[81]);
            	            				addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[82]);
            	            				addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[83]);
            	            				addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEPerson(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[84]);
            	            				addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[85]);
            	            			}

            	            }
            	            break;
            	        case 2 :
            	            // Textadventure.g:1978:8: a8= 'defense' a9= '=' (a10= INTEGER )
            	            {
            	            a8=(Token)match(input,22,FOLLOW_22_in_parse_textadventure_EPerson1558); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            				if (element == null) {
            	            					element = textadventure.TextadventureFactory.eINSTANCE.createEPerson();
            	            					startIncompleteElement(element);
            	            				}
            	            				collectHiddenTokens(element);
            	            				retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_9_0_0_5_0_1_0, null, true);
            	            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a8, element);
            	            			}

            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[86]);
            	            			}

            	            a9=(Token)match(input,14,FOLLOW_14_in_parse_textadventure_EPerson1578); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            				if (element == null) {
            	            					element = textadventure.TextadventureFactory.eINSTANCE.createEPerson();
            	            					startIncompleteElement(element);
            	            				}
            	            				collectHiddenTokens(element);
            	            				retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_9_0_0_5_0_1_1, null, true);
            	            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a9, element);
            	            			}

            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[87]);
            	            			}

            	            // Textadventure.g:2006:4: (a10= INTEGER )
            	            // Textadventure.g:2007:5: a10= INTEGER
            	            {
            	            a10=(Token)match(input,INTEGER,FOLLOW_INTEGER_in_parse_textadventure_EPerson1604); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            					if (terminateParsing) {
            	            						throw new textadventure.resource.textadventure.mopp.TextadventureTerminateParsingException();
            	            					}
            	            					if (element == null) {
            	            						element = textadventure.TextadventureFactory.eINSTANCE.createEPerson();
            	            						startIncompleteElement(element);
            	            					}
            	            					if (a10 != null) {
            	            						textadventure.resource.textadventure.ITextadventureTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("INTEGER");
            	            						tokenResolver.setOptions(getOptions());
            	            						textadventure.resource.textadventure.ITextadventureTokenResolveResult result = getFreshTokenResolveResult();
            	            						tokenResolver.resolve(a10.getText(), element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EPERSON__DEFENSE), result);
            	            						Object resolvedObject = result.getResolvedToken();
            	            						if (resolvedObject == null) {
            	            							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a10).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a10).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a10).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a10).getStopIndex());
            	            						}
            	            						java.lang.Integer resolved = (java.lang.Integer) resolvedObject;
            	            						if (resolved != null) {
            	            							Object value = resolved;
            	            							element.eSet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EPERSON__DEFENSE), value);
            	            							completedElement(value, false);
            	            						}
            	            						collectHiddenTokens(element);
            	            						retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_9_0_0_5_0_1_2, resolved, true);
            	            						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a10, element);
            	            					}
            	            				}

            	            }


            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[88]);
            	            				addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[89]);
            	            				addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[90]);
            	            				addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEPerson(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[91]);
            	            				addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[92]);
            	            			}

            	            }
            	            break;
            	        case 3 :
            	            // Textadventure.g:2047:8: a11= 'attack' a12= '=' (a13= INTEGER )
            	            {
            	            a11=(Token)match(input,19,FOLLOW_19_in_parse_textadventure_EPerson1645); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            				if (element == null) {
            	            					element = textadventure.TextadventureFactory.eINSTANCE.createEPerson();
            	            					startIncompleteElement(element);
            	            				}
            	            				collectHiddenTokens(element);
            	            				retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_9_0_0_5_0_2_0, null, true);
            	            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a11, element);
            	            			}

            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[93]);
            	            			}

            	            a12=(Token)match(input,14,FOLLOW_14_in_parse_textadventure_EPerson1665); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            				if (element == null) {
            	            					element = textadventure.TextadventureFactory.eINSTANCE.createEPerson();
            	            					startIncompleteElement(element);
            	            				}
            	            				collectHiddenTokens(element);
            	            				retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_9_0_0_5_0_2_1, null, true);
            	            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a12, element);
            	            			}

            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[94]);
            	            			}

            	            // Textadventure.g:2075:4: (a13= INTEGER )
            	            // Textadventure.g:2076:5: a13= INTEGER
            	            {
            	            a13=(Token)match(input,INTEGER,FOLLOW_INTEGER_in_parse_textadventure_EPerson1691); if (state.failed) return element;

            	            if ( state.backtracking==0 ) {
            	            					if (terminateParsing) {
            	            						throw new textadventure.resource.textadventure.mopp.TextadventureTerminateParsingException();
            	            					}
            	            					if (element == null) {
            	            						element = textadventure.TextadventureFactory.eINSTANCE.createEPerson();
            	            						startIncompleteElement(element);
            	            					}
            	            					if (a13 != null) {
            	            						textadventure.resource.textadventure.ITextadventureTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("INTEGER");
            	            						tokenResolver.setOptions(getOptions());
            	            						textadventure.resource.textadventure.ITextadventureTokenResolveResult result = getFreshTokenResolveResult();
            	            						tokenResolver.resolve(a13.getText(), element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EPERSON__ATTACK), result);
            	            						Object resolvedObject = result.getResolvedToken();
            	            						if (resolvedObject == null) {
            	            							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a13).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a13).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a13).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a13).getStopIndex());
            	            						}
            	            						java.lang.Integer resolved = (java.lang.Integer) resolvedObject;
            	            						if (resolved != null) {
            	            							Object value = resolved;
            	            							element.eSet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EPERSON__ATTACK), value);
            	            							completedElement(value, false);
            	            						}
            	            						collectHiddenTokens(element);
            	            						retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_9_0_0_5_0_2_2, resolved, true);
            	            						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a13, element);
            	            					}
            	            				}

            	            }


            	            if ( state.backtracking==0 ) {
            	            				// expected elements (follow set)
            	            				addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[95]);
            	            				addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[96]);
            	            				addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[97]);
            	            				addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEPerson(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[98]);
            	            				addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[99]);
            	            			}

            	            }
            	            break;

            	    }


            	    }
            	    break;

            	default :
            	    break loop11;
                }
            } while (true);


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[100]);
            		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[101]);
            		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[102]);
            		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEPerson(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[103]);
            		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[104]);
            	}

            // Textadventure.g:2126:2: ( (a14_0= parse_textadventure_EAbility ) )*
            loop12:
            do {
                int alt12=2;
                int LA12_0 = input.LA(1);

                if ( (LA12_0==29) ) {
                    alt12=1;
                }


                switch (alt12) {
            	case 1 :
            	    // Textadventure.g:2127:3: (a14_0= parse_textadventure_EAbility )
            	    {
            	    // Textadventure.g:2127:3: (a14_0= parse_textadventure_EAbility )
            	    // Textadventure.g:2128:4: a14_0= parse_textadventure_EAbility
            	    {
            	    pushFollow(FOLLOW_parse_textadventure_EAbility_in_parse_textadventure_EPerson1746);
            	    a14_0=parse_textadventure_EAbility();

            	    state._fsp--;
            	    if (state.failed) return element;

            	    if ( state.backtracking==0 ) {
            	    				if (terminateParsing) {
            	    					throw new textadventure.resource.textadventure.mopp.TextadventureTerminateParsingException();
            	    				}
            	    				if (element == null) {
            	    					element = textadventure.TextadventureFactory.eINSTANCE.createEPerson();
            	    					startIncompleteElement(element);
            	    				}
            	    				if (a14_0 != null) {
            	    					if (a14_0 != null) {
            	    						Object value = a14_0;
            	    						addObjectToList(element, textadventure.TextadventurePackage.EPERSON__ABILITIES, value);
            	    						completedElement(value, true);
            	    					}
            	    					collectHiddenTokens(element);
            	    					retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_9_0_0_6, a14_0, true);
            	    					copyLocalizationInfos(a14_0, element);
            	    				}
            	    			}

            	    }


            	    }
            	    break;

            	default :
            	    break loop12;
                }
            } while (true);


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEPerson(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[105]);
            		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[106]);
            	}

            a15=(Token)match(input,42,FOLLOW_42_in_parse_textadventure_EPerson1772); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = textadventure.TextadventureFactory.eINSTANCE.createEPerson();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_9_0_0_7, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a15, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEWorld(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[107]);
            		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEWorld(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[108]);
            		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEWorld(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[109]);
            		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEWorld(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[110]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 11, parse_textadventure_EPerson_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_textadventure_EPerson"



    // $ANTLR start "parse_textadventure_EBattle"
    // Textadventure.g:2174:1: parse_textadventure_EBattle returns [textadventure.EBattle element = null] : a0= 'battle' a1= '@' (a2= TEXT ) a3= '{' a4= 'enemy' a5= '=' (a6= TEXT ) ( (a7_0= parse_textadventure_EBattleCustomization ) )* a8= '}' ;
    public final textadventure.EBattle parse_textadventure_EBattle() throws RecognitionException {
        textadventure.EBattle element =  null;

        int parse_textadventure_EBattle_StartIndex = input.index();

        Token a0=null;
        Token a1=null;
        Token a2=null;
        Token a3=null;
        Token a4=null;
        Token a5=null;
        Token a6=null;
        Token a8=null;
        textadventure.EBattleCustomization a7_0 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 12) ) { return element; }

            // Textadventure.g:2177:2: (a0= 'battle' a1= '@' (a2= TEXT ) a3= '{' a4= 'enemy' a5= '=' (a6= TEXT ) ( (a7_0= parse_textadventure_EBattleCustomization ) )* a8= '}' )
            // Textadventure.g:2178:2: a0= 'battle' a1= '@' (a2= TEXT ) a3= '{' a4= 'enemy' a5= '=' (a6= TEXT ) ( (a7_0= parse_textadventure_EBattleCustomization ) )* a8= '}'
            {
            a0=(Token)match(input,20,FOLLOW_20_in_parse_textadventure_EBattle1801); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = textadventure.TextadventureFactory.eINSTANCE.createEBattle();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_10_0_0_0, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[111]);
            	}

            a1=(Token)match(input,15,FOLLOW_15_in_parse_textadventure_EBattle1815); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = textadventure.TextadventureFactory.eINSTANCE.createEBattle();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_10_0_0_1, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[112]);
            	}

            // Textadventure.g:2206:2: (a2= TEXT )
            // Textadventure.g:2207:3: a2= TEXT
            {
            a2=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_textadventure_EBattle1833); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new textadventure.resource.textadventure.mopp.TextadventureTerminateParsingException();
            			}
            			if (element == null) {
            				element = textadventure.TextadventureFactory.eINSTANCE.createEBattle();
            				startIncompleteElement(element);
            			}
            			if (a2 != null) {
            				textadventure.resource.textadventure.ITextadventureTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
            				tokenResolver.setOptions(getOptions());
            				textadventure.resource.textadventure.ITextadventureTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a2.getText(), element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EBATTLE__LOCATION), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a2).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStopIndex());
            				}
            				String resolved = (String) resolvedObject;
            				textadventure.ETile proxy = textadventure.TextadventureFactory.eINSTANCE.createETile();
            				collectHiddenTokens(element);
            				registerContextDependentProxy(new textadventure.resource.textadventure.mopp.TextadventureContextDependentURIFragmentFactory<textadventure.EBattle, textadventure.ETile>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getEBattleLocationReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EBATTLE__LOCATION), resolved, proxy);
            				if (proxy != null) {
            					Object value = proxy;
            					element.eSet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EBATTLE__LOCATION), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_10_0_0_2, proxy, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, element);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, proxy);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[113]);
            	}

            a3=(Token)match(input,41,FOLLOW_41_in_parse_textadventure_EBattle1854); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = textadventure.TextadventureFactory.eINSTANCE.createEBattle();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_10_0_0_3, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a3, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[114]);
            	}

            a4=(Token)match(input,25,FOLLOW_25_in_parse_textadventure_EBattle1868); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = textadventure.TextadventureFactory.eINSTANCE.createEBattle();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_10_0_0_4, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a4, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[115]);
            	}

            a5=(Token)match(input,14,FOLLOW_14_in_parse_textadventure_EBattle1882); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = textadventure.TextadventureFactory.eINSTANCE.createEBattle();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_10_0_0_5, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a5, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[116]);
            	}

            // Textadventure.g:2288:2: (a6= TEXT )
            // Textadventure.g:2289:3: a6= TEXT
            {
            a6=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_textadventure_EBattle1900); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new textadventure.resource.textadventure.mopp.TextadventureTerminateParsingException();
            			}
            			if (element == null) {
            				element = textadventure.TextadventureFactory.eINSTANCE.createEBattle();
            				startIncompleteElement(element);
            			}
            			if (a6 != null) {
            				textadventure.resource.textadventure.ITextadventureTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
            				tokenResolver.setOptions(getOptions());
            				textadventure.resource.textadventure.ITextadventureTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a6.getText(), element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EBATTLE__ENEMY), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a6).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a6).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a6).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a6).getStopIndex());
            				}
            				String resolved = (String) resolvedObject;
            				textadventure.EPerson proxy = textadventure.TextadventureFactory.eINSTANCE.createEPerson();
            				collectHiddenTokens(element);
            				registerContextDependentProxy(new textadventure.resource.textadventure.mopp.TextadventureContextDependentURIFragmentFactory<textadventure.EBattle, textadventure.EPerson>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getEBattleEnemyReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EBATTLE__ENEMY), resolved, proxy);
            				if (proxy != null) {
            					Object value = proxy;
            					element.eSet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EBATTLE__ENEMY), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_10_0_0_6, proxy, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a6, element);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a6, proxy);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEBattle(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[117]);
            		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[118]);
            	}

            // Textadventure.g:2329:2: ( (a7_0= parse_textadventure_EBattleCustomization ) )*
            loop13:
            do {
                int alt13=2;
                int LA13_0 = input.LA(1);

                if ( (LA13_0==TEXT) ) {
                    alt13=1;
                }


                switch (alt13) {
            	case 1 :
            	    // Textadventure.g:2330:3: (a7_0= parse_textadventure_EBattleCustomization )
            	    {
            	    // Textadventure.g:2330:3: (a7_0= parse_textadventure_EBattleCustomization )
            	    // Textadventure.g:2331:4: a7_0= parse_textadventure_EBattleCustomization
            	    {
            	    pushFollow(FOLLOW_parse_textadventure_EBattleCustomization_in_parse_textadventure_EBattle1930);
            	    a7_0=parse_textadventure_EBattleCustomization();

            	    state._fsp--;
            	    if (state.failed) return element;

            	    if ( state.backtracking==0 ) {
            	    				if (terminateParsing) {
            	    					throw new textadventure.resource.textadventure.mopp.TextadventureTerminateParsingException();
            	    				}
            	    				if (element == null) {
            	    					element = textadventure.TextadventureFactory.eINSTANCE.createEBattle();
            	    					startIncompleteElement(element);
            	    				}
            	    				if (a7_0 != null) {
            	    					if (a7_0 != null) {
            	    						Object value = a7_0;
            	    						addObjectToList(element, textadventure.TextadventurePackage.EBATTLE__LOGICS, value);
            	    						completedElement(value, true);
            	    					}
            	    					collectHiddenTokens(element);
            	    					retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_10_0_0_7, a7_0, true);
            	    					copyLocalizationInfos(a7_0, element);
            	    				}
            	    			}

            	    }


            	    }
            	    break;

            	default :
            	    break loop13;
                }
            } while (true);


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEBattle(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[119]);
            		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[120]);
            	}

            a8=(Token)match(input,42,FOLLOW_42_in_parse_textadventure_EBattle1956); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = textadventure.TextadventureFactory.eINSTANCE.createEBattle();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_10_0_0_8, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a8, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEWorld(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[121]);
            		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEWorld(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[122]);
            		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEWorld(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[123]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 12, parse_textadventure_EBattle_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_textadventure_EBattle"



    // $ANTLR start "parse_textadventure_EBattleCustomization"
    // Textadventure.g:2376:1: parse_textadventure_EBattleCustomization returns [textadventure.EBattleCustomization element = null] : (a0= TEXT ) a1= '=' (a2_0= parse_textadventure_EFunction ) ;
    public final textadventure.EBattleCustomization parse_textadventure_EBattleCustomization() throws RecognitionException {
        textadventure.EBattleCustomization element =  null;

        int parse_textadventure_EBattleCustomization_StartIndex = input.index();

        Token a0=null;
        Token a1=null;
        textadventure.EFunction a2_0 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 13) ) { return element; }

            // Textadventure.g:2379:2: ( (a0= TEXT ) a1= '=' (a2_0= parse_textadventure_EFunction ) )
            // Textadventure.g:2380:2: (a0= TEXT ) a1= '=' (a2_0= parse_textadventure_EFunction )
            {
            // Textadventure.g:2380:2: (a0= TEXT )
            // Textadventure.g:2381:3: a0= TEXT
            {
            a0=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_textadventure_EBattleCustomization1989); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new textadventure.resource.textadventure.mopp.TextadventureTerminateParsingException();
            			}
            			if (element == null) {
            				element = textadventure.TextadventureFactory.eINSTANCE.createEBattleCustomization();
            				startIncompleteElement(element);
            			}
            			if (a0 != null) {
            				textadventure.resource.textadventure.ITextadventureTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
            				tokenResolver.setOptions(getOptions());
            				textadventure.resource.textadventure.ITextadventureTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a0.getText(), element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EBATTLE_CUSTOMIZATION__WHEN), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a0).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStopIndex());
            				}
            				textadventure.EBattleWhen resolved = (textadventure.EBattleWhen) resolvedObject;
            				if (resolved != null) {
            					Object value = resolved;
            					element.eSet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EBATTLE_CUSTOMIZATION__WHEN), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_11_0_0_0, resolved, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a0, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[124]);
            	}

            a1=(Token)match(input,14,FOLLOW_14_in_parse_textadventure_EBattleCustomization2010); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = textadventure.TextadventureFactory.eINSTANCE.createEBattleCustomization();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_11_0_0_1, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEBattleCustomization(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[125]);
            	}

            // Textadventure.g:2430:2: (a2_0= parse_textadventure_EFunction )
            // Textadventure.g:2431:3: a2_0= parse_textadventure_EFunction
            {
            pushFollow(FOLLOW_parse_textadventure_EFunction_in_parse_textadventure_EBattleCustomization2028);
            a2_0=parse_textadventure_EFunction();

            state._fsp--;
            if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new textadventure.resource.textadventure.mopp.TextadventureTerminateParsingException();
            			}
            			if (element == null) {
            				element = textadventure.TextadventureFactory.eINSTANCE.createEBattleCustomization();
            				startIncompleteElement(element);
            			}
            			if (a2_0 != null) {
            				if (a2_0 != null) {
            					Object value = a2_0;
            					element.eSet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EBATTLE_CUSTOMIZATION__FUNC), value);
            					completedElement(value, true);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_11_0_0_2, a2_0, true);
            				copyLocalizationInfos(a2_0, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEBattle(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[126]);
            		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[127]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 13, parse_textadventure_EBattleCustomization_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_textadventure_EBattleCustomization"



    // $ANTLR start "parse_textadventure_EFunction"
    // Textadventure.g:2459:1: parse_textadventure_EFunction returns [textadventure.EFunction element = null] : a0= 'function(' ( ( (a1= TEXT ) ( (a2= ',' (a3= TEXT ) ) )* ) )? a4= ')' a5= '{' (a6_0= parse_textadventure_EFunctionBody ) a7= '}' ;
    public final textadventure.EFunction parse_textadventure_EFunction() throws RecognitionException {
        textadventure.EFunction element =  null;

        int parse_textadventure_EFunction_StartIndex = input.index();

        Token a0=null;
        Token a1=null;
        Token a2=null;
        Token a3=null;
        Token a4=null;
        Token a5=null;
        Token a7=null;
        textadventure.EFunctionBody a6_0 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 14) ) { return element; }

            // Textadventure.g:2462:2: (a0= 'function(' ( ( (a1= TEXT ) ( (a2= ',' (a3= TEXT ) ) )* ) )? a4= ')' a5= '{' (a6_0= parse_textadventure_EFunctionBody ) a7= '}' )
            // Textadventure.g:2463:2: a0= 'function(' ( ( (a1= TEXT ) ( (a2= ',' (a3= TEXT ) ) )* ) )? a4= ')' a5= '{' (a6_0= parse_textadventure_EFunctionBody ) a7= '}'
            {
            a0=(Token)match(input,27,FOLLOW_27_in_parse_textadventure_EFunction2061); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = textadventure.TextadventureFactory.eINSTANCE.createEFunction();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_12_0_0_0, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[128]);
            		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[129]);
            	}

            // Textadventure.g:2478:2: ( ( (a1= TEXT ) ( (a2= ',' (a3= TEXT ) ) )* ) )?
            int alt15=2;
            int LA15_0 = input.LA(1);

            if ( (LA15_0==TEXT) ) {
                alt15=1;
            }
            switch (alt15) {
                case 1 :
                    // Textadventure.g:2479:3: ( (a1= TEXT ) ( (a2= ',' (a3= TEXT ) ) )* )
                    {
                    // Textadventure.g:2479:3: ( (a1= TEXT ) ( (a2= ',' (a3= TEXT ) ) )* )
                    // Textadventure.g:2480:4: (a1= TEXT ) ( (a2= ',' (a3= TEXT ) ) )*
                    {
                    // Textadventure.g:2480:4: (a1= TEXT )
                    // Textadventure.g:2481:5: a1= TEXT
                    {
                    a1=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_textadventure_EFunction2090); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    					if (terminateParsing) {
                    						throw new textadventure.resource.textadventure.mopp.TextadventureTerminateParsingException();
                    					}
                    					if (element == null) {
                    						element = textadventure.TextadventureFactory.eINSTANCE.createEFunction();
                    						startIncompleteElement(element);
                    					}
                    					if (a1 != null) {
                    						textadventure.resource.textadventure.ITextadventureTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
                    						tokenResolver.setOptions(getOptions());
                    						textadventure.resource.textadventure.ITextadventureTokenResolveResult result = getFreshTokenResolveResult();
                    						tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EFUNCTION__PARAMS), result);
                    						Object resolvedObject = result.getResolvedToken();
                    						if (resolvedObject == null) {
                    							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a1).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStopIndex());
                    						}
                    						java.lang.String resolved = (java.lang.String) resolvedObject;
                    						if (resolved != null) {
                    							Object value = resolved;
                    							addObjectToList(element, textadventure.TextadventurePackage.EFUNCTION__PARAMS, value);
                    							completedElement(value, false);
                    						}
                    						collectHiddenTokens(element);
                    						retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_12_0_0_1_0_0_0, resolved, true);
                    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a1, element);
                    					}
                    				}

                    }


                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[130]);
                    				addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[131]);
                    			}

                    // Textadventure.g:2517:4: ( (a2= ',' (a3= TEXT ) ) )*
                    loop14:
                    do {
                        int alt14=2;
                        int LA14_0 = input.LA(1);

                        if ( (LA14_0==12) ) {
                            alt14=1;
                        }


                        switch (alt14) {
                    	case 1 :
                    	    // Textadventure.g:2518:5: (a2= ',' (a3= TEXT ) )
                    	    {
                    	    // Textadventure.g:2518:5: (a2= ',' (a3= TEXT ) )
                    	    // Textadventure.g:2519:6: a2= ',' (a3= TEXT )
                    	    {
                    	    a2=(Token)match(input,12,FOLLOW_12_in_parse_textadventure_EFunction2136); if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    						if (element == null) {
                    	    							element = textadventure.TextadventureFactory.eINSTANCE.createEFunction();
                    	    							startIncompleteElement(element);
                    	    						}
                    	    						collectHiddenTokens(element);
                    	    						retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_12_0_0_1_0_0_1_0_0_0, null, true);
                    	    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
                    	    					}

                    	    if ( state.backtracking==0 ) {
                    	    						// expected elements (follow set)
                    	    						addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[132]);
                    	    					}

                    	    // Textadventure.g:2533:6: (a3= TEXT )
                    	    // Textadventure.g:2534:7: a3= TEXT
                    	    {
                    	    a3=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_textadventure_EFunction2170); if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    							if (terminateParsing) {
                    	    								throw new textadventure.resource.textadventure.mopp.TextadventureTerminateParsingException();
                    	    							}
                    	    							if (element == null) {
                    	    								element = textadventure.TextadventureFactory.eINSTANCE.createEFunction();
                    	    								startIncompleteElement(element);
                    	    							}
                    	    							if (a3 != null) {
                    	    								textadventure.resource.textadventure.ITextadventureTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
                    	    								tokenResolver.setOptions(getOptions());
                    	    								textadventure.resource.textadventure.ITextadventureTokenResolveResult result = getFreshTokenResolveResult();
                    	    								tokenResolver.resolve(a3.getText(), element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EFUNCTION__PARAMS), result);
                    	    								Object resolvedObject = result.getResolvedToken();
                    	    								if (resolvedObject == null) {
                    	    									addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a3).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a3).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a3).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a3).getStopIndex());
                    	    								}
                    	    								java.lang.String resolved = (java.lang.String) resolvedObject;
                    	    								if (resolved != null) {
                    	    									Object value = resolved;
                    	    									addObjectToList(element, textadventure.TextadventurePackage.EFUNCTION__PARAMS, value);
                    	    									completedElement(value, false);
                    	    								}
                    	    								collectHiddenTokens(element);
                    	    								retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_12_0_0_1_0_0_1_0_0_1, resolved, true);
                    	    								copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a3, element);
                    	    							}
                    	    						}

                    	    }


                    	    if ( state.backtracking==0 ) {
                    	    						// expected elements (follow set)
                    	    						addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[133]);
                    	    						addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[134]);
                    	    					}

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop14;
                        }
                    } while (true);


                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[135]);
                    				addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[136]);
                    			}

                    }


                    }
                    break;

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[137]);
            	}

            a4=(Token)match(input,11,FOLLOW_11_in_parse_textadventure_EFunction2251); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = textadventure.TextadventureFactory.eINSTANCE.createEFunction();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_12_0_0_2, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a4, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[138]);
            	}

            a5=(Token)match(input,41,FOLLOW_41_in_parse_textadventure_EFunction2265); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = textadventure.TextadventureFactory.eINSTANCE.createEFunction();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_12_0_0_3, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a5, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEFunction(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[139]);
            		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEFunction(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[140]);
            		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEFunction(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[141]);
            		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[142]);
            	}

            // Textadventure.g:2616:2: (a6_0= parse_textadventure_EFunctionBody )
            // Textadventure.g:2617:3: a6_0= parse_textadventure_EFunctionBody
            {
            pushFollow(FOLLOW_parse_textadventure_EFunctionBody_in_parse_textadventure_EFunction2283);
            a6_0=parse_textadventure_EFunctionBody();

            state._fsp--;
            if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new textadventure.resource.textadventure.mopp.TextadventureTerminateParsingException();
            			}
            			if (element == null) {
            				element = textadventure.TextadventureFactory.eINSTANCE.createEFunction();
            				startIncompleteElement(element);
            			}
            			if (a6_0 != null) {
            				if (a6_0 != null) {
            					Object value = a6_0;
            					element.eSet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EFUNCTION__BODY), value);
            					completedElement(value, true);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_12_0_0_4, a6_0, true);
            				copyLocalizationInfos(a6_0, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[143]);
            	}

            a7=(Token)match(input,42,FOLLOW_42_in_parse_textadventure_EFunction2301); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = textadventure.TextadventureFactory.eINSTANCE.createEFunction();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_12_0_0_5, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a7, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEBattle(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[144]);
            		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[145]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 14, parse_textadventure_EFunction_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_textadventure_EFunction"



    // $ANTLR start "parse_textadventure_EFunctionBody"
    // Textadventure.g:2659:1: parse_textadventure_EFunctionBody returns [textadventure.EFunctionBody element = null] : ( ( (a0_0= parse_textadventure_EStatement ) a1= ';' ) )* ;
    public final textadventure.EFunctionBody parse_textadventure_EFunctionBody() throws RecognitionException {
        textadventure.EFunctionBody element =  null;

        int parse_textadventure_EFunctionBody_StartIndex = input.index();

        Token a1=null;
        textadventure.EStatement a0_0 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 15) ) { return element; }

            // Textadventure.g:2662:2: ( ( ( (a0_0= parse_textadventure_EStatement ) a1= ';' ) )* )
            // Textadventure.g:2663:2: ( ( (a0_0= parse_textadventure_EStatement ) a1= ';' ) )*
            {
            // Textadventure.g:2663:2: ( ( (a0_0= parse_textadventure_EStatement ) a1= ';' ) )*
            loop16:
            do {
                int alt16=2;
                int LA16_0 = input.LA(1);

                if ( (LA16_0==TEXT||(LA16_0 >= 34 && LA16_0 <= 35)) ) {
                    alt16=1;
                }


                switch (alt16) {
            	case 1 :
            	    // Textadventure.g:2664:3: ( (a0_0= parse_textadventure_EStatement ) a1= ';' )
            	    {
            	    // Textadventure.g:2664:3: ( (a0_0= parse_textadventure_EStatement ) a1= ';' )
            	    // Textadventure.g:2665:4: (a0_0= parse_textadventure_EStatement ) a1= ';'
            	    {
            	    // Textadventure.g:2665:4: (a0_0= parse_textadventure_EStatement )
            	    // Textadventure.g:2666:5: a0_0= parse_textadventure_EStatement
            	    {
            	    pushFollow(FOLLOW_parse_textadventure_EStatement_in_parse_textadventure_EFunctionBody2345);
            	    a0_0=parse_textadventure_EStatement();

            	    state._fsp--;
            	    if (state.failed) return element;

            	    if ( state.backtracking==0 ) {
            	    					if (terminateParsing) {
            	    						throw new textadventure.resource.textadventure.mopp.TextadventureTerminateParsingException();
            	    					}
            	    					if (element == null) {
            	    						element = textadventure.TextadventureFactory.eINSTANCE.createEFunctionBody();
            	    						startIncompleteElement(element);
            	    					}
            	    					if (a0_0 != null) {
            	    						if (a0_0 != null) {
            	    							Object value = a0_0;
            	    							addObjectToList(element, textadventure.TextadventurePackage.EFUNCTION_BODY__STATEMENTS, value);
            	    							completedElement(value, true);
            	    						}
            	    						collectHiddenTokens(element);
            	    						retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_13_0_0_0_0_0_0, a0_0, true);
            	    						copyLocalizationInfos(a0_0, element);
            	    					}
            	    				}

            	    }


            	    if ( state.backtracking==0 ) {
            	    				// expected elements (follow set)
            	    				addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[146]);
            	    			}

            	    a1=(Token)match(input,13,FOLLOW_13_in_parse_textadventure_EFunctionBody2373); if (state.failed) return element;

            	    if ( state.backtracking==0 ) {
            	    				if (element == null) {
            	    					element = textadventure.TextadventureFactory.eINSTANCE.createEFunctionBody();
            	    					startIncompleteElement(element);
            	    				}
            	    				collectHiddenTokens(element);
            	    				retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_13_0_0_0_0_0_1, null, true);
            	    				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
            	    			}

            	    if ( state.backtracking==0 ) {
            	    				// expected elements (follow set)
            	    				addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEFunctionBody(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[147]);
            	    				addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEFunctionBody(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[148]);
            	    				addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEFunctionBody(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[149]);
            	    				addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[150]);
            	    			}

            	    }


            	    }
            	    break;

            	default :
            	    break loop16;
                }
            } while (true);


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEFunctionBody(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[151]);
            		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEFunctionBody(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[152]);
            		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEFunctionBody(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[153]);
            		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[154]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 15, parse_textadventure_EFunctionBody_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_textadventure_EFunctionBody"



    // $ANTLR start "parse_textadventure_EPrintStatement"
    // Textadventure.g:2720:1: parse_textadventure_EPrintStatement returns [textadventure.EPrintStatement element = null] : a0= 'print' (a1= QUOTED_34_34 ) ;
    public final textadventure.EPrintStatement parse_textadventure_EPrintStatement() throws RecognitionException {
        textadventure.EPrintStatement element =  null;

        int parse_textadventure_EPrintStatement_StartIndex = input.index();

        Token a0=null;
        Token a1=null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 16) ) { return element; }

            // Textadventure.g:2723:2: (a0= 'print' (a1= QUOTED_34_34 ) )
            // Textadventure.g:2724:2: a0= 'print' (a1= QUOTED_34_34 )
            {
            a0=(Token)match(input,34,FOLLOW_34_in_parse_textadventure_EPrintStatement2421); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = textadventure.TextadventureFactory.eINSTANCE.createEPrintStatement();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_14_0_0_0, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[155]);
            	}

            // Textadventure.g:2738:2: (a1= QUOTED_34_34 )
            // Textadventure.g:2739:3: a1= QUOTED_34_34
            {
            a1=(Token)match(input,QUOTED_34_34,FOLLOW_QUOTED_34_34_in_parse_textadventure_EPrintStatement2439); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new textadventure.resource.textadventure.mopp.TextadventureTerminateParsingException();
            			}
            			if (element == null) {
            				element = textadventure.TextadventureFactory.eINSTANCE.createEPrintStatement();
            				startIncompleteElement(element);
            			}
            			if (a1 != null) {
            				textadventure.resource.textadventure.ITextadventureTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34");
            				tokenResolver.setOptions(getOptions());
            				textadventure.resource.textadventure.ITextadventureTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EPRINT_STATEMENT__OUTPUT), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a1).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStopIndex());
            				}
            				java.lang.String resolved = (java.lang.String) resolvedObject;
            				if (resolved != null) {
            					Object value = resolved;
            					element.eSet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EPRINT_STATEMENT__OUTPUT), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_14_0_0_1, resolved, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a1, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[156]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 16, parse_textadventure_EPrintStatement_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_textadventure_EPrintStatement"



    // $ANTLR start "parse_textadventure_ERemovePlayerStatement"
    // Textadventure.g:2776:1: parse_textadventure_ERemovePlayerStatement returns [textadventure.ERemovePlayerStatement element = null] : a0= 'remove' (a1= TEXT ) ;
    public final textadventure.ERemovePlayerStatement parse_textadventure_ERemovePlayerStatement() throws RecognitionException {
        textadventure.ERemovePlayerStatement element =  null;

        int parse_textadventure_ERemovePlayerStatement_StartIndex = input.index();

        Token a0=null;
        Token a1=null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 17) ) { return element; }

            // Textadventure.g:2779:2: (a0= 'remove' (a1= TEXT ) )
            // Textadventure.g:2780:2: a0= 'remove' (a1= TEXT )
            {
            a0=(Token)match(input,35,FOLLOW_35_in_parse_textadventure_ERemovePlayerStatement2475); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = textadventure.TextadventureFactory.eINSTANCE.createERemovePlayerStatement();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_15_0_0_0, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[157]);
            	}

            // Textadventure.g:2794:2: (a1= TEXT )
            // Textadventure.g:2795:3: a1= TEXT
            {
            a1=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_textadventure_ERemovePlayerStatement2493); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new textadventure.resource.textadventure.mopp.TextadventureTerminateParsingException();
            			}
            			if (element == null) {
            				element = textadventure.TextadventureFactory.eINSTANCE.createERemovePlayerStatement();
            				startIncompleteElement(element);
            			}
            			if (a1 != null) {
            				textadventure.resource.textadventure.ITextadventureTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
            				tokenResolver.setOptions(getOptions());
            				textadventure.resource.textadventure.ITextadventureTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EREMOVE_PLAYER_STATEMENT__TARGET), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a1).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStopIndex());
            				}
            				String resolved = (String) resolvedObject;
            				textadventure.EPerson proxy = textadventure.TextadventureFactory.eINSTANCE.createEPerson();
            				collectHiddenTokens(element);
            				registerContextDependentProxy(new textadventure.resource.textadventure.mopp.TextadventureContextDependentURIFragmentFactory<textadventure.ERemovePlayerStatement, textadventure.EPerson>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getERemovePlayerStatementTargetReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EREMOVE_PLAYER_STATEMENT__TARGET), resolved, proxy);
            				if (proxy != null) {
            					Object value = proxy;
            					element.eSet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EREMOVE_PLAYER_STATEMENT__TARGET), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_15_0_0_1, proxy, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a1, element);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a1, proxy);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[158]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 17, parse_textadventure_ERemovePlayerStatement_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_textadventure_ERemovePlayerStatement"



    // $ANTLR start "parse_textadventure_EMethodCallStatement"
    // Textadventure.g:2836:1: parse_textadventure_EMethodCallStatement returns [textadventure.EMethodCallStatement element = null] : (a0= TEXT ) (a1= TEXT ) ( (a2= QUOTED_34_34 ) )* ;
    public final textadventure.EMethodCallStatement parse_textadventure_EMethodCallStatement() throws RecognitionException {
        textadventure.EMethodCallStatement element =  null;

        int parse_textadventure_EMethodCallStatement_StartIndex = input.index();

        Token a0=null;
        Token a1=null;
        Token a2=null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 18) ) { return element; }

            // Textadventure.g:2839:2: ( (a0= TEXT ) (a1= TEXT ) ( (a2= QUOTED_34_34 ) )* )
            // Textadventure.g:2840:2: (a0= TEXT ) (a1= TEXT ) ( (a2= QUOTED_34_34 ) )*
            {
            // Textadventure.g:2840:2: (a0= TEXT )
            // Textadventure.g:2841:3: a0= TEXT
            {
            a0=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_textadventure_EMethodCallStatement2533); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new textadventure.resource.textadventure.mopp.TextadventureTerminateParsingException();
            			}
            			if (element == null) {
            				element = textadventure.TextadventureFactory.eINSTANCE.createEMethodCallStatement();
            				startIncompleteElement(element);
            			}
            			if (a0 != null) {
            				textadventure.resource.textadventure.ITextadventureTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
            				tokenResolver.setOptions(getOptions());
            				textadventure.resource.textadventure.ITextadventureTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a0.getText(), element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EMETHOD_CALL_STATEMENT__OBJECT), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a0).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStopIndex());
            				}
            				java.lang.String resolved = (java.lang.String) resolvedObject;
            				if (resolved != null) {
            					Object value = resolved;
            					element.eSet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EMETHOD_CALL_STATEMENT__OBJECT), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_16_0_0_0, resolved, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a0, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[159]);
            	}

            // Textadventure.g:2876:2: (a1= TEXT )
            // Textadventure.g:2877:3: a1= TEXT
            {
            a1=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_textadventure_EMethodCallStatement2558); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new textadventure.resource.textadventure.mopp.TextadventureTerminateParsingException();
            			}
            			if (element == null) {
            				element = textadventure.TextadventureFactory.eINSTANCE.createEMethodCallStatement();
            				startIncompleteElement(element);
            			}
            			if (a1 != null) {
            				textadventure.resource.textadventure.ITextadventureTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
            				tokenResolver.setOptions(getOptions());
            				textadventure.resource.textadventure.ITextadventureTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EMETHOD_CALL_STATEMENT__METHOD), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a1).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStopIndex());
            				}
            				java.lang.String resolved = (java.lang.String) resolvedObject;
            				if (resolved != null) {
            					Object value = resolved;
            					element.eSet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EMETHOD_CALL_STATEMENT__METHOD), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_16_0_0_1, resolved, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a1, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[160]);
            		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[161]);
            	}

            // Textadventure.g:2913:2: ( (a2= QUOTED_34_34 ) )*
            loop17:
            do {
                int alt17=2;
                int LA17_0 = input.LA(1);

                if ( (LA17_0==QUOTED_34_34) ) {
                    alt17=1;
                }


                switch (alt17) {
            	case 1 :
            	    // Textadventure.g:2914:3: (a2= QUOTED_34_34 )
            	    {
            	    // Textadventure.g:2914:3: (a2= QUOTED_34_34 )
            	    // Textadventure.g:2915:4: a2= QUOTED_34_34
            	    {
            	    a2=(Token)match(input,QUOTED_34_34,FOLLOW_QUOTED_34_34_in_parse_textadventure_EMethodCallStatement2588); if (state.failed) return element;

            	    if ( state.backtracking==0 ) {
            	    				if (terminateParsing) {
            	    					throw new textadventure.resource.textadventure.mopp.TextadventureTerminateParsingException();
            	    				}
            	    				if (element == null) {
            	    					element = textadventure.TextadventureFactory.eINSTANCE.createEMethodCallStatement();
            	    					startIncompleteElement(element);
            	    				}
            	    				if (a2 != null) {
            	    					textadventure.resource.textadventure.ITextadventureTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34");
            	    					tokenResolver.setOptions(getOptions());
            	    					textadventure.resource.textadventure.ITextadventureTokenResolveResult result = getFreshTokenResolveResult();
            	    					tokenResolver.resolve(a2.getText(), element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EMETHOD_CALL_STATEMENT__PARAMS), result);
            	    					Object resolvedObject = result.getResolvedToken();
            	    					if (resolvedObject == null) {
            	    						addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a2).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStopIndex());
            	    					}
            	    					java.lang.String resolved = (java.lang.String) resolvedObject;
            	    					if (resolved != null) {
            	    						Object value = resolved;
            	    						addObjectToList(element, textadventure.TextadventurePackage.EMETHOD_CALL_STATEMENT__PARAMS, value);
            	    						completedElement(value, false);
            	    					}
            	    					collectHiddenTokens(element);
            	    					retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_16_0_0_2, resolved, true);
            	    					copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, element);
            	    				}
            	    			}

            	    }


            	    }
            	    break;

            	default :
            	    break loop17;
                }
            } while (true);


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[162]);
            		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[163]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 18, parse_textadventure_EMethodCallStatement_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_textadventure_EMethodCallStatement"



    // $ANTLR start "parse_textadventure_EIgnoreItemAbility"
    // Textadventure.g:2954:1: parse_textadventure_EIgnoreItemAbility returns [textadventure.EIgnoreItemAbility element = null] : a0= 'ignore' (a1= TEXT ) ;
    public final textadventure.EIgnoreItemAbility parse_textadventure_EIgnoreItemAbility() throws RecognitionException {
        textadventure.EIgnoreItemAbility element =  null;

        int parse_textadventure_EIgnoreItemAbility_StartIndex = input.index();

        Token a0=null;
        Token a1=null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 19) ) { return element; }

            // Textadventure.g:2957:2: (a0= 'ignore' (a1= TEXT ) )
            // Textadventure.g:2958:2: a0= 'ignore' (a1= TEXT )
            {
            a0=(Token)match(input,29,FOLLOW_29_in_parse_textadventure_EIgnoreItemAbility2633); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = textadventure.TextadventureFactory.eINSTANCE.createEIgnoreItemAbility();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_17_0_0_0, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[164]);
            	}

            // Textadventure.g:2972:2: (a1= TEXT )
            // Textadventure.g:2973:3: a1= TEXT
            {
            a1=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_textadventure_EIgnoreItemAbility2651); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new textadventure.resource.textadventure.mopp.TextadventureTerminateParsingException();
            			}
            			if (element == null) {
            				element = textadventure.TextadventureFactory.eINSTANCE.createEIgnoreItemAbility();
            				startIncompleteElement(element);
            			}
            			if (a1 != null) {
            				textadventure.resource.textadventure.ITextadventureTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
            				tokenResolver.setOptions(getOptions());
            				textadventure.resource.textadventure.ITextadventureTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EIGNORE_ITEM_ABILITY__IGNORED_ITEM), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a1).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStopIndex());
            				}
            				String resolved = (String) resolvedObject;
            				textadventure.EItem proxy = textadventure.TextadventureFactory.eINSTANCE.createEItem();
            				collectHiddenTokens(element);
            				registerContextDependentProxy(new textadventure.resource.textadventure.mopp.TextadventureContextDependentURIFragmentFactory<textadventure.EIgnoreItemAbility, textadventure.EItem>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getEIgnoreItemAbilityIgnoredItemReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EIGNORE_ITEM_ABILITY__IGNORED_ITEM), resolved, proxy);
            				if (proxy != null) {
            					Object value = proxy;
            					element.eSet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EIGNORE_ITEM_ABILITY__IGNORED_ITEM), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_17_0_0_1, proxy, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a1, element);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a1, proxy);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEPerson(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[165]);
            		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[166]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 19, parse_textadventure_EIgnoreItemAbility_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_textadventure_EIgnoreItemAbility"



    // $ANTLR start "parse_textadventure_EEffect"
    // Textadventure.g:3015:1: parse_textadventure_EEffect returns [textadventure.EEffect element = null] : c0= parse_textadventure_EWhileEffect ;
    public final textadventure.EEffect parse_textadventure_EEffect() throws RecognitionException {
        textadventure.EEffect element =  null;

        int parse_textadventure_EEffect_StartIndex = input.index();

        textadventure.EWhileEffect c0 =null;


        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 20) ) { return element; }

            // Textadventure.g:3016:2: (c0= parse_textadventure_EWhileEffect )
            // Textadventure.g:3017:2: c0= parse_textadventure_EWhileEffect
            {
            pushFollow(FOLLOW_parse_textadventure_EWhileEffect_in_parse_textadventure_EEffect2683);
            c0=parse_textadventure_EWhileEffect();

            state._fsp--;
            if (state.failed) return element;

            if ( state.backtracking==0 ) { element = c0; /* this is a subclass or primitive expression choice */ }

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 20, parse_textadventure_EEffect_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_textadventure_EEffect"



    // $ANTLR start "parse_textadventure_EAction"
    // Textadventure.g:3021:1: parse_textadventure_EAction returns [textadventure.EAction element = null] : c0= parse_textadventure_ERemoveAction ;
    public final textadventure.EAction parse_textadventure_EAction() throws RecognitionException {
        textadventure.EAction element =  null;

        int parse_textadventure_EAction_StartIndex = input.index();

        textadventure.ERemoveAction c0 =null;


        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 21) ) { return element; }

            // Textadventure.g:3022:2: (c0= parse_textadventure_ERemoveAction )
            // Textadventure.g:3023:2: c0= parse_textadventure_ERemoveAction
            {
            pushFollow(FOLLOW_parse_textadventure_ERemoveAction_in_parse_textadventure_EAction2704);
            c0=parse_textadventure_ERemoveAction();

            state._fsp--;
            if (state.failed) return element;

            if ( state.backtracking==0 ) { element = c0; /* this is a subclass or primitive expression choice */ }

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 21, parse_textadventure_EAction_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_textadventure_EAction"



    // $ANTLR start "parse_textadventure_EAbility"
    // Textadventure.g:3027:1: parse_textadventure_EAbility returns [textadventure.EAbility element = null] : c0= parse_textadventure_EIgnoreItemAbility ;
    public final textadventure.EAbility parse_textadventure_EAbility() throws RecognitionException {
        textadventure.EAbility element =  null;

        int parse_textadventure_EAbility_StartIndex = input.index();

        textadventure.EIgnoreItemAbility c0 =null;


        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 22) ) { return element; }

            // Textadventure.g:3028:2: (c0= parse_textadventure_EIgnoreItemAbility )
            // Textadventure.g:3029:2: c0= parse_textadventure_EIgnoreItemAbility
            {
            pushFollow(FOLLOW_parse_textadventure_EIgnoreItemAbility_in_parse_textadventure_EAbility2725);
            c0=parse_textadventure_EIgnoreItemAbility();

            state._fsp--;
            if (state.failed) return element;

            if ( state.backtracking==0 ) { element = c0; /* this is a subclass or primitive expression choice */ }

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 22, parse_textadventure_EAbility_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_textadventure_EAbility"



    // $ANTLR start "parse_textadventure_EStatement"
    // Textadventure.g:3033:1: parse_textadventure_EStatement returns [textadventure.EStatement element = null] : (c0= parse_textadventure_EPrintStatement |c1= parse_textadventure_ERemovePlayerStatement |c2= parse_textadventure_EMethodCallStatement );
    public final textadventure.EStatement parse_textadventure_EStatement() throws RecognitionException {
        textadventure.EStatement element =  null;

        int parse_textadventure_EStatement_StartIndex = input.index();

        textadventure.EPrintStatement c0 =null;

        textadventure.ERemovePlayerStatement c1 =null;

        textadventure.EMethodCallStatement c2 =null;


        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 23) ) { return element; }

            // Textadventure.g:3034:2: (c0= parse_textadventure_EPrintStatement |c1= parse_textadventure_ERemovePlayerStatement |c2= parse_textadventure_EMethodCallStatement )
            int alt18=3;
            switch ( input.LA(1) ) {
            case 34:
                {
                alt18=1;
                }
                break;
            case 35:
                {
                alt18=2;
                }
                break;
            case TEXT:
                {
                alt18=3;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 18, 0, input);

                throw nvae;

            }

            switch (alt18) {
                case 1 :
                    // Textadventure.g:3035:2: c0= parse_textadventure_EPrintStatement
                    {
                    pushFollow(FOLLOW_parse_textadventure_EPrintStatement_in_parse_textadventure_EStatement2746);
                    c0=parse_textadventure_EPrintStatement();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c0; /* this is a subclass or primitive expression choice */ }

                    }
                    break;
                case 2 :
                    // Textadventure.g:3036:4: c1= parse_textadventure_ERemovePlayerStatement
                    {
                    pushFollow(FOLLOW_parse_textadventure_ERemovePlayerStatement_in_parse_textadventure_EStatement2756);
                    c1=parse_textadventure_ERemovePlayerStatement();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c1; /* this is a subclass or primitive expression choice */ }

                    }
                    break;
                case 3 :
                    // Textadventure.g:3037:4: c2= parse_textadventure_EMethodCallStatement
                    {
                    pushFollow(FOLLOW_parse_textadventure_EMethodCallStatement_in_parse_textadventure_EStatement2766);
                    c2=parse_textadventure_EMethodCallStatement();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c2; /* this is a subclass or primitive expression choice */ }

                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 23, parse_textadventure_EStatement_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_textadventure_EStatement"

    // Delegated rules


 

    public static final BitSet FOLLOW_parse_textadventure_EWorld_in_start82 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_start89 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_33_in_parse_textadventure_EWorld115 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_14_in_parse_textadventure_EWorld129 = new BitSet(new long[]{0x0000000000000200L});
    public static final BitSet FOLLOW_TEXT_in_parse_textadventure_EWorld147 = new BitSet(new long[]{0x0000001000000000L});
    public static final BitSet FOLLOW_parse_textadventure_ETile_in_parse_textadventure_EWorld177 = new BitSet(new long[]{0x0000001140000000L});
    public static final BitSet FOLLOW_parse_textadventure_EItem_in_parse_textadventure_EWorld212 = new BitSet(new long[]{0x0000000140000000L});
    public static final BitSet FOLLOW_parse_textadventure_EPerson_in_parse_textadventure_EWorld247 = new BitSet(new long[]{0x0000000101300002L});
    public static final BitSet FOLLOW_parse_textadventure_EBattle_in_parse_textadventure_EWorld282 = new BitSet(new long[]{0x0000000001300002L});
    public static final BitSet FOLLOW_parse_textadventure_ECheckPoint_in_parse_textadventure_EWorld317 = new BitSet(new long[]{0x0000000001200002L});
    public static final BitSet FOLLOW_parse_textadventure_EDestination_in_parse_textadventure_EWorld352 = new BitSet(new long[]{0x0000000001000002L});
    public static final BitSet FOLLOW_36_in_parse_textadventure_ETile393 = new BitSet(new long[]{0x0000000000000200L});
    public static final BitSet FOLLOW_TEXT_in_parse_textadventure_ETile411 = new BitSet(new long[]{0x0000020000000000L});
    public static final BitSet FOLLOW_41_in_parse_textadventure_ETile432 = new BitSet(new long[]{0x0000008000000000L});
    public static final BitSet FOLLOW_39_in_parse_textadventure_ETile446 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_14_in_parse_textadventure_ETile460 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_INTEGER_in_parse_textadventure_ETile478 = new BitSet(new long[]{0x0000010000000000L});
    public static final BitSet FOLLOW_40_in_parse_textadventure_ETile499 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_14_in_parse_textadventure_ETile513 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_INTEGER_in_parse_textadventure_ETile531 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_23_in_parse_textadventure_ETile552 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_14_in_parse_textadventure_ETile566 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_QUOTED_34_34_in_parse_textadventure_ETile584 = new BitSet(new long[]{0x0000040000000000L});
    public static final BitSet FOLLOW_42_in_parse_textadventure_ETile605 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_30_in_parse_textadventure_EItem634 = new BitSet(new long[]{0x0000000000000200L});
    public static final BitSet FOLLOW_TEXT_in_parse_textadventure_EItem652 = new BitSet(new long[]{0x0000020000008000L});
    public static final BitSet FOLLOW_15_in_parse_textadventure_EItem682 = new BitSet(new long[]{0x0000000000000200L});
    public static final BitSet FOLLOW_TEXT_in_parse_textadventure_EItem708 = new BitSet(new long[]{0x0000020000000000L});
    public static final BitSet FOLLOW_41_in_parse_textadventure_EItem754 = new BitSet(new long[]{0x0000044000000000L});
    public static final BitSet FOLLOW_parse_textadventure_EEffect_in_parse_textadventure_EItem783 = new BitSet(new long[]{0x0000044000000000L});
    public static final BitSet FOLLOW_42_in_parse_textadventure_EItem824 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_21_in_parse_textadventure_ECheckPoint853 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_15_in_parse_textadventure_ECheckPoint867 = new BitSet(new long[]{0x0000000000000200L});
    public static final BitSet FOLLOW_TEXT_in_parse_textadventure_ECheckPoint885 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_24_in_parse_textadventure_EDestination921 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_15_in_parse_textadventure_EDestination935 = new BitSet(new long[]{0x0000000000000200L});
    public static final BitSet FOLLOW_TEXT_in_parse_textadventure_EDestination953 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_31_in_parse_textadventure_EBinding998 = new BitSet(new long[]{0x0000000000000200L});
    public static final BitSet FOLLOW_TEXT_in_parse_textadventure_EBinding1023 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_38_in_parse_textadventure_EWhileEffect1059 = new BitSet(new long[]{0x0000002000000000L});
    public static final BitSet FOLLOW_37_in_parse_textadventure_EWhileEffect1082 = new BitSet(new long[]{0x0000000080000000L});
    public static final BitSet FOLLOW_parse_textadventure_EBinding_in_parse_textadventure_EWhileEffect1107 = new BitSet(new long[]{0x0000020000000000L});
    public static final BitSet FOLLOW_41_in_parse_textadventure_EWhileEffect1125 = new BitSet(new long[]{0x0000000800000000L});
    public static final BitSet FOLLOW_parse_textadventure_EAction_in_parse_textadventure_EWhileEffect1143 = new BitSet(new long[]{0x0000040000000000L});
    public static final BitSet FOLLOW_42_in_parse_textadventure_EWhileEffect1161 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_35_in_parse_textadventure_ERemoveAction1190 = new BitSet(new long[]{0x0000000000000200L});
    public static final BitSet FOLLOW_TEXT_in_parse_textadventure_ERemoveAction1208 = new BitSet(new long[]{0x0000000004000000L});
    public static final BitSet FOLLOW_26_in_parse_textadventure_ERemoveAction1229 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_parse_textadventure_EListDescription_in_parse_textadventure_ERemoveAction1247 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_18_in_parse_textadventure_EListDescription1289 = new BitSet(new long[]{0x0000000000030000L});
    public static final BitSet FOLLOW_17_in_parse_textadventure_EListDescription1319 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_16_in_parse_textadventure_EListDescription1334 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_32_in_parse_textadventure_EPerson1370 = new BitSet(new long[]{0x0000000000000200L});
    public static final BitSet FOLLOW_TEXT_in_parse_textadventure_EPerson1388 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_15_in_parse_textadventure_EPerson1409 = new BitSet(new long[]{0x0000000000000200L});
    public static final BitSet FOLLOW_TEXT_in_parse_textadventure_EPerson1427 = new BitSet(new long[]{0x0000020000000000L});
    public static final BitSet FOLLOW_41_in_parse_textadventure_EPerson1448 = new BitSet(new long[]{0x0000040030480000L});
    public static final BitSet FOLLOW_28_in_parse_textadventure_EPerson1471 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_14_in_parse_textadventure_EPerson1491 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_INTEGER_in_parse_textadventure_EPerson1517 = new BitSet(new long[]{0x0000040030480000L});
    public static final BitSet FOLLOW_22_in_parse_textadventure_EPerson1558 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_14_in_parse_textadventure_EPerson1578 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_INTEGER_in_parse_textadventure_EPerson1604 = new BitSet(new long[]{0x0000040030480000L});
    public static final BitSet FOLLOW_19_in_parse_textadventure_EPerson1645 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_14_in_parse_textadventure_EPerson1665 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_INTEGER_in_parse_textadventure_EPerson1691 = new BitSet(new long[]{0x0000040030480000L});
    public static final BitSet FOLLOW_parse_textadventure_EAbility_in_parse_textadventure_EPerson1746 = new BitSet(new long[]{0x0000040020000000L});
    public static final BitSet FOLLOW_42_in_parse_textadventure_EPerson1772 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_20_in_parse_textadventure_EBattle1801 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_15_in_parse_textadventure_EBattle1815 = new BitSet(new long[]{0x0000000000000200L});
    public static final BitSet FOLLOW_TEXT_in_parse_textadventure_EBattle1833 = new BitSet(new long[]{0x0000020000000000L});
    public static final BitSet FOLLOW_41_in_parse_textadventure_EBattle1854 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_25_in_parse_textadventure_EBattle1868 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_14_in_parse_textadventure_EBattle1882 = new BitSet(new long[]{0x0000000000000200L});
    public static final BitSet FOLLOW_TEXT_in_parse_textadventure_EBattle1900 = new BitSet(new long[]{0x0000040000000200L});
    public static final BitSet FOLLOW_parse_textadventure_EBattleCustomization_in_parse_textadventure_EBattle1930 = new BitSet(new long[]{0x0000040000000200L});
    public static final BitSet FOLLOW_42_in_parse_textadventure_EBattle1956 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_TEXT_in_parse_textadventure_EBattleCustomization1989 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_14_in_parse_textadventure_EBattleCustomization2010 = new BitSet(new long[]{0x0000000008000000L});
    public static final BitSet FOLLOW_parse_textadventure_EFunction_in_parse_textadventure_EBattleCustomization2028 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_27_in_parse_textadventure_EFunction2061 = new BitSet(new long[]{0x0000000000000A00L});
    public static final BitSet FOLLOW_TEXT_in_parse_textadventure_EFunction2090 = new BitSet(new long[]{0x0000000000001800L});
    public static final BitSet FOLLOW_12_in_parse_textadventure_EFunction2136 = new BitSet(new long[]{0x0000000000000200L});
    public static final BitSet FOLLOW_TEXT_in_parse_textadventure_EFunction2170 = new BitSet(new long[]{0x0000000000001800L});
    public static final BitSet FOLLOW_11_in_parse_textadventure_EFunction2251 = new BitSet(new long[]{0x0000020000000000L});
    public static final BitSet FOLLOW_41_in_parse_textadventure_EFunction2265 = new BitSet(new long[]{0x0000040C00000200L});
    public static final BitSet FOLLOW_parse_textadventure_EFunctionBody_in_parse_textadventure_EFunction2283 = new BitSet(new long[]{0x0000040000000000L});
    public static final BitSet FOLLOW_42_in_parse_textadventure_EFunction2301 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_textadventure_EStatement_in_parse_textadventure_EFunctionBody2345 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_13_in_parse_textadventure_EFunctionBody2373 = new BitSet(new long[]{0x0000000C00000202L});
    public static final BitSet FOLLOW_34_in_parse_textadventure_EPrintStatement2421 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_QUOTED_34_34_in_parse_textadventure_EPrintStatement2439 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_35_in_parse_textadventure_ERemovePlayerStatement2475 = new BitSet(new long[]{0x0000000000000200L});
    public static final BitSet FOLLOW_TEXT_in_parse_textadventure_ERemovePlayerStatement2493 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_TEXT_in_parse_textadventure_EMethodCallStatement2533 = new BitSet(new long[]{0x0000000000000200L});
    public static final BitSet FOLLOW_TEXT_in_parse_textadventure_EMethodCallStatement2558 = new BitSet(new long[]{0x0000000000000102L});
    public static final BitSet FOLLOW_QUOTED_34_34_in_parse_textadventure_EMethodCallStatement2588 = new BitSet(new long[]{0x0000000000000102L});
    public static final BitSet FOLLOW_29_in_parse_textadventure_EIgnoreItemAbility2633 = new BitSet(new long[]{0x0000000000000200L});
    public static final BitSet FOLLOW_TEXT_in_parse_textadventure_EIgnoreItemAbility2651 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_textadventure_EWhileEffect_in_parse_textadventure_EEffect2683 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_textadventure_ERemoveAction_in_parse_textadventure_EAction2704 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_textadventure_EIgnoreItemAbility_in_parse_textadventure_EAbility2725 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_textadventure_EPrintStatement_in_parse_textadventure_EStatement2746 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_textadventure_ERemovePlayerStatement_in_parse_textadventure_EStatement2756 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_textadventure_EMethodCallStatement_in_parse_textadventure_EStatement2766 = new BitSet(new long[]{0x0000000000000002L});

}