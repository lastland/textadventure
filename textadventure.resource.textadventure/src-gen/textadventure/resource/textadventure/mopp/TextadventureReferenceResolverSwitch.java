/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package textadventure.resource.textadventure.mopp;

public class TextadventureReferenceResolverSwitch implements textadventure.resource.textadventure.ITextadventureReferenceResolverSwitch {
	
	/**
	 * This map stores a copy of the options the were set for loading the resource.
	 */
	private java.util.Map<Object, Object> options;
	
	protected textadventure.resource.textadventure.analysis.EWorldPlayerPersonReferenceResolver eWorldPlayerPersonReferenceResolver = new textadventure.resource.textadventure.analysis.EWorldPlayerPersonReferenceResolver();
	protected textadventure.resource.textadventure.analysis.EItemLocationReferenceResolver eItemLocationReferenceResolver = new textadventure.resource.textadventure.analysis.EItemLocationReferenceResolver();
	protected textadventure.resource.textadventure.analysis.ECheckPointPointReferenceResolver eCheckPointPointReferenceResolver = new textadventure.resource.textadventure.analysis.ECheckPointPointReferenceResolver();
	protected textadventure.resource.textadventure.analysis.EDestinationPointReferenceResolver eDestinationPointReferenceResolver = new textadventure.resource.textadventure.analysis.EDestinationPointReferenceResolver();
	protected textadventure.resource.textadventure.analysis.ERemoveActionWhatReferenceResolver eRemoveActionWhatReferenceResolver = new textadventure.resource.textadventure.analysis.ERemoveActionWhatReferenceResolver();
	protected textadventure.resource.textadventure.analysis.EPersonLocationReferenceResolver ePersonLocationReferenceResolver = new textadventure.resource.textadventure.analysis.EPersonLocationReferenceResolver();
	protected textadventure.resource.textadventure.analysis.EBattleLocationReferenceResolver eBattleLocationReferenceResolver = new textadventure.resource.textadventure.analysis.EBattleLocationReferenceResolver();
	protected textadventure.resource.textadventure.analysis.EBattleEnemyReferenceResolver eBattleEnemyReferenceResolver = new textadventure.resource.textadventure.analysis.EBattleEnemyReferenceResolver();
	protected textadventure.resource.textadventure.analysis.ERemovePlayerStatementTargetReferenceResolver eRemovePlayerStatementTargetReferenceResolver = new textadventure.resource.textadventure.analysis.ERemovePlayerStatementTargetReferenceResolver();
	protected textadventure.resource.textadventure.analysis.EIgnoreItemAbilityIgnoredItemReferenceResolver eIgnoreItemAbilityIgnoredItemReferenceResolver = new textadventure.resource.textadventure.analysis.EIgnoreItemAbilityIgnoredItemReferenceResolver();
	
	public textadventure.resource.textadventure.ITextadventureReferenceResolver<textadventure.EWorld, textadventure.EPerson> getEWorldPlayerPersonReferenceResolver() {
		return getResolverChain(textadventure.TextadventurePackage.eINSTANCE.getEWorld_PlayerPerson(), eWorldPlayerPersonReferenceResolver);
	}
	
	public textadventure.resource.textadventure.ITextadventureReferenceResolver<textadventure.EItem, textadventure.ETile> getEItemLocationReferenceResolver() {
		return getResolverChain(textadventure.TextadventurePackage.eINSTANCE.getEItem_Location(), eItemLocationReferenceResolver);
	}
	
	public textadventure.resource.textadventure.ITextadventureReferenceResolver<textadventure.ECheckPoint, textadventure.ETile> getECheckPointPointReferenceResolver() {
		return getResolverChain(textadventure.TextadventurePackage.eINSTANCE.getECheckPoint_Point(), eCheckPointPointReferenceResolver);
	}
	
	public textadventure.resource.textadventure.ITextadventureReferenceResolver<textadventure.EDestination, textadventure.ETile> getEDestinationPointReferenceResolver() {
		return getResolverChain(textadventure.TextadventurePackage.eINSTANCE.getEDestination_Point(), eDestinationPointReferenceResolver);
	}
	
	public textadventure.resource.textadventure.ITextadventureReferenceResolver<textadventure.ERemoveAction, textadventure.EBinding> getERemoveActionWhatReferenceResolver() {
		return getResolverChain(textadventure.TextadventurePackage.eINSTANCE.getERemoveAction_What(), eRemoveActionWhatReferenceResolver);
	}
	
	public textadventure.resource.textadventure.ITextadventureReferenceResolver<textadventure.EPerson, textadventure.ETile> getEPersonLocationReferenceResolver() {
		return getResolverChain(textadventure.TextadventurePackage.eINSTANCE.getEPerson_Location(), ePersonLocationReferenceResolver);
	}
	
	public textadventure.resource.textadventure.ITextadventureReferenceResolver<textadventure.EBattle, textadventure.ETile> getEBattleLocationReferenceResolver() {
		return getResolverChain(textadventure.TextadventurePackage.eINSTANCE.getEBattle_Location(), eBattleLocationReferenceResolver);
	}
	
	public textadventure.resource.textadventure.ITextadventureReferenceResolver<textadventure.EBattle, textadventure.EPerson> getEBattleEnemyReferenceResolver() {
		return getResolverChain(textadventure.TextadventurePackage.eINSTANCE.getEBattle_Enemy(), eBattleEnemyReferenceResolver);
	}
	
	public textadventure.resource.textadventure.ITextadventureReferenceResolver<textadventure.ERemovePlayerStatement, textadventure.EPerson> getERemovePlayerStatementTargetReferenceResolver() {
		return getResolverChain(textadventure.TextadventurePackage.eINSTANCE.getERemovePlayerStatement_Target(), eRemovePlayerStatementTargetReferenceResolver);
	}
	
	public textadventure.resource.textadventure.ITextadventureReferenceResolver<textadventure.EIgnoreItemAbility, textadventure.EItem> getEIgnoreItemAbilityIgnoredItemReferenceResolver() {
		return getResolverChain(textadventure.TextadventurePackage.eINSTANCE.getEIgnoreItemAbility_IgnoredItem(), eIgnoreItemAbilityIgnoredItemReferenceResolver);
	}
	
	public void setOptions(java.util.Map<?, ?> options) {
		if (options != null) {
			this.options = new java.util.LinkedHashMap<Object, Object>();
			this.options.putAll(options);
		}
		eWorldPlayerPersonReferenceResolver.setOptions(options);
		eItemLocationReferenceResolver.setOptions(options);
		eCheckPointPointReferenceResolver.setOptions(options);
		eDestinationPointReferenceResolver.setOptions(options);
		eRemoveActionWhatReferenceResolver.setOptions(options);
		ePersonLocationReferenceResolver.setOptions(options);
		eBattleLocationReferenceResolver.setOptions(options);
		eBattleEnemyReferenceResolver.setOptions(options);
		eRemovePlayerStatementTargetReferenceResolver.setOptions(options);
		eIgnoreItemAbilityIgnoredItemReferenceResolver.setOptions(options);
	}
	
	public void resolveFuzzy(String identifier, org.eclipse.emf.ecore.EObject container, org.eclipse.emf.ecore.EReference reference, int position, textadventure.resource.textadventure.ITextadventureReferenceResolveResult<org.eclipse.emf.ecore.EObject> result) {
		if (container == null) {
			return;
		}
		if (textadventure.TextadventurePackage.eINSTANCE.getEWorld().isInstance(container)) {
			TextadventureFuzzyResolveResult<textadventure.EPerson> frr = new TextadventureFuzzyResolveResult<textadventure.EPerson>(result);
			String referenceName = reference.getName();
			org.eclipse.emf.ecore.EStructuralFeature feature = container.eClass().getEStructuralFeature(referenceName);
			if (feature != null && feature instanceof org.eclipse.emf.ecore.EReference && referenceName != null && referenceName.equals("playerPerson")) {
				eWorldPlayerPersonReferenceResolver.resolve(identifier, (textadventure.EWorld) container, (org.eclipse.emf.ecore.EReference) feature, position, true, frr);
			}
		}
		if (textadventure.TextadventurePackage.eINSTANCE.getEItem().isInstance(container)) {
			TextadventureFuzzyResolveResult<textadventure.ETile> frr = new TextadventureFuzzyResolveResult<textadventure.ETile>(result);
			String referenceName = reference.getName();
			org.eclipse.emf.ecore.EStructuralFeature feature = container.eClass().getEStructuralFeature(referenceName);
			if (feature != null && feature instanceof org.eclipse.emf.ecore.EReference && referenceName != null && referenceName.equals("location")) {
				eItemLocationReferenceResolver.resolve(identifier, (textadventure.EItem) container, (org.eclipse.emf.ecore.EReference) feature, position, true, frr);
			}
		}
		if (textadventure.TextadventurePackage.eINSTANCE.getECheckPoint().isInstance(container)) {
			TextadventureFuzzyResolveResult<textadventure.ETile> frr = new TextadventureFuzzyResolveResult<textadventure.ETile>(result);
			String referenceName = reference.getName();
			org.eclipse.emf.ecore.EStructuralFeature feature = container.eClass().getEStructuralFeature(referenceName);
			if (feature != null && feature instanceof org.eclipse.emf.ecore.EReference && referenceName != null && referenceName.equals("point")) {
				eCheckPointPointReferenceResolver.resolve(identifier, (textadventure.ECheckPoint) container, (org.eclipse.emf.ecore.EReference) feature, position, true, frr);
			}
		}
		if (textadventure.TextadventurePackage.eINSTANCE.getEDestination().isInstance(container)) {
			TextadventureFuzzyResolveResult<textadventure.ETile> frr = new TextadventureFuzzyResolveResult<textadventure.ETile>(result);
			String referenceName = reference.getName();
			org.eclipse.emf.ecore.EStructuralFeature feature = container.eClass().getEStructuralFeature(referenceName);
			if (feature != null && feature instanceof org.eclipse.emf.ecore.EReference && referenceName != null && referenceName.equals("point")) {
				eDestinationPointReferenceResolver.resolve(identifier, (textadventure.EDestination) container, (org.eclipse.emf.ecore.EReference) feature, position, true, frr);
			}
		}
		if (textadventure.TextadventurePackage.eINSTANCE.getERemoveAction().isInstance(container)) {
			TextadventureFuzzyResolveResult<textadventure.EBinding> frr = new TextadventureFuzzyResolveResult<textadventure.EBinding>(result);
			String referenceName = reference.getName();
			org.eclipse.emf.ecore.EStructuralFeature feature = container.eClass().getEStructuralFeature(referenceName);
			if (feature != null && feature instanceof org.eclipse.emf.ecore.EReference && referenceName != null && referenceName.equals("what")) {
				eRemoveActionWhatReferenceResolver.resolve(identifier, (textadventure.ERemoveAction) container, (org.eclipse.emf.ecore.EReference) feature, position, true, frr);
			}
		}
		if (textadventure.TextadventurePackage.eINSTANCE.getEPerson().isInstance(container)) {
			TextadventureFuzzyResolveResult<textadventure.ETile> frr = new TextadventureFuzzyResolveResult<textadventure.ETile>(result);
			String referenceName = reference.getName();
			org.eclipse.emf.ecore.EStructuralFeature feature = container.eClass().getEStructuralFeature(referenceName);
			if (feature != null && feature instanceof org.eclipse.emf.ecore.EReference && referenceName != null && referenceName.equals("location")) {
				ePersonLocationReferenceResolver.resolve(identifier, (textadventure.EPerson) container, (org.eclipse.emf.ecore.EReference) feature, position, true, frr);
			}
		}
		if (textadventure.TextadventurePackage.eINSTANCE.getEBattle().isInstance(container)) {
			TextadventureFuzzyResolveResult<textadventure.ETile> frr = new TextadventureFuzzyResolveResult<textadventure.ETile>(result);
			String referenceName = reference.getName();
			org.eclipse.emf.ecore.EStructuralFeature feature = container.eClass().getEStructuralFeature(referenceName);
			if (feature != null && feature instanceof org.eclipse.emf.ecore.EReference && referenceName != null && referenceName.equals("location")) {
				eBattleLocationReferenceResolver.resolve(identifier, (textadventure.EBattle) container, (org.eclipse.emf.ecore.EReference) feature, position, true, frr);
			}
		}
		if (textadventure.TextadventurePackage.eINSTANCE.getEBattle().isInstance(container)) {
			TextadventureFuzzyResolveResult<textadventure.EPerson> frr = new TextadventureFuzzyResolveResult<textadventure.EPerson>(result);
			String referenceName = reference.getName();
			org.eclipse.emf.ecore.EStructuralFeature feature = container.eClass().getEStructuralFeature(referenceName);
			if (feature != null && feature instanceof org.eclipse.emf.ecore.EReference && referenceName != null && referenceName.equals("enemy")) {
				eBattleEnemyReferenceResolver.resolve(identifier, (textadventure.EBattle) container, (org.eclipse.emf.ecore.EReference) feature, position, true, frr);
			}
		}
		if (textadventure.TextadventurePackage.eINSTANCE.getERemovePlayerStatement().isInstance(container)) {
			TextadventureFuzzyResolveResult<textadventure.EPerson> frr = new TextadventureFuzzyResolveResult<textadventure.EPerson>(result);
			String referenceName = reference.getName();
			org.eclipse.emf.ecore.EStructuralFeature feature = container.eClass().getEStructuralFeature(referenceName);
			if (feature != null && feature instanceof org.eclipse.emf.ecore.EReference && referenceName != null && referenceName.equals("target")) {
				eRemovePlayerStatementTargetReferenceResolver.resolve(identifier, (textadventure.ERemovePlayerStatement) container, (org.eclipse.emf.ecore.EReference) feature, position, true, frr);
			}
		}
		if (textadventure.TextadventurePackage.eINSTANCE.getEIgnoreItemAbility().isInstance(container)) {
			TextadventureFuzzyResolveResult<textadventure.EItem> frr = new TextadventureFuzzyResolveResult<textadventure.EItem>(result);
			String referenceName = reference.getName();
			org.eclipse.emf.ecore.EStructuralFeature feature = container.eClass().getEStructuralFeature(referenceName);
			if (feature != null && feature instanceof org.eclipse.emf.ecore.EReference && referenceName != null && referenceName.equals("ignoredItem")) {
				eIgnoreItemAbilityIgnoredItemReferenceResolver.resolve(identifier, (textadventure.EIgnoreItemAbility) container, (org.eclipse.emf.ecore.EReference) feature, position, true, frr);
			}
		}
	}
	
	public textadventure.resource.textadventure.ITextadventureReferenceResolver<? extends org.eclipse.emf.ecore.EObject, ? extends org.eclipse.emf.ecore.EObject> getResolver(org.eclipse.emf.ecore.EStructuralFeature reference) {
		if (reference == textadventure.TextadventurePackage.eINSTANCE.getEWorld_PlayerPerson()) {
			return getResolverChain(reference, eWorldPlayerPersonReferenceResolver);
		}
		if (reference == textadventure.TextadventurePackage.eINSTANCE.getEItem_Location()) {
			return getResolverChain(reference, eItemLocationReferenceResolver);
		}
		if (reference == textadventure.TextadventurePackage.eINSTANCE.getECheckPoint_Point()) {
			return getResolverChain(reference, eCheckPointPointReferenceResolver);
		}
		if (reference == textadventure.TextadventurePackage.eINSTANCE.getEDestination_Point()) {
			return getResolverChain(reference, eDestinationPointReferenceResolver);
		}
		if (reference == textadventure.TextadventurePackage.eINSTANCE.getERemoveAction_What()) {
			return getResolverChain(reference, eRemoveActionWhatReferenceResolver);
		}
		if (reference == textadventure.TextadventurePackage.eINSTANCE.getEPerson_Location()) {
			return getResolverChain(reference, ePersonLocationReferenceResolver);
		}
		if (reference == textadventure.TextadventurePackage.eINSTANCE.getEBattle_Location()) {
			return getResolverChain(reference, eBattleLocationReferenceResolver);
		}
		if (reference == textadventure.TextadventurePackage.eINSTANCE.getEBattle_Enemy()) {
			return getResolverChain(reference, eBattleEnemyReferenceResolver);
		}
		if (reference == textadventure.TextadventurePackage.eINSTANCE.getERemovePlayerStatement_Target()) {
			return getResolverChain(reference, eRemovePlayerStatementTargetReferenceResolver);
		}
		if (reference == textadventure.TextadventurePackage.eINSTANCE.getEIgnoreItemAbility_IgnoredItem()) {
			return getResolverChain(reference, eIgnoreItemAbilityIgnoredItemReferenceResolver);
		}
		return null;
	}
	
	@SuppressWarnings({"rawtypes", "unchecked"})	
	public <ContainerType extends org.eclipse.emf.ecore.EObject, ReferenceType extends org.eclipse.emf.ecore.EObject> textadventure.resource.textadventure.ITextadventureReferenceResolver<ContainerType, ReferenceType> getResolverChain(org.eclipse.emf.ecore.EStructuralFeature reference, textadventure.resource.textadventure.ITextadventureReferenceResolver<ContainerType, ReferenceType> originalResolver) {
		if (options == null) {
			return originalResolver;
		}
		Object value = options.get(textadventure.resource.textadventure.ITextadventureOptions.ADDITIONAL_REFERENCE_RESOLVERS);
		if (value == null) {
			return originalResolver;
		}
		if (!(value instanceof java.util.Map)) {
			// send this to the error log
			new textadventure.resource.textadventure.util.TextadventureRuntimeUtil().logWarning("Found value with invalid type for option " + textadventure.resource.textadventure.ITextadventureOptions.ADDITIONAL_REFERENCE_RESOLVERS + " (expected " + java.util.Map.class.getName() + ", but was " + value.getClass().getName() + ")", null);
			return originalResolver;
		}
		java.util.Map<?,?> resolverMap = (java.util.Map<?,?>) value;
		Object resolverValue = resolverMap.get(reference);
		if (resolverValue == null) {
			return originalResolver;
		}
		if (resolverValue instanceof textadventure.resource.textadventure.ITextadventureReferenceResolver) {
			textadventure.resource.textadventure.ITextadventureReferenceResolver replacingResolver = (textadventure.resource.textadventure.ITextadventureReferenceResolver) resolverValue;
			if (replacingResolver instanceof textadventure.resource.textadventure.ITextadventureDelegatingReferenceResolver) {
				// pass original resolver to the replacing one
				((textadventure.resource.textadventure.ITextadventureDelegatingReferenceResolver) replacingResolver).setDelegate(originalResolver);
			}
			return replacingResolver;
		} else if (resolverValue instanceof java.util.Collection) {
			java.util.Collection replacingResolvers = (java.util.Collection) resolverValue;
			textadventure.resource.textadventure.ITextadventureReferenceResolver replacingResolver = originalResolver;
			for (Object next : replacingResolvers) {
				if (next instanceof textadventure.resource.textadventure.ITextadventureReferenceCache) {
					textadventure.resource.textadventure.ITextadventureReferenceResolver nextResolver = (textadventure.resource.textadventure.ITextadventureReferenceResolver) next;
					if (nextResolver instanceof textadventure.resource.textadventure.ITextadventureDelegatingReferenceResolver) {
						// pass original resolver to the replacing one
						((textadventure.resource.textadventure.ITextadventureDelegatingReferenceResolver) nextResolver).setDelegate(replacingResolver);
					}
					replacingResolver = nextResolver;
				} else {
					// The collection contains a non-resolver. Send a warning to the error log.
					new textadventure.resource.textadventure.util.TextadventureRuntimeUtil().logWarning("Found value with invalid type in value map for option " + textadventure.resource.textadventure.ITextadventureOptions.ADDITIONAL_REFERENCE_RESOLVERS + " (expected " + textadventure.resource.textadventure.ITextadventureDelegatingReferenceResolver.class.getName() + ", but was " + next.getClass().getName() + ")", null);
				}
			}
			return replacingResolver;
		} else {
			// The value for the option ADDITIONAL_REFERENCE_RESOLVERS has an unknown type.
			new textadventure.resource.textadventure.util.TextadventureRuntimeUtil().logWarning("Found value with invalid type in value map for option " + textadventure.resource.textadventure.ITextadventureOptions.ADDITIONAL_REFERENCE_RESOLVERS + " (expected " + textadventure.resource.textadventure.ITextadventureDelegatingReferenceResolver.class.getName() + ", but was " + resolverValue.getClass().getName() + ")", null);
			return originalResolver;
		}
	}
	
}
