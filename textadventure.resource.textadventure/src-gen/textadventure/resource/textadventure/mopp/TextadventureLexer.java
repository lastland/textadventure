// $ANTLR 3.4

	package textadventure.resource.textadventure.mopp;


import org.antlr.runtime3_4_0.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked"})
public class TextadventureLexer extends Lexer {
    public static final int EOF=-1;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__19=19;
    public static final int T__20=20;
    public static final int T__21=21;
    public static final int T__22=22;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int T__29=29;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__42=42;
    public static final int COMMENT=4;
    public static final int FLOAT=5;
    public static final int INTEGER=6;
    public static final int LINEBREAK=7;
    public static final int QUOTED_34_34=8;
    public static final int TEXT=9;
    public static final int WHITESPACE=10;

    	public java.util.List<org.antlr.runtime3_4_0.RecognitionException> lexerExceptions  = new java.util.ArrayList<org.antlr.runtime3_4_0.RecognitionException>();
    	public java.util.List<Integer> lexerExceptionsPosition = new java.util.ArrayList<Integer>();
    	
    	public void reportError(org.antlr.runtime3_4_0.RecognitionException e) {
    		lexerExceptions.add(e);
    		lexerExceptionsPosition.add(((org.antlr.runtime3_4_0.ANTLRStringStream) input).index());
    	}


    // delegates
    // delegators
    public Lexer[] getDelegates() {
        return new Lexer[] {};
    }

    public TextadventureLexer() {} 
    public TextadventureLexer(CharStream input) {
        this(input, new RecognizerSharedState());
    }
    public TextadventureLexer(CharStream input, RecognizerSharedState state) {
        super(input,state);
    }
    public String getGrammarFileName() { return "Textadventure.g"; }

    // $ANTLR start "T__11"
    public final void mT__11() throws RecognitionException {
        try {
            int _type = T__11;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Textadventure.g:15:7: ( ')' )
            // Textadventure.g:15:9: ')'
            {
            match(')'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__11"

    // $ANTLR start "T__12"
    public final void mT__12() throws RecognitionException {
        try {
            int _type = T__12;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Textadventure.g:16:7: ( ',' )
            // Textadventure.g:16:9: ','
            {
            match(','); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__12"

    // $ANTLR start "T__13"
    public final void mT__13() throws RecognitionException {
        try {
            int _type = T__13;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Textadventure.g:17:7: ( ';' )
            // Textadventure.g:17:9: ';'
            {
            match(';'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__13"

    // $ANTLR start "T__14"
    public final void mT__14() throws RecognitionException {
        try {
            int _type = T__14;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Textadventure.g:18:7: ( '=' )
            // Textadventure.g:18:9: '='
            {
            match('='); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__14"

    // $ANTLR start "T__15"
    public final void mT__15() throws RecognitionException {
        try {
            int _type = T__15;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Textadventure.g:19:7: ( '@' )
            // Textadventure.g:19:9: '@'
            {
            match('@'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__15"

    // $ANTLR start "T__16"
    public final void mT__16() throws RecognitionException {
        try {
            int _type = T__16;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Textadventure.g:20:7: ( 'Items' )
            // Textadventure.g:20:9: 'Items'
            {
            match("Items"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__16"

    // $ANTLR start "T__17"
    public final void mT__17() throws RecognitionException {
        try {
            int _type = T__17;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Textadventure.g:21:7: ( 'Persons' )
            // Textadventure.g:21:9: 'Persons'
            {
            match("Persons"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__17"

    // $ANTLR start "T__18"
    public final void mT__18() throws RecognitionException {
        try {
            int _type = T__18;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Textadventure.g:22:7: ( 'Tile' )
            // Textadventure.g:22:9: 'Tile'
            {
            match("Tile"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__18"

    // $ANTLR start "T__19"
    public final void mT__19() throws RecognitionException {
        try {
            int _type = T__19;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Textadventure.g:23:7: ( 'attack' )
            // Textadventure.g:23:9: 'attack'
            {
            match("attack"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__19"

    // $ANTLR start "T__20"
    public final void mT__20() throws RecognitionException {
        try {
            int _type = T__20;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Textadventure.g:24:7: ( 'battle' )
            // Textadventure.g:24:9: 'battle'
            {
            match("battle"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__20"

    // $ANTLR start "T__21"
    public final void mT__21() throws RecognitionException {
        try {
            int _type = T__21;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Textadventure.g:25:7: ( 'checkpoint' )
            // Textadventure.g:25:9: 'checkpoint'
            {
            match("checkpoint"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__21"

    // $ANTLR start "T__22"
    public final void mT__22() throws RecognitionException {
        try {
            int _type = T__22;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Textadventure.g:26:7: ( 'defense' )
            // Textadventure.g:26:9: 'defense'
            {
            match("defense"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__22"

    // $ANTLR start "T__23"
    public final void mT__23() throws RecognitionException {
        try {
            int _type = T__23;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Textadventure.g:27:7: ( 'description' )
            // Textadventure.g:27:9: 'description'
            {
            match("description"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__23"

    // $ANTLR start "T__24"
    public final void mT__24() throws RecognitionException {
        try {
            int _type = T__24;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Textadventure.g:28:7: ( 'destination' )
            // Textadventure.g:28:9: 'destination'
            {
            match("destination"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__24"

    // $ANTLR start "T__25"
    public final void mT__25() throws RecognitionException {
        try {
            int _type = T__25;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Textadventure.g:29:7: ( 'enemy' )
            // Textadventure.g:29:9: 'enemy'
            {
            match("enemy"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__25"

    // $ANTLR start "T__26"
    public final void mT__26() throws RecognitionException {
        try {
            int _type = T__26;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Textadventure.g:30:7: ( 'from' )
            // Textadventure.g:30:9: 'from'
            {
            match("from"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__26"

    // $ANTLR start "T__27"
    public final void mT__27() throws RecognitionException {
        try {
            int _type = T__27;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Textadventure.g:31:7: ( 'function(' )
            // Textadventure.g:31:9: 'function('
            {
            match("function("); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__27"

    // $ANTLR start "T__28"
    public final void mT__28() throws RecognitionException {
        try {
            int _type = T__28;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Textadventure.g:32:7: ( 'hp' )
            // Textadventure.g:32:9: 'hp'
            {
            match("hp"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__28"

    // $ANTLR start "T__29"
    public final void mT__29() throws RecognitionException {
        try {
            int _type = T__29;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Textadventure.g:33:7: ( 'ignore' )
            // Textadventure.g:33:9: 'ignore'
            {
            match("ignore"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__29"

    // $ANTLR start "T__30"
    public final void mT__30() throws RecognitionException {
        try {
            int _type = T__30;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Textadventure.g:34:7: ( 'item' )
            // Textadventure.g:34:9: 'item'
            {
            match("item"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__30"

    // $ANTLR start "T__31"
    public final void mT__31() throws RecognitionException {
        try {
            int _type = T__31;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Textadventure.g:35:7: ( 'on' )
            // Textadventure.g:35:9: 'on'
            {
            match("on"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__31"

    // $ANTLR start "T__32"
    public final void mT__32() throws RecognitionException {
        try {
            int _type = T__32;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Textadventure.g:36:7: ( 'person' )
            // Textadventure.g:36:9: 'person'
            {
            match("person"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__32"

    // $ANTLR start "T__33"
    public final void mT__33() throws RecognitionException {
        try {
            int _type = T__33;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Textadventure.g:37:7: ( 'player' )
            // Textadventure.g:37:9: 'player'
            {
            match("player"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__33"

    // $ANTLR start "T__34"
    public final void mT__34() throws RecognitionException {
        try {
            int _type = T__34;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Textadventure.g:38:7: ( 'print' )
            // Textadventure.g:38:9: 'print'
            {
            match("print"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__34"

    // $ANTLR start "T__35"
    public final void mT__35() throws RecognitionException {
        try {
            int _type = T__35;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Textadventure.g:39:7: ( 'remove' )
            // Textadventure.g:39:9: 'remove'
            {
            match("remove"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__35"

    // $ANTLR start "T__36"
    public final void mT__36() throws RecognitionException {
        try {
            int _type = T__36;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Textadventure.g:40:7: ( 'tile' )
            // Textadventure.g:40:9: 'tile'
            {
            match("tile"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__36"

    // $ANTLR start "T__37"
    public final void mT__37() throws RecognitionException {
        try {
            int _type = T__37;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Textadventure.g:41:7: ( 'used' )
            // Textadventure.g:41:9: 'used'
            {
            match("used"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__37"

    // $ANTLR start "T__38"
    public final void mT__38() throws RecognitionException {
        try {
            int _type = T__38;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Textadventure.g:42:7: ( 'while' )
            // Textadventure.g:42:9: 'while'
            {
            match("while"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__38"

    // $ANTLR start "T__39"
    public final void mT__39() throws RecognitionException {
        try {
            int _type = T__39;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Textadventure.g:43:7: ( 'x' )
            // Textadventure.g:43:9: 'x'
            {
            match('x'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__39"

    // $ANTLR start "T__40"
    public final void mT__40() throws RecognitionException {
        try {
            int _type = T__40;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Textadventure.g:44:7: ( 'y' )
            // Textadventure.g:44:9: 'y'
            {
            match('y'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__40"

    // $ANTLR start "T__41"
    public final void mT__41() throws RecognitionException {
        try {
            int _type = T__41;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Textadventure.g:45:7: ( '{' )
            // Textadventure.g:45:9: '{'
            {
            match('{'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__41"

    // $ANTLR start "T__42"
    public final void mT__42() throws RecognitionException {
        try {
            int _type = T__42;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Textadventure.g:46:7: ( '}' )
            // Textadventure.g:46:9: '}'
            {
            match('}'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__42"

    // $ANTLR start "COMMENT"
    public final void mCOMMENT() throws RecognitionException {
        try {
            int _type = COMMENT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Textadventure.g:3041:8: ( ( '//' (~ ( '\\n' | '\\r' | '\\uffff' ) )* ) )
            // Textadventure.g:3042:2: ( '//' (~ ( '\\n' | '\\r' | '\\uffff' ) )* )
            {
            // Textadventure.g:3042:2: ( '//' (~ ( '\\n' | '\\r' | '\\uffff' ) )* )
            // Textadventure.g:3042:3: '//' (~ ( '\\n' | '\\r' | '\\uffff' ) )*
            {
            match("//"); 



            // Textadventure.g:3042:7: (~ ( '\\n' | '\\r' | '\\uffff' ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( ((LA1_0 >= '\u0000' && LA1_0 <= '\t')||(LA1_0 >= '\u000B' && LA1_0 <= '\f')||(LA1_0 >= '\u000E' && LA1_0 <= '\uFFFE')) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // Textadventure.g:
            	    {
            	    if ( (input.LA(1) >= '\u0000' && input.LA(1) <= '\t')||(input.LA(1) >= '\u000B' && input.LA(1) <= '\f')||(input.LA(1) >= '\u000E' && input.LA(1) <= '\uFFFE') ) {
            	        input.consume();
            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);


            }


             _channel = 99; 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "COMMENT"

    // $ANTLR start "INTEGER"
    public final void mINTEGER() throws RecognitionException {
        try {
            int _type = INTEGER;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Textadventure.g:3045:8: ( ( ( '-' )? ( '1' .. '9' ) ( '0' .. '9' )* | '0' ) )
            // Textadventure.g:3046:2: ( ( '-' )? ( '1' .. '9' ) ( '0' .. '9' )* | '0' )
            {
            // Textadventure.g:3046:2: ( ( '-' )? ( '1' .. '9' ) ( '0' .. '9' )* | '0' )
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0=='-'||(LA4_0 >= '1' && LA4_0 <= '9')) ) {
                alt4=1;
            }
            else if ( (LA4_0=='0') ) {
                alt4=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;

            }
            switch (alt4) {
                case 1 :
                    // Textadventure.g:3046:3: ( '-' )? ( '1' .. '9' ) ( '0' .. '9' )*
                    {
                    // Textadventure.g:3046:3: ( '-' )?
                    int alt2=2;
                    int LA2_0 = input.LA(1);

                    if ( (LA2_0=='-') ) {
                        alt2=1;
                    }
                    switch (alt2) {
                        case 1 :
                            // Textadventure.g:3046:4: '-'
                            {
                            match('-'); 

                            }
                            break;

                    }


                    if ( (input.LA(1) >= '1' && input.LA(1) <= '9') ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    // Textadventure.g:3046:19: ( '0' .. '9' )*
                    loop3:
                    do {
                        int alt3=2;
                        int LA3_0 = input.LA(1);

                        if ( ((LA3_0 >= '0' && LA3_0 <= '9')) ) {
                            alt3=1;
                        }


                        switch (alt3) {
                    	case 1 :
                    	    // Textadventure.g:
                    	    {
                    	    if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
                    	        input.consume();
                    	    }
                    	    else {
                    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	        recover(mse);
                    	        throw mse;
                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop3;
                        }
                    } while (true);


                    }
                    break;
                case 2 :
                    // Textadventure.g:3046:31: '0'
                    {
                    match('0'); 

                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "INTEGER"

    // $ANTLR start "FLOAT"
    public final void mFLOAT() throws RecognitionException {
        try {
            int _type = FLOAT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Textadventure.g:3048:6: ( ( ( '-' )? ( ( '1' .. '9' ) ( '0' .. '9' )* | '0' ) '.' ( '0' .. '9' )+ ) )
            // Textadventure.g:3049:2: ( ( '-' )? ( ( '1' .. '9' ) ( '0' .. '9' )* | '0' ) '.' ( '0' .. '9' )+ )
            {
            // Textadventure.g:3049:2: ( ( '-' )? ( ( '1' .. '9' ) ( '0' .. '9' )* | '0' ) '.' ( '0' .. '9' )+ )
            // Textadventure.g:3049:3: ( '-' )? ( ( '1' .. '9' ) ( '0' .. '9' )* | '0' ) '.' ( '0' .. '9' )+
            {
            // Textadventure.g:3049:3: ( '-' )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0=='-') ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // Textadventure.g:3049:4: '-'
                    {
                    match('-'); 

                    }
                    break;

            }


            // Textadventure.g:3049:9: ( ( '1' .. '9' ) ( '0' .. '9' )* | '0' )
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( ((LA7_0 >= '1' && LA7_0 <= '9')) ) {
                alt7=1;
            }
            else if ( (LA7_0=='0') ) {
                alt7=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 7, 0, input);

                throw nvae;

            }
            switch (alt7) {
                case 1 :
                    // Textadventure.g:3049:10: ( '1' .. '9' ) ( '0' .. '9' )*
                    {
                    if ( (input.LA(1) >= '1' && input.LA(1) <= '9') ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    // Textadventure.g:3049:21: ( '0' .. '9' )*
                    loop6:
                    do {
                        int alt6=2;
                        int LA6_0 = input.LA(1);

                        if ( ((LA6_0 >= '0' && LA6_0 <= '9')) ) {
                            alt6=1;
                        }


                        switch (alt6) {
                    	case 1 :
                    	    // Textadventure.g:
                    	    {
                    	    if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
                    	        input.consume();
                    	    }
                    	    else {
                    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	        recover(mse);
                    	        throw mse;
                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop6;
                        }
                    } while (true);


                    }
                    break;
                case 2 :
                    // Textadventure.g:3049:35: '0'
                    {
                    match('0'); 

                    }
                    break;

            }


            match('.'); 

            // Textadventure.g:3049:44: ( '0' .. '9' )+
            int cnt8=0;
            loop8:
            do {
                int alt8=2;
                int LA8_0 = input.LA(1);

                if ( ((LA8_0 >= '0' && LA8_0 <= '9')) ) {
                    alt8=1;
                }


                switch (alt8) {
            	case 1 :
            	    // Textadventure.g:
            	    {
            	    if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
            	        input.consume();
            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt8 >= 1 ) break loop8;
                        EarlyExitException eee =
                            new EarlyExitException(8, input);
                        throw eee;
                }
                cnt8++;
            } while (true);


            }


             _channel = 99; 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "FLOAT"

    // $ANTLR start "TEXT"
    public final void mTEXT() throws RecognitionException {
        try {
            int _type = TEXT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Textadventure.g:3052:5: ( ( ( 'A' .. 'Z' | 'a' .. 'z' | '0' .. '9' | '_' | '-' )+ ) )
            // Textadventure.g:3053:2: ( ( 'A' .. 'Z' | 'a' .. 'z' | '0' .. '9' | '_' | '-' )+ )
            {
            // Textadventure.g:3053:2: ( ( 'A' .. 'Z' | 'a' .. 'z' | '0' .. '9' | '_' | '-' )+ )
            // Textadventure.g:3053:3: ( 'A' .. 'Z' | 'a' .. 'z' | '0' .. '9' | '_' | '-' )+
            {
            // Textadventure.g:3053:3: ( 'A' .. 'Z' | 'a' .. 'z' | '0' .. '9' | '_' | '-' )+
            int cnt9=0;
            loop9:
            do {
                int alt9=2;
                int LA9_0 = input.LA(1);

                if ( (LA9_0=='-'||(LA9_0 >= '0' && LA9_0 <= '9')||(LA9_0 >= 'A' && LA9_0 <= 'Z')||LA9_0=='_'||(LA9_0 >= 'a' && LA9_0 <= 'z')) ) {
                    alt9=1;
                }


                switch (alt9) {
            	case 1 :
            	    // Textadventure.g:
            	    {
            	    if ( input.LA(1)=='-'||(input.LA(1) >= '0' && input.LA(1) <= '9')||(input.LA(1) >= 'A' && input.LA(1) <= 'Z')||input.LA(1)=='_'||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
            	        input.consume();
            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt9 >= 1 ) break loop9;
                        EarlyExitException eee =
                            new EarlyExitException(9, input);
                        throw eee;
                }
                cnt9++;
            } while (true);


            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "TEXT"

    // $ANTLR start "WHITESPACE"
    public final void mWHITESPACE() throws RecognitionException {
        try {
            int _type = WHITESPACE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Textadventure.g:3055:11: ( ( ( ' ' | '\\t' | '\\f' ) ) )
            // Textadventure.g:3056:2: ( ( ' ' | '\\t' | '\\f' ) )
            {
            if ( input.LA(1)=='\t'||input.LA(1)=='\f'||input.LA(1)==' ' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


             _channel = 99; 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "WHITESPACE"

    // $ANTLR start "LINEBREAK"
    public final void mLINEBREAK() throws RecognitionException {
        try {
            int _type = LINEBREAK;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Textadventure.g:3059:10: ( ( ( '\\r\\n' | '\\r' | '\\n' ) ) )
            // Textadventure.g:3060:2: ( ( '\\r\\n' | '\\r' | '\\n' ) )
            {
            // Textadventure.g:3060:2: ( ( '\\r\\n' | '\\r' | '\\n' ) )
            // Textadventure.g:3060:3: ( '\\r\\n' | '\\r' | '\\n' )
            {
            // Textadventure.g:3060:3: ( '\\r\\n' | '\\r' | '\\n' )
            int alt10=3;
            int LA10_0 = input.LA(1);

            if ( (LA10_0=='\r') ) {
                int LA10_1 = input.LA(2);

                if ( (LA10_1=='\n') ) {
                    alt10=1;
                }
                else {
                    alt10=2;
                }
            }
            else if ( (LA10_0=='\n') ) {
                alt10=3;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 10, 0, input);

                throw nvae;

            }
            switch (alt10) {
                case 1 :
                    // Textadventure.g:3060:4: '\\r\\n'
                    {
                    match("\r\n"); 



                    }
                    break;
                case 2 :
                    // Textadventure.g:3060:13: '\\r'
                    {
                    match('\r'); 

                    }
                    break;
                case 3 :
                    // Textadventure.g:3060:20: '\\n'
                    {
                    match('\n'); 

                    }
                    break;

            }


            }


             _channel = 99; 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "LINEBREAK"

    // $ANTLR start "QUOTED_34_34"
    public final void mQUOTED_34_34() throws RecognitionException {
        try {
            int _type = QUOTED_34_34;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Textadventure.g:3063:13: ( ( ( '\"' ) (~ ( '\"' ) )* ( '\"' ) ) )
            // Textadventure.g:3064:2: ( ( '\"' ) (~ ( '\"' ) )* ( '\"' ) )
            {
            // Textadventure.g:3064:2: ( ( '\"' ) (~ ( '\"' ) )* ( '\"' ) )
            // Textadventure.g:3064:3: ( '\"' ) (~ ( '\"' ) )* ( '\"' )
            {
            // Textadventure.g:3064:3: ( '\"' )
            // Textadventure.g:3064:4: '\"'
            {
            match('\"'); 

            }


            // Textadventure.g:3064:8: (~ ( '\"' ) )*
            loop11:
            do {
                int alt11=2;
                int LA11_0 = input.LA(1);

                if ( ((LA11_0 >= '\u0000' && LA11_0 <= '!')||(LA11_0 >= '#' && LA11_0 <= '\uFFFF')) ) {
                    alt11=1;
                }


                switch (alt11) {
            	case 1 :
            	    // Textadventure.g:
            	    {
            	    if ( (input.LA(1) >= '\u0000' && input.LA(1) <= '!')||(input.LA(1) >= '#' && input.LA(1) <= '\uFFFF') ) {
            	        input.consume();
            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    break loop11;
                }
            } while (true);


            // Textadventure.g:3064:17: ( '\"' )
            // Textadventure.g:3064:18: '\"'
            {
            match('\"'); 

            }


            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "QUOTED_34_34"

    public void mTokens() throws RecognitionException {
        // Textadventure.g:1:8: ( T__11 | T__12 | T__13 | T__14 | T__15 | T__16 | T__17 | T__18 | T__19 | T__20 | T__21 | T__22 | T__23 | T__24 | T__25 | T__26 | T__27 | T__28 | T__29 | T__30 | T__31 | T__32 | T__33 | T__34 | T__35 | T__36 | T__37 | T__38 | T__39 | T__40 | T__41 | T__42 | COMMENT | INTEGER | FLOAT | TEXT | WHITESPACE | LINEBREAK | QUOTED_34_34 )
        int alt12=39;
        alt12 = dfa12.predict(input);
        switch (alt12) {
            case 1 :
                // Textadventure.g:1:10: T__11
                {
                mT__11(); 


                }
                break;
            case 2 :
                // Textadventure.g:1:16: T__12
                {
                mT__12(); 


                }
                break;
            case 3 :
                // Textadventure.g:1:22: T__13
                {
                mT__13(); 


                }
                break;
            case 4 :
                // Textadventure.g:1:28: T__14
                {
                mT__14(); 


                }
                break;
            case 5 :
                // Textadventure.g:1:34: T__15
                {
                mT__15(); 


                }
                break;
            case 6 :
                // Textadventure.g:1:40: T__16
                {
                mT__16(); 


                }
                break;
            case 7 :
                // Textadventure.g:1:46: T__17
                {
                mT__17(); 


                }
                break;
            case 8 :
                // Textadventure.g:1:52: T__18
                {
                mT__18(); 


                }
                break;
            case 9 :
                // Textadventure.g:1:58: T__19
                {
                mT__19(); 


                }
                break;
            case 10 :
                // Textadventure.g:1:64: T__20
                {
                mT__20(); 


                }
                break;
            case 11 :
                // Textadventure.g:1:70: T__21
                {
                mT__21(); 


                }
                break;
            case 12 :
                // Textadventure.g:1:76: T__22
                {
                mT__22(); 


                }
                break;
            case 13 :
                // Textadventure.g:1:82: T__23
                {
                mT__23(); 


                }
                break;
            case 14 :
                // Textadventure.g:1:88: T__24
                {
                mT__24(); 


                }
                break;
            case 15 :
                // Textadventure.g:1:94: T__25
                {
                mT__25(); 


                }
                break;
            case 16 :
                // Textadventure.g:1:100: T__26
                {
                mT__26(); 


                }
                break;
            case 17 :
                // Textadventure.g:1:106: T__27
                {
                mT__27(); 


                }
                break;
            case 18 :
                // Textadventure.g:1:112: T__28
                {
                mT__28(); 


                }
                break;
            case 19 :
                // Textadventure.g:1:118: T__29
                {
                mT__29(); 


                }
                break;
            case 20 :
                // Textadventure.g:1:124: T__30
                {
                mT__30(); 


                }
                break;
            case 21 :
                // Textadventure.g:1:130: T__31
                {
                mT__31(); 


                }
                break;
            case 22 :
                // Textadventure.g:1:136: T__32
                {
                mT__32(); 


                }
                break;
            case 23 :
                // Textadventure.g:1:142: T__33
                {
                mT__33(); 


                }
                break;
            case 24 :
                // Textadventure.g:1:148: T__34
                {
                mT__34(); 


                }
                break;
            case 25 :
                // Textadventure.g:1:154: T__35
                {
                mT__35(); 


                }
                break;
            case 26 :
                // Textadventure.g:1:160: T__36
                {
                mT__36(); 


                }
                break;
            case 27 :
                // Textadventure.g:1:166: T__37
                {
                mT__37(); 


                }
                break;
            case 28 :
                // Textadventure.g:1:172: T__38
                {
                mT__38(); 


                }
                break;
            case 29 :
                // Textadventure.g:1:178: T__39
                {
                mT__39(); 


                }
                break;
            case 30 :
                // Textadventure.g:1:184: T__40
                {
                mT__40(); 


                }
                break;
            case 31 :
                // Textadventure.g:1:190: T__41
                {
                mT__41(); 


                }
                break;
            case 32 :
                // Textadventure.g:1:196: T__42
                {
                mT__42(); 


                }
                break;
            case 33 :
                // Textadventure.g:1:202: COMMENT
                {
                mCOMMENT(); 


                }
                break;
            case 34 :
                // Textadventure.g:1:210: INTEGER
                {
                mINTEGER(); 


                }
                break;
            case 35 :
                // Textadventure.g:1:218: FLOAT
                {
                mFLOAT(); 


                }
                break;
            case 36 :
                // Textadventure.g:1:224: TEXT
                {
                mTEXT(); 


                }
                break;
            case 37 :
                // Textadventure.g:1:229: WHITESPACE
                {
                mWHITESPACE(); 


                }
                break;
            case 38 :
                // Textadventure.g:1:240: LINEBREAK
                {
                mLINEBREAK(); 


                }
                break;
            case 39 :
                // Textadventure.g:1:250: QUOTED_34_34
                {
                mQUOTED_34_34(); 


                }
                break;

        }

    }


    protected DFA12 dfa12 = new DFA12(this);
    static final String DFA12_eotS =
        "\6\uffff\21\37\1\70\1\71\3\uffff\1\37\2\74\4\uffff\12\37\1\111\2"+
        "\37\1\114\7\37\2\uffff\1\37\1\74\2\uffff\13\37\1\uffff\2\37\1\uffff"+
        "\11\37\1\153\7\37\1\163\2\37\1\166\4\37\1\173\1\174\1\37\1\176\1"+
        "\37\1\uffff\6\37\1\u0086\1\uffff\2\37\1\uffff\2\37\1\u008b\1\37"+
        "\2\uffff\1\u008d\1\uffff\1\37\1\u008f\1\u0090\4\37\1\uffff\1\37"+
        "\1\u0096\1\u0097\1\u0098\1\uffff\1\u0099\1\uffff\1\u009a\2\uffff"+
        "\1\37\1\u009c\3\37\5\uffff\1\37\1\uffff\6\37\1\uffff\1\u00a7\2\37"+
        "\1\uffff\1\u00aa\1\u00ab\2\uffff";
    static final String DFA12_eofS =
        "\u00ac\uffff";
    static final String DFA12_minS =
        "\1\11\5\uffff\1\164\1\145\1\151\1\164\1\141\1\150\1\145\1\156\1"+
        "\162\1\160\1\147\1\156\2\145\1\151\1\163\1\150\2\55\3\uffff\1\60"+
        "\2\55\4\uffff\1\145\1\162\1\154\2\164\1\145\1\146\1\145\1\157\1"+
        "\156\1\55\1\156\1\145\1\55\1\162\1\141\1\151\1\155\1\154\1\145\1"+
        "\151\2\uffff\1\56\1\55\2\uffff\1\155\1\163\1\145\1\141\1\164\1\143"+
        "\1\145\1\143\2\155\1\143\1\uffff\1\157\1\155\1\uffff\1\163\1\171"+
        "\1\156\1\157\1\145\1\144\1\154\1\163\1\157\1\55\1\143\1\154\1\153"+
        "\1\156\1\162\1\151\1\171\1\55\1\164\1\162\1\55\1\157\1\145\1\164"+
        "\1\166\2\55\1\145\1\55\1\156\1\uffff\1\153\1\145\1\160\1\163\1\151"+
        "\1\156\1\55\1\uffff\1\151\1\145\1\uffff\1\156\1\162\1\55\1\145\2"+
        "\uffff\1\55\1\uffff\1\163\2\55\1\157\1\145\1\160\1\141\1\uffff\1"+
        "\157\3\55\1\uffff\1\55\1\uffff\1\55\2\uffff\1\151\1\55\2\164\1\156"+
        "\5\uffff\1\156\1\uffff\2\151\1\50\1\164\2\157\1\uffff\1\55\2\156"+
        "\1\uffff\2\55\2\uffff";
    static final String DFA12_maxS =
        "\1\175\5\uffff\1\164\1\145\1\151\1\164\1\141\1\150\1\145\1\156\1"+
        "\165\1\160\1\164\1\156\1\162\1\145\1\151\1\163\1\150\2\172\3\uffff"+
        "\1\71\2\172\4\uffff\1\145\1\162\1\154\2\164\1\145\1\163\1\145\1"+
        "\157\1\156\1\172\1\156\1\145\1\172\1\162\1\141\1\151\1\155\1\154"+
        "\1\145\1\151\2\uffff\1\56\1\172\2\uffff\1\155\1\163\1\145\1\141"+
        "\1\164\1\143\1\145\1\164\2\155\1\143\1\uffff\1\157\1\155\1\uffff"+
        "\1\163\1\171\1\156\1\157\1\145\1\144\1\154\1\163\1\157\1\172\1\143"+
        "\1\154\1\153\1\156\1\162\1\151\1\171\1\172\1\164\1\162\1\172\1\157"+
        "\1\145\1\164\1\166\2\172\1\145\1\172\1\156\1\uffff\1\153\1\145\1"+
        "\160\1\163\1\151\1\156\1\172\1\uffff\1\151\1\145\1\uffff\1\156\1"+
        "\162\1\172\1\145\2\uffff\1\172\1\uffff\1\163\2\172\1\157\1\145\1"+
        "\160\1\141\1\uffff\1\157\3\172\1\uffff\1\172\1\uffff\1\172\2\uffff"+
        "\1\151\1\172\2\164\1\156\5\uffff\1\156\1\uffff\2\151\1\50\1\164"+
        "\2\157\1\uffff\1\172\2\156\1\uffff\2\172\2\uffff";
    static final String DFA12_acceptS =
        "\1\uffff\1\1\1\2\1\3\1\4\1\5\23\uffff\1\37\1\40\1\41\3\uffff\1\44"+
        "\1\45\1\46\1\47\25\uffff\1\35\1\36\2\uffff\1\42\1\43\13\uffff\1"+
        "\22\2\uffff\1\25\36\uffff\1\10\7\uffff\1\20\2\uffff\1\24\4\uffff"+
        "\1\32\1\33\1\uffff\1\6\7\uffff\1\17\4\uffff\1\30\1\uffff\1\34\1"+
        "\uffff\1\11\1\12\5\uffff\1\23\1\26\1\27\1\31\1\7\1\uffff\1\14\6"+
        "\uffff\1\21\3\uffff\1\13\2\uffff\1\15\1\16";
    static final String DFA12_specialS =
        "\u00ac\uffff}>";
    static final String[] DFA12_transitionS = {
            "\1\40\1\41\1\uffff\1\40\1\41\22\uffff\1\40\1\uffff\1\42\6\uffff"+
            "\1\1\2\uffff\1\2\1\34\1\uffff\1\33\1\36\11\35\1\uffff\1\3\1"+
            "\uffff\1\4\2\uffff\1\5\10\37\1\6\6\37\1\7\3\37\1\10\6\37\4\uffff"+
            "\1\37\1\uffff\1\11\1\12\1\13\1\14\1\15\1\16\1\37\1\17\1\20\5"+
            "\37\1\21\1\22\1\37\1\23\1\37\1\24\1\25\1\37\1\26\1\27\1\30\1"+
            "\37\1\31\1\uffff\1\32",
            "",
            "",
            "",
            "",
            "",
            "\1\43",
            "\1\44",
            "\1\45",
            "\1\46",
            "\1\47",
            "\1\50",
            "\1\51",
            "\1\52",
            "\1\53\2\uffff\1\54",
            "\1\55",
            "\1\56\14\uffff\1\57",
            "\1\60",
            "\1\61\6\uffff\1\62\5\uffff\1\63",
            "\1\64",
            "\1\65",
            "\1\66",
            "\1\67",
            "\1\37\2\uffff\12\37\7\uffff\32\37\4\uffff\1\37\1\uffff\32\37",
            "\1\37\2\uffff\12\37\7\uffff\32\37\4\uffff\1\37\1\uffff\32\37",
            "",
            "",
            "",
            "\1\72\11\35",
            "\1\37\1\75\1\uffff\12\73\7\uffff\32\37\4\uffff\1\37\1\uffff"+
            "\32\37",
            "\1\37\1\75\1\uffff\12\37\7\uffff\32\37\4\uffff\1\37\1\uffff"+
            "\32\37",
            "",
            "",
            "",
            "",
            "\1\76",
            "\1\77",
            "\1\100",
            "\1\101",
            "\1\102",
            "\1\103",
            "\1\104\14\uffff\1\105",
            "\1\106",
            "\1\107",
            "\1\110",
            "\1\37\2\uffff\12\37\7\uffff\32\37\4\uffff\1\37\1\uffff\32\37",
            "\1\112",
            "\1\113",
            "\1\37\2\uffff\12\37\7\uffff\32\37\4\uffff\1\37\1\uffff\32\37",
            "\1\115",
            "\1\116",
            "\1\117",
            "\1\120",
            "\1\121",
            "\1\122",
            "\1\123",
            "",
            "",
            "\1\75",
            "\1\37\1\75\1\uffff\12\73\7\uffff\32\37\4\uffff\1\37\1\uffff"+
            "\32\37",
            "",
            "",
            "\1\124",
            "\1\125",
            "\1\126",
            "\1\127",
            "\1\130",
            "\1\131",
            "\1\132",
            "\1\133\20\uffff\1\134",
            "\1\135",
            "\1\136",
            "\1\137",
            "",
            "\1\140",
            "\1\141",
            "",
            "\1\142",
            "\1\143",
            "\1\144",
            "\1\145",
            "\1\146",
            "\1\147",
            "\1\150",
            "\1\151",
            "\1\152",
            "\1\37\2\uffff\12\37\7\uffff\32\37\4\uffff\1\37\1\uffff\32\37",
            "\1\154",
            "\1\155",
            "\1\156",
            "\1\157",
            "\1\160",
            "\1\161",
            "\1\162",
            "\1\37\2\uffff\12\37\7\uffff\32\37\4\uffff\1\37\1\uffff\32\37",
            "\1\164",
            "\1\165",
            "\1\37\2\uffff\12\37\7\uffff\32\37\4\uffff\1\37\1\uffff\32\37",
            "\1\167",
            "\1\170",
            "\1\171",
            "\1\172",
            "\1\37\2\uffff\12\37\7\uffff\32\37\4\uffff\1\37\1\uffff\32\37",
            "\1\37\2\uffff\12\37\7\uffff\32\37\4\uffff\1\37\1\uffff\32\37",
            "\1\175",
            "\1\37\2\uffff\12\37\7\uffff\32\37\4\uffff\1\37\1\uffff\32\37",
            "\1\177",
            "",
            "\1\u0080",
            "\1\u0081",
            "\1\u0082",
            "\1\u0083",
            "\1\u0084",
            "\1\u0085",
            "\1\37\2\uffff\12\37\7\uffff\32\37\4\uffff\1\37\1\uffff\32\37",
            "",
            "\1\u0087",
            "\1\u0088",
            "",
            "\1\u0089",
            "\1\u008a",
            "\1\37\2\uffff\12\37\7\uffff\32\37\4\uffff\1\37\1\uffff\32\37",
            "\1\u008c",
            "",
            "",
            "\1\37\2\uffff\12\37\7\uffff\32\37\4\uffff\1\37\1\uffff\32\37",
            "",
            "\1\u008e",
            "\1\37\2\uffff\12\37\7\uffff\32\37\4\uffff\1\37\1\uffff\32\37",
            "\1\37\2\uffff\12\37\7\uffff\32\37\4\uffff\1\37\1\uffff\32\37",
            "\1\u0091",
            "\1\u0092",
            "\1\u0093",
            "\1\u0094",
            "",
            "\1\u0095",
            "\1\37\2\uffff\12\37\7\uffff\32\37\4\uffff\1\37\1\uffff\32\37",
            "\1\37\2\uffff\12\37\7\uffff\32\37\4\uffff\1\37\1\uffff\32\37",
            "\1\37\2\uffff\12\37\7\uffff\32\37\4\uffff\1\37\1\uffff\32\37",
            "",
            "\1\37\2\uffff\12\37\7\uffff\32\37\4\uffff\1\37\1\uffff\32\37",
            "",
            "\1\37\2\uffff\12\37\7\uffff\32\37\4\uffff\1\37\1\uffff\32\37",
            "",
            "",
            "\1\u009b",
            "\1\37\2\uffff\12\37\7\uffff\32\37\4\uffff\1\37\1\uffff\32\37",
            "\1\u009d",
            "\1\u009e",
            "\1\u009f",
            "",
            "",
            "",
            "",
            "",
            "\1\u00a0",
            "",
            "\1\u00a1",
            "\1\u00a2",
            "\1\u00a3",
            "\1\u00a4",
            "\1\u00a5",
            "\1\u00a6",
            "",
            "\1\37\2\uffff\12\37\7\uffff\32\37\4\uffff\1\37\1\uffff\32\37",
            "\1\u00a8",
            "\1\u00a9",
            "",
            "\1\37\2\uffff\12\37\7\uffff\32\37\4\uffff\1\37\1\uffff\32\37",
            "\1\37\2\uffff\12\37\7\uffff\32\37\4\uffff\1\37\1\uffff\32\37",
            "",
            ""
    };

    static final short[] DFA12_eot = DFA.unpackEncodedString(DFA12_eotS);
    static final short[] DFA12_eof = DFA.unpackEncodedString(DFA12_eofS);
    static final char[] DFA12_min = DFA.unpackEncodedStringToUnsignedChars(DFA12_minS);
    static final char[] DFA12_max = DFA.unpackEncodedStringToUnsignedChars(DFA12_maxS);
    static final short[] DFA12_accept = DFA.unpackEncodedString(DFA12_acceptS);
    static final short[] DFA12_special = DFA.unpackEncodedString(DFA12_specialS);
    static final short[][] DFA12_transition;

    static {
        int numStates = DFA12_transitionS.length;
        DFA12_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA12_transition[i] = DFA.unpackEncodedString(DFA12_transitionS[i]);
        }
    }

    class DFA12 extends DFA {

        public DFA12(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 12;
            this.eot = DFA12_eot;
            this.eof = DFA12_eof;
            this.min = DFA12_min;
            this.max = DFA12_max;
            this.accept = DFA12_accept;
            this.special = DFA12_special;
            this.transition = DFA12_transition;
        }
        public String getDescription() {
            return "1:1: Tokens : ( T__11 | T__12 | T__13 | T__14 | T__15 | T__16 | T__17 | T__18 | T__19 | T__20 | T__21 | T__22 | T__23 | T__24 | T__25 | T__26 | T__27 | T__28 | T__29 | T__30 | T__31 | T__32 | T__33 | T__34 | T__35 | T__36 | T__37 | T__38 | T__39 | T__40 | T__41 | T__42 | COMMENT | INTEGER | FLOAT | TEXT | WHITESPACE | LINEBREAK | QUOTED_34_34 );";
        }
    }
 

}