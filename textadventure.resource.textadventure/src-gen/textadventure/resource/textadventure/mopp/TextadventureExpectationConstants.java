/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package textadventure.resource.textadventure.mopp;

/**
 * This class contains some constants that are used during code completion.
 */
public class TextadventureExpectationConstants {
	
	public final static int EXPECTATIONS[][] = new int[167][];
	
	public static void initialize0() {
		EXPECTATIONS[0] = new int[2];
		EXPECTATIONS[0][0] = 0;
		EXPECTATIONS[0][1] = 0;
		EXPECTATIONS[1] = new int[2];
		EXPECTATIONS[1][0] = 1;
		EXPECTATIONS[1][1] = 1;
		EXPECTATIONS[2] = new int[2];
		EXPECTATIONS[2][0] = 2;
		EXPECTATIONS[2][1] = 2;
		EXPECTATIONS[3] = new int[3];
		EXPECTATIONS[3][0] = 3;
		EXPECTATIONS[3][1] = 3;
		EXPECTATIONS[3][2] = 0;
		EXPECTATIONS[4] = new int[3];
		EXPECTATIONS[4][0] = 3;
		EXPECTATIONS[4][1] = 4;
		EXPECTATIONS[4][2] = 1;
		EXPECTATIONS[5] = new int[3];
		EXPECTATIONS[5][0] = 4;
		EXPECTATIONS[5][1] = 4;
		EXPECTATIONS[5][2] = 2;
		EXPECTATIONS[6] = new int[3];
		EXPECTATIONS[6][0] = 5;
		EXPECTATIONS[6][1] = 4;
		EXPECTATIONS[6][2] = 3;
		EXPECTATIONS[7] = new int[3];
		EXPECTATIONS[7][0] = 4;
		EXPECTATIONS[7][1] = 5;
		EXPECTATIONS[7][2] = 4;
		EXPECTATIONS[8] = new int[3];
		EXPECTATIONS[8][0] = 5;
		EXPECTATIONS[8][1] = 5;
		EXPECTATIONS[8][2] = 5;
		EXPECTATIONS[9] = new int[3];
		EXPECTATIONS[9][0] = 5;
		EXPECTATIONS[9][1] = 6;
		EXPECTATIONS[9][2] = 6;
		EXPECTATIONS[10] = new int[3];
		EXPECTATIONS[10][0] = 6;
		EXPECTATIONS[10][1] = 6;
		EXPECTATIONS[10][2] = 7;
		EXPECTATIONS[11] = new int[3];
		EXPECTATIONS[11][0] = 7;
		EXPECTATIONS[11][1] = 6;
		EXPECTATIONS[11][2] = 8;
		EXPECTATIONS[12] = new int[3];
		EXPECTATIONS[12][0] = 8;
		EXPECTATIONS[12][1] = 6;
		EXPECTATIONS[12][2] = 9;
		EXPECTATIONS[13] = new int[3];
		EXPECTATIONS[13][0] = 6;
		EXPECTATIONS[13][1] = 7;
		EXPECTATIONS[13][2] = 10;
		EXPECTATIONS[14] = new int[3];
		EXPECTATIONS[14][0] = 7;
		EXPECTATIONS[14][1] = 7;
		EXPECTATIONS[14][2] = 11;
		EXPECTATIONS[15] = new int[3];
		EXPECTATIONS[15][0] = 8;
		EXPECTATIONS[15][1] = 7;
		EXPECTATIONS[15][2] = 12;
		EXPECTATIONS[16] = new int[3];
		EXPECTATIONS[16][0] = 7;
		EXPECTATIONS[16][1] = 8;
		EXPECTATIONS[16][2] = 13;
		EXPECTATIONS[17] = new int[3];
		EXPECTATIONS[17][0] = 8;
		EXPECTATIONS[17][1] = 8;
		EXPECTATIONS[17][2] = 14;
		EXPECTATIONS[18] = new int[3];
		EXPECTATIONS[18][0] = 8;
		EXPECTATIONS[18][1] = 9;
		EXPECTATIONS[18][2] = 15;
		EXPECTATIONS[19] = new int[2];
		EXPECTATIONS[19][0] = 9;
		EXPECTATIONS[19][1] = 10;
		EXPECTATIONS[20] = new int[2];
		EXPECTATIONS[20][0] = 10;
		EXPECTATIONS[20][1] = 11;
		EXPECTATIONS[21] = new int[2];
		EXPECTATIONS[21][0] = 11;
		EXPECTATIONS[21][1] = 12;
		EXPECTATIONS[22] = new int[2];
		EXPECTATIONS[22][0] = 12;
		EXPECTATIONS[22][1] = 13;
		EXPECTATIONS[23] = new int[2];
		EXPECTATIONS[23][0] = 13;
		EXPECTATIONS[23][1] = 14;
		EXPECTATIONS[24] = new int[2];
		EXPECTATIONS[24][0] = 14;
		EXPECTATIONS[24][1] = 15;
		EXPECTATIONS[25] = new int[2];
		EXPECTATIONS[25][0] = 15;
		EXPECTATIONS[25][1] = 16;
		EXPECTATIONS[26] = new int[2];
		EXPECTATIONS[26][0] = 16;
		EXPECTATIONS[26][1] = 17;
		EXPECTATIONS[27] = new int[2];
		EXPECTATIONS[27][0] = 17;
		EXPECTATIONS[27][1] = 18;
		EXPECTATIONS[28] = new int[2];
		EXPECTATIONS[28][0] = 18;
		EXPECTATIONS[28][1] = 19;
		EXPECTATIONS[29] = new int[2];
		EXPECTATIONS[29][0] = 19;
		EXPECTATIONS[29][1] = 20;
		EXPECTATIONS[30] = new int[2];
		EXPECTATIONS[30][0] = 20;
		EXPECTATIONS[30][1] = 21;
		EXPECTATIONS[31] = new int[3];
		EXPECTATIONS[31][0] = 3;
		EXPECTATIONS[31][1] = 22;
		EXPECTATIONS[31][2] = 16;
		EXPECTATIONS[32] = new int[3];
		EXPECTATIONS[32][0] = 4;
		EXPECTATIONS[32][1] = 22;
		EXPECTATIONS[32][2] = 17;
		EXPECTATIONS[33] = new int[3];
		EXPECTATIONS[33][0] = 5;
		EXPECTATIONS[33][1] = 22;
		EXPECTATIONS[33][2] = 18;
		EXPECTATIONS[34] = new int[2];
		EXPECTATIONS[34][0] = 21;
		EXPECTATIONS[34][1] = 23;
		EXPECTATIONS[35] = new int[2];
		EXPECTATIONS[35][0] = 22;
		EXPECTATIONS[35][1] = 24;
		EXPECTATIONS[36] = new int[2];
		EXPECTATIONS[36][0] = 23;
		EXPECTATIONS[36][1] = 24;
		EXPECTATIONS[37] = new int[2];
		EXPECTATIONS[37][0] = 24;
		EXPECTATIONS[37][1] = 25;
		EXPECTATIONS[38] = new int[2];
		EXPECTATIONS[38][0] = 23;
		EXPECTATIONS[38][1] = 26;
		EXPECTATIONS[39] = new int[2];
		EXPECTATIONS[39][0] = 23;
		EXPECTATIONS[39][1] = 27;
		EXPECTATIONS[40] = new int[3];
		EXPECTATIONS[40][0] = 25;
		EXPECTATIONS[40][1] = 28;
		EXPECTATIONS[40][2] = 19;
		EXPECTATIONS[41] = new int[2];
		EXPECTATIONS[41][0] = 26;
		EXPECTATIONS[41][1] = 28;
		EXPECTATIONS[42] = new int[3];
		EXPECTATIONS[42][0] = 25;
		EXPECTATIONS[42][1] = 29;
		EXPECTATIONS[42][2] = 20;
		EXPECTATIONS[43] = new int[2];
		EXPECTATIONS[43][0] = 26;
		EXPECTATIONS[43][1] = 29;
		EXPECTATIONS[44] = new int[3];
		EXPECTATIONS[44][0] = 25;
		EXPECTATIONS[44][1] = 30;
		EXPECTATIONS[44][2] = 21;
		EXPECTATIONS[45] = new int[2];
		EXPECTATIONS[45][0] = 26;
		EXPECTATIONS[45][1] = 30;
		EXPECTATIONS[46] = new int[3];
		EXPECTATIONS[46][0] = 4;
		EXPECTATIONS[46][1] = 31;
		EXPECTATIONS[46][2] = 22;
		EXPECTATIONS[47] = new int[3];
		EXPECTATIONS[47][0] = 5;
		EXPECTATIONS[47][1] = 31;
		EXPECTATIONS[47][2] = 23;
		EXPECTATIONS[48] = new int[2];
		EXPECTATIONS[48][0] = 27;
		EXPECTATIONS[48][1] = 32;
		EXPECTATIONS[49] = new int[2];
		EXPECTATIONS[49][0] = 28;
		EXPECTATIONS[49][1] = 33;
		EXPECTATIONS[50] = new int[3];
		EXPECTATIONS[50][0] = 7;
		EXPECTATIONS[50][1] = 34;
		EXPECTATIONS[50][2] = 24;
		EXPECTATIONS[51] = new int[3];
		EXPECTATIONS[51][0] = 8;
		EXPECTATIONS[51][1] = 34;
		EXPECTATIONS[51][2] = 25;
		EXPECTATIONS[52] = new int[2];
		EXPECTATIONS[52][0] = 29;
		EXPECTATIONS[52][1] = 35;
		EXPECTATIONS[53] = new int[2];
		EXPECTATIONS[53][0] = 30;
		EXPECTATIONS[53][1] = 36;
		EXPECTATIONS[54] = new int[3];
		EXPECTATIONS[54][0] = 8;
		EXPECTATIONS[54][1] = 37;
		EXPECTATIONS[54][2] = 26;
		EXPECTATIONS[55] = new int[2];
		EXPECTATIONS[55][0] = 31;
		EXPECTATIONS[55][1] = 38;
		EXPECTATIONS[56] = new int[2];
		EXPECTATIONS[56][0] = 32;
		EXPECTATIONS[56][1] = 39;
		EXPECTATIONS[57] = new int[2];
		EXPECTATIONS[57][0] = 33;
		EXPECTATIONS[57][1] = 40;
		EXPECTATIONS[58] = new int[3];
		EXPECTATIONS[58][0] = 34;
		EXPECTATIONS[58][1] = 41;
		EXPECTATIONS[58][2] = 27;
		EXPECTATIONS[59] = new int[2];
		EXPECTATIONS[59][0] = 32;
		EXPECTATIONS[59][1] = 42;
		EXPECTATIONS[60] = new int[3];
		EXPECTATIONS[60][0] = 35;
		EXPECTATIONS[60][1] = 43;
		EXPECTATIONS[60][2] = 28;
		EXPECTATIONS[61] = new int[2];
		EXPECTATIONS[61][0] = 36;
		EXPECTATIONS[61][1] = 44;
		EXPECTATIONS[62] = new int[3];
		EXPECTATIONS[62][0] = 25;
		EXPECTATIONS[62][1] = 45;
		EXPECTATIONS[62][2] = 29;
		EXPECTATIONS[63] = new int[2];
		EXPECTATIONS[63][0] = 26;
		EXPECTATIONS[63][1] = 45;
		EXPECTATIONS[64] = new int[2];
		EXPECTATIONS[64][0] = 37;
		EXPECTATIONS[64][1] = 46;
		EXPECTATIONS[65] = new int[2];
		EXPECTATIONS[65][0] = 38;
		EXPECTATIONS[65][1] = 47;
		EXPECTATIONS[66] = new int[3];
		EXPECTATIONS[66][0] = 39;
		EXPECTATIONS[66][1] = 48;
		EXPECTATIONS[66][2] = 30;
		EXPECTATIONS[67] = new int[2];
		EXPECTATIONS[67][0] = 36;
		EXPECTATIONS[67][1] = 49;
		EXPECTATIONS[68] = new int[2];
		EXPECTATIONS[68][0] = 40;
		EXPECTATIONS[68][1] = 50;
		EXPECTATIONS[69] = new int[2];
		EXPECTATIONS[69][0] = 36;
		EXPECTATIONS[69][1] = 51;
		EXPECTATIONS[70] = new int[2];
		EXPECTATIONS[70][0] = 41;
		EXPECTATIONS[70][1] = 52;
		EXPECTATIONS[71] = new int[2];
		EXPECTATIONS[71][0] = 42;
		EXPECTATIONS[71][1] = 53;
		EXPECTATIONS[72] = new int[2];
		EXPECTATIONS[72][0] = 43;
		EXPECTATIONS[72][1] = 54;
		EXPECTATIONS[73] = new int[2];
		EXPECTATIONS[73][0] = 44;
		EXPECTATIONS[73][1] = 55;
		EXPECTATIONS[74] = new int[2];
		EXPECTATIONS[74][0] = 45;
		EXPECTATIONS[74][1] = 56;
		EXPECTATIONS[75] = new int[2];
		EXPECTATIONS[75][0] = 46;
		EXPECTATIONS[75][1] = 56;
		EXPECTATIONS[76] = new int[2];
		EXPECTATIONS[76][0] = 47;
		EXPECTATIONS[76][1] = 56;
		EXPECTATIONS[77] = new int[3];
		EXPECTATIONS[77][0] = 48;
		EXPECTATIONS[77][1] = 56;
		EXPECTATIONS[77][2] = 31;
		EXPECTATIONS[78] = new int[2];
		EXPECTATIONS[78][0] = 49;
		EXPECTATIONS[78][1] = 56;
		EXPECTATIONS[79] = new int[2];
		EXPECTATIONS[79][0] = 50;
		EXPECTATIONS[79][1] = 57;
		EXPECTATIONS[80] = new int[2];
		EXPECTATIONS[80][0] = 51;
		EXPECTATIONS[80][1] = 58;
		EXPECTATIONS[81] = new int[2];
		EXPECTATIONS[81][0] = 45;
		EXPECTATIONS[81][1] = 59;
		EXPECTATIONS[82] = new int[2];
		EXPECTATIONS[82][0] = 46;
		EXPECTATIONS[82][1] = 59;
		EXPECTATIONS[83] = new int[2];
		EXPECTATIONS[83][0] = 47;
		EXPECTATIONS[83][1] = 59;
		EXPECTATIONS[84] = new int[3];
		EXPECTATIONS[84][0] = 48;
		EXPECTATIONS[84][1] = 59;
		EXPECTATIONS[84][2] = 32;
		EXPECTATIONS[85] = new int[2];
		EXPECTATIONS[85][0] = 49;
		EXPECTATIONS[85][1] = 59;
		EXPECTATIONS[86] = new int[2];
		EXPECTATIONS[86][0] = 52;
		EXPECTATIONS[86][1] = 60;
		EXPECTATIONS[87] = new int[2];
		EXPECTATIONS[87][0] = 53;
		EXPECTATIONS[87][1] = 61;
		EXPECTATIONS[88] = new int[2];
		EXPECTATIONS[88][0] = 45;
		EXPECTATIONS[88][1] = 62;
		EXPECTATIONS[89] = new int[2];
		EXPECTATIONS[89][0] = 46;
		EXPECTATIONS[89][1] = 62;
		EXPECTATIONS[90] = new int[2];
		EXPECTATIONS[90][0] = 47;
		EXPECTATIONS[90][1] = 62;
		EXPECTATIONS[91] = new int[3];
		EXPECTATIONS[91][0] = 48;
		EXPECTATIONS[91][1] = 62;
		EXPECTATIONS[91][2] = 33;
		EXPECTATIONS[92] = new int[2];
		EXPECTATIONS[92][0] = 49;
		EXPECTATIONS[92][1] = 62;
		EXPECTATIONS[93] = new int[2];
		EXPECTATIONS[93][0] = 54;
		EXPECTATIONS[93][1] = 63;
		EXPECTATIONS[94] = new int[2];
		EXPECTATIONS[94][0] = 55;
		EXPECTATIONS[94][1] = 64;
		EXPECTATIONS[95] = new int[2];
		EXPECTATIONS[95][0] = 45;
		EXPECTATIONS[95][1] = 65;
		EXPECTATIONS[96] = new int[2];
		EXPECTATIONS[96][0] = 46;
		EXPECTATIONS[96][1] = 65;
		EXPECTATIONS[97] = new int[2];
		EXPECTATIONS[97][0] = 47;
		EXPECTATIONS[97][1] = 65;
		EXPECTATIONS[98] = new int[3];
		EXPECTATIONS[98][0] = 48;
		EXPECTATIONS[98][1] = 65;
		EXPECTATIONS[98][2] = 34;
		EXPECTATIONS[99] = new int[2];
		EXPECTATIONS[99][0] = 49;
		EXPECTATIONS[99][1] = 65;
		EXPECTATIONS[100] = new int[2];
		EXPECTATIONS[100][0] = 45;
		EXPECTATIONS[100][1] = 66;
		EXPECTATIONS[101] = new int[2];
		EXPECTATIONS[101][0] = 46;
		EXPECTATIONS[101][1] = 66;
		EXPECTATIONS[102] = new int[2];
		EXPECTATIONS[102][0] = 47;
		EXPECTATIONS[102][1] = 66;
		EXPECTATIONS[103] = new int[3];
		EXPECTATIONS[103][0] = 48;
		EXPECTATIONS[103][1] = 66;
		EXPECTATIONS[103][2] = 35;
		EXPECTATIONS[104] = new int[2];
		EXPECTATIONS[104][0] = 49;
		EXPECTATIONS[104][1] = 66;
		EXPECTATIONS[105] = new int[3];
		EXPECTATIONS[105][0] = 48;
		EXPECTATIONS[105][1] = 67;
		EXPECTATIONS[105][2] = 36;
		EXPECTATIONS[106] = new int[2];
		EXPECTATIONS[106][0] = 49;
		EXPECTATIONS[106][1] = 67;
		EXPECTATIONS[107] = new int[3];
		EXPECTATIONS[107][0] = 5;
		EXPECTATIONS[107][1] = 68;
		EXPECTATIONS[107][2] = 37;
		EXPECTATIONS[108] = new int[3];
		EXPECTATIONS[108][0] = 6;
		EXPECTATIONS[108][1] = 68;
		EXPECTATIONS[108][2] = 38;
		EXPECTATIONS[109] = new int[3];
		EXPECTATIONS[109][0] = 7;
		EXPECTATIONS[109][1] = 68;
		EXPECTATIONS[109][2] = 39;
		EXPECTATIONS[110] = new int[3];
		EXPECTATIONS[110][0] = 8;
		EXPECTATIONS[110][1] = 68;
		EXPECTATIONS[110][2] = 40;
		EXPECTATIONS[111] = new int[2];
		EXPECTATIONS[111][0] = 56;
		EXPECTATIONS[111][1] = 69;
		EXPECTATIONS[112] = new int[2];
		EXPECTATIONS[112][0] = 57;
		EXPECTATIONS[112][1] = 70;
		EXPECTATIONS[113] = new int[2];
		EXPECTATIONS[113][0] = 58;
		EXPECTATIONS[113][1] = 71;
		EXPECTATIONS[114] = new int[2];
		EXPECTATIONS[114][0] = 59;
		EXPECTATIONS[114][1] = 72;
		EXPECTATIONS[115] = new int[2];
		EXPECTATIONS[115][0] = 60;
		EXPECTATIONS[115][1] = 73;
		EXPECTATIONS[116] = new int[2];
		EXPECTATIONS[116][0] = 61;
		EXPECTATIONS[116][1] = 74;
		EXPECTATIONS[117] = new int[3];
		EXPECTATIONS[117][0] = 62;
		EXPECTATIONS[117][1] = 75;
		EXPECTATIONS[117][2] = 41;
		EXPECTATIONS[118] = new int[2];
		EXPECTATIONS[118][0] = 63;
		EXPECTATIONS[118][1] = 75;
		EXPECTATIONS[119] = new int[3];
		EXPECTATIONS[119][0] = 62;
		EXPECTATIONS[119][1] = 76;
		EXPECTATIONS[119][2] = 42;
		EXPECTATIONS[120] = new int[2];
		EXPECTATIONS[120][0] = 63;
		EXPECTATIONS[120][1] = 76;
		EXPECTATIONS[121] = new int[3];
		EXPECTATIONS[121][0] = 6;
		EXPECTATIONS[121][1] = 77;
		EXPECTATIONS[121][2] = 43;
		EXPECTATIONS[122] = new int[3];
		EXPECTATIONS[122][0] = 7;
		EXPECTATIONS[122][1] = 77;
		EXPECTATIONS[122][2] = 44;
		EXPECTATIONS[123] = new int[3];
		EXPECTATIONS[123][0] = 8;
		EXPECTATIONS[123][1] = 77;
		EXPECTATIONS[123][2] = 45;
		EXPECTATIONS[124] = new int[2];
		EXPECTATIONS[124][0] = 64;
		EXPECTATIONS[124][1] = 78;
		EXPECTATIONS[125] = new int[3];
		EXPECTATIONS[125][0] = 65;
		EXPECTATIONS[125][1] = 79;
		EXPECTATIONS[125][2] = 46;
		EXPECTATIONS[126] = new int[3];
		EXPECTATIONS[126][0] = 62;
		EXPECTATIONS[126][1] = 80;
		EXPECTATIONS[126][2] = 47;
		EXPECTATIONS[127] = new int[2];
		EXPECTATIONS[127][0] = 63;
		EXPECTATIONS[127][1] = 80;
		EXPECTATIONS[128] = new int[2];
		EXPECTATIONS[128][0] = 66;
		EXPECTATIONS[128][1] = 81;
		EXPECTATIONS[129] = new int[2];
		EXPECTATIONS[129][0] = 67;
		EXPECTATIONS[129][1] = 81;
		EXPECTATIONS[130] = new int[2];
		EXPECTATIONS[130][0] = 68;
		EXPECTATIONS[130][1] = 82;
		EXPECTATIONS[131] = new int[2];
		EXPECTATIONS[131][0] = 67;
		EXPECTATIONS[131][1] = 82;
		EXPECTATIONS[132] = new int[2];
		EXPECTATIONS[132][0] = 69;
		EXPECTATIONS[132][1] = 83;
		EXPECTATIONS[133] = new int[2];
		EXPECTATIONS[133][0] = 68;
		EXPECTATIONS[133][1] = 84;
		EXPECTATIONS[134] = new int[2];
		EXPECTATIONS[134][0] = 67;
		EXPECTATIONS[134][1] = 84;
		EXPECTATIONS[135] = new int[2];
		EXPECTATIONS[135][0] = 68;
		EXPECTATIONS[135][1] = 85;
		EXPECTATIONS[136] = new int[2];
		EXPECTATIONS[136][0] = 67;
		EXPECTATIONS[136][1] = 85;
		EXPECTATIONS[137] = new int[2];
		EXPECTATIONS[137][0] = 67;
		EXPECTATIONS[137][1] = 86;
		EXPECTATIONS[138] = new int[2];
		EXPECTATIONS[138][0] = 70;
		EXPECTATIONS[138][1] = 87;
		EXPECTATIONS[139] = new int[4];
		EXPECTATIONS[139][0] = 71;
		EXPECTATIONS[139][1] = 88;
		EXPECTATIONS[139][2] = 48;
		EXPECTATIONS[139][3] = 49;
		EXPECTATIONS[140] = new int[4];
		EXPECTATIONS[140][0] = 72;
		EXPECTATIONS[140][1] = 88;
		EXPECTATIONS[140][2] = 50;
		EXPECTATIONS[140][3] = 51;
		EXPECTATIONS[141] = new int[4];
		EXPECTATIONS[141][0] = 73;
		EXPECTATIONS[141][1] = 88;
		EXPECTATIONS[141][2] = 52;
		EXPECTATIONS[141][3] = 53;
		EXPECTATIONS[142] = new int[2];
		EXPECTATIONS[142][0] = 74;
		EXPECTATIONS[142][1] = 88;
		EXPECTATIONS[143] = new int[2];
		EXPECTATIONS[143][0] = 74;
		EXPECTATIONS[143][1] = 89;
		EXPECTATIONS[144] = new int[3];
		EXPECTATIONS[144][0] = 62;
		EXPECTATIONS[144][1] = 90;
		EXPECTATIONS[144][2] = 54;
		EXPECTATIONS[145] = new int[2];
		EXPECTATIONS[145][0] = 63;
		EXPECTATIONS[145][1] = 90;
		EXPECTATIONS[146] = new int[2];
		EXPECTATIONS[146][0] = 75;
		EXPECTATIONS[146][1] = 91;
		EXPECTATIONS[147] = new int[3];
		EXPECTATIONS[147][0] = 71;
		EXPECTATIONS[147][1] = 92;
		EXPECTATIONS[147][2] = 55;
		EXPECTATIONS[148] = new int[3];
		EXPECTATIONS[148][0] = 72;
		EXPECTATIONS[148][1] = 92;
		EXPECTATIONS[148][2] = 56;
		EXPECTATIONS[149] = new int[3];
		EXPECTATIONS[149][0] = 73;
		EXPECTATIONS[149][1] = 92;
		EXPECTATIONS[149][2] = 57;
		EXPECTATIONS[150] = new int[2];
		EXPECTATIONS[150][0] = 74;
		EXPECTATIONS[150][1] = 92;
		EXPECTATIONS[151] = new int[3];
		EXPECTATIONS[151][0] = 71;
		EXPECTATIONS[151][1] = 93;
		EXPECTATIONS[151][2] = 58;
		EXPECTATIONS[152] = new int[3];
		EXPECTATIONS[152][0] = 72;
		EXPECTATIONS[152][1] = 93;
		EXPECTATIONS[152][2] = 59;
		EXPECTATIONS[153] = new int[3];
		EXPECTATIONS[153][0] = 73;
		EXPECTATIONS[153][1] = 93;
		EXPECTATIONS[153][2] = 60;
		EXPECTATIONS[154] = new int[2];
		EXPECTATIONS[154][0] = 74;
		EXPECTATIONS[154][1] = 93;
		EXPECTATIONS[155] = new int[2];
		EXPECTATIONS[155][0] = 76;
		EXPECTATIONS[155][1] = 94;
		EXPECTATIONS[156] = new int[2];
		EXPECTATIONS[156][0] = 75;
		EXPECTATIONS[156][1] = 95;
		EXPECTATIONS[157] = new int[2];
		EXPECTATIONS[157][0] = 77;
		EXPECTATIONS[157][1] = 96;
		EXPECTATIONS[158] = new int[2];
		EXPECTATIONS[158][0] = 75;
		EXPECTATIONS[158][1] = 97;
		EXPECTATIONS[159] = new int[2];
		EXPECTATIONS[159][0] = 78;
		EXPECTATIONS[159][1] = 98;
		EXPECTATIONS[160] = new int[2];
		EXPECTATIONS[160][0] = 79;
		EXPECTATIONS[160][1] = 99;
		EXPECTATIONS[161] = new int[2];
		EXPECTATIONS[161][0] = 75;
		EXPECTATIONS[161][1] = 99;
		EXPECTATIONS[162] = new int[2];
		EXPECTATIONS[162][0] = 79;
		EXPECTATIONS[162][1] = 100;
		EXPECTATIONS[163] = new int[2];
		EXPECTATIONS[163][0] = 75;
		EXPECTATIONS[163][1] = 100;
		EXPECTATIONS[164] = new int[2];
		EXPECTATIONS[164][0] = 80;
		EXPECTATIONS[164][1] = 101;
		EXPECTATIONS[165] = new int[3];
		EXPECTATIONS[165][0] = 48;
		EXPECTATIONS[165][1] = 102;
		EXPECTATIONS[165][2] = 61;
		EXPECTATIONS[166] = new int[2];
		EXPECTATIONS[166][0] = 49;
		EXPECTATIONS[166][1] = 102;
	}
	
	public static void initialize() {
		initialize0();
	}
	
	static {
		initialize();
	}
	
}
