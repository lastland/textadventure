/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package textadventure.resource.textadventure.mopp;

public class TextadventureSyntaxCoverageInformationProvider {
	
	public org.eclipse.emf.ecore.EClass[] getClassesWithSyntax() {
		return new org.eclipse.emf.ecore.EClass[] {
			textadventure.TextadventurePackage.eINSTANCE.getEWorld(),
			textadventure.TextadventurePackage.eINSTANCE.getETile(),
			textadventure.TextadventurePackage.eINSTANCE.getEItem(),
			textadventure.TextadventurePackage.eINSTANCE.getECheckPoint(),
			textadventure.TextadventurePackage.eINSTANCE.getEDestination(),
			textadventure.TextadventurePackage.eINSTANCE.getEBinding(),
			textadventure.TextadventurePackage.eINSTANCE.getEWhileEffect(),
			textadventure.TextadventurePackage.eINSTANCE.getERemoveAction(),
			textadventure.TextadventurePackage.eINSTANCE.getEListDescription(),
			textadventure.TextadventurePackage.eINSTANCE.getEPerson(),
			textadventure.TextadventurePackage.eINSTANCE.getEBattle(),
			textadventure.TextadventurePackage.eINSTANCE.getEBattleCustomization(),
			textadventure.TextadventurePackage.eINSTANCE.getEFunction(),
			textadventure.TextadventurePackage.eINSTANCE.getEFunctionBody(),
			textadventure.TextadventurePackage.eINSTANCE.getEPrintStatement(),
			textadventure.TextadventurePackage.eINSTANCE.getERemovePlayerStatement(),
			textadventure.TextadventurePackage.eINSTANCE.getEMethodCallStatement(),
			textadventure.TextadventurePackage.eINSTANCE.getEIgnoreItemAbility(),
		};
	}
	
	public org.eclipse.emf.ecore.EClass[] getStartSymbols() {
		return new org.eclipse.emf.ecore.EClass[] {
			textadventure.TextadventurePackage.eINSTANCE.getEWorld(),
		};
	}
	
}
