/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package textadventure.resource.textadventure.mopp;

/**
 * Abstract super class for all expected elements. Provides methods to add
 * followers.
 */
public abstract class TextadventureAbstractExpectedElement implements textadventure.resource.textadventure.ITextadventureExpectedElement {
	
	private org.eclipse.emf.ecore.EClass ruleMetaclass;
	
	private java.util.Set<textadventure.resource.textadventure.util.TextadventurePair<textadventure.resource.textadventure.ITextadventureExpectedElement, textadventure.resource.textadventure.mopp.TextadventureContainedFeature[]>> followers = new java.util.LinkedHashSet<textadventure.resource.textadventure.util.TextadventurePair<textadventure.resource.textadventure.ITextadventureExpectedElement, textadventure.resource.textadventure.mopp.TextadventureContainedFeature[]>>();
	
	public TextadventureAbstractExpectedElement(org.eclipse.emf.ecore.EClass ruleMetaclass) {
		super();
		this.ruleMetaclass = ruleMetaclass;
	}
	
	public org.eclipse.emf.ecore.EClass getRuleMetaclass() {
		return ruleMetaclass;
	}
	
	public void addFollower(textadventure.resource.textadventure.ITextadventureExpectedElement follower, textadventure.resource.textadventure.mopp.TextadventureContainedFeature[] path) {
		followers.add(new textadventure.resource.textadventure.util.TextadventurePair<textadventure.resource.textadventure.ITextadventureExpectedElement, textadventure.resource.textadventure.mopp.TextadventureContainedFeature[]>(follower, path));
	}
	
	public java.util.Collection<textadventure.resource.textadventure.util.TextadventurePair<textadventure.resource.textadventure.ITextadventureExpectedElement, textadventure.resource.textadventure.mopp.TextadventureContainedFeature[]>> getFollowers() {
		return followers;
	}
	
}
