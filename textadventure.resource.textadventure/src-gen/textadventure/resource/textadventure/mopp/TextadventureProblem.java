/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package textadventure.resource.textadventure.mopp;

public class TextadventureProblem implements textadventure.resource.textadventure.ITextadventureProblem {
	
	private String message;
	private textadventure.resource.textadventure.TextadventureEProblemType type;
	private textadventure.resource.textadventure.TextadventureEProblemSeverity severity;
	private java.util.Collection<textadventure.resource.textadventure.ITextadventureQuickFix> quickFixes;
	
	public TextadventureProblem(String message, textadventure.resource.textadventure.TextadventureEProblemType type, textadventure.resource.textadventure.TextadventureEProblemSeverity severity) {
		this(message, type, severity, java.util.Collections.<textadventure.resource.textadventure.ITextadventureQuickFix>emptySet());
	}
	
	public TextadventureProblem(String message, textadventure.resource.textadventure.TextadventureEProblemType type, textadventure.resource.textadventure.TextadventureEProblemSeverity severity, textadventure.resource.textadventure.ITextadventureQuickFix quickFix) {
		this(message, type, severity, java.util.Collections.singleton(quickFix));
	}
	
	public TextadventureProblem(String message, textadventure.resource.textadventure.TextadventureEProblemType type, textadventure.resource.textadventure.TextadventureEProblemSeverity severity, java.util.Collection<textadventure.resource.textadventure.ITextadventureQuickFix> quickFixes) {
		super();
		this.message = message;
		this.type = type;
		this.severity = severity;
		this.quickFixes = new java.util.LinkedHashSet<textadventure.resource.textadventure.ITextadventureQuickFix>();
		this.quickFixes.addAll(quickFixes);
	}
	
	public textadventure.resource.textadventure.TextadventureEProblemType getType() {
		return type;
	}
	
	public textadventure.resource.textadventure.TextadventureEProblemSeverity getSeverity() {
		return severity;
	}
	
	public String getMessage() {
		return message;
	}
	
	public java.util.Collection<textadventure.resource.textadventure.ITextadventureQuickFix> getQuickFixes() {
		return quickFixes;
	}
	
}
