/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package textadventure.resource.textadventure.mopp;

public class TextadventureParseResult implements textadventure.resource.textadventure.ITextadventureParseResult {
	
	private org.eclipse.emf.ecore.EObject root;
	private java.util.Collection<textadventure.resource.textadventure.ITextadventureCommand<textadventure.resource.textadventure.ITextadventureTextResource>> commands = new java.util.ArrayList<textadventure.resource.textadventure.ITextadventureCommand<textadventure.resource.textadventure.ITextadventureTextResource>>();
	
	public TextadventureParseResult() {
		super();
	}
	
	public void setRoot(org.eclipse.emf.ecore.EObject root) {
		this.root = root;
	}
	
	public org.eclipse.emf.ecore.EObject getRoot() {
		return root;
	}
	
	public java.util.Collection<textadventure.resource.textadventure.ITextadventureCommand<textadventure.resource.textadventure.ITextadventureTextResource>> getPostParseCommands() {
		return commands;
	}
	
}
