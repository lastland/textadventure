/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package textadventure.resource.textadventure.mopp;

public class TextadventurePrinter implements textadventure.resource.textadventure.ITextadventureTextPrinter {
	
	protected textadventure.resource.textadventure.ITextadventureTokenResolverFactory tokenResolverFactory = new textadventure.resource.textadventure.mopp.TextadventureTokenResolverFactory();
	
	protected java.io.OutputStream outputStream;
	
	/**
	 * Holds the resource that is associated with this printer. This may be null if
	 * the printer is used stand alone.
	 */
	private textadventure.resource.textadventure.ITextadventureTextResource resource;
	
	private java.util.Map<?, ?> options;
	private String encoding = System.getProperty("file.encoding");
	
	public TextadventurePrinter(java.io.OutputStream outputStream, textadventure.resource.textadventure.ITextadventureTextResource resource) {
		super();
		this.outputStream = outputStream;
		this.resource = resource;
	}
	
	protected int matchCount(java.util.Map<String, Integer> featureCounter, java.util.Collection<String> needed) {
		int pos = 0;
		int neg = 0;
		
		for (String featureName : featureCounter.keySet()) {
			if (needed.contains(featureName)) {
				int value = featureCounter.get(featureName);
				if (value == 0) {
					neg += 1;
				} else {
					pos += 1;
				}
			}
		}
		return neg > 0 ? -neg : pos;
	}
	
	protected void doPrint(org.eclipse.emf.ecore.EObject element, java.io.PrintWriter out, String globaltab) {
		if (element == null) {
			throw new java.lang.IllegalArgumentException("Nothing to write.");
		}
		if (out == null) {
			throw new java.lang.IllegalArgumentException("Nothing to write on.");
		}
		
		if (element instanceof textadventure.EWorld) {
			print_textadventure_EWorld((textadventure.EWorld) element, globaltab, out);
			return;
		}
		if (element instanceof textadventure.ETile) {
			print_textadventure_ETile((textadventure.ETile) element, globaltab, out);
			return;
		}
		if (element instanceof textadventure.EItem) {
			print_textadventure_EItem((textadventure.EItem) element, globaltab, out);
			return;
		}
		if (element instanceof textadventure.ECheckPoint) {
			print_textadventure_ECheckPoint((textadventure.ECheckPoint) element, globaltab, out);
			return;
		}
		if (element instanceof textadventure.EDestination) {
			print_textadventure_EDestination((textadventure.EDestination) element, globaltab, out);
			return;
		}
		if (element instanceof textadventure.EBinding) {
			print_textadventure_EBinding((textadventure.EBinding) element, globaltab, out);
			return;
		}
		if (element instanceof textadventure.EWhileEffect) {
			print_textadventure_EWhileEffect((textadventure.EWhileEffect) element, globaltab, out);
			return;
		}
		if (element instanceof textadventure.ERemoveAction) {
			print_textadventure_ERemoveAction((textadventure.ERemoveAction) element, globaltab, out);
			return;
		}
		if (element instanceof textadventure.EListDescription) {
			print_textadventure_EListDescription((textadventure.EListDescription) element, globaltab, out);
			return;
		}
		if (element instanceof textadventure.EPerson) {
			print_textadventure_EPerson((textadventure.EPerson) element, globaltab, out);
			return;
		}
		if (element instanceof textadventure.EBattle) {
			print_textadventure_EBattle((textadventure.EBattle) element, globaltab, out);
			return;
		}
		if (element instanceof textadventure.EBattleCustomization) {
			print_textadventure_EBattleCustomization((textadventure.EBattleCustomization) element, globaltab, out);
			return;
		}
		if (element instanceof textadventure.EFunction) {
			print_textadventure_EFunction((textadventure.EFunction) element, globaltab, out);
			return;
		}
		if (element instanceof textadventure.EFunctionBody) {
			print_textadventure_EFunctionBody((textadventure.EFunctionBody) element, globaltab, out);
			return;
		}
		if (element instanceof textadventure.EPrintStatement) {
			print_textadventure_EPrintStatement((textadventure.EPrintStatement) element, globaltab, out);
			return;
		}
		if (element instanceof textadventure.ERemovePlayerStatement) {
			print_textadventure_ERemovePlayerStatement((textadventure.ERemovePlayerStatement) element, globaltab, out);
			return;
		}
		if (element instanceof textadventure.EMethodCallStatement) {
			print_textadventure_EMethodCallStatement((textadventure.EMethodCallStatement) element, globaltab, out);
			return;
		}
		if (element instanceof textadventure.EIgnoreItemAbility) {
			print_textadventure_EIgnoreItemAbility((textadventure.EIgnoreItemAbility) element, globaltab, out);
			return;
		}
		
		addWarningToResource("The printer can not handle " + element.eClass().getName() + " elements", element);
	}
	
	protected textadventure.resource.textadventure.mopp.TextadventureReferenceResolverSwitch getReferenceResolverSwitch() {
		return (textadventure.resource.textadventure.mopp.TextadventureReferenceResolverSwitch) new textadventure.resource.textadventure.mopp.TextadventureMetaInformation().getReferenceResolverSwitch();
	}
	
	protected void addWarningToResource(final String errorMessage, org.eclipse.emf.ecore.EObject cause) {
		textadventure.resource.textadventure.ITextadventureTextResource resource = getResource();
		if (resource == null) {
			// the resource can be null if the printer is used stand alone
			return;
		}
		resource.addProblem(new textadventure.resource.textadventure.mopp.TextadventureProblem(errorMessage, textadventure.resource.textadventure.TextadventureEProblemType.PRINT_PROBLEM, textadventure.resource.textadventure.TextadventureEProblemSeverity.WARNING), cause);
	}
	
	public void setOptions(java.util.Map<?,?> options) {
		this.options = options;
	}
	
	public java.util.Map<?,?> getOptions() {
		return options;
	}
	
	public void setEncoding(String encoding) {
		if (encoding != null) {
			this.encoding = encoding;
		}
	}
	
	public String getEncoding() {
		return encoding;
	}
	
	public textadventure.resource.textadventure.ITextadventureTextResource getResource() {
		return resource;
	}
	
	/**
	 * Calls {@link #doPrint(EObject, PrintWriter, String)} and writes the result to
	 * the underlying output stream.
	 */
	public void print(org.eclipse.emf.ecore.EObject element) throws java.io.IOException {
		java.io.PrintWriter out = new java.io.PrintWriter(new java.io.OutputStreamWriter(new java.io.BufferedOutputStream(outputStream), encoding));
		doPrint(element, out, "");
		out.flush();
		out.close();
	}
	
	public void print_textadventure_EWorld(textadventure.EWorld element, String outertab, java.io.PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(7);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EWORLD__ITEMS));
		printCountingMap.put("items", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EWORLD__PERSONS));
		printCountingMap.put("persons", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EWORLD__TILES));
		printCountingMap.put("tiles", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EWORLD__CHECKPOINT));
		printCountingMap.put("checkpoint", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EWORLD__BATTLE));
		printCountingMap.put("battle", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EWORLD__DESTINATION));
		printCountingMap.put("destination", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EWORLD__PLAYER_PERSON));
		printCountingMap.put("playerPerson", temp == null ? 0 : 1);
		// print collected hidden tokens
		int count;
		// DEFINITION PART BEGINS (CsString)
		out.print("player");
		out.print(" ");
		// DEFINITION PART BEGINS (CsString)
		out.print("=");
		out.print(" ");
		// DEFINITION PART BEGINS (PlaceholderUsingDefaultToken)
		count = printCountingMap.get("playerPerson");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EWORLD__PLAYER_PERSON));
			if (o != null) {
				textadventure.resource.textadventure.ITextadventureTokenResolver resolver = tokenResolverFactory.createTokenResolver("TEXT");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getEWorldPlayerPersonReferenceResolver().deResolve((textadventure.EPerson) o, element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EWORLD__PLAYER_PERSON)), element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EWORLD__PLAYER_PERSON), element));
				out.print(" ");
			}
			printCountingMap.put("playerPerson", count - 1);
		}
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("tiles");
		if (count > 0) {
			java.util.List<?> list = (java.util.List<?>)element.eGet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EWORLD__TILES));
			int index  = list.size() - count;
			if (index < 0) {
				index = 0;
			}
			java.util.ListIterator<?> it  = list.listIterator(index);
			while (it.hasNext()) {
				Object o = it.next();
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("tiles", 0);
		}
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("items");
		if (count > 0) {
			java.util.List<?> list = (java.util.List<?>)element.eGet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EWORLD__ITEMS));
			int index  = list.size() - count;
			if (index < 0) {
				index = 0;
			}
			java.util.ListIterator<?> it  = list.listIterator(index);
			while (it.hasNext()) {
				Object o = it.next();
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("items", 0);
		}
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("persons");
		if (count > 0) {
			java.util.List<?> list = (java.util.List<?>)element.eGet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EWORLD__PERSONS));
			int index  = list.size() - count;
			if (index < 0) {
				index = 0;
			}
			java.util.ListIterator<?> it  = list.listIterator(index);
			while (it.hasNext()) {
				Object o = it.next();
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("persons", 0);
		}
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("battle");
		if (count > 0) {
			java.util.List<?> list = (java.util.List<?>)element.eGet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EWORLD__BATTLE));
			int index  = list.size() - count;
			if (index < 0) {
				index = 0;
			}
			java.util.ListIterator<?> it  = list.listIterator(index);
			while (it.hasNext()) {
				Object o = it.next();
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("battle", 0);
		}
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("checkpoint");
		if (count > 0) {
			java.util.List<?> list = (java.util.List<?>)element.eGet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EWORLD__CHECKPOINT));
			int index  = list.size() - count;
			if (index < 0) {
				index = 0;
			}
			java.util.ListIterator<?> it  = list.listIterator(index);
			while (it.hasNext()) {
				Object o = it.next();
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("checkpoint", 0);
		}
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("destination");
		if (count > 0) {
			java.util.List<?> list = (java.util.List<?>)element.eGet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EWORLD__DESTINATION));
			int index  = list.size() - count;
			if (index < 0) {
				index = 0;
			}
			java.util.ListIterator<?> it  = list.listIterator(index);
			while (it.hasNext()) {
				Object o = it.next();
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("destination", 0);
		}
	}
	
	
	public void print_textadventure_ETile(textadventure.ETile element, String outertab, java.io.PrintWriter out) {
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(4);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.ETILE__X));
		printCountingMap.put("x", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.ETILE__Y));
		printCountingMap.put("y", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.ETILE__DESCRIPTION));
		printCountingMap.put("description", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.ETILE__ID));
		printCountingMap.put("id", temp == null ? 0 : 1);
		// print collected hidden tokens
		int count;
		// DEFINITION PART BEGINS (CsString)
		out.print("tile");
		out.print(" ");
		// DEFINITION PART BEGINS (PlaceholderUsingDefaultToken)
		count = printCountingMap.get("id");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.ETILE__ID));
			if (o != null) {
				textadventure.resource.textadventure.ITextadventureTokenResolver resolver = tokenResolverFactory.createTokenResolver("TEXT");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.ETILE__ID), element));
				out.print(" ");
			}
			printCountingMap.put("id", count - 1);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print("{");
		out.print(" ");
		// DEFINITION PART BEGINS (CsString)
		out.print("x");
		out.print(" ");
		// DEFINITION PART BEGINS (CsString)
		out.print("=");
		out.print(" ");
		// DEFINITION PART BEGINS (PlaceholderUsingSpecifiedToken)
		count = printCountingMap.get("x");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.ETILE__X));
			if (o != null) {
				textadventure.resource.textadventure.ITextadventureTokenResolver resolver = tokenResolverFactory.createTokenResolver("INTEGER");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.ETILE__X), element));
				out.print(" ");
			}
			printCountingMap.put("x", count - 1);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print("y");
		out.print(" ");
		// DEFINITION PART BEGINS (CsString)
		out.print("=");
		out.print(" ");
		// DEFINITION PART BEGINS (PlaceholderUsingSpecifiedToken)
		count = printCountingMap.get("y");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.ETILE__Y));
			if (o != null) {
				textadventure.resource.textadventure.ITextadventureTokenResolver resolver = tokenResolverFactory.createTokenResolver("INTEGER");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.ETILE__Y), element));
				out.print(" ");
			}
			printCountingMap.put("y", count - 1);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print("description");
		out.print(" ");
		// DEFINITION PART BEGINS (CsString)
		out.print("=");
		out.print(" ");
		// DEFINITION PART BEGINS (PlaceholderInQuotes)
		count = printCountingMap.get("description");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.ETILE__DESCRIPTION));
			if (o != null) {
				textadventure.resource.textadventure.ITextadventureTokenResolver resolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.ETILE__DESCRIPTION), element));
				out.print(" ");
			}
			printCountingMap.put("description", count - 1);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print("}");
		out.print(" ");
	}
	
	
	public void print_textadventure_EItem(textadventure.EItem element, String outertab, java.io.PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(3);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EITEM__NAME));
		printCountingMap.put("name", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EITEM__EFFECTS));
		printCountingMap.put("effects", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EITEM__LOCATION));
		printCountingMap.put("location", temp == null ? 0 : 1);
		// print collected hidden tokens
		int count;
		boolean iterate = true;
		java.io.StringWriter sWriter = null;
		java.io.PrintWriter out1 = null;
		java.util.Map<String, Integer> printCountingMap1 = null;
		// DEFINITION PART BEGINS (CsString)
		out.print("item");
		out.print(" ");
		// DEFINITION PART BEGINS (PlaceholderUsingDefaultToken)
		count = printCountingMap.get("name");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EITEM__NAME));
			if (o != null) {
				textadventure.resource.textadventure.ITextadventureTokenResolver resolver = tokenResolverFactory.createTokenResolver("TEXT");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EITEM__NAME), element));
				out.print(" ");
			}
			printCountingMap.put("name", count - 1);
		}
		// DEFINITION PART BEGINS (CompoundDefinition)
		sWriter = new java.io.StringWriter();
		out1 = new java.io.PrintWriter(sWriter);
		printCountingMap1 = new java.util.LinkedHashMap<String, Integer>(printCountingMap);
		print_textadventure_EItem_0(element, localtab, out1, printCountingMap1);
		if (printCountingMap.equals(printCountingMap1)) {
			out1.close();
		} else {
			out1.flush();
			out1.close();
			out.print(sWriter.toString());
			printCountingMap.putAll(printCountingMap1);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print("{");
		out.print(" ");
		// DEFINITION PART BEGINS (CompoundDefinition)
		iterate = true;
		while (iterate) {
			sWriter = new java.io.StringWriter();
			out1 = new java.io.PrintWriter(sWriter);
			printCountingMap1 = new java.util.LinkedHashMap<String, Integer>(printCountingMap);
			print_textadventure_EItem_1(element, localtab, out1, printCountingMap1);
			if (printCountingMap.equals(printCountingMap1)) {
				iterate = false;
				out1.close();
			} else {
				out1.flush();
				out1.close();
				out.print(sWriter.toString());
				printCountingMap.putAll(printCountingMap1);
			}
		}
		// DEFINITION PART BEGINS (CsString)
		out.print("}");
		out.print(" ");
	}
	
	public void print_textadventure_EItem_0(textadventure.EItem element, String outertab, java.io.PrintWriter out, java.util.Map<String, Integer> printCountingMap) {
		int count;
		// DEFINITION PART BEGINS (CsString)
		out.print("@");
		out.print(" ");
		// DEFINITION PART BEGINS (PlaceholderUsingDefaultToken)
		count = printCountingMap.get("location");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EITEM__LOCATION));
			if (o != null) {
				textadventure.resource.textadventure.ITextadventureTokenResolver resolver = tokenResolverFactory.createTokenResolver("TEXT");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getEItemLocationReferenceResolver().deResolve((textadventure.ETile) o, element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EITEM__LOCATION)), element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EITEM__LOCATION), element));
				out.print(" ");
			}
			printCountingMap.put("location", count - 1);
		}
	}
	
	public void print_textadventure_EItem_1(textadventure.EItem element, String outertab, java.io.PrintWriter out, java.util.Map<String, Integer> printCountingMap) {
		String localtab = outertab;
		int count;
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("effects");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EITEM__EFFECTS));
			java.util.List<?> list = (java.util.List<?>) o;
			int index = list.size() - count;
			if (index >= 0) {
				o = list.get(index);
			} else {
				o = null;
			}
			if (o != null) {
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("effects", count - 1);
		}
	}
	
	
	public void print_textadventure_ECheckPoint(textadventure.ECheckPoint element, String outertab, java.io.PrintWriter out) {
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(1);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.ECHECK_POINT__POINT));
		printCountingMap.put("point", temp == null ? 0 : 1);
		// print collected hidden tokens
		int count;
		// DEFINITION PART BEGINS (CsString)
		out.print("checkpoint");
		out.print(" ");
		// DEFINITION PART BEGINS (CsString)
		out.print("@");
		out.print(" ");
		// DEFINITION PART BEGINS (PlaceholderUsingDefaultToken)
		count = printCountingMap.get("point");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.ECHECK_POINT__POINT));
			if (o != null) {
				textadventure.resource.textadventure.ITextadventureTokenResolver resolver = tokenResolverFactory.createTokenResolver("TEXT");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getECheckPointPointReferenceResolver().deResolve((textadventure.ETile) o, element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.ECHECK_POINT__POINT)), element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.ECHECK_POINT__POINT), element));
				out.print(" ");
			}
			printCountingMap.put("point", count - 1);
		}
	}
	
	
	public void print_textadventure_EDestination(textadventure.EDestination element, String outertab, java.io.PrintWriter out) {
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(1);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EDESTINATION__POINT));
		printCountingMap.put("point", temp == null ? 0 : 1);
		// print collected hidden tokens
		int count;
		// DEFINITION PART BEGINS (CsString)
		out.print("destination");
		out.print(" ");
		// DEFINITION PART BEGINS (CsString)
		out.print("@");
		out.print(" ");
		// DEFINITION PART BEGINS (PlaceholderUsingDefaultToken)
		count = printCountingMap.get("point");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EDESTINATION__POINT));
			if (o != null) {
				textadventure.resource.textadventure.ITextadventureTokenResolver resolver = tokenResolverFactory.createTokenResolver("TEXT");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getEDestinationPointReferenceResolver().deResolve((textadventure.ETile) o, element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EDESTINATION__POINT)), element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EDESTINATION__POINT), element));
				out.print(" ");
			}
			printCountingMap.put("point", count - 1);
		}
	}
	
	
	public void print_textadventure_EBinding(textadventure.EBinding element, String outertab, java.io.PrintWriter out) {
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(2);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EBINDING__PARTICIPANT));
		printCountingMap.put("participant", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EBINDING__ID));
		printCountingMap.put("id", temp == null ? 0 : 1);
		// print collected hidden tokens
		int count;
		// DEFINITION PART BEGINS (EnumTerminal)
		count = printCountingMap.get("participant");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EBINDING__PARTICIPANT));
			if (o != null) {
			}
			printCountingMap.put("participant", count - 1);
		}
		// DEFINITION PART BEGINS (PlaceholderUsingDefaultToken)
		count = printCountingMap.get("id");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EBINDING__ID));
			if (o != null) {
				textadventure.resource.textadventure.ITextadventureTokenResolver resolver = tokenResolverFactory.createTokenResolver("TEXT");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EBINDING__ID), element));
				out.print(" ");
			}
			printCountingMap.put("id", count - 1);
		}
	}
	
	
	public void print_textadventure_EWhileEffect(textadventure.EWhileEffect element, String outertab, java.io.PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(3);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EWHILE_EFFECT__CONDITION));
		printCountingMap.put("condition", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EWHILE_EFFECT__BINDING));
		printCountingMap.put("binding", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EWHILE_EFFECT__ACTION));
		printCountingMap.put("action", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		// print collected hidden tokens
		int count;
		// DEFINITION PART BEGINS (CsString)
		out.print("while");
		out.print(" ");
		// DEFINITION PART BEGINS (EnumTerminal)
		count = printCountingMap.get("condition");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EWHILE_EFFECT__CONDITION));
			if (o != null) {
			}
			printCountingMap.put("condition", count - 1);
		}
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("binding");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EWHILE_EFFECT__BINDING));
			if (o != null) {
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("binding", count - 1);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print("{");
		out.print(" ");
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("action");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EWHILE_EFFECT__ACTION));
			java.util.List<?> list = (java.util.List<?>) o;
			int index = list.size() - count;
			if (index >= 0) {
				o = list.get(index);
			} else {
				o = null;
			}
			if (o != null) {
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("action", count - 1);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print("}");
		out.print(" ");
	}
	
	
	public void print_textadventure_ERemoveAction(textadventure.ERemoveAction element, String outertab, java.io.PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(2);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EREMOVE_ACTION__WHAT));
		printCountingMap.put("what", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EREMOVE_ACTION__FROM));
		printCountingMap.put("from", temp == null ? 0 : 1);
		// print collected hidden tokens
		int count;
		// DEFINITION PART BEGINS (CsString)
		out.print("remove");
		out.print(" ");
		// DEFINITION PART BEGINS (PlaceholderUsingDefaultToken)
		count = printCountingMap.get("what");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EREMOVE_ACTION__WHAT));
			if (o != null) {
				textadventure.resource.textadventure.ITextadventureTokenResolver resolver = tokenResolverFactory.createTokenResolver("TEXT");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getERemoveActionWhatReferenceResolver().deResolve((textadventure.EBinding) o, element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EREMOVE_ACTION__WHAT)), element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EREMOVE_ACTION__WHAT), element));
				out.print(" ");
			}
			printCountingMap.put("what", count - 1);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print("from");
		out.print(" ");
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("from");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EREMOVE_ACTION__FROM));
			if (o != null) {
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("from", count - 1);
		}
	}
	
	
	public void print_textadventure_EListDescription(textadventure.EListDescription element, String outertab, java.io.PrintWriter out) {
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(2);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.ELIST_DESCRIPTION__TARGET));
		printCountingMap.put("target", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.ELIST_DESCRIPTION__PROPERTY));
		printCountingMap.put("property", temp == null ? 0 : 1);
		// print collected hidden tokens
		int count;
		// DEFINITION PART BEGINS (EnumTerminal)
		count = printCountingMap.get("target");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.ELIST_DESCRIPTION__TARGET));
			if (o != null) {
			}
			printCountingMap.put("target", count - 1);
		}
		// DEFINITION PART BEGINS (EnumTerminal)
		count = printCountingMap.get("property");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.ELIST_DESCRIPTION__PROPERTY));
			if (o != null) {
			}
			printCountingMap.put("property", count - 1);
		}
	}
	
	
	public void print_textadventure_EPerson(textadventure.EPerson element, String outertab, java.io.PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(6);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EPERSON__NAME));
		printCountingMap.put("name", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EPERSON__ABILITIES));
		printCountingMap.put("abilities", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EPERSON__LOCATION));
		printCountingMap.put("location", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EPERSON__HP));
		printCountingMap.put("hp", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EPERSON__DEFENSE));
		printCountingMap.put("defense", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EPERSON__ATTACK));
		printCountingMap.put("attack", temp == null ? 0 : 1);
		// print collected hidden tokens
		int count;
		boolean iterate = true;
		java.io.StringWriter sWriter = null;
		java.io.PrintWriter out1 = null;
		java.util.Map<String, Integer> printCountingMap1 = null;
		// DEFINITION PART BEGINS (CsString)
		out.print("person");
		out.print(" ");
		// DEFINITION PART BEGINS (PlaceholderUsingDefaultToken)
		count = printCountingMap.get("name");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EPERSON__NAME));
			if (o != null) {
				textadventure.resource.textadventure.ITextadventureTokenResolver resolver = tokenResolverFactory.createTokenResolver("TEXT");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EPERSON__NAME), element));
				out.print(" ");
			}
			printCountingMap.put("name", count - 1);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print("@");
		out.print(" ");
		// DEFINITION PART BEGINS (PlaceholderUsingDefaultToken)
		count = printCountingMap.get("location");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EPERSON__LOCATION));
			if (o != null) {
				textadventure.resource.textadventure.ITextadventureTokenResolver resolver = tokenResolverFactory.createTokenResolver("TEXT");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getEPersonLocationReferenceResolver().deResolve((textadventure.ETile) o, element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EPERSON__LOCATION)), element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EPERSON__LOCATION), element));
				out.print(" ");
			}
			printCountingMap.put("location", count - 1);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print("{");
		out.print(" ");
		// DEFINITION PART BEGINS (CompoundDefinition)
		iterate = true;
		while (iterate) {
			sWriter = new java.io.StringWriter();
			out1 = new java.io.PrintWriter(sWriter);
			printCountingMap1 = new java.util.LinkedHashMap<String, Integer>(printCountingMap);
			print_textadventure_EPerson_0(element, localtab, out1, printCountingMap1);
			if (printCountingMap.equals(printCountingMap1)) {
				iterate = false;
				out1.close();
			} else {
				out1.flush();
				out1.close();
				out.print(sWriter.toString());
				printCountingMap.putAll(printCountingMap1);
			}
		}
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("abilities");
		if (count > 0) {
			java.util.List<?> list = (java.util.List<?>)element.eGet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EPERSON__ABILITIES));
			int index  = list.size() - count;
			if (index < 0) {
				index = 0;
			}
			java.util.ListIterator<?> it  = list.listIterator(index);
			while (it.hasNext()) {
				Object o = it.next();
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("abilities", 0);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print("}");
		out.print(" ");
	}
	
	public void print_textadventure_EPerson_0(textadventure.EPerson element, String outertab, java.io.PrintWriter out, java.util.Map<String, Integer> printCountingMap) {
		int count;
		int alt = -1;
		alt = 0;
		int matches = 		matchCount(printCountingMap, java.util.Arrays.asList(		"hp"		));
		int tempMatchCount;
		tempMatchCount = 		matchCount(printCountingMap, java.util.Arrays.asList(		"defense"		));
		if (tempMatchCount > matches) {
			alt = 1;
			matches = tempMatchCount;
		}
		tempMatchCount = 		matchCount(printCountingMap, java.util.Arrays.asList(		"attack"		));
		if (tempMatchCount > matches) {
			alt = 2;
			matches = tempMatchCount;
		}
		switch(alt) {
			case 1:			{
				// DEFINITION PART BEGINS (CsString)
				out.print("defense");
				out.print(" ");
				// DEFINITION PART BEGINS (CsString)
				out.print("=");
				out.print(" ");
				// DEFINITION PART BEGINS (PlaceholderUsingSpecifiedToken)
				count = printCountingMap.get("defense");
				if (count > 0) {
					Object o = element.eGet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EPERSON__DEFENSE));
					if (o != null) {
						textadventure.resource.textadventure.ITextadventureTokenResolver resolver = tokenResolverFactory.createTokenResolver("INTEGER");
						resolver.setOptions(getOptions());
						out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EPERSON__DEFENSE), element));
						out.print(" ");
					}
					printCountingMap.put("defense", count - 1);
				}
			}
			break;
			case 2:			{
				// DEFINITION PART BEGINS (CsString)
				out.print("attack");
				out.print(" ");
				// DEFINITION PART BEGINS (CsString)
				out.print("=");
				out.print(" ");
				// DEFINITION PART BEGINS (PlaceholderUsingSpecifiedToken)
				count = printCountingMap.get("attack");
				if (count > 0) {
					Object o = element.eGet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EPERSON__ATTACK));
					if (o != null) {
						textadventure.resource.textadventure.ITextadventureTokenResolver resolver = tokenResolverFactory.createTokenResolver("INTEGER");
						resolver.setOptions(getOptions());
						out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EPERSON__ATTACK), element));
						out.print(" ");
					}
					printCountingMap.put("attack", count - 1);
				}
			}
			break;
			default:			// DEFINITION PART BEGINS (CsString)
			out.print("hp");
			out.print(" ");
			// DEFINITION PART BEGINS (CsString)
			out.print("=");
			out.print(" ");
			// DEFINITION PART BEGINS (PlaceholderUsingSpecifiedToken)
			count = printCountingMap.get("hp");
			if (count > 0) {
				Object o = element.eGet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EPERSON__HP));
				if (o != null) {
					textadventure.resource.textadventure.ITextadventureTokenResolver resolver = tokenResolverFactory.createTokenResolver("INTEGER");
					resolver.setOptions(getOptions());
					out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EPERSON__HP), element));
					out.print(" ");
				}
				printCountingMap.put("hp", count - 1);
			}
		}
	}
	
	
	public void print_textadventure_EBattle(textadventure.EBattle element, String outertab, java.io.PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(3);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EBATTLE__LOCATION));
		printCountingMap.put("location", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EBATTLE__ENEMY));
		printCountingMap.put("enemy", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EBATTLE__LOGICS));
		printCountingMap.put("logics", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		// print collected hidden tokens
		int count;
		// DEFINITION PART BEGINS (CsString)
		out.print("battle");
		out.print(" ");
		// DEFINITION PART BEGINS (CsString)
		out.print("@");
		out.print(" ");
		// DEFINITION PART BEGINS (PlaceholderUsingDefaultToken)
		count = printCountingMap.get("location");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EBATTLE__LOCATION));
			if (o != null) {
				textadventure.resource.textadventure.ITextadventureTokenResolver resolver = tokenResolverFactory.createTokenResolver("TEXT");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getEBattleLocationReferenceResolver().deResolve((textadventure.ETile) o, element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EBATTLE__LOCATION)), element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EBATTLE__LOCATION), element));
				out.print(" ");
			}
			printCountingMap.put("location", count - 1);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print("{");
		out.print(" ");
		// DEFINITION PART BEGINS (CsString)
		out.print("enemy");
		out.print(" ");
		// DEFINITION PART BEGINS (CsString)
		out.print("=");
		out.print(" ");
		// DEFINITION PART BEGINS (PlaceholderUsingDefaultToken)
		count = printCountingMap.get("enemy");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EBATTLE__ENEMY));
			if (o != null) {
				textadventure.resource.textadventure.ITextadventureTokenResolver resolver = tokenResolverFactory.createTokenResolver("TEXT");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getEBattleEnemyReferenceResolver().deResolve((textadventure.EPerson) o, element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EBATTLE__ENEMY)), element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EBATTLE__ENEMY), element));
				out.print(" ");
			}
			printCountingMap.put("enemy", count - 1);
		}
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("logics");
		if (count > 0) {
			java.util.List<?> list = (java.util.List<?>)element.eGet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EBATTLE__LOGICS));
			int index  = list.size() - count;
			if (index < 0) {
				index = 0;
			}
			java.util.ListIterator<?> it  = list.listIterator(index);
			while (it.hasNext()) {
				Object o = it.next();
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("logics", 0);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print("}");
		out.print(" ");
	}
	
	
	public void print_textadventure_EBattleCustomization(textadventure.EBattleCustomization element, String outertab, java.io.PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(2);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EBATTLE_CUSTOMIZATION__WHEN));
		printCountingMap.put("when", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EBATTLE_CUSTOMIZATION__FUNC));
		printCountingMap.put("func", temp == null ? 0 : 1);
		// print collected hidden tokens
		int count;
		// DEFINITION PART BEGINS (PlaceholderUsingDefaultToken)
		count = printCountingMap.get("when");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EBATTLE_CUSTOMIZATION__WHEN));
			if (o != null) {
				textadventure.resource.textadventure.ITextadventureTokenResolver resolver = tokenResolverFactory.createTokenResolver("TEXT");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EBATTLE_CUSTOMIZATION__WHEN), element));
				out.print(" ");
			}
			printCountingMap.put("when", count - 1);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print("=");
		out.print(" ");
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("func");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EBATTLE_CUSTOMIZATION__FUNC));
			if (o != null) {
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("func", count - 1);
		}
	}
	
	
	public void print_textadventure_EFunction(textadventure.EFunction element, String outertab, java.io.PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(2);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EFUNCTION__PARAMS));
		printCountingMap.put("params", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EFUNCTION__BODY));
		printCountingMap.put("body", temp == null ? 0 : 1);
		// print collected hidden tokens
		int count;
		java.io.StringWriter sWriter = null;
		java.io.PrintWriter out1 = null;
		java.util.Map<String, Integer> printCountingMap1 = null;
		// DEFINITION PART BEGINS (CsString)
		out.print("function(");
		out.print(" ");
		// DEFINITION PART BEGINS (CompoundDefinition)
		sWriter = new java.io.StringWriter();
		out1 = new java.io.PrintWriter(sWriter);
		printCountingMap1 = new java.util.LinkedHashMap<String, Integer>(printCountingMap);
		print_textadventure_EFunction_0(element, localtab, out1, printCountingMap1);
		if (printCountingMap.equals(printCountingMap1)) {
			out1.close();
		} else {
			out1.flush();
			out1.close();
			out.print(sWriter.toString());
			printCountingMap.putAll(printCountingMap1);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print(")");
		out.print(" ");
		// DEFINITION PART BEGINS (CsString)
		out.print("{");
		out.print(" ");
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("body");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EFUNCTION__BODY));
			if (o != null) {
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("body", count - 1);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print("}");
		out.print(" ");
	}
	
	public void print_textadventure_EFunction_0(textadventure.EFunction element, String outertab, java.io.PrintWriter out, java.util.Map<String, Integer> printCountingMap) {
		String localtab = outertab;
		int count;
		boolean iterate = true;
		java.io.StringWriter sWriter = null;
		java.io.PrintWriter out1 = null;
		java.util.Map<String, Integer> printCountingMap1 = null;
		// DEFINITION PART BEGINS (PlaceholderUsingDefaultToken)
		count = printCountingMap.get("params");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EFUNCTION__PARAMS));
			java.util.List<?> list = (java.util.List<?>) o;
			int index = list.size() - count;
			if (index >= 0) {
				o = list.get(index);
			} else {
				o = null;
			}
			if (o != null) {
				textadventure.resource.textadventure.ITextadventureTokenResolver resolver = tokenResolverFactory.createTokenResolver("TEXT");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EFUNCTION__PARAMS), element));
				out.print(" ");
			}
			printCountingMap.put("params", count - 1);
		}
		// DEFINITION PART BEGINS (CompoundDefinition)
		iterate = true;
		while (iterate) {
			sWriter = new java.io.StringWriter();
			out1 = new java.io.PrintWriter(sWriter);
			printCountingMap1 = new java.util.LinkedHashMap<String, Integer>(printCountingMap);
			print_textadventure_EFunction_0_0(element, localtab, out1, printCountingMap1);
			if (printCountingMap.equals(printCountingMap1)) {
				iterate = false;
				out1.close();
			} else {
				out1.flush();
				out1.close();
				out.print(sWriter.toString());
				printCountingMap.putAll(printCountingMap1);
			}
		}
	}
	
	public void print_textadventure_EFunction_0_0(textadventure.EFunction element, String outertab, java.io.PrintWriter out, java.util.Map<String, Integer> printCountingMap) {
		int count;
		// DEFINITION PART BEGINS (CsString)
		out.print(",");
		out.print(" ");
		// DEFINITION PART BEGINS (PlaceholderUsingDefaultToken)
		count = printCountingMap.get("params");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EFUNCTION__PARAMS));
			java.util.List<?> list = (java.util.List<?>) o;
			int index = list.size() - count;
			if (index >= 0) {
				o = list.get(index);
			} else {
				o = null;
			}
			if (o != null) {
				textadventure.resource.textadventure.ITextadventureTokenResolver resolver = tokenResolverFactory.createTokenResolver("TEXT");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EFUNCTION__PARAMS), element));
				out.print(" ");
			}
			printCountingMap.put("params", count - 1);
		}
	}
	
	
	public void print_textadventure_EFunctionBody(textadventure.EFunctionBody element, String outertab, java.io.PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(1);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EFUNCTION_BODY__STATEMENTS));
		printCountingMap.put("statements", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		// print collected hidden tokens
		boolean iterate = true;
		java.io.StringWriter sWriter = null;
		java.io.PrintWriter out1 = null;
		java.util.Map<String, Integer> printCountingMap1 = null;
		// DEFINITION PART BEGINS (CompoundDefinition)
		iterate = true;
		while (iterate) {
			sWriter = new java.io.StringWriter();
			out1 = new java.io.PrintWriter(sWriter);
			printCountingMap1 = new java.util.LinkedHashMap<String, Integer>(printCountingMap);
			print_textadventure_EFunctionBody_0(element, localtab, out1, printCountingMap1);
			if (printCountingMap.equals(printCountingMap1)) {
				iterate = false;
				out1.close();
			} else {
				out1.flush();
				out1.close();
				out.print(sWriter.toString());
				printCountingMap.putAll(printCountingMap1);
			}
		}
	}
	
	public void print_textadventure_EFunctionBody_0(textadventure.EFunctionBody element, String outertab, java.io.PrintWriter out, java.util.Map<String, Integer> printCountingMap) {
		String localtab = outertab;
		int count;
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("statements");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EFUNCTION_BODY__STATEMENTS));
			java.util.List<?> list = (java.util.List<?>) o;
			int index = list.size() - count;
			if (index >= 0) {
				o = list.get(index);
			} else {
				o = null;
			}
			if (o != null) {
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("statements", count - 1);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print(";");
		out.print(" ");
	}
	
	
	public void print_textadventure_EPrintStatement(textadventure.EPrintStatement element, String outertab, java.io.PrintWriter out) {
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(1);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EPRINT_STATEMENT__OUTPUT));
		printCountingMap.put("output", temp == null ? 0 : 1);
		// print collected hidden tokens
		int count;
		// DEFINITION PART BEGINS (CsString)
		out.print("print");
		out.print(" ");
		// DEFINITION PART BEGINS (PlaceholderInQuotes)
		count = printCountingMap.get("output");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EPRINT_STATEMENT__OUTPUT));
			if (o != null) {
				textadventure.resource.textadventure.ITextadventureTokenResolver resolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EPRINT_STATEMENT__OUTPUT), element));
				out.print(" ");
			}
			printCountingMap.put("output", count - 1);
		}
	}
	
	
	public void print_textadventure_ERemovePlayerStatement(textadventure.ERemovePlayerStatement element, String outertab, java.io.PrintWriter out) {
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(1);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EREMOVE_PLAYER_STATEMENT__TARGET));
		printCountingMap.put("target", temp == null ? 0 : 1);
		// print collected hidden tokens
		int count;
		// DEFINITION PART BEGINS (CsString)
		out.print("remove");
		out.print(" ");
		// DEFINITION PART BEGINS (PlaceholderUsingDefaultToken)
		count = printCountingMap.get("target");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EREMOVE_PLAYER_STATEMENT__TARGET));
			if (o != null) {
				textadventure.resource.textadventure.ITextadventureTokenResolver resolver = tokenResolverFactory.createTokenResolver("TEXT");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getERemovePlayerStatementTargetReferenceResolver().deResolve((textadventure.EPerson) o, element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EREMOVE_PLAYER_STATEMENT__TARGET)), element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EREMOVE_PLAYER_STATEMENT__TARGET), element));
				out.print(" ");
			}
			printCountingMap.put("target", count - 1);
		}
	}
	
	
	public void print_textadventure_EMethodCallStatement(textadventure.EMethodCallStatement element, String outertab, java.io.PrintWriter out) {
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(3);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EMETHOD_CALL_STATEMENT__OBJECT));
		printCountingMap.put("object", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EMETHOD_CALL_STATEMENT__METHOD));
		printCountingMap.put("method", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EMETHOD_CALL_STATEMENT__PARAMS));
		printCountingMap.put("params", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		// print collected hidden tokens
		int count;
		// DEFINITION PART BEGINS (PlaceholderUsingDefaultToken)
		count = printCountingMap.get("object");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EMETHOD_CALL_STATEMENT__OBJECT));
			if (o != null) {
				textadventure.resource.textadventure.ITextadventureTokenResolver resolver = tokenResolverFactory.createTokenResolver("TEXT");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EMETHOD_CALL_STATEMENT__OBJECT), element));
				out.print(" ");
			}
			printCountingMap.put("object", count - 1);
		}
		// DEFINITION PART BEGINS (PlaceholderUsingDefaultToken)
		count = printCountingMap.get("method");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EMETHOD_CALL_STATEMENT__METHOD));
			if (o != null) {
				textadventure.resource.textadventure.ITextadventureTokenResolver resolver = tokenResolverFactory.createTokenResolver("TEXT");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EMETHOD_CALL_STATEMENT__METHOD), element));
				out.print(" ");
			}
			printCountingMap.put("method", count - 1);
		}
		// DEFINITION PART BEGINS (PlaceholderInQuotes)
		count = printCountingMap.get("params");
		if (count > 0) {
			java.util.List<?> list = (java.util.List<?>)element.eGet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EMETHOD_CALL_STATEMENT__PARAMS));
			int index  = list.size() - count;
			if (index < 0) {
				index = 0;
			}
			java.util.ListIterator<?> it  = list.listIterator(index);
			while (it.hasNext()) {
				Object o = it.next();
				textadventure.resource.textadventure.ITextadventureTokenResolver resolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EMETHOD_CALL_STATEMENT__PARAMS), element));
				out.print(" ");
			}
			printCountingMap.put("params", 0);
		}
	}
	
	
	public void print_textadventure_EIgnoreItemAbility(textadventure.EIgnoreItemAbility element, String outertab, java.io.PrintWriter out) {
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(1);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EIGNORE_ITEM_ABILITY__IGNORED_ITEM));
		printCountingMap.put("ignoredItem", temp == null ? 0 : 1);
		// print collected hidden tokens
		int count;
		// DEFINITION PART BEGINS (CsString)
		out.print("ignore");
		out.print(" ");
		// DEFINITION PART BEGINS (PlaceholderUsingDefaultToken)
		count = printCountingMap.get("ignoredItem");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EIGNORE_ITEM_ABILITY__IGNORED_ITEM));
			if (o != null) {
				textadventure.resource.textadventure.ITextadventureTokenResolver resolver = tokenResolverFactory.createTokenResolver("TEXT");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getEIgnoreItemAbilityIgnoredItemReferenceResolver().deResolve((textadventure.EItem) o, element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EIGNORE_ITEM_ABILITY__IGNORED_ITEM)), element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EIGNORE_ITEM_ABILITY__IGNORED_ITEM), element));
				out.print(" ");
			}
			printCountingMap.put("ignoredItem", count - 1);
		}
	}
	
	
}
