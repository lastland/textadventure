/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package textadventure.resource.textadventure.grammar;

public class TextadventureContainment extends textadventure.resource.textadventure.grammar.TextadventureTerminal {
	
	private final org.eclipse.emf.ecore.EClass[] allowedTypes;
	
	public TextadventureContainment(org.eclipse.emf.ecore.EStructuralFeature feature, textadventure.resource.textadventure.grammar.TextadventureCardinality cardinality, org.eclipse.emf.ecore.EClass[] allowedTypes, int mandatoryOccurencesAfter) {
		super(feature, cardinality, mandatoryOccurencesAfter);
		this.allowedTypes = allowedTypes;
	}
	
	public org.eclipse.emf.ecore.EClass[] getAllowedTypes() {
		return allowedTypes;
	}
	
	public String toString() {
		String typeRestrictions = null;
		if (allowedTypes != null && allowedTypes.length > 0) {
			typeRestrictions = textadventure.resource.textadventure.util.TextadventureStringUtil.explode(allowedTypes, ", ", new textadventure.resource.textadventure.ITextadventureFunction1<String, org.eclipse.emf.ecore.EClass>() {
				public String execute(org.eclipse.emf.ecore.EClass eClass) {
					return eClass.getName();
				}
			});
		}
		return getFeature().getName() + (typeRestrictions == null ? "" : "[" + typeRestrictions + "]");
	}
	
}
