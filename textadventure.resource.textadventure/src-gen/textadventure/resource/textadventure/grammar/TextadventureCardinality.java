/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package textadventure.resource.textadventure.grammar;

public enum TextadventureCardinality {
	
	ONE, PLUS, QUESTIONMARK, STAR;
	
}
