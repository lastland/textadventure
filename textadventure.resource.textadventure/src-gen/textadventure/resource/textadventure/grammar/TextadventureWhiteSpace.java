/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package textadventure.resource.textadventure.grammar;

public class TextadventureWhiteSpace extends textadventure.resource.textadventure.grammar.TextadventureFormattingElement {
	
	private final int amount;
	
	public TextadventureWhiteSpace(int amount, textadventure.resource.textadventure.grammar.TextadventureCardinality cardinality) {
		super(cardinality);
		this.amount = amount;
	}
	
	public int getAmount() {
		return amount;
	}
	
	public String toString() {
		return "#" + getAmount();
	}
	
}
