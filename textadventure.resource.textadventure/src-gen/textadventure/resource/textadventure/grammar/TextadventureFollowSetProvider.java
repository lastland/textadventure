/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package textadventure.resource.textadventure.grammar;

/**
 * This class provides the follow sets for all terminals of the grammar. These
 * sets are used during code completion.
 */
public class TextadventureFollowSetProvider {
	
	public final static textadventure.resource.textadventure.ITextadventureExpectedElement TERMINALS[] = new textadventure.resource.textadventure.ITextadventureExpectedElement[81];
	
	public final static org.eclipse.emf.ecore.EStructuralFeature[] FEATURES = new org.eclipse.emf.ecore.EStructuralFeature[15];
	
	public final static textadventure.resource.textadventure.mopp.TextadventureContainedFeature[] LINKS = new textadventure.resource.textadventure.mopp.TextadventureContainedFeature[62];
	
	public final static textadventure.resource.textadventure.mopp.TextadventureContainedFeature[] EMPTY_LINK_ARRAY = new textadventure.resource.textadventure.mopp.TextadventureContainedFeature[0];
	
	public static void initializeTerminals0() {
		TERMINALS[0] = new textadventure.resource.textadventure.mopp.TextadventureExpectedCsString(textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_0_0_0_0);
		TERMINALS[1] = new textadventure.resource.textadventure.mopp.TextadventureExpectedCsString(textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_0_0_0_1);
		TERMINALS[2] = new textadventure.resource.textadventure.mopp.TextadventureExpectedStructuralFeature(textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_0_0_0_2);
		TERMINALS[3] = new textadventure.resource.textadventure.mopp.TextadventureExpectedCsString(textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_1_0_0_0);
		TERMINALS[4] = new textadventure.resource.textadventure.mopp.TextadventureExpectedCsString(textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_2_0_0_0);
		TERMINALS[5] = new textadventure.resource.textadventure.mopp.TextadventureExpectedCsString(textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_9_0_0_0);
		TERMINALS[6] = new textadventure.resource.textadventure.mopp.TextadventureExpectedCsString(textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_10_0_0_0);
		TERMINALS[7] = new textadventure.resource.textadventure.mopp.TextadventureExpectedCsString(textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_3_0_0_0);
		TERMINALS[8] = new textadventure.resource.textadventure.mopp.TextadventureExpectedCsString(textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_4_0_0_0);
		TERMINALS[9] = new textadventure.resource.textadventure.mopp.TextadventureExpectedStructuralFeature(textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_1_0_0_1);
		TERMINALS[10] = new textadventure.resource.textadventure.mopp.TextadventureExpectedCsString(textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_1_0_0_2);
		TERMINALS[11] = new textadventure.resource.textadventure.mopp.TextadventureExpectedCsString(textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_1_0_0_3);
		TERMINALS[12] = new textadventure.resource.textadventure.mopp.TextadventureExpectedCsString(textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_1_0_0_4);
		TERMINALS[13] = new textadventure.resource.textadventure.mopp.TextadventureExpectedStructuralFeature(textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_1_0_0_5);
		TERMINALS[14] = new textadventure.resource.textadventure.mopp.TextadventureExpectedCsString(textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_1_0_0_6);
		TERMINALS[15] = new textadventure.resource.textadventure.mopp.TextadventureExpectedCsString(textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_1_0_0_7);
		TERMINALS[16] = new textadventure.resource.textadventure.mopp.TextadventureExpectedStructuralFeature(textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_1_0_0_8);
		TERMINALS[17] = new textadventure.resource.textadventure.mopp.TextadventureExpectedCsString(textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_1_0_0_9);
		TERMINALS[18] = new textadventure.resource.textadventure.mopp.TextadventureExpectedCsString(textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_1_0_0_10);
		TERMINALS[19] = new textadventure.resource.textadventure.mopp.TextadventureExpectedStructuralFeature(textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_1_0_0_11);
		TERMINALS[20] = new textadventure.resource.textadventure.mopp.TextadventureExpectedCsString(textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_1_0_0_12);
		TERMINALS[21] = new textadventure.resource.textadventure.mopp.TextadventureExpectedStructuralFeature(textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_2_0_0_1);
		TERMINALS[22] = new textadventure.resource.textadventure.mopp.TextadventureExpectedCsString(textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_2_0_0_2_0_0_0);
		TERMINALS[23] = new textadventure.resource.textadventure.mopp.TextadventureExpectedCsString(textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_2_0_0_3);
		TERMINALS[24] = new textadventure.resource.textadventure.mopp.TextadventureExpectedStructuralFeature(textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_2_0_0_2_0_0_1);
		TERMINALS[25] = new textadventure.resource.textadventure.mopp.TextadventureExpectedCsString(textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_6_0_0_0);
		TERMINALS[26] = new textadventure.resource.textadventure.mopp.TextadventureExpectedCsString(textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_2_0_0_5);
		TERMINALS[27] = new textadventure.resource.textadventure.mopp.TextadventureExpectedCsString(textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_3_0_0_1);
		TERMINALS[28] = new textadventure.resource.textadventure.mopp.TextadventureExpectedStructuralFeature(textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_3_0_0_2);
		TERMINALS[29] = new textadventure.resource.textadventure.mopp.TextadventureExpectedCsString(textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_4_0_0_1);
		TERMINALS[30] = new textadventure.resource.textadventure.mopp.TextadventureExpectedStructuralFeature(textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_4_0_0_2);
		TERMINALS[31] = new textadventure.resource.textadventure.mopp.TextadventureExpectedStructuralFeature(textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_5_0_0_1);
		TERMINALS[32] = new textadventure.resource.textadventure.mopp.TextadventureExpectedCsString(textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_6_0_0_3);
		TERMINALS[33] = new textadventure.resource.textadventure.mopp.TextadventureExpectedEnumerationTerminal(textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_6_0_0_1);
		TERMINALS[34] = new textadventure.resource.textadventure.mopp.TextadventureExpectedEnumerationTerminal(textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_5_0_0_0);
		TERMINALS[35] = new textadventure.resource.textadventure.mopp.TextadventureExpectedCsString(textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_7_0_0_0);
		TERMINALS[36] = new textadventure.resource.textadventure.mopp.TextadventureExpectedCsString(textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_6_0_0_5);
		TERMINALS[37] = new textadventure.resource.textadventure.mopp.TextadventureExpectedStructuralFeature(textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_7_0_0_1);
		TERMINALS[38] = new textadventure.resource.textadventure.mopp.TextadventureExpectedCsString(textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_7_0_0_2);
		TERMINALS[39] = new textadventure.resource.textadventure.mopp.TextadventureExpectedEnumerationTerminal(textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_8_0_0_0);
		TERMINALS[40] = new textadventure.resource.textadventure.mopp.TextadventureExpectedEnumerationTerminal(textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_8_0_0_1);
		TERMINALS[41] = new textadventure.resource.textadventure.mopp.TextadventureExpectedStructuralFeature(textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_9_0_0_1);
		TERMINALS[42] = new textadventure.resource.textadventure.mopp.TextadventureExpectedCsString(textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_9_0_0_2);
		TERMINALS[43] = new textadventure.resource.textadventure.mopp.TextadventureExpectedStructuralFeature(textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_9_0_0_3);
		TERMINALS[44] = new textadventure.resource.textadventure.mopp.TextadventureExpectedCsString(textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_9_0_0_4);
		TERMINALS[45] = new textadventure.resource.textadventure.mopp.TextadventureExpectedCsString(textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_9_0_0_5_0_0_0);
		TERMINALS[46] = new textadventure.resource.textadventure.mopp.TextadventureExpectedCsString(textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_9_0_0_5_0_1_0);
		TERMINALS[47] = new textadventure.resource.textadventure.mopp.TextadventureExpectedCsString(textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_9_0_0_5_0_2_0);
		TERMINALS[48] = new textadventure.resource.textadventure.mopp.TextadventureExpectedCsString(textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_17_0_0_0);
		TERMINALS[49] = new textadventure.resource.textadventure.mopp.TextadventureExpectedCsString(textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_9_0_0_7);
		TERMINALS[50] = new textadventure.resource.textadventure.mopp.TextadventureExpectedCsString(textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_9_0_0_5_0_0_1);
		TERMINALS[51] = new textadventure.resource.textadventure.mopp.TextadventureExpectedStructuralFeature(textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_9_0_0_5_0_0_2);
		TERMINALS[52] = new textadventure.resource.textadventure.mopp.TextadventureExpectedCsString(textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_9_0_0_5_0_1_1);
		TERMINALS[53] = new textadventure.resource.textadventure.mopp.TextadventureExpectedStructuralFeature(textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_9_0_0_5_0_1_2);
		TERMINALS[54] = new textadventure.resource.textadventure.mopp.TextadventureExpectedCsString(textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_9_0_0_5_0_2_1);
		TERMINALS[55] = new textadventure.resource.textadventure.mopp.TextadventureExpectedStructuralFeature(textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_9_0_0_5_0_2_2);
		TERMINALS[56] = new textadventure.resource.textadventure.mopp.TextadventureExpectedCsString(textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_10_0_0_1);
		TERMINALS[57] = new textadventure.resource.textadventure.mopp.TextadventureExpectedStructuralFeature(textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_10_0_0_2);
		TERMINALS[58] = new textadventure.resource.textadventure.mopp.TextadventureExpectedCsString(textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_10_0_0_3);
		TERMINALS[59] = new textadventure.resource.textadventure.mopp.TextadventureExpectedCsString(textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_10_0_0_4);
		TERMINALS[60] = new textadventure.resource.textadventure.mopp.TextadventureExpectedCsString(textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_10_0_0_5);
		TERMINALS[61] = new textadventure.resource.textadventure.mopp.TextadventureExpectedStructuralFeature(textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_10_0_0_6);
		TERMINALS[62] = new textadventure.resource.textadventure.mopp.TextadventureExpectedStructuralFeature(textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_11_0_0_0);
		TERMINALS[63] = new textadventure.resource.textadventure.mopp.TextadventureExpectedCsString(textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_10_0_0_8);
		TERMINALS[64] = new textadventure.resource.textadventure.mopp.TextadventureExpectedCsString(textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_11_0_0_1);
		TERMINALS[65] = new textadventure.resource.textadventure.mopp.TextadventureExpectedCsString(textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_12_0_0_0);
		TERMINALS[66] = new textadventure.resource.textadventure.mopp.TextadventureExpectedStructuralFeature(textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_12_0_0_1_0_0_0);
		TERMINALS[67] = new textadventure.resource.textadventure.mopp.TextadventureExpectedCsString(textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_12_0_0_2);
		TERMINALS[68] = new textadventure.resource.textadventure.mopp.TextadventureExpectedCsString(textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_12_0_0_1_0_0_1_0_0_0);
		TERMINALS[69] = new textadventure.resource.textadventure.mopp.TextadventureExpectedStructuralFeature(textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_12_0_0_1_0_0_1_0_0_1);
		TERMINALS[70] = new textadventure.resource.textadventure.mopp.TextadventureExpectedCsString(textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_12_0_0_3);
		TERMINALS[71] = new textadventure.resource.textadventure.mopp.TextadventureExpectedCsString(textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_14_0_0_0);
		TERMINALS[72] = new textadventure.resource.textadventure.mopp.TextadventureExpectedCsString(textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_15_0_0_0);
		TERMINALS[73] = new textadventure.resource.textadventure.mopp.TextadventureExpectedStructuralFeature(textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_16_0_0_0);
		TERMINALS[74] = new textadventure.resource.textadventure.mopp.TextadventureExpectedCsString(textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_12_0_0_5);
		TERMINALS[75] = new textadventure.resource.textadventure.mopp.TextadventureExpectedCsString(textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_13_0_0_0_0_0_1);
		TERMINALS[76] = new textadventure.resource.textadventure.mopp.TextadventureExpectedStructuralFeature(textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_14_0_0_1);
		TERMINALS[77] = new textadventure.resource.textadventure.mopp.TextadventureExpectedStructuralFeature(textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_15_0_0_1);
		TERMINALS[78] = new textadventure.resource.textadventure.mopp.TextadventureExpectedStructuralFeature(textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_16_0_0_1);
		TERMINALS[79] = new textadventure.resource.textadventure.mopp.TextadventureExpectedStructuralFeature(textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_16_0_0_2);
		TERMINALS[80] = new textadventure.resource.textadventure.mopp.TextadventureExpectedStructuralFeature(textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_17_0_0_1);
	}
	
	public static void initializeTerminals() {
		initializeTerminals0();
	}
	
	public static void initializeFeatures0() {
		FEATURES[0] = textadventure.TextadventurePackage.eINSTANCE.getEWorld().getEStructuralFeature(textadventure.TextadventurePackage.EWORLD__TILES);
		FEATURES[1] = textadventure.TextadventurePackage.eINSTANCE.getEWorld().getEStructuralFeature(textadventure.TextadventurePackage.EWORLD__ITEMS);
		FEATURES[2] = textadventure.TextadventurePackage.eINSTANCE.getEWorld().getEStructuralFeature(textadventure.TextadventurePackage.EWORLD__PERSONS);
		FEATURES[3] = textadventure.TextadventurePackage.eINSTANCE.getEItem().getEStructuralFeature(textadventure.TextadventurePackage.EITEM__EFFECTS);
		FEATURES[4] = textadventure.TextadventurePackage.eINSTANCE.getEWorld().getEStructuralFeature(textadventure.TextadventurePackage.EWORLD__CHECKPOINT);
		FEATURES[5] = textadventure.TextadventurePackage.eINSTANCE.getEWorld().getEStructuralFeature(textadventure.TextadventurePackage.EWORLD__DESTINATION);
		FEATURES[6] = textadventure.TextadventurePackage.eINSTANCE.getEEffect().getEStructuralFeature(textadventure.TextadventurePackage.EEFFECT__ACTION);
		FEATURES[7] = textadventure.TextadventurePackage.eINSTANCE.getERemoveAction().getEStructuralFeature(textadventure.TextadventurePackage.EREMOVE_ACTION__FROM);
		FEATURES[8] = textadventure.TextadventurePackage.eINSTANCE.getEPerson().getEStructuralFeature(textadventure.TextadventurePackage.EPERSON__ABILITIES);
		FEATURES[9] = textadventure.TextadventurePackage.eINSTANCE.getEWorld().getEStructuralFeature(textadventure.TextadventurePackage.EWORLD__BATTLE);
		FEATURES[10] = textadventure.TextadventurePackage.eINSTANCE.getEBattle().getEStructuralFeature(textadventure.TextadventurePackage.EBATTLE__LOGICS);
		FEATURES[11] = textadventure.TextadventurePackage.eINSTANCE.getEBattleCustomization().getEStructuralFeature(textadventure.TextadventurePackage.EBATTLE_CUSTOMIZATION__FUNC);
		FEATURES[12] = textadventure.TextadventurePackage.eINSTANCE.getEFunctionBody().getEStructuralFeature(textadventure.TextadventurePackage.EFUNCTION_BODY__STATEMENTS);
		FEATURES[13] = textadventure.TextadventurePackage.eINSTANCE.getEFunction().getEStructuralFeature(textadventure.TextadventurePackage.EFUNCTION__BODY);
		FEATURES[14] = textadventure.TextadventurePackage.eINSTANCE.getEEffect().getEStructuralFeature(textadventure.TextadventurePackage.EEFFECT__BINDING);
	}
	
	public static void initializeFeatures() {
		initializeFeatures0();
	}
	
	public static void initializeLinks0() {
		LINKS[0] = new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getETile(), FEATURES[0]);
		LINKS[1] = new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getETile(), FEATURES[0]);
		LINKS[2] = new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getEItem(), FEATURES[1]);
		LINKS[3] = new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getEPerson(), FEATURES[2]);
		LINKS[4] = new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getEItem(), FEATURES[1]);
		LINKS[5] = new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getEPerson(), FEATURES[2]);
		LINKS[6] = new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getEPerson(), FEATURES[2]);
		LINKS[7] = new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getEBattle(), FEATURES[9]);
		LINKS[8] = new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getECheckPoint(), FEATURES[4]);
		LINKS[9] = new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getEDestination(), FEATURES[5]);
		LINKS[10] = new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getEBattle(), FEATURES[9]);
		LINKS[11] = new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getECheckPoint(), FEATURES[4]);
		LINKS[12] = new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getEDestination(), FEATURES[5]);
		LINKS[13] = new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getECheckPoint(), FEATURES[4]);
		LINKS[14] = new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getEDestination(), FEATURES[5]);
		LINKS[15] = new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getEDestination(), FEATURES[5]);
		LINKS[16] = new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getETile(), FEATURES[0]);
		LINKS[17] = new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getEItem(), FEATURES[1]);
		LINKS[18] = new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getEPerson(), FEATURES[2]);
		LINKS[19] = new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getEWhileEffect(), FEATURES[3]);
		LINKS[20] = new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getEWhileEffect(), FEATURES[3]);
		LINKS[21] = new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getEWhileEffect(), FEATURES[3]);
		LINKS[22] = new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getEItem(), FEATURES[1]);
		LINKS[23] = new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getEPerson(), FEATURES[2]);
		LINKS[24] = new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getECheckPoint(), FEATURES[4]);
		LINKS[25] = new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getEDestination(), FEATURES[5]);
		LINKS[26] = new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getEDestination(), FEATURES[5]);
		LINKS[27] = new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getEBinding(), FEATURES[14]);
		LINKS[28] = new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getERemoveAction(), FEATURES[6]);
		LINKS[29] = new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getEWhileEffect(), FEATURES[3]);
		LINKS[30] = new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getEListDescription(), FEATURES[7]);
		LINKS[31] = new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getEIgnoreItemAbility(), FEATURES[8]);
		LINKS[32] = new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getEIgnoreItemAbility(), FEATURES[8]);
		LINKS[33] = new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getEIgnoreItemAbility(), FEATURES[8]);
		LINKS[34] = new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getEIgnoreItemAbility(), FEATURES[8]);
		LINKS[35] = new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getEIgnoreItemAbility(), FEATURES[8]);
		LINKS[36] = new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getEIgnoreItemAbility(), FEATURES[8]);
		LINKS[37] = new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getEPerson(), FEATURES[2]);
		LINKS[38] = new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getEBattle(), FEATURES[9]);
		LINKS[39] = new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getECheckPoint(), FEATURES[4]);
		LINKS[40] = new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getEDestination(), FEATURES[5]);
		LINKS[41] = new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getEBattleCustomization(), FEATURES[10]);
		LINKS[42] = new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getEBattleCustomization(), FEATURES[10]);
		LINKS[43] = new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getEBattle(), FEATURES[9]);
		LINKS[44] = new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getECheckPoint(), FEATURES[4]);
		LINKS[45] = new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getEDestination(), FEATURES[5]);
		LINKS[46] = new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getEFunction(), FEATURES[11]);
		LINKS[47] = new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getEBattleCustomization(), FEATURES[10]);
		LINKS[48] = new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getEPrintStatement(), FEATURES[12]);
		LINKS[49] = new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getEFunctionBody(), FEATURES[13]);
		LINKS[50] = new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getERemovePlayerStatement(), FEATURES[12]);
		LINKS[51] = new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getEFunctionBody(), FEATURES[13]);
		LINKS[52] = new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getEMethodCallStatement(), FEATURES[12]);
		LINKS[53] = new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getEFunctionBody(), FEATURES[13]);
		LINKS[54] = new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getEBattleCustomization(), FEATURES[10]);
		LINKS[55] = new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getEPrintStatement(), FEATURES[12]);
		LINKS[56] = new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getERemovePlayerStatement(), FEATURES[12]);
		LINKS[57] = new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getEMethodCallStatement(), FEATURES[12]);
		LINKS[58] = new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getEPrintStatement(), FEATURES[12]);
		LINKS[59] = new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getERemovePlayerStatement(), FEATURES[12]);
		LINKS[60] = new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getEMethodCallStatement(), FEATURES[12]);
		LINKS[61] = new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getEIgnoreItemAbility(), FEATURES[8]);
	}
	
	public static void initializeLinks() {
		initializeLinks0();
	}
	
	public static void wire0() {
		TERMINALS[0].addFollower(TERMINALS[1], EMPTY_LINK_ARRAY);
		TERMINALS[1].addFollower(TERMINALS[2], EMPTY_LINK_ARRAY);
		TERMINALS[2].addFollower(TERMINALS[3], new textadventure.resource.textadventure.mopp.TextadventureContainedFeature[] {new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getETile(), FEATURES[0]), });
		TERMINALS[3].addFollower(TERMINALS[9], EMPTY_LINK_ARRAY);
		TERMINALS[9].addFollower(TERMINALS[10], EMPTY_LINK_ARRAY);
		TERMINALS[10].addFollower(TERMINALS[11], EMPTY_LINK_ARRAY);
		TERMINALS[11].addFollower(TERMINALS[12], EMPTY_LINK_ARRAY);
		TERMINALS[12].addFollower(TERMINALS[13], EMPTY_LINK_ARRAY);
		TERMINALS[13].addFollower(TERMINALS[14], EMPTY_LINK_ARRAY);
		TERMINALS[14].addFollower(TERMINALS[15], EMPTY_LINK_ARRAY);
		TERMINALS[15].addFollower(TERMINALS[16], EMPTY_LINK_ARRAY);
		TERMINALS[16].addFollower(TERMINALS[17], EMPTY_LINK_ARRAY);
		TERMINALS[17].addFollower(TERMINALS[18], EMPTY_LINK_ARRAY);
		TERMINALS[18].addFollower(TERMINALS[19], EMPTY_LINK_ARRAY);
		TERMINALS[19].addFollower(TERMINALS[20], EMPTY_LINK_ARRAY);
		TERMINALS[20].addFollower(TERMINALS[3], new textadventure.resource.textadventure.mopp.TextadventureContainedFeature[] {new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getETile(), FEATURES[0]), });
		TERMINALS[20].addFollower(TERMINALS[4], new textadventure.resource.textadventure.mopp.TextadventureContainedFeature[] {new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getEItem(), FEATURES[1]), });
		TERMINALS[20].addFollower(TERMINALS[5], new textadventure.resource.textadventure.mopp.TextadventureContainedFeature[] {new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getEPerson(), FEATURES[2]), });
		TERMINALS[4].addFollower(TERMINALS[21], EMPTY_LINK_ARRAY);
		TERMINALS[21].addFollower(TERMINALS[22], EMPTY_LINK_ARRAY);
		TERMINALS[21].addFollower(TERMINALS[23], EMPTY_LINK_ARRAY);
		TERMINALS[22].addFollower(TERMINALS[24], EMPTY_LINK_ARRAY);
		TERMINALS[24].addFollower(TERMINALS[23], EMPTY_LINK_ARRAY);
		TERMINALS[23].addFollower(TERMINALS[25], new textadventure.resource.textadventure.mopp.TextadventureContainedFeature[] {new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getEWhileEffect(), FEATURES[3]), });
		TERMINALS[23].addFollower(TERMINALS[26], EMPTY_LINK_ARRAY);
		TERMINALS[26].addFollower(TERMINALS[4], new textadventure.resource.textadventure.mopp.TextadventureContainedFeature[] {new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getEItem(), FEATURES[1]), });
		TERMINALS[26].addFollower(TERMINALS[5], new textadventure.resource.textadventure.mopp.TextadventureContainedFeature[] {new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getEPerson(), FEATURES[2]), });
		TERMINALS[7].addFollower(TERMINALS[27], EMPTY_LINK_ARRAY);
		TERMINALS[27].addFollower(TERMINALS[28], EMPTY_LINK_ARRAY);
		TERMINALS[28].addFollower(TERMINALS[7], new textadventure.resource.textadventure.mopp.TextadventureContainedFeature[] {new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getECheckPoint(), FEATURES[4]), });
		TERMINALS[28].addFollower(TERMINALS[8], new textadventure.resource.textadventure.mopp.TextadventureContainedFeature[] {new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getEDestination(), FEATURES[5]), });
		TERMINALS[8].addFollower(TERMINALS[29], EMPTY_LINK_ARRAY);
		TERMINALS[29].addFollower(TERMINALS[30], EMPTY_LINK_ARRAY);
		TERMINALS[30].addFollower(TERMINALS[8], new textadventure.resource.textadventure.mopp.TextadventureContainedFeature[] {new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getEDestination(), FEATURES[5]), });
		TERMINALS[31].addFollower(TERMINALS[32], EMPTY_LINK_ARRAY);
		TERMINALS[25].addFollower(TERMINALS[33], EMPTY_LINK_ARRAY);
		TERMINALS[32].addFollower(TERMINALS[35], new textadventure.resource.textadventure.mopp.TextadventureContainedFeature[] {new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getERemoveAction(), FEATURES[6]), });
		TERMINALS[36].addFollower(TERMINALS[25], new textadventure.resource.textadventure.mopp.TextadventureContainedFeature[] {new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getEWhileEffect(), FEATURES[3]), });
		TERMINALS[36].addFollower(TERMINALS[26], EMPTY_LINK_ARRAY);
		TERMINALS[35].addFollower(TERMINALS[37], EMPTY_LINK_ARRAY);
		TERMINALS[37].addFollower(TERMINALS[38], EMPTY_LINK_ARRAY);
		TERMINALS[38].addFollower(TERMINALS[39], new textadventure.resource.textadventure.mopp.TextadventureContainedFeature[] {new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getEListDescription(), FEATURES[7]), });
		TERMINALS[5].addFollower(TERMINALS[41], EMPTY_LINK_ARRAY);
		TERMINALS[41].addFollower(TERMINALS[42], EMPTY_LINK_ARRAY);
		TERMINALS[42].addFollower(TERMINALS[43], EMPTY_LINK_ARRAY);
		TERMINALS[43].addFollower(TERMINALS[44], EMPTY_LINK_ARRAY);
		TERMINALS[44].addFollower(TERMINALS[45], EMPTY_LINK_ARRAY);
		TERMINALS[44].addFollower(TERMINALS[46], EMPTY_LINK_ARRAY);
		TERMINALS[44].addFollower(TERMINALS[47], EMPTY_LINK_ARRAY);
		TERMINALS[44].addFollower(TERMINALS[48], new textadventure.resource.textadventure.mopp.TextadventureContainedFeature[] {new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getEIgnoreItemAbility(), FEATURES[8]), });
		TERMINALS[44].addFollower(TERMINALS[49], EMPTY_LINK_ARRAY);
		TERMINALS[45].addFollower(TERMINALS[50], EMPTY_LINK_ARRAY);
		TERMINALS[50].addFollower(TERMINALS[51], EMPTY_LINK_ARRAY);
		TERMINALS[51].addFollower(TERMINALS[45], EMPTY_LINK_ARRAY);
		TERMINALS[51].addFollower(TERMINALS[46], EMPTY_LINK_ARRAY);
		TERMINALS[51].addFollower(TERMINALS[47], EMPTY_LINK_ARRAY);
		TERMINALS[51].addFollower(TERMINALS[48], new textadventure.resource.textadventure.mopp.TextadventureContainedFeature[] {new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getEIgnoreItemAbility(), FEATURES[8]), });
		TERMINALS[51].addFollower(TERMINALS[49], EMPTY_LINK_ARRAY);
		TERMINALS[46].addFollower(TERMINALS[52], EMPTY_LINK_ARRAY);
		TERMINALS[52].addFollower(TERMINALS[53], EMPTY_LINK_ARRAY);
		TERMINALS[53].addFollower(TERMINALS[45], EMPTY_LINK_ARRAY);
		TERMINALS[53].addFollower(TERMINALS[46], EMPTY_LINK_ARRAY);
		TERMINALS[53].addFollower(TERMINALS[47], EMPTY_LINK_ARRAY);
		TERMINALS[53].addFollower(TERMINALS[48], new textadventure.resource.textadventure.mopp.TextadventureContainedFeature[] {new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getEIgnoreItemAbility(), FEATURES[8]), });
		TERMINALS[53].addFollower(TERMINALS[49], EMPTY_LINK_ARRAY);
		TERMINALS[47].addFollower(TERMINALS[54], EMPTY_LINK_ARRAY);
		TERMINALS[54].addFollower(TERMINALS[55], EMPTY_LINK_ARRAY);
		TERMINALS[55].addFollower(TERMINALS[45], EMPTY_LINK_ARRAY);
		TERMINALS[55].addFollower(TERMINALS[46], EMPTY_LINK_ARRAY);
		TERMINALS[55].addFollower(TERMINALS[47], EMPTY_LINK_ARRAY);
		TERMINALS[55].addFollower(TERMINALS[48], new textadventure.resource.textadventure.mopp.TextadventureContainedFeature[] {new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getEIgnoreItemAbility(), FEATURES[8]), });
		TERMINALS[55].addFollower(TERMINALS[49], EMPTY_LINK_ARRAY);
		TERMINALS[49].addFollower(TERMINALS[5], new textadventure.resource.textadventure.mopp.TextadventureContainedFeature[] {new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getEPerson(), FEATURES[2]), });
		TERMINALS[49].addFollower(TERMINALS[6], new textadventure.resource.textadventure.mopp.TextadventureContainedFeature[] {new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getEBattle(), FEATURES[9]), });
		TERMINALS[49].addFollower(TERMINALS[7], new textadventure.resource.textadventure.mopp.TextadventureContainedFeature[] {new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getECheckPoint(), FEATURES[4]), });
		TERMINALS[49].addFollower(TERMINALS[8], new textadventure.resource.textadventure.mopp.TextadventureContainedFeature[] {new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getEDestination(), FEATURES[5]), });
		TERMINALS[6].addFollower(TERMINALS[56], EMPTY_LINK_ARRAY);
		TERMINALS[56].addFollower(TERMINALS[57], EMPTY_LINK_ARRAY);
		TERMINALS[57].addFollower(TERMINALS[58], EMPTY_LINK_ARRAY);
		TERMINALS[58].addFollower(TERMINALS[59], EMPTY_LINK_ARRAY);
		TERMINALS[59].addFollower(TERMINALS[60], EMPTY_LINK_ARRAY);
		TERMINALS[60].addFollower(TERMINALS[61], EMPTY_LINK_ARRAY);
		TERMINALS[61].addFollower(TERMINALS[62], new textadventure.resource.textadventure.mopp.TextadventureContainedFeature[] {new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getEBattleCustomization(), FEATURES[10]), });
		TERMINALS[61].addFollower(TERMINALS[63], EMPTY_LINK_ARRAY);
		TERMINALS[63].addFollower(TERMINALS[6], new textadventure.resource.textadventure.mopp.TextadventureContainedFeature[] {new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getEBattle(), FEATURES[9]), });
		TERMINALS[63].addFollower(TERMINALS[7], new textadventure.resource.textadventure.mopp.TextadventureContainedFeature[] {new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getECheckPoint(), FEATURES[4]), });
		TERMINALS[63].addFollower(TERMINALS[8], new textadventure.resource.textadventure.mopp.TextadventureContainedFeature[] {new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getEDestination(), FEATURES[5]), });
		TERMINALS[62].addFollower(TERMINALS[64], EMPTY_LINK_ARRAY);
		TERMINALS[64].addFollower(TERMINALS[65], new textadventure.resource.textadventure.mopp.TextadventureContainedFeature[] {new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getEFunction(), FEATURES[11]), });
		TERMINALS[65].addFollower(TERMINALS[66], EMPTY_LINK_ARRAY);
		TERMINALS[65].addFollower(TERMINALS[67], EMPTY_LINK_ARRAY);
		TERMINALS[66].addFollower(TERMINALS[68], EMPTY_LINK_ARRAY);
		TERMINALS[66].addFollower(TERMINALS[67], EMPTY_LINK_ARRAY);
		TERMINALS[68].addFollower(TERMINALS[69], EMPTY_LINK_ARRAY);
		TERMINALS[69].addFollower(TERMINALS[68], EMPTY_LINK_ARRAY);
		TERMINALS[69].addFollower(TERMINALS[67], EMPTY_LINK_ARRAY);
		TERMINALS[67].addFollower(TERMINALS[70], EMPTY_LINK_ARRAY);
		TERMINALS[70].addFollower(TERMINALS[71], new textadventure.resource.textadventure.mopp.TextadventureContainedFeature[] {new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getEPrintStatement(), FEATURES[12]), new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getEFunctionBody(), FEATURES[13]), });
		TERMINALS[70].addFollower(TERMINALS[72], new textadventure.resource.textadventure.mopp.TextadventureContainedFeature[] {new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getERemovePlayerStatement(), FEATURES[12]), new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getEFunctionBody(), FEATURES[13]), });
		TERMINALS[70].addFollower(TERMINALS[73], new textadventure.resource.textadventure.mopp.TextadventureContainedFeature[] {new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getEMethodCallStatement(), FEATURES[12]), new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getEFunctionBody(), FEATURES[13]), });
		TERMINALS[70].addFollower(TERMINALS[74], EMPTY_LINK_ARRAY);
		TERMINALS[74].addFollower(TERMINALS[62], new textadventure.resource.textadventure.mopp.TextadventureContainedFeature[] {new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getEBattleCustomization(), FEATURES[10]), });
		TERMINALS[74].addFollower(TERMINALS[63], EMPTY_LINK_ARRAY);
		TERMINALS[75].addFollower(TERMINALS[71], new textadventure.resource.textadventure.mopp.TextadventureContainedFeature[] {new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getEPrintStatement(), FEATURES[12]), });
		TERMINALS[75].addFollower(TERMINALS[72], new textadventure.resource.textadventure.mopp.TextadventureContainedFeature[] {new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getERemovePlayerStatement(), FEATURES[12]), });
		TERMINALS[75].addFollower(TERMINALS[73], new textadventure.resource.textadventure.mopp.TextadventureContainedFeature[] {new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getEMethodCallStatement(), FEATURES[12]), });
		TERMINALS[75].addFollower(TERMINALS[74], EMPTY_LINK_ARRAY);
		TERMINALS[71].addFollower(TERMINALS[76], EMPTY_LINK_ARRAY);
		TERMINALS[76].addFollower(TERMINALS[75], EMPTY_LINK_ARRAY);
		TERMINALS[72].addFollower(TERMINALS[77], EMPTY_LINK_ARRAY);
		TERMINALS[77].addFollower(TERMINALS[75], EMPTY_LINK_ARRAY);
		TERMINALS[73].addFollower(TERMINALS[78], EMPTY_LINK_ARRAY);
		TERMINALS[78].addFollower(TERMINALS[79], EMPTY_LINK_ARRAY);
		TERMINALS[78].addFollower(TERMINALS[75], EMPTY_LINK_ARRAY);
		TERMINALS[79].addFollower(TERMINALS[79], EMPTY_LINK_ARRAY);
		TERMINALS[79].addFollower(TERMINALS[75], EMPTY_LINK_ARRAY);
		TERMINALS[48].addFollower(TERMINALS[80], EMPTY_LINK_ARRAY);
		TERMINALS[80].addFollower(TERMINALS[48], new textadventure.resource.textadventure.mopp.TextadventureContainedFeature[] {new textadventure.resource.textadventure.mopp.TextadventureContainedFeature(textadventure.TextadventurePackage.eINSTANCE.getEIgnoreItemAbility(), FEATURES[8]), });
		TERMINALS[80].addFollower(TERMINALS[49], EMPTY_LINK_ARRAY);
	}
	
	public static void wire() {
		wire0();
	}
	
	static {
		// initialize the arrays
		initializeTerminals();
		initializeFeatures();
		initializeLinks();
		// wire the terminals
		wire();
	}
}
