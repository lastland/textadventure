/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package textadventure.resource.textadventure.grammar;

/**
 * A class to represent a keyword in the grammar.
 */
public class TextadventureKeyword extends textadventure.resource.textadventure.grammar.TextadventureSyntaxElement {
	
	private final String value;
	
	public TextadventureKeyword(String value, textadventure.resource.textadventure.grammar.TextadventureCardinality cardinality) {
		super(cardinality, null);
		this.value = value;
	}
	
	public String getValue() {
		return value;
	}
	
	public String toString() {
		return value;
	}
	
}
