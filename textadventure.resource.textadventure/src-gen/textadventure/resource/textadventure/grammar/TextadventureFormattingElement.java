/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package textadventure.resource.textadventure.grammar;

public abstract class TextadventureFormattingElement extends textadventure.resource.textadventure.grammar.TextadventureSyntaxElement {
	
	public TextadventureFormattingElement(textadventure.resource.textadventure.grammar.TextadventureCardinality cardinality) {
		super(cardinality, null);
	}
	
}
