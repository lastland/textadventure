/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package textadventure.resource.textadventure.grammar;

/**
 * A class to represent a rules in the grammar.
 */
public class TextadventureRule extends textadventure.resource.textadventure.grammar.TextadventureSyntaxElement {
	
	private final org.eclipse.emf.ecore.EClass metaclass;
	
	public TextadventureRule(org.eclipse.emf.ecore.EClass metaclass, textadventure.resource.textadventure.grammar.TextadventureChoice choice, textadventure.resource.textadventure.grammar.TextadventureCardinality cardinality) {
		super(cardinality, new textadventure.resource.textadventure.grammar.TextadventureSyntaxElement[] {choice});
		this.metaclass = metaclass;
	}
	
	public org.eclipse.emf.ecore.EClass getMetaclass() {
		return metaclass;
	}
	
	public textadventure.resource.textadventure.grammar.TextadventureChoice getDefinition() {
		return (textadventure.resource.textadventure.grammar.TextadventureChoice) getChildren()[0];
	}
	
}

