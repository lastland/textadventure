/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package textadventure.resource.textadventure.grammar;

public class TextadventureChoice extends textadventure.resource.textadventure.grammar.TextadventureSyntaxElement {
	
	public TextadventureChoice(textadventure.resource.textadventure.grammar.TextadventureCardinality cardinality, textadventure.resource.textadventure.grammar.TextadventureSyntaxElement... choices) {
		super(cardinality, choices);
	}
	
	public String toString() {
		return textadventure.resource.textadventure.util.TextadventureStringUtil.explode(getChildren(), "|");
	}
	
}
