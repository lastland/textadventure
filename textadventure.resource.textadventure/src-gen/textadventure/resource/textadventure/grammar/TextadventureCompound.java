/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package textadventure.resource.textadventure.grammar;

public class TextadventureCompound extends textadventure.resource.textadventure.grammar.TextadventureSyntaxElement {
	
	public TextadventureCompound(textadventure.resource.textadventure.grammar.TextadventureChoice choice, textadventure.resource.textadventure.grammar.TextadventureCardinality cardinality) {
		super(cardinality, new textadventure.resource.textadventure.grammar.TextadventureSyntaxElement[] {choice});
	}
	
	public String toString() {
		return "(" + getChildren()[0] + ")";
	}
	
}
