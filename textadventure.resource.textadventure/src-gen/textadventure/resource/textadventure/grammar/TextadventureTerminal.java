/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package textadventure.resource.textadventure.grammar;

public class TextadventureTerminal extends textadventure.resource.textadventure.grammar.TextadventureSyntaxElement {
	
	private final org.eclipse.emf.ecore.EStructuralFeature feature;
	private final int mandatoryOccurencesAfter;
	
	public TextadventureTerminal(org.eclipse.emf.ecore.EStructuralFeature feature, textadventure.resource.textadventure.grammar.TextadventureCardinality cardinality, int mandatoryOccurencesAfter) {
		super(cardinality, null);
		this.feature = feature;
		this.mandatoryOccurencesAfter = mandatoryOccurencesAfter;
	}
	
	public org.eclipse.emf.ecore.EStructuralFeature getFeature() {
		return feature;
	}
	
	public int getMandatoryOccurencesAfter() {
		return mandatoryOccurencesAfter;
	}
	
	public String toString() {
		return feature.getName() + "[]";
	}
	
}
