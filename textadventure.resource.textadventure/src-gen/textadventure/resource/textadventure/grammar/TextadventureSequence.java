/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package textadventure.resource.textadventure.grammar;

public class TextadventureSequence extends textadventure.resource.textadventure.grammar.TextadventureSyntaxElement {
	
	public TextadventureSequence(textadventure.resource.textadventure.grammar.TextadventureCardinality cardinality, textadventure.resource.textadventure.grammar.TextadventureSyntaxElement... elements) {
		super(cardinality, elements);
	}
	
	public String toString() {
		return textadventure.resource.textadventure.util.TextadventureStringUtil.explode(getChildren(), " ");
	}
	
}
