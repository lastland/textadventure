/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package textadventure.resource.textadventure.util;

/**
 * Utility class that provides a method to cast objects to type parameterized
 * classes without a warning.
 */
public class TextadventureCastUtil {
	
	@SuppressWarnings("unchecked")	
	public static <T> T cast(Object temp) {
		return (T) temp;
	}
}
