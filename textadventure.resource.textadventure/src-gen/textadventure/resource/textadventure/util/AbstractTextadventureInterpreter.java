/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package textadventure.resource.textadventure.util;

/**
 * This class provides basic infrastructure to interpret models. To implement
 * concrete interpreters, subclass this abstract interpreter and override the
 * interprete_* methods. The interpretation can be customized by binding the two
 * type parameters (ResultType, ContextType). The former is returned by all
 * interprete_* methods, while the latter is passed from method to method while
 * traversing the model. The concrete traversal strategy can also be exchanged.
 * One can use a static traversal strategy by pushing all objects to interpret on
 * the interpretation stack (using addObjectToInterprete()) before calling
 * interprete(). Alternatively, the traversal strategy can be dynamic by pushing
 * objects on the interpretation stack during interpretation.
 */
public class AbstractTextadventureInterpreter<ResultType, ContextType> {
	
	private java.util.Stack<org.eclipse.emf.ecore.EObject> interpretationStack = new java.util.Stack<org.eclipse.emf.ecore.EObject>();
	private java.util.List<textadventure.resource.textadventure.ITextadventureInterpreterListener> listeners = new java.util.ArrayList<textadventure.resource.textadventure.ITextadventureInterpreterListener>();
	private org.eclipse.emf.ecore.EObject nextObjectToInterprete;
	private Object currentContext;
	
	public ResultType interprete(ContextType context) {
		ResultType result = null;
		org.eclipse.emf.ecore.EObject next = null;
		currentContext = context;
		while (!interpretationStack.empty()) {
			try {
				next = interpretationStack.pop();
			} catch (java.util.EmptyStackException ese) {
				// this can happen when the interpreter was terminated between the call to empty()
				// and pop()
				break;
			}
			nextObjectToInterprete = next;
			notifyListeners(next);
			result = interprete(next, context);
			if (!continueInterpretation(context, result)) {
				break;
			}
		}
		currentContext = null;
		return result;
	}
	
	/**
	 * Override this method to stop the overall interpretation depending on the result
	 * of the interpretation of a single model elements.
	 */
	public boolean continueInterpretation(ContextType context, ResultType result) {
		return true;
	}
	
	public ResultType interprete(org.eclipse.emf.ecore.EObject object, ContextType context) {
		ResultType result = null;
		if (object instanceof textadventure.EWorld) {
			result = interprete_textadventure_EWorld((textadventure.EWorld) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof textadventure.ETile) {
			result = interprete_textadventure_ETile((textadventure.ETile) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof textadventure.ECheckPoint) {
			result = interprete_textadventure_ECheckPoint((textadventure.ECheckPoint) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof textadventure.EDestination) {
			result = interprete_textadventure_EDestination((textadventure.EDestination) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof textadventure.EItem) {
			result = interprete_textadventure_EItem((textadventure.EItem) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof textadventure.EBinding) {
			result = interprete_textadventure_EBinding((textadventure.EBinding) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof textadventure.EWhileEffect) {
			result = interprete_textadventure_EWhileEffect((textadventure.EWhileEffect) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof textadventure.EEffect) {
			result = interprete_textadventure_EEffect((textadventure.EEffect) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof textadventure.ERemoveAction) {
			result = interprete_textadventure_ERemoveAction((textadventure.ERemoveAction) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof textadventure.EAction) {
			result = interprete_textadventure_EAction((textadventure.EAction) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof textadventure.EListDescription) {
			result = interprete_textadventure_EListDescription((textadventure.EListDescription) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof textadventure.EPerson) {
			result = interprete_textadventure_EPerson((textadventure.EPerson) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof textadventure.EBattle) {
			result = interprete_textadventure_EBattle((textadventure.EBattle) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof textadventure.EBattleCustomization) {
			result = interprete_textadventure_EBattleCustomization((textadventure.EBattleCustomization) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof textadventure.EFunction) {
			result = interprete_textadventure_EFunction((textadventure.EFunction) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof textadventure.EFunctionBody) {
			result = interprete_textadventure_EFunctionBody((textadventure.EFunctionBody) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof textadventure.EPrintStatement) {
			result = interprete_textadventure_EPrintStatement((textadventure.EPrintStatement) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof textadventure.ERemovePlayerStatement) {
			result = interprete_textadventure_ERemovePlayerStatement((textadventure.ERemovePlayerStatement) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof textadventure.EMethodCallStatement) {
			result = interprete_textadventure_EMethodCallStatement((textadventure.EMethodCallStatement) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof textadventure.EStatement) {
			result = interprete_textadventure_EStatement((textadventure.EStatement) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof textadventure.EIgnoreItemAbility) {
			result = interprete_textadventure_EIgnoreItemAbility((textadventure.EIgnoreItemAbility) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof textadventure.EAbility) {
			result = interprete_textadventure_EAbility((textadventure.EAbility) object, context);
		}
		if (result != null) {
			return result;
		}
		return result;
	}
	
	public ResultType interprete_textadventure_EWorld(textadventure.EWorld eWorld, ContextType context) {
		return null;
	}
	
	public ResultType interprete_textadventure_ETile(textadventure.ETile eTile, ContextType context) {
		return null;
	}
	
	public ResultType interprete_textadventure_ECheckPoint(textadventure.ECheckPoint eCheckPoint, ContextType context) {
		return null;
	}
	
	public ResultType interprete_textadventure_EDestination(textadventure.EDestination eDestination, ContextType context) {
		return null;
	}
	
	public ResultType interprete_textadventure_EItem(textadventure.EItem eItem, ContextType context) {
		return null;
	}
	
	public ResultType interprete_textadventure_EBinding(textadventure.EBinding eBinding, ContextType context) {
		return null;
	}
	
	public ResultType interprete_textadventure_EEffect(textadventure.EEffect eEffect, ContextType context) {
		return null;
	}
	
	public ResultType interprete_textadventure_EWhileEffect(textadventure.EWhileEffect eWhileEffect, ContextType context) {
		return null;
	}
	
	public ResultType interprete_textadventure_EAction(textadventure.EAction eAction, ContextType context) {
		return null;
	}
	
	public ResultType interprete_textadventure_ERemoveAction(textadventure.ERemoveAction eRemoveAction, ContextType context) {
		return null;
	}
	
	public ResultType interprete_textadventure_EListDescription(textadventure.EListDescription eListDescription, ContextType context) {
		return null;
	}
	
	public ResultType interprete_textadventure_EPerson(textadventure.EPerson ePerson, ContextType context) {
		return null;
	}
	
	public ResultType interprete_textadventure_EBattle(textadventure.EBattle eBattle, ContextType context) {
		return null;
	}
	
	public ResultType interprete_textadventure_EBattleCustomization(textadventure.EBattleCustomization eBattleCustomization, ContextType context) {
		return null;
	}
	
	public ResultType interprete_textadventure_EFunction(textadventure.EFunction eFunction, ContextType context) {
		return null;
	}
	
	public ResultType interprete_textadventure_EFunctionBody(textadventure.EFunctionBody eFunctionBody, ContextType context) {
		return null;
	}
	
	public ResultType interprete_textadventure_EStatement(textadventure.EStatement eStatement, ContextType context) {
		return null;
	}
	
	public ResultType interprete_textadventure_EPrintStatement(textadventure.EPrintStatement ePrintStatement, ContextType context) {
		return null;
	}
	
	public ResultType interprete_textadventure_ERemovePlayerStatement(textadventure.ERemovePlayerStatement eRemovePlayerStatement, ContextType context) {
		return null;
	}
	
	public ResultType interprete_textadventure_EMethodCallStatement(textadventure.EMethodCallStatement eMethodCallStatement, ContextType context) {
		return null;
	}
	
	public ResultType interprete_textadventure_EAbility(textadventure.EAbility eAbility, ContextType context) {
		return null;
	}
	
	public ResultType interprete_textadventure_EIgnoreItemAbility(textadventure.EIgnoreItemAbility eIgnoreItemAbility, ContextType context) {
		return null;
	}
	
	private void notifyListeners(org.eclipse.emf.ecore.EObject element) {
		for (textadventure.resource.textadventure.ITextadventureInterpreterListener listener : listeners) {
			listener.handleInterpreteObject(element);
		}
	}
	
	/**
	 * Adds the given object to the interpretation stack. Attention: Objects that are
	 * added first, are interpret last.
	 */
	public void addObjectToInterprete(org.eclipse.emf.ecore.EObject object) {
		interpretationStack.push(object);
	}
	
	/**
	 * Adds the given collection of objects to the interpretation stack. Attention:
	 * Collections that are added first, are interpret last.
	 */
	public void addObjectsToInterprete(java.util.Collection<? extends org.eclipse.emf.ecore.EObject> objects) {
		for (org.eclipse.emf.ecore.EObject object : objects) {
			addObjectToInterprete(object);
		}
	}
	
	/**
	 * Adds the given collection of objects in reverse order to the interpretation
	 * stack.
	 */
	public void addObjectsToInterpreteInReverseOrder(java.util.Collection<? extends org.eclipse.emf.ecore.EObject> objects) {
		java.util.List<org.eclipse.emf.ecore.EObject> reverse = new java.util.ArrayList<org.eclipse.emf.ecore.EObject>(objects.size());
		reverse.addAll(objects);
		java.util.Collections.reverse(reverse);
		addObjectsToInterprete(reverse);
	}
	
	/**
	 * Adds the given object and all its children to the interpretation stack such
	 * that they are interpret in top down order.
	 */
	public void addObjectTreeToInterpreteTopDown(org.eclipse.emf.ecore.EObject root) {
		java.util.List<org.eclipse.emf.ecore.EObject> objects = new java.util.ArrayList<org.eclipse.emf.ecore.EObject>();
		objects.add(root);
		java.util.Iterator<org.eclipse.emf.ecore.EObject> it = root.eAllContents();
		while (it.hasNext()) {
			org.eclipse.emf.ecore.EObject eObject = (org.eclipse.emf.ecore.EObject) it.next();
			objects.add(eObject);
		}
		addObjectsToInterpreteInReverseOrder(objects);
	}
	
	public void addListener(textadventure.resource.textadventure.ITextadventureInterpreterListener newListener) {
		listeners.add(newListener);
	}
	
	public boolean removeListener(textadventure.resource.textadventure.ITextadventureInterpreterListener listener) {
		return listeners.remove(listener);
	}
	
	public org.eclipse.emf.ecore.EObject getNextObjectToInterprete() {
		return nextObjectToInterprete;
	}
	
	public java.util.Stack<org.eclipse.emf.ecore.EObject> getInterpretationStack() {
		return interpretationStack;
	}
	
	public void terminate() {
		interpretationStack.clear();
	}
	
	public Object getCurrentContext() {
		return currentContext;
	}
	
}
