/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package textadventure.resource.textadventure.util;

/**
 * Class TextadventureTextResourceUtil can be used to perform common tasks on text
 * resources, such as loading and saving resources, as well as, checking them for
 * errors. This class is deprecated and has been replaced by
 * textadventure.resource.textadventure.util.TextadventureResourceUtil.
 */
public class TextadventureTextResourceUtil {
	
	/**
	 * Use
	 * textadventure.resource.textadventure.util.TextadventureResourceUtil.getResource(
	 * ) instead.
	 */
	@Deprecated	
	public static textadventure.resource.textadventure.mopp.TextadventureResource getResource(org.eclipse.core.resources.IFile file) {
		return new textadventure.resource.textadventure.util.TextadventureEclipseProxy().getResource(file);
	}
	
	/**
	 * Use
	 * textadventure.resource.textadventure.util.TextadventureResourceUtil.getResource(
	 * ) instead.
	 */
	@Deprecated	
	public static textadventure.resource.textadventure.mopp.TextadventureResource getResource(java.io.File file, java.util.Map<?,?> options) {
		return textadventure.resource.textadventure.util.TextadventureResourceUtil.getResource(file, options);
	}
	
	/**
	 * Use
	 * textadventure.resource.textadventure.util.TextadventureResourceUtil.getResource(
	 * ) instead.
	 */
	@Deprecated	
	public static textadventure.resource.textadventure.mopp.TextadventureResource getResource(org.eclipse.emf.common.util.URI uri) {
		return textadventure.resource.textadventure.util.TextadventureResourceUtil.getResource(uri);
	}
	
	/**
	 * Use
	 * textadventure.resource.textadventure.util.TextadventureResourceUtil.getResource(
	 * ) instead.
	 */
	@Deprecated	
	public static textadventure.resource.textadventure.mopp.TextadventureResource getResource(org.eclipse.emf.common.util.URI uri, java.util.Map<?,?> options) {
		return textadventure.resource.textadventure.util.TextadventureResourceUtil.getResource(uri, options);
	}
	
}
