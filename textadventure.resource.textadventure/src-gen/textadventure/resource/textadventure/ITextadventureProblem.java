/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package textadventure.resource.textadventure;

public interface ITextadventureProblem {
	public String getMessage();
	public textadventure.resource.textadventure.TextadventureEProblemSeverity getSeverity();
	public textadventure.resource.textadventure.TextadventureEProblemType getType();
	public java.util.Collection<textadventure.resource.textadventure.ITextadventureQuickFix> getQuickFixes();
}
