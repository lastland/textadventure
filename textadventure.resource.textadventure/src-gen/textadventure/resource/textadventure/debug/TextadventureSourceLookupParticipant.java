/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package textadventure.resource.textadventure.debug;

public class TextadventureSourceLookupParticipant extends org.eclipse.debug.core.sourcelookup.AbstractSourceLookupParticipant {
	
	public String getSourceName(Object object) throws org.eclipse.core.runtime.CoreException {
		if (object instanceof textadventure.resource.textadventure.debug.TextadventureStackFrame) {
			textadventure.resource.textadventure.debug.TextadventureStackFrame frame = (textadventure.resource.textadventure.debug.TextadventureStackFrame) object;
			return frame.getResourceURI();
		}
		return null;
	}
	
}
