/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package textadventure.resource.textadventure.debug;

public class TextadventureSourceLocator extends org.eclipse.debug.core.sourcelookup.AbstractSourceLookupDirector {
	
	public void initializeParticipants() {
		addParticipants(new org.eclipse.debug.core.sourcelookup.ISourceLookupParticipant[]{new textadventure.resource.textadventure.debug.TextadventureSourceLookupParticipant()});
	}
	
}
