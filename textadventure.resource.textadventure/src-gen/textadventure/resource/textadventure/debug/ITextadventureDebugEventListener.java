/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package textadventure.resource.textadventure.debug;

public interface ITextadventureDebugEventListener {
	
	/**
	 * Notification that the given event occurred in the while debugging.
	 */
	public void handleMessage(textadventure.resource.textadventure.debug.TextadventureDebugMessage message);
}
