/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package textadventure.resource.textadventure;

public enum TextadventureEProblemSeverity {
	WARNING, ERROR;
}
