/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package textadventure.resource.textadventure;

/**
 * A simple interface for generic functions with one argument.
 */
public interface ITextadventureFunction1<ReturnType, ArgumentType1> {
	
	public ReturnType execute(ArgumentType1 argument1);
}
