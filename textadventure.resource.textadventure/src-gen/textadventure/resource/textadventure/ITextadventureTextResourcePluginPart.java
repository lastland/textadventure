/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package textadventure.resource.textadventure;

/**
 * This interface is extended by some other generated classes. It provides access
 * to the plug-in meta information.
 */
public interface ITextadventureTextResourcePluginPart {
	
	/**
	 * Returns a meta information object for the language plug-in that contains this
	 * part.
	 */
	public textadventure.resource.textadventure.ITextadventureMetaInformation getMetaInformation();
	
}
