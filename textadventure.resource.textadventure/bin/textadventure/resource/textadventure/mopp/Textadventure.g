grammar Textadventure;

options {
	superClass = TextadventureANTLRParserBase;
	backtrack = true;
	memoize = true;
}

@lexer::header {
	package textadventure.resource.textadventure.mopp;
}

@lexer::members {
	public java.util.List<org.antlr.runtime3_4_0.RecognitionException> lexerExceptions  = new java.util.ArrayList<org.antlr.runtime3_4_0.RecognitionException>();
	public java.util.List<Integer> lexerExceptionsPosition = new java.util.ArrayList<Integer>();
	
	public void reportError(org.antlr.runtime3_4_0.RecognitionException e) {
		lexerExceptions.add(e);
		lexerExceptionsPosition.add(((org.antlr.runtime3_4_0.ANTLRStringStream) input).index());
	}
}
@header{
	package textadventure.resource.textadventure.mopp;
}

@members{
	private textadventure.resource.textadventure.ITextadventureTokenResolverFactory tokenResolverFactory = new textadventure.resource.textadventure.mopp.TextadventureTokenResolverFactory();
	
	/**
	 * the index of the last token that was handled by collectHiddenTokens()
	 */
	private int lastPosition;
	
	/**
	 * A flag that indicates whether the parser should remember all expected elements.
	 * This flag is set to true when using the parse for code completion. Otherwise it
	 * is set to false.
	 */
	private boolean rememberExpectedElements = false;
	
	private Object parseToIndexTypeObject;
	private int lastTokenIndex = 0;
	
	/**
	 * A list of expected elements the were collected while parsing the input stream.
	 * This list is only filled if <code>rememberExpectedElements</code> is set to
	 * true.
	 */
	private java.util.List<textadventure.resource.textadventure.mopp.TextadventureExpectedTerminal> expectedElements = new java.util.ArrayList<textadventure.resource.textadventure.mopp.TextadventureExpectedTerminal>();
	
	private int mismatchedTokenRecoveryTries = 0;
	/**
	 * A helper list to allow a lexer to pass errors to its parser
	 */
	protected java.util.List<org.antlr.runtime3_4_0.RecognitionException> lexerExceptions = java.util.Collections.synchronizedList(new java.util.ArrayList<org.antlr.runtime3_4_0.RecognitionException>());
	
	/**
	 * Another helper list to allow a lexer to pass positions of errors to its parser
	 */
	protected java.util.List<Integer> lexerExceptionsPosition = java.util.Collections.synchronizedList(new java.util.ArrayList<Integer>());
	
	/**
	 * A stack for incomplete objects. This stack is used filled when the parser is
	 * used for code completion. Whenever the parser starts to read an object it is
	 * pushed on the stack. Once the element was parser completely it is popped from
	 * the stack.
	 */
	java.util.List<org.eclipse.emf.ecore.EObject> incompleteObjects = new java.util.ArrayList<org.eclipse.emf.ecore.EObject>();
	
	private int stopIncludingHiddenTokens;
	private int stopExcludingHiddenTokens;
	private int tokenIndexOfLastCompleteElement;
	
	private int expectedElementsIndexOfLastCompleteElement;
	
	/**
	 * The offset indicating the cursor position when the parser is used for code
	 * completion by calling parseToExpectedElements().
	 */
	private int cursorOffset;
	
	/**
	 * The offset of the first hidden token of the last expected element. This offset
	 * is used to discard expected elements, which are not needed for code completion.
	 */
	private int lastStartIncludingHidden;
	
	protected void addErrorToResource(final String errorMessage, final int column, final int line, final int startIndex, final int stopIndex) {
		postParseCommands.add(new textadventure.resource.textadventure.ITextadventureCommand<textadventure.resource.textadventure.ITextadventureTextResource>() {
			public boolean execute(textadventure.resource.textadventure.ITextadventureTextResource resource) {
				if (resource == null) {
					// the resource can be null if the parser is used for code completion
					return true;
				}
				resource.addProblem(new textadventure.resource.textadventure.ITextadventureProblem() {
					public textadventure.resource.textadventure.TextadventureEProblemSeverity getSeverity() {
						return textadventure.resource.textadventure.TextadventureEProblemSeverity.ERROR;
					}
					public textadventure.resource.textadventure.TextadventureEProblemType getType() {
						return textadventure.resource.textadventure.TextadventureEProblemType.SYNTAX_ERROR;
					}
					public String getMessage() {
						return errorMessage;
					}
					public java.util.Collection<textadventure.resource.textadventure.ITextadventureQuickFix> getQuickFixes() {
						return null;
					}
				}, column, line, startIndex, stopIndex);
				return true;
			}
		});
	}
	
	public void addExpectedElement(org.eclipse.emf.ecore.EClass eClass, int[] ids) {
		if (!this.rememberExpectedElements) {
			return;
		}
		int terminalID = ids[0];
		int followSetID = ids[1];
		textadventure.resource.textadventure.ITextadventureExpectedElement terminal = textadventure.resource.textadventure.grammar.TextadventureFollowSetProvider.TERMINALS[terminalID];
		textadventure.resource.textadventure.mopp.TextadventureContainedFeature[] containmentFeatures = new textadventure.resource.textadventure.mopp.TextadventureContainedFeature[ids.length - 2];
		for (int i = 2; i < ids.length; i++) {
			containmentFeatures[i - 2] = textadventure.resource.textadventure.grammar.TextadventureFollowSetProvider.LINKS[ids[i]];
		}
		textadventure.resource.textadventure.grammar.TextadventureContainmentTrace containmentTrace = new textadventure.resource.textadventure.grammar.TextadventureContainmentTrace(eClass, containmentFeatures);
		org.eclipse.emf.ecore.EObject container = getLastIncompleteElement();
		textadventure.resource.textadventure.mopp.TextadventureExpectedTerminal expectedElement = new textadventure.resource.textadventure.mopp.TextadventureExpectedTerminal(container, terminal, followSetID, containmentTrace);
		setPosition(expectedElement, input.index());
		int startIncludingHiddenTokens = expectedElement.getStartIncludingHiddenTokens();
		if (lastStartIncludingHidden >= 0 && lastStartIncludingHidden < startIncludingHiddenTokens && cursorOffset > startIncludingHiddenTokens) {
			// clear list of expected elements
			this.expectedElements.clear();
			this.expectedElementsIndexOfLastCompleteElement = 0;
		}
		lastStartIncludingHidden = startIncludingHiddenTokens;
		this.expectedElements.add(expectedElement);
	}
	
	protected void collectHiddenTokens(org.eclipse.emf.ecore.EObject element) {
	}
	
	protected void copyLocalizationInfos(final org.eclipse.emf.ecore.EObject source, final org.eclipse.emf.ecore.EObject target) {
		if (disableLocationMap) {
			return;
		}
		postParseCommands.add(new textadventure.resource.textadventure.ITextadventureCommand<textadventure.resource.textadventure.ITextadventureTextResource>() {
			public boolean execute(textadventure.resource.textadventure.ITextadventureTextResource resource) {
				textadventure.resource.textadventure.ITextadventureLocationMap locationMap = resource.getLocationMap();
				if (locationMap == null) {
					// the locationMap can be null if the parser is used for code completion
					return true;
				}
				locationMap.setCharStart(target, locationMap.getCharStart(source));
				locationMap.setCharEnd(target, locationMap.getCharEnd(source));
				locationMap.setColumn(target, locationMap.getColumn(source));
				locationMap.setLine(target, locationMap.getLine(source));
				return true;
			}
		});
	}
	
	protected void copyLocalizationInfos(final org.antlr.runtime3_4_0.CommonToken source, final org.eclipse.emf.ecore.EObject target) {
		if (disableLocationMap) {
			return;
		}
		postParseCommands.add(new textadventure.resource.textadventure.ITextadventureCommand<textadventure.resource.textadventure.ITextadventureTextResource>() {
			public boolean execute(textadventure.resource.textadventure.ITextadventureTextResource resource) {
				textadventure.resource.textadventure.ITextadventureLocationMap locationMap = resource.getLocationMap();
				if (locationMap == null) {
					// the locationMap can be null if the parser is used for code completion
					return true;
				}
				if (source == null) {
					return true;
				}
				locationMap.setCharStart(target, source.getStartIndex());
				locationMap.setCharEnd(target, source.getStopIndex());
				locationMap.setColumn(target, source.getCharPositionInLine());
				locationMap.setLine(target, source.getLine());
				return true;
			}
		});
	}
	
	/**
	 * Sets the end character index and the last line for the given object in the
	 * location map.
	 */
	protected void setLocalizationEnd(java.util.Collection<textadventure.resource.textadventure.ITextadventureCommand<textadventure.resource.textadventure.ITextadventureTextResource>> postParseCommands , final org.eclipse.emf.ecore.EObject object, final int endChar, final int endLine) {
		if (disableLocationMap) {
			return;
		}
		postParseCommands.add(new textadventure.resource.textadventure.ITextadventureCommand<textadventure.resource.textadventure.ITextadventureTextResource>() {
			public boolean execute(textadventure.resource.textadventure.ITextadventureTextResource resource) {
				textadventure.resource.textadventure.ITextadventureLocationMap locationMap = resource.getLocationMap();
				if (locationMap == null) {
					// the locationMap can be null if the parser is used for code completion
					return true;
				}
				locationMap.setCharEnd(object, endChar);
				locationMap.setLine(object, endLine);
				return true;
			}
		});
	}
	
	public textadventure.resource.textadventure.ITextadventureTextParser createInstance(java.io.InputStream actualInputStream, String encoding) {
		try {
			if (encoding == null) {
				return new TextadventureParser(new org.antlr.runtime3_4_0.CommonTokenStream(new TextadventureLexer(new org.antlr.runtime3_4_0.ANTLRInputStream(actualInputStream))));
			} else {
				return new TextadventureParser(new org.antlr.runtime3_4_0.CommonTokenStream(new TextadventureLexer(new org.antlr.runtime3_4_0.ANTLRInputStream(actualInputStream, encoding))));
			}
		} catch (java.io.IOException e) {
			new textadventure.resource.textadventure.util.TextadventureRuntimeUtil().logError("Error while creating parser.", e);
			return null;
		}
	}
	
	/**
	 * This default constructor is only used to call createInstance() on it.
	 */
	public TextadventureParser() {
		super(null);
	}
	
	protected org.eclipse.emf.ecore.EObject doParse() throws org.antlr.runtime3_4_0.RecognitionException {
		this.lastPosition = 0;
		// required because the lexer class can not be subclassed
		((TextadventureLexer) getTokenStream().getTokenSource()).lexerExceptions = lexerExceptions;
		((TextadventureLexer) getTokenStream().getTokenSource()).lexerExceptionsPosition = lexerExceptionsPosition;
		Object typeObject = getTypeObject();
		if (typeObject == null) {
			return start();
		} else if (typeObject instanceof org.eclipse.emf.ecore.EClass) {
			org.eclipse.emf.ecore.EClass type = (org.eclipse.emf.ecore.EClass) typeObject;
			if (type.getInstanceClass() == textadventure.EWorld.class) {
				return parse_textadventure_EWorld();
			}
			if (type.getInstanceClass() == textadventure.ETile.class) {
				return parse_textadventure_ETile();
			}
			if (type.getInstanceClass() == textadventure.EItem.class) {
				return parse_textadventure_EItem();
			}
			if (type.getInstanceClass() == textadventure.ECheckPoint.class) {
				return parse_textadventure_ECheckPoint();
			}
			if (type.getInstanceClass() == textadventure.EDestination.class) {
				return parse_textadventure_EDestination();
			}
			if (type.getInstanceClass() == textadventure.EBinding.class) {
				return parse_textadventure_EBinding();
			}
			if (type.getInstanceClass() == textadventure.EWhileEffect.class) {
				return parse_textadventure_EWhileEffect();
			}
			if (type.getInstanceClass() == textadventure.ERemoveAction.class) {
				return parse_textadventure_ERemoveAction();
			}
			if (type.getInstanceClass() == textadventure.EListDescription.class) {
				return parse_textadventure_EListDescription();
			}
			if (type.getInstanceClass() == textadventure.EPerson.class) {
				return parse_textadventure_EPerson();
			}
			if (type.getInstanceClass() == textadventure.EBattle.class) {
				return parse_textadventure_EBattle();
			}
			if (type.getInstanceClass() == textadventure.EBattleCustomization.class) {
				return parse_textadventure_EBattleCustomization();
			}
			if (type.getInstanceClass() == textadventure.EFunction.class) {
				return parse_textadventure_EFunction();
			}
			if (type.getInstanceClass() == textadventure.EFunctionBody.class) {
				return parse_textadventure_EFunctionBody();
			}
			if (type.getInstanceClass() == textadventure.EPrintStatement.class) {
				return parse_textadventure_EPrintStatement();
			}
			if (type.getInstanceClass() == textadventure.ERemovePlayerStatement.class) {
				return parse_textadventure_ERemovePlayerStatement();
			}
			if (type.getInstanceClass() == textadventure.EMethodCallStatement.class) {
				return parse_textadventure_EMethodCallStatement();
			}
			if (type.getInstanceClass() == textadventure.EIgnoreItemAbility.class) {
				return parse_textadventure_EIgnoreItemAbility();
			}
		}
		throw new textadventure.resource.textadventure.mopp.TextadventureUnexpectedContentTypeException(typeObject);
	}
	
	public int getMismatchedTokenRecoveryTries() {
		return mismatchedTokenRecoveryTries;
	}
	
	public Object getMissingSymbol(org.antlr.runtime3_4_0.IntStream arg0, org.antlr.runtime3_4_0.RecognitionException arg1, int arg2, org.antlr.runtime3_4_0.BitSet arg3) {
		mismatchedTokenRecoveryTries++;
		return super.getMissingSymbol(arg0, arg1, arg2, arg3);
	}
	
	public Object getParseToIndexTypeObject() {
		return parseToIndexTypeObject;
	}
	
	protected Object getTypeObject() {
		Object typeObject = getParseToIndexTypeObject();
		if (typeObject != null) {
			return typeObject;
		}
		java.util.Map<?,?> options = getOptions();
		if (options != null) {
			typeObject = options.get(textadventure.resource.textadventure.ITextadventureOptions.RESOURCE_CONTENT_TYPE);
		}
		return typeObject;
	}
	
	/**
	 * Implementation that calls {@link #doParse()} and handles the thrown
	 * RecognitionExceptions.
	 */
	public textadventure.resource.textadventure.ITextadventureParseResult parse() {
		terminateParsing = false;
		postParseCommands = new java.util.ArrayList<textadventure.resource.textadventure.ITextadventureCommand<textadventure.resource.textadventure.ITextadventureTextResource>>();
		textadventure.resource.textadventure.mopp.TextadventureParseResult parseResult = new textadventure.resource.textadventure.mopp.TextadventureParseResult();
		try {
			org.eclipse.emf.ecore.EObject result =  doParse();
			if (lexerExceptions.isEmpty()) {
				parseResult.setRoot(result);
			}
		} catch (org.antlr.runtime3_4_0.RecognitionException re) {
			reportError(re);
		} catch (java.lang.IllegalArgumentException iae) {
			if ("The 'no null' constraint is violated".equals(iae.getMessage())) {
				// can be caused if a null is set on EMF models where not allowed. this will just
				// happen if other errors occurred before
			} else {
				iae.printStackTrace();
			}
		}
		for (org.antlr.runtime3_4_0.RecognitionException re : lexerExceptions) {
			reportLexicalError(re);
		}
		parseResult.getPostParseCommands().addAll(postParseCommands);
		return parseResult;
	}
	
	public java.util.List<textadventure.resource.textadventure.mopp.TextadventureExpectedTerminal> parseToExpectedElements(org.eclipse.emf.ecore.EClass type, textadventure.resource.textadventure.ITextadventureTextResource dummyResource, int cursorOffset) {
		this.rememberExpectedElements = true;
		this.parseToIndexTypeObject = type;
		this.cursorOffset = cursorOffset;
		this.lastStartIncludingHidden = -1;
		final org.antlr.runtime3_4_0.CommonTokenStream tokenStream = (org.antlr.runtime3_4_0.CommonTokenStream) getTokenStream();
		textadventure.resource.textadventure.ITextadventureParseResult result = parse();
		for (org.eclipse.emf.ecore.EObject incompleteObject : incompleteObjects) {
			org.antlr.runtime3_4_0.Lexer lexer = (org.antlr.runtime3_4_0.Lexer) tokenStream.getTokenSource();
			int endChar = lexer.getCharIndex();
			int endLine = lexer.getLine();
			setLocalizationEnd(result.getPostParseCommands(), incompleteObject, endChar, endLine);
		}
		if (result != null) {
			org.eclipse.emf.ecore.EObject root = result.getRoot();
			if (root != null) {
				dummyResource.getContentsInternal().add(root);
			}
			for (textadventure.resource.textadventure.ITextadventureCommand<textadventure.resource.textadventure.ITextadventureTextResource> command : result.getPostParseCommands()) {
				command.execute(dummyResource);
			}
		}
		// remove all expected elements that were added after the last complete element
		expectedElements = expectedElements.subList(0, expectedElementsIndexOfLastCompleteElement + 1);
		int lastFollowSetID = expectedElements.get(expectedElementsIndexOfLastCompleteElement).getFollowSetID();
		java.util.Set<textadventure.resource.textadventure.mopp.TextadventureExpectedTerminal> currentFollowSet = new java.util.LinkedHashSet<textadventure.resource.textadventure.mopp.TextadventureExpectedTerminal>();
		java.util.List<textadventure.resource.textadventure.mopp.TextadventureExpectedTerminal> newFollowSet = new java.util.ArrayList<textadventure.resource.textadventure.mopp.TextadventureExpectedTerminal>();
		for (int i = expectedElementsIndexOfLastCompleteElement; i >= 0; i--) {
			textadventure.resource.textadventure.mopp.TextadventureExpectedTerminal expectedElementI = expectedElements.get(i);
			if (expectedElementI.getFollowSetID() == lastFollowSetID) {
				currentFollowSet.add(expectedElementI);
			} else {
				break;
			}
		}
		int followSetID = 103;
		int i;
		for (i = tokenIndexOfLastCompleteElement; i < tokenStream.size(); i++) {
			org.antlr.runtime3_4_0.CommonToken nextToken = (org.antlr.runtime3_4_0.CommonToken) tokenStream.get(i);
			if (nextToken.getType() < 0) {
				break;
			}
			if (nextToken.getChannel() == 99) {
				// hidden tokens do not reduce the follow set
			} else {
				// now that we have found the next visible token the position for that expected
				// terminals can be set
				for (textadventure.resource.textadventure.mopp.TextadventureExpectedTerminal nextFollow : newFollowSet) {
					lastTokenIndex = 0;
					setPosition(nextFollow, i);
				}
				newFollowSet.clear();
				// normal tokens do reduce the follow set - only elements that match the token are
				// kept
				for (textadventure.resource.textadventure.mopp.TextadventureExpectedTerminal nextFollow : currentFollowSet) {
					if (nextFollow.getTerminal().getTokenNames().contains(getTokenNames()[nextToken.getType()])) {
						// keep this one - it matches
						java.util.Collection<textadventure.resource.textadventure.util.TextadventurePair<textadventure.resource.textadventure.ITextadventureExpectedElement, textadventure.resource.textadventure.mopp.TextadventureContainedFeature[]>> newFollowers = nextFollow.getTerminal().getFollowers();
						for (textadventure.resource.textadventure.util.TextadventurePair<textadventure.resource.textadventure.ITextadventureExpectedElement, textadventure.resource.textadventure.mopp.TextadventureContainedFeature[]> newFollowerPair : newFollowers) {
							textadventure.resource.textadventure.ITextadventureExpectedElement newFollower = newFollowerPair.getLeft();
							org.eclipse.emf.ecore.EObject container = getLastIncompleteElement();
							textadventure.resource.textadventure.grammar.TextadventureContainmentTrace containmentTrace = new textadventure.resource.textadventure.grammar.TextadventureContainmentTrace(null, newFollowerPair.getRight());
							textadventure.resource.textadventure.mopp.TextadventureExpectedTerminal newFollowTerminal = new textadventure.resource.textadventure.mopp.TextadventureExpectedTerminal(container, newFollower, followSetID, containmentTrace);
							newFollowSet.add(newFollowTerminal);
							expectedElements.add(newFollowTerminal);
						}
					}
				}
				currentFollowSet.clear();
				currentFollowSet.addAll(newFollowSet);
			}
			followSetID++;
		}
		// after the last token in the stream we must set the position for the elements
		// that were added during the last iteration of the loop
		for (textadventure.resource.textadventure.mopp.TextadventureExpectedTerminal nextFollow : newFollowSet) {
			lastTokenIndex = 0;
			setPosition(nextFollow, i);
		}
		return this.expectedElements;
	}
	
	public void setPosition(textadventure.resource.textadventure.mopp.TextadventureExpectedTerminal expectedElement, int tokenIndex) {
		int currentIndex = Math.max(0, tokenIndex);
		for (int index = lastTokenIndex; index < currentIndex; index++) {
			if (index >= input.size()) {
				break;
			}
			org.antlr.runtime3_4_0.CommonToken tokenAtIndex = (org.antlr.runtime3_4_0.CommonToken) input.get(index);
			stopIncludingHiddenTokens = tokenAtIndex.getStopIndex() + 1;
			if (tokenAtIndex.getChannel() != 99 && !anonymousTokens.contains(tokenAtIndex)) {
				stopExcludingHiddenTokens = tokenAtIndex.getStopIndex() + 1;
			}
		}
		lastTokenIndex = Math.max(0, currentIndex);
		expectedElement.setPosition(stopExcludingHiddenTokens, stopIncludingHiddenTokens);
	}
	
	public Object recoverFromMismatchedToken(org.antlr.runtime3_4_0.IntStream input, int ttype, org.antlr.runtime3_4_0.BitSet follow) throws org.antlr.runtime3_4_0.RecognitionException {
		if (!rememberExpectedElements) {
			return super.recoverFromMismatchedToken(input, ttype, follow);
		} else {
			return null;
		}
	}
	
	/**
	 * Translates errors thrown by the parser into human readable messages.
	 */
	public void reportError(final org.antlr.runtime3_4_0.RecognitionException e)  {
		String message = e.getMessage();
		if (e instanceof org.antlr.runtime3_4_0.MismatchedTokenException) {
			org.antlr.runtime3_4_0.MismatchedTokenException mte = (org.antlr.runtime3_4_0.MismatchedTokenException) e;
			String expectedTokenName = formatTokenName(mte.expecting);
			String actualTokenName = formatTokenName(e.token.getType());
			message = "Syntax error on token \"" + e.token.getText() + " (" + actualTokenName + ")\", \"" + expectedTokenName + "\" expected";
		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedTreeNodeException) {
			org.antlr.runtime3_4_0.MismatchedTreeNodeException mtne = (org.antlr.runtime3_4_0.MismatchedTreeNodeException) e;
			String expectedTokenName = formatTokenName(mtne.expecting);
			message = "mismatched tree node: " + "xxx" + "; tokenName " + expectedTokenName;
		} else if (e instanceof org.antlr.runtime3_4_0.NoViableAltException) {
			message = "Syntax error on token \"" + e.token.getText() + "\", check following tokens";
		} else if (e instanceof org.antlr.runtime3_4_0.EarlyExitException) {
			message = "Syntax error on token \"" + e.token.getText() + "\", delete this token";
		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedSetException) {
			org.antlr.runtime3_4_0.MismatchedSetException mse = (org.antlr.runtime3_4_0.MismatchedSetException) e;
			message = "mismatched token: " + e.token + "; expecting set " + mse.expecting;
		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedNotSetException) {
			org.antlr.runtime3_4_0.MismatchedNotSetException mse = (org.antlr.runtime3_4_0.MismatchedNotSetException) e;
			message = "mismatched token: " +  e.token + "; expecting set " + mse.expecting;
		} else if (e instanceof org.antlr.runtime3_4_0.FailedPredicateException) {
			org.antlr.runtime3_4_0.FailedPredicateException fpe = (org.antlr.runtime3_4_0.FailedPredicateException) e;
			message = "rule " + fpe.ruleName + " failed predicate: {" +  fpe.predicateText + "}?";
		}
		// the resource may be null if the parser is used for code completion
		final String finalMessage = message;
		if (e.token instanceof org.antlr.runtime3_4_0.CommonToken) {
			final org.antlr.runtime3_4_0.CommonToken ct = (org.antlr.runtime3_4_0.CommonToken) e.token;
			addErrorToResource(finalMessage, ct.getCharPositionInLine(), ct.getLine(), ct.getStartIndex(), ct.getStopIndex());
		} else {
			addErrorToResource(finalMessage, e.token.getCharPositionInLine(), e.token.getLine(), 1, 5);
		}
	}
	
	/**
	 * Translates errors thrown by the lexer into human readable messages.
	 */
	public void reportLexicalError(final org.antlr.runtime3_4_0.RecognitionException e)  {
		String message = "";
		if (e instanceof org.antlr.runtime3_4_0.MismatchedTokenException) {
			org.antlr.runtime3_4_0.MismatchedTokenException mte = (org.antlr.runtime3_4_0.MismatchedTokenException) e;
			message = "Syntax error on token \"" + ((char) e.c) + "\", \"" + (char) mte.expecting + "\" expected";
		} else if (e instanceof org.antlr.runtime3_4_0.NoViableAltException) {
			message = "Syntax error on token \"" + ((char) e.c) + "\", delete this token";
		} else if (e instanceof org.antlr.runtime3_4_0.EarlyExitException) {
			org.antlr.runtime3_4_0.EarlyExitException eee = (org.antlr.runtime3_4_0.EarlyExitException) e;
			message = "required (...)+ loop (decision=" + eee.decisionNumber + ") did not match anything; on line " + e.line + ":" + e.charPositionInLine + " char=" + ((char) e.c) + "'";
		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedSetException) {
			org.antlr.runtime3_4_0.MismatchedSetException mse = (org.antlr.runtime3_4_0.MismatchedSetException) e;
			message = "mismatched char: '" + ((char) e.c) + "' on line " + e.line + ":" + e.charPositionInLine + "; expecting set " + mse.expecting;
		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedNotSetException) {
			org.antlr.runtime3_4_0.MismatchedNotSetException mse = (org.antlr.runtime3_4_0.MismatchedNotSetException) e;
			message = "mismatched char: '" + ((char) e.c) + "' on line " + e.line + ":" + e.charPositionInLine + "; expecting set " + mse.expecting;
		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedRangeException) {
			org.antlr.runtime3_4_0.MismatchedRangeException mre = (org.antlr.runtime3_4_0.MismatchedRangeException) e;
			message = "mismatched char: '" + ((char) e.c) + "' on line " + e.line + ":" + e.charPositionInLine + "; expecting set '" + (char) mre.a + "'..'" + (char) mre.b + "'";
		} else if (e instanceof org.antlr.runtime3_4_0.FailedPredicateException) {
			org.antlr.runtime3_4_0.FailedPredicateException fpe = (org.antlr.runtime3_4_0.FailedPredicateException) e;
			message = "rule " + fpe.ruleName + " failed predicate: {" + fpe.predicateText + "}?";
		}
		addErrorToResource(message, e.charPositionInLine, e.line, lexerExceptionsPosition.get(lexerExceptions.indexOf(e)), lexerExceptionsPosition.get(lexerExceptions.indexOf(e)));
	}
	
	private void startIncompleteElement(Object object) {
		if (object instanceof org.eclipse.emf.ecore.EObject) {
			this.incompleteObjects.add((org.eclipse.emf.ecore.EObject) object);
		}
	}
	
	private void completedElement(Object object, boolean isContainment) {
		if (isContainment && !this.incompleteObjects.isEmpty()) {
			boolean exists = this.incompleteObjects.remove(object);
			if (!exists) {
			}
		}
		if (object instanceof org.eclipse.emf.ecore.EObject) {
			this.tokenIndexOfLastCompleteElement = getTokenStream().index();
			this.expectedElementsIndexOfLastCompleteElement = expectedElements.size() - 1;
		}
	}
	
	private org.eclipse.emf.ecore.EObject getLastIncompleteElement() {
		if (incompleteObjects.isEmpty()) {
			return null;
		}
		return incompleteObjects.get(incompleteObjects.size() - 1);
	}
	
}

start returns [ org.eclipse.emf.ecore.EObject element = null]
:
	{
		// follow set for start rule(s)
		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[0]);
		expectedElementsIndexOfLastCompleteElement = 0;
	}
	(
		c0 = parse_textadventure_EWorld{ element = c0; }
	)
	EOF	{
		retrieveLayoutInformation(element, null, null, false);
	}
	
;

parse_textadventure_EWorld returns [textadventure.EWorld element = null]
@init{
}
:
	a0 = 'player' {
		if (element == null) {
			element = textadventure.TextadventureFactory.eINSTANCE.createEWorld();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_0_0_0_0, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[1]);
	}
	
	a1 = '=' {
		if (element == null) {
			element = textadventure.TextadventureFactory.eINSTANCE.createEWorld();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_0_0_0_1, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[2]);
	}
	
	(
		a2 = TEXT		
		{
			if (terminateParsing) {
				throw new textadventure.resource.textadventure.mopp.TextadventureTerminateParsingException();
			}
			if (element == null) {
				element = textadventure.TextadventureFactory.eINSTANCE.createEWorld();
				startIncompleteElement(element);
			}
			if (a2 != null) {
				textadventure.resource.textadventure.ITextadventureTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
				tokenResolver.setOptions(getOptions());
				textadventure.resource.textadventure.ITextadventureTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a2.getText(), element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EWORLD__PLAYER_PERSON), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a2).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStopIndex());
				}
				String resolved = (String) resolvedObject;
				textadventure.EPerson proxy = textadventure.TextadventureFactory.eINSTANCE.createEPerson();
				collectHiddenTokens(element);
				registerContextDependentProxy(new textadventure.resource.textadventure.mopp.TextadventureContextDependentURIFragmentFactory<textadventure.EWorld, textadventure.EPerson>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getEWorldPlayerPersonReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EWORLD__PLAYER_PERSON), resolved, proxy);
				if (proxy != null) {
					Object value = proxy;
					element.eSet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EWORLD__PLAYER_PERSON), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_0_0_0_2, proxy, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, element);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, proxy);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEWorld(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[3]);
	}
	
	(
		(
			a3_0 = parse_textadventure_ETile			{
				if (terminateParsing) {
					throw new textadventure.resource.textadventure.mopp.TextadventureTerminateParsingException();
				}
				if (element == null) {
					element = textadventure.TextadventureFactory.eINSTANCE.createEWorld();
					startIncompleteElement(element);
				}
				if (a3_0 != null) {
					if (a3_0 != null) {
						Object value = a3_0;
						addObjectToList(element, textadventure.TextadventurePackage.EWORLD__TILES, value);
						completedElement(value, true);
					}
					collectHiddenTokens(element);
					retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_0_0_0_3, a3_0, true);
					copyLocalizationInfos(a3_0, element);
				}
			}
		)
		
	)+	{
		// expected elements (follow set)
		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEWorld(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[4]);
		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEWorld(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[5]);
		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEWorld(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[6]);
	}
	
	(
		(
			a4_0 = parse_textadventure_EItem			{
				if (terminateParsing) {
					throw new textadventure.resource.textadventure.mopp.TextadventureTerminateParsingException();
				}
				if (element == null) {
					element = textadventure.TextadventureFactory.eINSTANCE.createEWorld();
					startIncompleteElement(element);
				}
				if (a4_0 != null) {
					if (a4_0 != null) {
						Object value = a4_0;
						addObjectToList(element, textadventure.TextadventurePackage.EWORLD__ITEMS, value);
						completedElement(value, true);
					}
					collectHiddenTokens(element);
					retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_0_0_0_4, a4_0, true);
					copyLocalizationInfos(a4_0, element);
				}
			}
		)
		
	)*	{
		// expected elements (follow set)
		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEWorld(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[7]);
		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEWorld(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[8]);
	}
	
	(
		(
			a5_0 = parse_textadventure_EPerson			{
				if (terminateParsing) {
					throw new textadventure.resource.textadventure.mopp.TextadventureTerminateParsingException();
				}
				if (element == null) {
					element = textadventure.TextadventureFactory.eINSTANCE.createEWorld();
					startIncompleteElement(element);
				}
				if (a5_0 != null) {
					if (a5_0 != null) {
						Object value = a5_0;
						addObjectToList(element, textadventure.TextadventurePackage.EWORLD__PERSONS, value);
						completedElement(value, true);
					}
					collectHiddenTokens(element);
					retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_0_0_0_5, a5_0, true);
					copyLocalizationInfos(a5_0, element);
				}
			}
		)
		
	)+	{
		// expected elements (follow set)
		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEWorld(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[9]);
		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEWorld(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[10]);
		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEWorld(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[11]);
		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEWorld(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[12]);
	}
	
	(
		(
			a6_0 = parse_textadventure_EBattle			{
				if (terminateParsing) {
					throw new textadventure.resource.textadventure.mopp.TextadventureTerminateParsingException();
				}
				if (element == null) {
					element = textadventure.TextadventureFactory.eINSTANCE.createEWorld();
					startIncompleteElement(element);
				}
				if (a6_0 != null) {
					if (a6_0 != null) {
						Object value = a6_0;
						addObjectToList(element, textadventure.TextadventurePackage.EWORLD__BATTLE, value);
						completedElement(value, true);
					}
					collectHiddenTokens(element);
					retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_0_0_0_6, a6_0, true);
					copyLocalizationInfos(a6_0, element);
				}
			}
		)
		
	)*	{
		// expected elements (follow set)
		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEWorld(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[13]);
		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEWorld(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[14]);
		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEWorld(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[15]);
	}
	
	(
		(
			a7_0 = parse_textadventure_ECheckPoint			{
				if (terminateParsing) {
					throw new textadventure.resource.textadventure.mopp.TextadventureTerminateParsingException();
				}
				if (element == null) {
					element = textadventure.TextadventureFactory.eINSTANCE.createEWorld();
					startIncompleteElement(element);
				}
				if (a7_0 != null) {
					if (a7_0 != null) {
						Object value = a7_0;
						addObjectToList(element, textadventure.TextadventurePackage.EWORLD__CHECKPOINT, value);
						completedElement(value, true);
					}
					collectHiddenTokens(element);
					retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_0_0_0_7, a7_0, true);
					copyLocalizationInfos(a7_0, element);
				}
			}
		)
		
	)*	{
		// expected elements (follow set)
		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEWorld(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[16]);
		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEWorld(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[17]);
	}
	
	(
		(
			a8_0 = parse_textadventure_EDestination			{
				if (terminateParsing) {
					throw new textadventure.resource.textadventure.mopp.TextadventureTerminateParsingException();
				}
				if (element == null) {
					element = textadventure.TextadventureFactory.eINSTANCE.createEWorld();
					startIncompleteElement(element);
				}
				if (a8_0 != null) {
					if (a8_0 != null) {
						Object value = a8_0;
						addObjectToList(element, textadventure.TextadventurePackage.EWORLD__DESTINATION, value);
						completedElement(value, true);
					}
					collectHiddenTokens(element);
					retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_0_0_0_8, a8_0, true);
					copyLocalizationInfos(a8_0, element);
				}
			}
		)
		
	)*	{
		// expected elements (follow set)
		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEWorld(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[18]);
	}
	
;

parse_textadventure_ETile returns [textadventure.ETile element = null]
@init{
}
:
	a0 = 'tile' {
		if (element == null) {
			element = textadventure.TextadventureFactory.eINSTANCE.createETile();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_1_0_0_0, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[19]);
	}
	
	(
		a1 = TEXT		
		{
			if (terminateParsing) {
				throw new textadventure.resource.textadventure.mopp.TextadventureTerminateParsingException();
			}
			if (element == null) {
				element = textadventure.TextadventureFactory.eINSTANCE.createETile();
				startIncompleteElement(element);
			}
			if (a1 != null) {
				textadventure.resource.textadventure.ITextadventureTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
				tokenResolver.setOptions(getOptions());
				textadventure.resource.textadventure.ITextadventureTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.ETILE__ID), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a1).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStopIndex());
				}
				java.lang.String resolved = (java.lang.String) resolvedObject;
				if (resolved != null) {
					Object value = resolved;
					element.eSet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.ETILE__ID), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_1_0_0_1, resolved, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a1, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[20]);
	}
	
	a2 = '{' {
		if (element == null) {
			element = textadventure.TextadventureFactory.eINSTANCE.createETile();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_1_0_0_2, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[21]);
	}
	
	a3 = 'x' {
		if (element == null) {
			element = textadventure.TextadventureFactory.eINSTANCE.createETile();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_1_0_0_3, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a3, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[22]);
	}
	
	a4 = '=' {
		if (element == null) {
			element = textadventure.TextadventureFactory.eINSTANCE.createETile();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_1_0_0_4, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a4, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[23]);
	}
	
	(
		a5 = INTEGER		
		{
			if (terminateParsing) {
				throw new textadventure.resource.textadventure.mopp.TextadventureTerminateParsingException();
			}
			if (element == null) {
				element = textadventure.TextadventureFactory.eINSTANCE.createETile();
				startIncompleteElement(element);
			}
			if (a5 != null) {
				textadventure.resource.textadventure.ITextadventureTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("INTEGER");
				tokenResolver.setOptions(getOptions());
				textadventure.resource.textadventure.ITextadventureTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a5.getText(), element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.ETILE__X), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a5).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a5).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a5).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a5).getStopIndex());
				}
				java.lang.Integer resolved = (java.lang.Integer) resolvedObject;
				if (resolved != null) {
					Object value = resolved;
					element.eSet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.ETILE__X), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_1_0_0_5, resolved, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a5, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[24]);
	}
	
	a6 = 'y' {
		if (element == null) {
			element = textadventure.TextadventureFactory.eINSTANCE.createETile();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_1_0_0_6, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a6, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[25]);
	}
	
	a7 = '=' {
		if (element == null) {
			element = textadventure.TextadventureFactory.eINSTANCE.createETile();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_1_0_0_7, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a7, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[26]);
	}
	
	(
		a8 = INTEGER		
		{
			if (terminateParsing) {
				throw new textadventure.resource.textadventure.mopp.TextadventureTerminateParsingException();
			}
			if (element == null) {
				element = textadventure.TextadventureFactory.eINSTANCE.createETile();
				startIncompleteElement(element);
			}
			if (a8 != null) {
				textadventure.resource.textadventure.ITextadventureTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("INTEGER");
				tokenResolver.setOptions(getOptions());
				textadventure.resource.textadventure.ITextadventureTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a8.getText(), element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.ETILE__Y), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a8).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a8).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a8).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a8).getStopIndex());
				}
				java.lang.Integer resolved = (java.lang.Integer) resolvedObject;
				if (resolved != null) {
					Object value = resolved;
					element.eSet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.ETILE__Y), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_1_0_0_8, resolved, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a8, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[27]);
	}
	
	a9 = 'description' {
		if (element == null) {
			element = textadventure.TextadventureFactory.eINSTANCE.createETile();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_1_0_0_9, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a9, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[28]);
	}
	
	a10 = '=' {
		if (element == null) {
			element = textadventure.TextadventureFactory.eINSTANCE.createETile();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_1_0_0_10, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a10, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[29]);
	}
	
	(
		a11 = QUOTED_34_34		
		{
			if (terminateParsing) {
				throw new textadventure.resource.textadventure.mopp.TextadventureTerminateParsingException();
			}
			if (element == null) {
				element = textadventure.TextadventureFactory.eINSTANCE.createETile();
				startIncompleteElement(element);
			}
			if (a11 != null) {
				textadventure.resource.textadventure.ITextadventureTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34");
				tokenResolver.setOptions(getOptions());
				textadventure.resource.textadventure.ITextadventureTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a11.getText(), element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.ETILE__DESCRIPTION), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a11).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a11).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a11).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a11).getStopIndex());
				}
				java.lang.String resolved = (java.lang.String) resolvedObject;
				if (resolved != null) {
					Object value = resolved;
					element.eSet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.ETILE__DESCRIPTION), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_1_0_0_11, resolved, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a11, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[30]);
	}
	
	a12 = '}' {
		if (element == null) {
			element = textadventure.TextadventureFactory.eINSTANCE.createETile();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_1_0_0_12, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a12, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEWorld(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[31]);
		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEWorld(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[32]);
		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEWorld(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[33]);
	}
	
;

parse_textadventure_EItem returns [textadventure.EItem element = null]
@init{
}
:
	a0 = 'item' {
		if (element == null) {
			element = textadventure.TextadventureFactory.eINSTANCE.createEItem();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_2_0_0_0, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[34]);
	}
	
	(
		a1 = TEXT		
		{
			if (terminateParsing) {
				throw new textadventure.resource.textadventure.mopp.TextadventureTerminateParsingException();
			}
			if (element == null) {
				element = textadventure.TextadventureFactory.eINSTANCE.createEItem();
				startIncompleteElement(element);
			}
			if (a1 != null) {
				textadventure.resource.textadventure.ITextadventureTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
				tokenResolver.setOptions(getOptions());
				textadventure.resource.textadventure.ITextadventureTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EITEM__NAME), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a1).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStopIndex());
				}
				java.lang.String resolved = (java.lang.String) resolvedObject;
				if (resolved != null) {
					Object value = resolved;
					element.eSet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EITEM__NAME), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_2_0_0_1, resolved, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a1, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[35]);
		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[36]);
	}
	
	(
		(
			a2 = '@' {
				if (element == null) {
					element = textadventure.TextadventureFactory.eINSTANCE.createEItem();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_2_0_0_2_0_0_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[37]);
			}
			
			(
				a3 = TEXT				
				{
					if (terminateParsing) {
						throw new textadventure.resource.textadventure.mopp.TextadventureTerminateParsingException();
					}
					if (element == null) {
						element = textadventure.TextadventureFactory.eINSTANCE.createEItem();
						startIncompleteElement(element);
					}
					if (a3 != null) {
						textadventure.resource.textadventure.ITextadventureTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
						tokenResolver.setOptions(getOptions());
						textadventure.resource.textadventure.ITextadventureTokenResolveResult result = getFreshTokenResolveResult();
						tokenResolver.resolve(a3.getText(), element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EITEM__LOCATION), result);
						Object resolvedObject = result.getResolvedToken();
						if (resolvedObject == null) {
							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a3).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a3).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a3).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a3).getStopIndex());
						}
						String resolved = (String) resolvedObject;
						textadventure.ETile proxy = textadventure.TextadventureFactory.eINSTANCE.createETile();
						collectHiddenTokens(element);
						registerContextDependentProxy(new textadventure.resource.textadventure.mopp.TextadventureContextDependentURIFragmentFactory<textadventure.EItem, textadventure.ETile>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getEItemLocationReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EITEM__LOCATION), resolved, proxy);
						if (proxy != null) {
							Object value = proxy;
							element.eSet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EITEM__LOCATION), value);
							completedElement(value, false);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_2_0_0_2_0_0_1, proxy, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a3, element);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a3, proxy);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[38]);
			}
			
		)
		
	)?	{
		// expected elements (follow set)
		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[39]);
	}
	
	a4 = '{' {
		if (element == null) {
			element = textadventure.TextadventureFactory.eINSTANCE.createEItem();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_2_0_0_3, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a4, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEItem(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[40]);
		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[41]);
	}
	
	(
		(
			(
				a5_0 = parse_textadventure_EEffect				{
					if (terminateParsing) {
						throw new textadventure.resource.textadventure.mopp.TextadventureTerminateParsingException();
					}
					if (element == null) {
						element = textadventure.TextadventureFactory.eINSTANCE.createEItem();
						startIncompleteElement(element);
					}
					if (a5_0 != null) {
						if (a5_0 != null) {
							Object value = a5_0;
							addObjectToList(element, textadventure.TextadventurePackage.EITEM__EFFECTS, value);
							completedElement(value, true);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_2_0_0_4_0_0_0, a5_0, true);
						copyLocalizationInfos(a5_0, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEItem(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[42]);
				addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[43]);
			}
			
		)
		
	)*	{
		// expected elements (follow set)
		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEItem(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[44]);
		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[45]);
	}
	
	a6 = '}' {
		if (element == null) {
			element = textadventure.TextadventureFactory.eINSTANCE.createEItem();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_2_0_0_5, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a6, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEWorld(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[46]);
		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEWorld(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[47]);
	}
	
;

parse_textadventure_ECheckPoint returns [textadventure.ECheckPoint element = null]
@init{
}
:
	a0 = 'checkpoint' {
		if (element == null) {
			element = textadventure.TextadventureFactory.eINSTANCE.createECheckPoint();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_3_0_0_0, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[48]);
	}
	
	a1 = '@' {
		if (element == null) {
			element = textadventure.TextadventureFactory.eINSTANCE.createECheckPoint();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_3_0_0_1, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[49]);
	}
	
	(
		a2 = TEXT		
		{
			if (terminateParsing) {
				throw new textadventure.resource.textadventure.mopp.TextadventureTerminateParsingException();
			}
			if (element == null) {
				element = textadventure.TextadventureFactory.eINSTANCE.createECheckPoint();
				startIncompleteElement(element);
			}
			if (a2 != null) {
				textadventure.resource.textadventure.ITextadventureTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
				tokenResolver.setOptions(getOptions());
				textadventure.resource.textadventure.ITextadventureTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a2.getText(), element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.ECHECK_POINT__POINT), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a2).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStopIndex());
				}
				String resolved = (String) resolvedObject;
				textadventure.ETile proxy = textadventure.TextadventureFactory.eINSTANCE.createETile();
				collectHiddenTokens(element);
				registerContextDependentProxy(new textadventure.resource.textadventure.mopp.TextadventureContextDependentURIFragmentFactory<textadventure.ECheckPoint, textadventure.ETile>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getECheckPointPointReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.ECHECK_POINT__POINT), resolved, proxy);
				if (proxy != null) {
					Object value = proxy;
					element.eSet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.ECHECK_POINT__POINT), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_3_0_0_2, proxy, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, element);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, proxy);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEWorld(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[50]);
		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEWorld(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[51]);
	}
	
;

parse_textadventure_EDestination returns [textadventure.EDestination element = null]
@init{
}
:
	a0 = 'destination' {
		if (element == null) {
			element = textadventure.TextadventureFactory.eINSTANCE.createEDestination();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_4_0_0_0, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[52]);
	}
	
	a1 = '@' {
		if (element == null) {
			element = textadventure.TextadventureFactory.eINSTANCE.createEDestination();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_4_0_0_1, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[53]);
	}
	
	(
		a2 = TEXT		
		{
			if (terminateParsing) {
				throw new textadventure.resource.textadventure.mopp.TextadventureTerminateParsingException();
			}
			if (element == null) {
				element = textadventure.TextadventureFactory.eINSTANCE.createEDestination();
				startIncompleteElement(element);
			}
			if (a2 != null) {
				textadventure.resource.textadventure.ITextadventureTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
				tokenResolver.setOptions(getOptions());
				textadventure.resource.textadventure.ITextadventureTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a2.getText(), element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EDESTINATION__POINT), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a2).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStopIndex());
				}
				String resolved = (String) resolvedObject;
				textadventure.ETile proxy = textadventure.TextadventureFactory.eINSTANCE.createETile();
				collectHiddenTokens(element);
				registerContextDependentProxy(new textadventure.resource.textadventure.mopp.TextadventureContextDependentURIFragmentFactory<textadventure.EDestination, textadventure.ETile>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getEDestinationPointReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EDESTINATION__POINT), resolved, proxy);
				if (proxy != null) {
					Object value = proxy;
					element.eSet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EDESTINATION__POINT), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_4_0_0_2, proxy, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, element);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, proxy);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEWorld(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[54]);
	}
	
;

parse_textadventure_EBinding returns [textadventure.EBinding element = null]
@init{
}
:
	(
		(
			a0 = 'on' {
				if (element == null) {
					element = textadventure.TextadventureFactory.eINSTANCE.createEBinding();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_5_0_0_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
				// set value of enumeration attribute
				Object value = textadventure.TextadventurePackage.eINSTANCE.getEParticipant().getEEnumLiteral(textadventure.EParticipant.ON_VALUE).getInstance();
				element.eSet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EBINDING__PARTICIPANT), value);
				completedElement(value, false);
			}
		)
	)
	{
		// expected elements (follow set)
		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[55]);
	}
	
	(
		a3 = TEXT		
		{
			if (terminateParsing) {
				throw new textadventure.resource.textadventure.mopp.TextadventureTerminateParsingException();
			}
			if (element == null) {
				element = textadventure.TextadventureFactory.eINSTANCE.createEBinding();
				startIncompleteElement(element);
			}
			if (a3 != null) {
				textadventure.resource.textadventure.ITextadventureTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
				tokenResolver.setOptions(getOptions());
				textadventure.resource.textadventure.ITextadventureTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a3.getText(), element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EBINDING__ID), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a3).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a3).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a3).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a3).getStopIndex());
				}
				java.lang.String resolved = (java.lang.String) resolvedObject;
				if (resolved != null) {
					Object value = resolved;
					element.eSet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EBINDING__ID), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_5_0_0_1, resolved, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a3, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[56]);
	}
	
;

parse_textadventure_EWhileEffect returns [textadventure.EWhileEffect element = null]
@init{
}
:
	a0 = 'while' {
		if (element == null) {
			element = textadventure.TextadventureFactory.eINSTANCE.createEWhileEffect();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_6_0_0_0, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[57]);
	}
	
	(
		(
			a1 = 'used' {
				if (element == null) {
					element = textadventure.TextadventureFactory.eINSTANCE.createEWhileEffect();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_6_0_0_1, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
				// set value of enumeration attribute
				Object value = textadventure.TextadventurePackage.eINSTANCE.getECondition().getEEnumLiteral(textadventure.ECondition.USED_VALUE).getInstance();
				element.eSet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EWHILE_EFFECT__CONDITION), value);
				completedElement(value, false);
			}
		)
	)
	{
		// expected elements (follow set)
		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEWhileEffect(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[58]);
	}
	
	(
		a4_0 = parse_textadventure_EBinding		{
			if (terminateParsing) {
				throw new textadventure.resource.textadventure.mopp.TextadventureTerminateParsingException();
			}
			if (element == null) {
				element = textadventure.TextadventureFactory.eINSTANCE.createEWhileEffect();
				startIncompleteElement(element);
			}
			if (a4_0 != null) {
				if (a4_0 != null) {
					Object value = a4_0;
					element.eSet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EWHILE_EFFECT__BINDING), value);
					completedElement(value, true);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_6_0_0_2, a4_0, true);
				copyLocalizationInfos(a4_0, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[59]);
	}
	
	a5 = '{' {
		if (element == null) {
			element = textadventure.TextadventureFactory.eINSTANCE.createEWhileEffect();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_6_0_0_3, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a5, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEWhileEffect(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[60]);
	}
	
	(
		a6_0 = parse_textadventure_EAction		{
			if (terminateParsing) {
				throw new textadventure.resource.textadventure.mopp.TextadventureTerminateParsingException();
			}
			if (element == null) {
				element = textadventure.TextadventureFactory.eINSTANCE.createEWhileEffect();
				startIncompleteElement(element);
			}
			if (a6_0 != null) {
				if (a6_0 != null) {
					Object value = a6_0;
					addObjectToList(element, textadventure.TextadventurePackage.EWHILE_EFFECT__ACTION, value);
					completedElement(value, true);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_6_0_0_4, a6_0, true);
				copyLocalizationInfos(a6_0, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[61]);
	}
	
	a7 = '}' {
		if (element == null) {
			element = textadventure.TextadventureFactory.eINSTANCE.createEWhileEffect();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_6_0_0_5, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a7, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEItem(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[62]);
		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[63]);
	}
	
;

parse_textadventure_ERemoveAction returns [textadventure.ERemoveAction element = null]
@init{
}
:
	a0 = 'remove' {
		if (element == null) {
			element = textadventure.TextadventureFactory.eINSTANCE.createERemoveAction();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_7_0_0_0, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[64]);
	}
	
	(
		a1 = TEXT		
		{
			if (terminateParsing) {
				throw new textadventure.resource.textadventure.mopp.TextadventureTerminateParsingException();
			}
			if (element == null) {
				element = textadventure.TextadventureFactory.eINSTANCE.createERemoveAction();
				startIncompleteElement(element);
			}
			if (a1 != null) {
				textadventure.resource.textadventure.ITextadventureTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
				tokenResolver.setOptions(getOptions());
				textadventure.resource.textadventure.ITextadventureTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EREMOVE_ACTION__WHAT), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a1).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStopIndex());
				}
				String resolved = (String) resolvedObject;
				textadventure.EBinding proxy = textadventure.TextadventureFactory.eINSTANCE.createEBinding();
				collectHiddenTokens(element);
				registerContextDependentProxy(new textadventure.resource.textadventure.mopp.TextadventureContextDependentURIFragmentFactory<textadventure.ERemoveAction, textadventure.EBinding>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getERemoveActionWhatReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EREMOVE_ACTION__WHAT), resolved, proxy);
				if (proxy != null) {
					Object value = proxy;
					element.eSet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EREMOVE_ACTION__WHAT), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_7_0_0_1, proxy, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a1, element);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a1, proxy);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[65]);
	}
	
	a2 = 'from' {
		if (element == null) {
			element = textadventure.TextadventureFactory.eINSTANCE.createERemoveAction();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_7_0_0_2, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getERemoveAction(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[66]);
	}
	
	(
		a3_0 = parse_textadventure_EListDescription		{
			if (terminateParsing) {
				throw new textadventure.resource.textadventure.mopp.TextadventureTerminateParsingException();
			}
			if (element == null) {
				element = textadventure.TextadventureFactory.eINSTANCE.createERemoveAction();
				startIncompleteElement(element);
			}
			if (a3_0 != null) {
				if (a3_0 != null) {
					Object value = a3_0;
					element.eSet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EREMOVE_ACTION__FROM), value);
					completedElement(value, true);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_7_0_0_3, a3_0, true);
				copyLocalizationInfos(a3_0, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[67]);
	}
	
;

parse_textadventure_EListDescription returns [textadventure.EListDescription element = null]
@init{
}
:
	(
		(
			a0 = 'Tile' {
				if (element == null) {
					element = textadventure.TextadventureFactory.eINSTANCE.createEListDescription();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_8_0_0_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
				// set value of enumeration attribute
				Object value = textadventure.TextadventurePackage.eINSTANCE.getETarget().getEEnumLiteral(textadventure.ETarget.TILE_VALUE).getInstance();
				element.eSet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.ELIST_DESCRIPTION__TARGET), value);
				completedElement(value, false);
			}
		)
	)
	{
		// expected elements (follow set)
		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[68]);
	}
	
	(
		(
			a3 = 'Persons' {
				if (element == null) {
					element = textadventure.TextadventureFactory.eINSTANCE.createEListDescription();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_8_0_0_1, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a3, element);
				// set value of enumeration attribute
				Object value = textadventure.TextadventurePackage.eINSTANCE.getEProperty().getEEnumLiteral(textadventure.EProperty.PERSONS_VALUE).getInstance();
				element.eSet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.ELIST_DESCRIPTION__PROPERTY), value);
				completedElement(value, false);
			}
			|			a4 = 'Items' {
				if (element == null) {
					element = textadventure.TextadventureFactory.eINSTANCE.createEListDescription();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_8_0_0_1, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a4, element);
				// set value of enumeration attribute
				Object value = textadventure.TextadventurePackage.eINSTANCE.getEProperty().getEEnumLiteral(textadventure.EProperty.ITEMS_VALUE).getInstance();
				element.eSet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.ELIST_DESCRIPTION__PROPERTY), value);
				completedElement(value, false);
			}
		)
	)
	{
		// expected elements (follow set)
		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[69]);
	}
	
;

parse_textadventure_EPerson returns [textadventure.EPerson element = null]
@init{
}
:
	a0 = 'person' {
		if (element == null) {
			element = textadventure.TextadventureFactory.eINSTANCE.createEPerson();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_9_0_0_0, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[70]);
	}
	
	(
		a1 = TEXT		
		{
			if (terminateParsing) {
				throw new textadventure.resource.textadventure.mopp.TextadventureTerminateParsingException();
			}
			if (element == null) {
				element = textadventure.TextadventureFactory.eINSTANCE.createEPerson();
				startIncompleteElement(element);
			}
			if (a1 != null) {
				textadventure.resource.textadventure.ITextadventureTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
				tokenResolver.setOptions(getOptions());
				textadventure.resource.textadventure.ITextadventureTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EPERSON__NAME), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a1).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStopIndex());
				}
				java.lang.String resolved = (java.lang.String) resolvedObject;
				if (resolved != null) {
					Object value = resolved;
					element.eSet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EPERSON__NAME), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_9_0_0_1, resolved, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a1, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[71]);
	}
	
	a2 = '@' {
		if (element == null) {
			element = textadventure.TextadventureFactory.eINSTANCE.createEPerson();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_9_0_0_2, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[72]);
	}
	
	(
		a3 = TEXT		
		{
			if (terminateParsing) {
				throw new textadventure.resource.textadventure.mopp.TextadventureTerminateParsingException();
			}
			if (element == null) {
				element = textadventure.TextadventureFactory.eINSTANCE.createEPerson();
				startIncompleteElement(element);
			}
			if (a3 != null) {
				textadventure.resource.textadventure.ITextadventureTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
				tokenResolver.setOptions(getOptions());
				textadventure.resource.textadventure.ITextadventureTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a3.getText(), element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EPERSON__LOCATION), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a3).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a3).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a3).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a3).getStopIndex());
				}
				String resolved = (String) resolvedObject;
				textadventure.ETile proxy = textadventure.TextadventureFactory.eINSTANCE.createETile();
				collectHiddenTokens(element);
				registerContextDependentProxy(new textadventure.resource.textadventure.mopp.TextadventureContextDependentURIFragmentFactory<textadventure.EPerson, textadventure.ETile>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getEPersonLocationReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EPERSON__LOCATION), resolved, proxy);
				if (proxy != null) {
					Object value = proxy;
					element.eSet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EPERSON__LOCATION), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_9_0_0_3, proxy, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a3, element);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a3, proxy);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[73]);
	}
	
	a4 = '{' {
		if (element == null) {
			element = textadventure.TextadventureFactory.eINSTANCE.createEPerson();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_9_0_0_4, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a4, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[74]);
		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[75]);
		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[76]);
		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEPerson(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[77]);
		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[78]);
	}
	
	(
		(
			a5 = 'hp' {
				if (element == null) {
					element = textadventure.TextadventureFactory.eINSTANCE.createEPerson();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_9_0_0_5_0_0_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a5, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[79]);
			}
			
			a6 = '=' {
				if (element == null) {
					element = textadventure.TextadventureFactory.eINSTANCE.createEPerson();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_9_0_0_5_0_0_1, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a6, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[80]);
			}
			
			(
				a7 = INTEGER				
				{
					if (terminateParsing) {
						throw new textadventure.resource.textadventure.mopp.TextadventureTerminateParsingException();
					}
					if (element == null) {
						element = textadventure.TextadventureFactory.eINSTANCE.createEPerson();
						startIncompleteElement(element);
					}
					if (a7 != null) {
						textadventure.resource.textadventure.ITextadventureTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("INTEGER");
						tokenResolver.setOptions(getOptions());
						textadventure.resource.textadventure.ITextadventureTokenResolveResult result = getFreshTokenResolveResult();
						tokenResolver.resolve(a7.getText(), element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EPERSON__HP), result);
						Object resolvedObject = result.getResolvedToken();
						if (resolvedObject == null) {
							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a7).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a7).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a7).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a7).getStopIndex());
						}
						java.lang.Integer resolved = (java.lang.Integer) resolvedObject;
						if (resolved != null) {
							Object value = resolved;
							element.eSet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EPERSON__HP), value);
							completedElement(value, false);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_9_0_0_5_0_0_2, resolved, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a7, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[81]);
				addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[82]);
				addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[83]);
				addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEPerson(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[84]);
				addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[85]);
			}
			
			
			|			a8 = 'defense' {
				if (element == null) {
					element = textadventure.TextadventureFactory.eINSTANCE.createEPerson();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_9_0_0_5_0_1_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a8, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[86]);
			}
			
			a9 = '=' {
				if (element == null) {
					element = textadventure.TextadventureFactory.eINSTANCE.createEPerson();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_9_0_0_5_0_1_1, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a9, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[87]);
			}
			
			(
				a10 = INTEGER				
				{
					if (terminateParsing) {
						throw new textadventure.resource.textadventure.mopp.TextadventureTerminateParsingException();
					}
					if (element == null) {
						element = textadventure.TextadventureFactory.eINSTANCE.createEPerson();
						startIncompleteElement(element);
					}
					if (a10 != null) {
						textadventure.resource.textadventure.ITextadventureTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("INTEGER");
						tokenResolver.setOptions(getOptions());
						textadventure.resource.textadventure.ITextadventureTokenResolveResult result = getFreshTokenResolveResult();
						tokenResolver.resolve(a10.getText(), element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EPERSON__DEFENSE), result);
						Object resolvedObject = result.getResolvedToken();
						if (resolvedObject == null) {
							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a10).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a10).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a10).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a10).getStopIndex());
						}
						java.lang.Integer resolved = (java.lang.Integer) resolvedObject;
						if (resolved != null) {
							Object value = resolved;
							element.eSet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EPERSON__DEFENSE), value);
							completedElement(value, false);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_9_0_0_5_0_1_2, resolved, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a10, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[88]);
				addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[89]);
				addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[90]);
				addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEPerson(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[91]);
				addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[92]);
			}
			
			
			|			a11 = 'attack' {
				if (element == null) {
					element = textadventure.TextadventureFactory.eINSTANCE.createEPerson();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_9_0_0_5_0_2_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a11, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[93]);
			}
			
			a12 = '=' {
				if (element == null) {
					element = textadventure.TextadventureFactory.eINSTANCE.createEPerson();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_9_0_0_5_0_2_1, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a12, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[94]);
			}
			
			(
				a13 = INTEGER				
				{
					if (terminateParsing) {
						throw new textadventure.resource.textadventure.mopp.TextadventureTerminateParsingException();
					}
					if (element == null) {
						element = textadventure.TextadventureFactory.eINSTANCE.createEPerson();
						startIncompleteElement(element);
					}
					if (a13 != null) {
						textadventure.resource.textadventure.ITextadventureTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("INTEGER");
						tokenResolver.setOptions(getOptions());
						textadventure.resource.textadventure.ITextadventureTokenResolveResult result = getFreshTokenResolveResult();
						tokenResolver.resolve(a13.getText(), element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EPERSON__ATTACK), result);
						Object resolvedObject = result.getResolvedToken();
						if (resolvedObject == null) {
							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a13).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a13).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a13).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a13).getStopIndex());
						}
						java.lang.Integer resolved = (java.lang.Integer) resolvedObject;
						if (resolved != null) {
							Object value = resolved;
							element.eSet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EPERSON__ATTACK), value);
							completedElement(value, false);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_9_0_0_5_0_2_2, resolved, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a13, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[95]);
				addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[96]);
				addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[97]);
				addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEPerson(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[98]);
				addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[99]);
			}
			
		)
		
	)*	{
		// expected elements (follow set)
		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[100]);
		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[101]);
		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[102]);
		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEPerson(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[103]);
		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[104]);
	}
	
	(
		(
			a14_0 = parse_textadventure_EAbility			{
				if (terminateParsing) {
					throw new textadventure.resource.textadventure.mopp.TextadventureTerminateParsingException();
				}
				if (element == null) {
					element = textadventure.TextadventureFactory.eINSTANCE.createEPerson();
					startIncompleteElement(element);
				}
				if (a14_0 != null) {
					if (a14_0 != null) {
						Object value = a14_0;
						addObjectToList(element, textadventure.TextadventurePackage.EPERSON__ABILITIES, value);
						completedElement(value, true);
					}
					collectHiddenTokens(element);
					retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_9_0_0_6, a14_0, true);
					copyLocalizationInfos(a14_0, element);
				}
			}
		)
		
	)*	{
		// expected elements (follow set)
		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEPerson(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[105]);
		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[106]);
	}
	
	a15 = '}' {
		if (element == null) {
			element = textadventure.TextadventureFactory.eINSTANCE.createEPerson();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_9_0_0_7, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a15, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEWorld(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[107]);
		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEWorld(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[108]);
		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEWorld(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[109]);
		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEWorld(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[110]);
	}
	
;

parse_textadventure_EBattle returns [textadventure.EBattle element = null]
@init{
}
:
	a0 = 'battle' {
		if (element == null) {
			element = textadventure.TextadventureFactory.eINSTANCE.createEBattle();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_10_0_0_0, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[111]);
	}
	
	a1 = '@' {
		if (element == null) {
			element = textadventure.TextadventureFactory.eINSTANCE.createEBattle();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_10_0_0_1, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[112]);
	}
	
	(
		a2 = TEXT		
		{
			if (terminateParsing) {
				throw new textadventure.resource.textadventure.mopp.TextadventureTerminateParsingException();
			}
			if (element == null) {
				element = textadventure.TextadventureFactory.eINSTANCE.createEBattle();
				startIncompleteElement(element);
			}
			if (a2 != null) {
				textadventure.resource.textadventure.ITextadventureTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
				tokenResolver.setOptions(getOptions());
				textadventure.resource.textadventure.ITextadventureTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a2.getText(), element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EBATTLE__LOCATION), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a2).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStopIndex());
				}
				String resolved = (String) resolvedObject;
				textadventure.ETile proxy = textadventure.TextadventureFactory.eINSTANCE.createETile();
				collectHiddenTokens(element);
				registerContextDependentProxy(new textadventure.resource.textadventure.mopp.TextadventureContextDependentURIFragmentFactory<textadventure.EBattle, textadventure.ETile>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getEBattleLocationReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EBATTLE__LOCATION), resolved, proxy);
				if (proxy != null) {
					Object value = proxy;
					element.eSet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EBATTLE__LOCATION), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_10_0_0_2, proxy, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, element);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, proxy);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[113]);
	}
	
	a3 = '{' {
		if (element == null) {
			element = textadventure.TextadventureFactory.eINSTANCE.createEBattle();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_10_0_0_3, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a3, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[114]);
	}
	
	a4 = 'enemy' {
		if (element == null) {
			element = textadventure.TextadventureFactory.eINSTANCE.createEBattle();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_10_0_0_4, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a4, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[115]);
	}
	
	a5 = '=' {
		if (element == null) {
			element = textadventure.TextadventureFactory.eINSTANCE.createEBattle();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_10_0_0_5, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a5, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[116]);
	}
	
	(
		a6 = TEXT		
		{
			if (terminateParsing) {
				throw new textadventure.resource.textadventure.mopp.TextadventureTerminateParsingException();
			}
			if (element == null) {
				element = textadventure.TextadventureFactory.eINSTANCE.createEBattle();
				startIncompleteElement(element);
			}
			if (a6 != null) {
				textadventure.resource.textadventure.ITextadventureTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
				tokenResolver.setOptions(getOptions());
				textadventure.resource.textadventure.ITextadventureTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a6.getText(), element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EBATTLE__ENEMY), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a6).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a6).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a6).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a6).getStopIndex());
				}
				String resolved = (String) resolvedObject;
				textadventure.EPerson proxy = textadventure.TextadventureFactory.eINSTANCE.createEPerson();
				collectHiddenTokens(element);
				registerContextDependentProxy(new textadventure.resource.textadventure.mopp.TextadventureContextDependentURIFragmentFactory<textadventure.EBattle, textadventure.EPerson>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getEBattleEnemyReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EBATTLE__ENEMY), resolved, proxy);
				if (proxy != null) {
					Object value = proxy;
					element.eSet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EBATTLE__ENEMY), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_10_0_0_6, proxy, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a6, element);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a6, proxy);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEBattle(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[117]);
		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[118]);
	}
	
	(
		(
			a7_0 = parse_textadventure_EBattleCustomization			{
				if (terminateParsing) {
					throw new textadventure.resource.textadventure.mopp.TextadventureTerminateParsingException();
				}
				if (element == null) {
					element = textadventure.TextadventureFactory.eINSTANCE.createEBattle();
					startIncompleteElement(element);
				}
				if (a7_0 != null) {
					if (a7_0 != null) {
						Object value = a7_0;
						addObjectToList(element, textadventure.TextadventurePackage.EBATTLE__LOGICS, value);
						completedElement(value, true);
					}
					collectHiddenTokens(element);
					retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_10_0_0_7, a7_0, true);
					copyLocalizationInfos(a7_0, element);
				}
			}
		)
		
	)*	{
		// expected elements (follow set)
		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEBattle(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[119]);
		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[120]);
	}
	
	a8 = '}' {
		if (element == null) {
			element = textadventure.TextadventureFactory.eINSTANCE.createEBattle();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_10_0_0_8, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a8, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEWorld(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[121]);
		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEWorld(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[122]);
		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEWorld(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[123]);
	}
	
;

parse_textadventure_EBattleCustomization returns [textadventure.EBattleCustomization element = null]
@init{
}
:
	(
		a0 = TEXT		
		{
			if (terminateParsing) {
				throw new textadventure.resource.textadventure.mopp.TextadventureTerminateParsingException();
			}
			if (element == null) {
				element = textadventure.TextadventureFactory.eINSTANCE.createEBattleCustomization();
				startIncompleteElement(element);
			}
			if (a0 != null) {
				textadventure.resource.textadventure.ITextadventureTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
				tokenResolver.setOptions(getOptions());
				textadventure.resource.textadventure.ITextadventureTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a0.getText(), element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EBATTLE_CUSTOMIZATION__WHEN), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a0).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStopIndex());
				}
				textadventure.EBattleWhen resolved = (textadventure.EBattleWhen) resolvedObject;
				if (resolved != null) {
					Object value = resolved;
					element.eSet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EBATTLE_CUSTOMIZATION__WHEN), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_11_0_0_0, resolved, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a0, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[124]);
	}
	
	a1 = '=' {
		if (element == null) {
			element = textadventure.TextadventureFactory.eINSTANCE.createEBattleCustomization();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_11_0_0_1, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEBattleCustomization(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[125]);
	}
	
	(
		a2_0 = parse_textadventure_EFunction		{
			if (terminateParsing) {
				throw new textadventure.resource.textadventure.mopp.TextadventureTerminateParsingException();
			}
			if (element == null) {
				element = textadventure.TextadventureFactory.eINSTANCE.createEBattleCustomization();
				startIncompleteElement(element);
			}
			if (a2_0 != null) {
				if (a2_0 != null) {
					Object value = a2_0;
					element.eSet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EBATTLE_CUSTOMIZATION__FUNC), value);
					completedElement(value, true);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_11_0_0_2, a2_0, true);
				copyLocalizationInfos(a2_0, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEBattle(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[126]);
		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[127]);
	}
	
;

parse_textadventure_EFunction returns [textadventure.EFunction element = null]
@init{
}
:
	a0 = 'function(' {
		if (element == null) {
			element = textadventure.TextadventureFactory.eINSTANCE.createEFunction();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_12_0_0_0, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[128]);
		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[129]);
	}
	
	(
		(
			(
				a1 = TEXT				
				{
					if (terminateParsing) {
						throw new textadventure.resource.textadventure.mopp.TextadventureTerminateParsingException();
					}
					if (element == null) {
						element = textadventure.TextadventureFactory.eINSTANCE.createEFunction();
						startIncompleteElement(element);
					}
					if (a1 != null) {
						textadventure.resource.textadventure.ITextadventureTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
						tokenResolver.setOptions(getOptions());
						textadventure.resource.textadventure.ITextadventureTokenResolveResult result = getFreshTokenResolveResult();
						tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EFUNCTION__PARAMS), result);
						Object resolvedObject = result.getResolvedToken();
						if (resolvedObject == null) {
							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a1).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStopIndex());
						}
						java.lang.String resolved = (java.lang.String) resolvedObject;
						if (resolved != null) {
							Object value = resolved;
							addObjectToList(element, textadventure.TextadventurePackage.EFUNCTION__PARAMS, value);
							completedElement(value, false);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_12_0_0_1_0_0_0, resolved, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a1, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[130]);
				addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[131]);
			}
			
			(
				(
					a2 = ',' {
						if (element == null) {
							element = textadventure.TextadventureFactory.eINSTANCE.createEFunction();
							startIncompleteElement(element);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_12_0_0_1_0_0_1_0_0_0, null, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
					}
					{
						// expected elements (follow set)
						addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[132]);
					}
					
					(
						a3 = TEXT						
						{
							if (terminateParsing) {
								throw new textadventure.resource.textadventure.mopp.TextadventureTerminateParsingException();
							}
							if (element == null) {
								element = textadventure.TextadventureFactory.eINSTANCE.createEFunction();
								startIncompleteElement(element);
							}
							if (a3 != null) {
								textadventure.resource.textadventure.ITextadventureTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
								tokenResolver.setOptions(getOptions());
								textadventure.resource.textadventure.ITextadventureTokenResolveResult result = getFreshTokenResolveResult();
								tokenResolver.resolve(a3.getText(), element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EFUNCTION__PARAMS), result);
								Object resolvedObject = result.getResolvedToken();
								if (resolvedObject == null) {
									addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a3).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a3).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a3).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a3).getStopIndex());
								}
								java.lang.String resolved = (java.lang.String) resolvedObject;
								if (resolved != null) {
									Object value = resolved;
									addObjectToList(element, textadventure.TextadventurePackage.EFUNCTION__PARAMS, value);
									completedElement(value, false);
								}
								collectHiddenTokens(element);
								retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_12_0_0_1_0_0_1_0_0_1, resolved, true);
								copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a3, element);
							}
						}
					)
					{
						// expected elements (follow set)
						addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[133]);
						addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[134]);
					}
					
				)
				
			)*			{
				// expected elements (follow set)
				addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[135]);
				addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[136]);
			}
			
		)
		
	)?	{
		// expected elements (follow set)
		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[137]);
	}
	
	a4 = ')' {
		if (element == null) {
			element = textadventure.TextadventureFactory.eINSTANCE.createEFunction();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_12_0_0_2, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a4, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[138]);
	}
	
	a5 = '{' {
		if (element == null) {
			element = textadventure.TextadventureFactory.eINSTANCE.createEFunction();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_12_0_0_3, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a5, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEFunction(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[139]);
		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEFunction(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[140]);
		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEFunction(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[141]);
		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[142]);
	}
	
	(
		a6_0 = parse_textadventure_EFunctionBody		{
			if (terminateParsing) {
				throw new textadventure.resource.textadventure.mopp.TextadventureTerminateParsingException();
			}
			if (element == null) {
				element = textadventure.TextadventureFactory.eINSTANCE.createEFunction();
				startIncompleteElement(element);
			}
			if (a6_0 != null) {
				if (a6_0 != null) {
					Object value = a6_0;
					element.eSet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EFUNCTION__BODY), value);
					completedElement(value, true);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_12_0_0_4, a6_0, true);
				copyLocalizationInfos(a6_0, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[143]);
	}
	
	a7 = '}' {
		if (element == null) {
			element = textadventure.TextadventureFactory.eINSTANCE.createEFunction();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_12_0_0_5, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a7, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEBattle(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[144]);
		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[145]);
	}
	
;

parse_textadventure_EFunctionBody returns [textadventure.EFunctionBody element = null]
@init{
}
:
	(
		(
			(
				a0_0 = parse_textadventure_EStatement				{
					if (terminateParsing) {
						throw new textadventure.resource.textadventure.mopp.TextadventureTerminateParsingException();
					}
					if (element == null) {
						element = textadventure.TextadventureFactory.eINSTANCE.createEFunctionBody();
						startIncompleteElement(element);
					}
					if (a0_0 != null) {
						if (a0_0 != null) {
							Object value = a0_0;
							addObjectToList(element, textadventure.TextadventurePackage.EFUNCTION_BODY__STATEMENTS, value);
							completedElement(value, true);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_13_0_0_0_0_0_0, a0_0, true);
						copyLocalizationInfos(a0_0, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[146]);
			}
			
			a1 = ';' {
				if (element == null) {
					element = textadventure.TextadventureFactory.eINSTANCE.createEFunctionBody();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_13_0_0_0_0_0_1, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEFunctionBody(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[147]);
				addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEFunctionBody(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[148]);
				addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEFunctionBody(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[149]);
				addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[150]);
			}
			
		)
		
	)*	{
		// expected elements (follow set)
		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEFunctionBody(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[151]);
		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEFunctionBody(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[152]);
		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEFunctionBody(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[153]);
		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[154]);
	}
	
;

parse_textadventure_EPrintStatement returns [textadventure.EPrintStatement element = null]
@init{
}
:
	a0 = 'print' {
		if (element == null) {
			element = textadventure.TextadventureFactory.eINSTANCE.createEPrintStatement();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_14_0_0_0, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[155]);
	}
	
	(
		a1 = QUOTED_34_34		
		{
			if (terminateParsing) {
				throw new textadventure.resource.textadventure.mopp.TextadventureTerminateParsingException();
			}
			if (element == null) {
				element = textadventure.TextadventureFactory.eINSTANCE.createEPrintStatement();
				startIncompleteElement(element);
			}
			if (a1 != null) {
				textadventure.resource.textadventure.ITextadventureTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34");
				tokenResolver.setOptions(getOptions());
				textadventure.resource.textadventure.ITextadventureTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EPRINT_STATEMENT__OUTPUT), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a1).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStopIndex());
				}
				java.lang.String resolved = (java.lang.String) resolvedObject;
				if (resolved != null) {
					Object value = resolved;
					element.eSet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EPRINT_STATEMENT__OUTPUT), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_14_0_0_1, resolved, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a1, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[156]);
	}
	
;

parse_textadventure_ERemovePlayerStatement returns [textadventure.ERemovePlayerStatement element = null]
@init{
}
:
	a0 = 'remove' {
		if (element == null) {
			element = textadventure.TextadventureFactory.eINSTANCE.createERemovePlayerStatement();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_15_0_0_0, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[157]);
	}
	
	(
		a1 = TEXT		
		{
			if (terminateParsing) {
				throw new textadventure.resource.textadventure.mopp.TextadventureTerminateParsingException();
			}
			if (element == null) {
				element = textadventure.TextadventureFactory.eINSTANCE.createERemovePlayerStatement();
				startIncompleteElement(element);
			}
			if (a1 != null) {
				textadventure.resource.textadventure.ITextadventureTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
				tokenResolver.setOptions(getOptions());
				textadventure.resource.textadventure.ITextadventureTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EREMOVE_PLAYER_STATEMENT__TARGET), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a1).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStopIndex());
				}
				String resolved = (String) resolvedObject;
				textadventure.EPerson proxy = textadventure.TextadventureFactory.eINSTANCE.createEPerson();
				collectHiddenTokens(element);
				registerContextDependentProxy(new textadventure.resource.textadventure.mopp.TextadventureContextDependentURIFragmentFactory<textadventure.ERemovePlayerStatement, textadventure.EPerson>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getERemovePlayerStatementTargetReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EREMOVE_PLAYER_STATEMENT__TARGET), resolved, proxy);
				if (proxy != null) {
					Object value = proxy;
					element.eSet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EREMOVE_PLAYER_STATEMENT__TARGET), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_15_0_0_1, proxy, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a1, element);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a1, proxy);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[158]);
	}
	
;

parse_textadventure_EMethodCallStatement returns [textadventure.EMethodCallStatement element = null]
@init{
}
:
	(
		a0 = TEXT		
		{
			if (terminateParsing) {
				throw new textadventure.resource.textadventure.mopp.TextadventureTerminateParsingException();
			}
			if (element == null) {
				element = textadventure.TextadventureFactory.eINSTANCE.createEMethodCallStatement();
				startIncompleteElement(element);
			}
			if (a0 != null) {
				textadventure.resource.textadventure.ITextadventureTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
				tokenResolver.setOptions(getOptions());
				textadventure.resource.textadventure.ITextadventureTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a0.getText(), element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EMETHOD_CALL_STATEMENT__OBJECT), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a0).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStopIndex());
				}
				java.lang.String resolved = (java.lang.String) resolvedObject;
				if (resolved != null) {
					Object value = resolved;
					element.eSet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EMETHOD_CALL_STATEMENT__OBJECT), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_16_0_0_0, resolved, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a0, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[159]);
	}
	
	(
		a1 = TEXT		
		{
			if (terminateParsing) {
				throw new textadventure.resource.textadventure.mopp.TextadventureTerminateParsingException();
			}
			if (element == null) {
				element = textadventure.TextadventureFactory.eINSTANCE.createEMethodCallStatement();
				startIncompleteElement(element);
			}
			if (a1 != null) {
				textadventure.resource.textadventure.ITextadventureTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
				tokenResolver.setOptions(getOptions());
				textadventure.resource.textadventure.ITextadventureTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EMETHOD_CALL_STATEMENT__METHOD), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a1).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStopIndex());
				}
				java.lang.String resolved = (java.lang.String) resolvedObject;
				if (resolved != null) {
					Object value = resolved;
					element.eSet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EMETHOD_CALL_STATEMENT__METHOD), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_16_0_0_1, resolved, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a1, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[160]);
		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[161]);
	}
	
	(
		(
			a2 = QUOTED_34_34			
			{
				if (terminateParsing) {
					throw new textadventure.resource.textadventure.mopp.TextadventureTerminateParsingException();
				}
				if (element == null) {
					element = textadventure.TextadventureFactory.eINSTANCE.createEMethodCallStatement();
					startIncompleteElement(element);
				}
				if (a2 != null) {
					textadventure.resource.textadventure.ITextadventureTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34");
					tokenResolver.setOptions(getOptions());
					textadventure.resource.textadventure.ITextadventureTokenResolveResult result = getFreshTokenResolveResult();
					tokenResolver.resolve(a2.getText(), element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EMETHOD_CALL_STATEMENT__PARAMS), result);
					Object resolvedObject = result.getResolvedToken();
					if (resolvedObject == null) {
						addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a2).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStopIndex());
					}
					java.lang.String resolved = (java.lang.String) resolvedObject;
					if (resolved != null) {
						Object value = resolved;
						addObjectToList(element, textadventure.TextadventurePackage.EMETHOD_CALL_STATEMENT__PARAMS, value);
						completedElement(value, false);
					}
					collectHiddenTokens(element);
					retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_16_0_0_2, resolved, true);
					copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, element);
				}
			}
		)
		
	)*	{
		// expected elements (follow set)
		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[162]);
		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[163]);
	}
	
;

parse_textadventure_EIgnoreItemAbility returns [textadventure.EIgnoreItemAbility element = null]
@init{
}
:
	a0 = 'ignore' {
		if (element == null) {
			element = textadventure.TextadventureFactory.eINSTANCE.createEIgnoreItemAbility();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_17_0_0_0, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[164]);
	}
	
	(
		a1 = TEXT		
		{
			if (terminateParsing) {
				throw new textadventure.resource.textadventure.mopp.TextadventureTerminateParsingException();
			}
			if (element == null) {
				element = textadventure.TextadventureFactory.eINSTANCE.createEIgnoreItemAbility();
				startIncompleteElement(element);
			}
			if (a1 != null) {
				textadventure.resource.textadventure.ITextadventureTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
				tokenResolver.setOptions(getOptions());
				textadventure.resource.textadventure.ITextadventureTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EIGNORE_ITEM_ABILITY__IGNORED_ITEM), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a1).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStopIndex());
				}
				String resolved = (String) resolvedObject;
				textadventure.EItem proxy = textadventure.TextadventureFactory.eINSTANCE.createEItem();
				collectHiddenTokens(element);
				registerContextDependentProxy(new textadventure.resource.textadventure.mopp.TextadventureContextDependentURIFragmentFactory<textadventure.EIgnoreItemAbility, textadventure.EItem>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getEIgnoreItemAbilityIgnoredItemReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EIGNORE_ITEM_ABILITY__IGNORED_ITEM), resolved, proxy);
				if (proxy != null) {
					Object value = proxy;
					element.eSet(element.eClass().getEStructuralFeature(textadventure.TextadventurePackage.EIGNORE_ITEM_ABILITY__IGNORED_ITEM), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, textadventure.resource.textadventure.grammar.TextadventureGrammarInformationProvider.TEXTADVENTURE_17_0_0_1, proxy, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a1, element);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a1, proxy);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(textadventure.TextadventurePackage.eINSTANCE.getEPerson(), textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[165]);
		addExpectedElement(null, textadventure.resource.textadventure.mopp.TextadventureExpectationConstants.EXPECTATIONS[166]);
	}
	
;

parse_textadventure_EEffect returns [textadventure.EEffect element = null]
:
	c0 = parse_textadventure_EWhileEffect{ element = c0; /* this is a subclass or primitive expression choice */ }
	
;

parse_textadventure_EAction returns [textadventure.EAction element = null]
:
	c0 = parse_textadventure_ERemoveAction{ element = c0; /* this is a subclass or primitive expression choice */ }
	
;

parse_textadventure_EAbility returns [textadventure.EAbility element = null]
:
	c0 = parse_textadventure_EIgnoreItemAbility{ element = c0; /* this is a subclass or primitive expression choice */ }
	
;

parse_textadventure_EStatement returns [textadventure.EStatement element = null]
:
	c0 = parse_textadventure_EPrintStatement{ element = c0; /* this is a subclass or primitive expression choice */ }
	|	c1 = parse_textadventure_ERemovePlayerStatement{ element = c1; /* this is a subclass or primitive expression choice */ }
	|	c2 = parse_textadventure_EMethodCallStatement{ element = c2; /* this is a subclass or primitive expression choice */ }
	
;

COMMENT:
	('//'(~('\n'|'\r'|'\uffff'))*)
	{ _channel = 99; }
;
INTEGER:
	(('-')?('1'..'9')('0'..'9')*|'0')
;
FLOAT:
	(('-')?(('1'..'9') ('0'..'9')* | '0') '.' ('0'..'9')+ )
	{ _channel = 99; }
;
TEXT:
	(('A'..'Z' | 'a'..'z' | '0'..'9' | '_' | '-' )+)
;
WHITESPACE:
	((' ' | '\t' | '\f'))
	{ _channel = 99; }
;
LINEBREAK:
	(('\r\n' | '\r' | '\n'))
	{ _channel = 99; }
;
QUOTED_34_34:
	(('"')(~('"'))*('"'))
;

