package textadventure.interpreter;

import java.io.Console;
import java.util.ArrayList;
import java.util.List;

import textadventure.model.Battle;
import textadventure.model.Coordinate;
import textadventure.model.Item;
import textadventure.model.Person;
import textadventure.model.Tile;
import textadventure.model.World;

/**
 * Implements the main event look that reads and interprets input from the user.
 * Textual commands are parsed and corresponding method invoctions are made.
 * @author bockischcm
 *
 */
public class TextadventureEventLoop {

	private TextadventureConsole console;
	
	private World world;
	
	private Tile currentTile;
	
	private enum Context {
		MAP, BATTLE, GAME_OVER
	}
	
	private Context context = Context.MAP;

	public TextadventureEventLoop(TextadventureConsole console) {
		this.console = console;
	}

	private static final String BECOME_COMMAND = "become";			// <person-name>
	private static final String USE_COMMAND = "use"; 				// <item-name> on <person-name> || <item-name>+
	private static final String STOP_USING_COMMAND = "stop using";	// <item-name>
	private static final String DROP_COMMAND = "drop";				// <item-name>
	private static final String PICK_COMMAND = "pick up"; 			// <item-name>
	private static final String LOOK_COMMAND = "look"; 				//
	private static final String GO_COMMAND = "go";					// north || east || south || west
	private static final String EXIT_COMMAND = "exit";				//
	
	private static final String ATTACK_COMMAND = "attack";
	private static final String DEFEND_COMMAND = "defend";
	private static final String DRAW_COMMAND = "draw";

	private void mapCommands() {
		console.println("You are now at " + currentTile.getDescription() + ". What would you like to do?");
		
		String line = console.readln();
		
		if (line.equals(EXIT_COMMAND)) {
			context = Context.GAME_OVER;
			return;
		}
		
		if (line.startsWith(GO_COMMAND)) {
			String detail = line.substring(GO_COMMAND.length()).trim();
			int dx, dy;
			if (detail.equals("north")) {
				dx = 0; dy = 1;
			}
			else if (detail.equals("south")) {
				dx = 0; dy = -1;
			}
			else if (detail.equals("west")) {
				dx = -1; dy = 0;
			}
			else if (detail.equals("east")) {
				dx = 1; dy = 0;
			}
			else {
				console.println("ERROR: Don't understand 'go' diretion: " + detail);
				return;
			}
			Coordinate currentCoordinate = world.getCoordinate(currentTile);
			int newX = currentCoordinate.x + dx;
			int newY = currentCoordinate.y + dy;
			currentTile.removePerson(world.getPlayerPerson());
			currentTile = world.getTile(newX, newY);
			currentTile.addPerson(world.getPlayerPerson());
			console.println("You have arrived at " + currentTile.getDescription());
			if (currentTile.hasBattle()) {
				context = Context.BATTLE;
				console.println("Ooops! You enter a battle with " + currentTile.getBattle().getEnemy().getName() + "!");
			} else if (currentTile.isDestination()) {
				context = Context.GAME_OVER;
				console.println("You have arrived " + currentTile.getDescription() + " and beat the game! Congratulations!");
			}
		}
		else if (line.equals(LOOK_COMMAND)) {
			console.println("You see " + currentTile.getDescription());
			console.println("On the floor you see ");
			for (Item item : currentTile.getItems()) {
				console.println(item.getName() + ", ");
			}
			console.println();
			console.println("Together with you are ");
			for (Person person : currentTile.getPersons()) {
				console.println(person.getName() + ", ");
			}
		}
		else if (line.startsWith(PICK_COMMAND)) {
			String detail = line.substring(PICK_COMMAND.length()).trim();
			Item item = Item.getItem(detail);
			currentTile.removeItem(item);
			world.getPlayerPerson().addToInventory(item);
		}
		else if (line.startsWith(DROP_COMMAND)) {
			String detail = line.substring(DROP_COMMAND.length()).trim();
			Item item = Item.getItem(detail);
			world.getPlayerPerson().removeFromInventory(item);
			currentTile.addItem(item);
		}
		else if (line.startsWith(USE_COMMAND)) {
			String detail[] = line.substring(USE_COMMAND.length()).trim().split(" ");
			Item mainItem = Item.getItem(detail[0]);
			if (detail.length >= 2 && detail[1].equals("on")) {
				Person person = Person.getPerson(detail[2]);
				mainItem.use(person);
			}
			else {
				List<Item> otherItems = new ArrayList<Item>();
				if (detail.length > 1) {
					for (int i = 2; i < detail.length; i++)
						otherItems.add(Item.getItem(detail[i]));
				}
				mainItem.use(otherItems);
			}
		}
		else if (line.startsWith(STOP_USING_COMMAND)) {
			String detail = line.substring(STOP_USING_COMMAND.length()).trim();
			Item item = Item.getItem(detail);
			item.stopUsing();
		}
		else if (line.startsWith(BECOME_COMMAND)) {
			String detail = line.substring(BECOME_COMMAND.length()).trim();
			world.setPlayerPerson(Person.getPerson(detail));
			console.println("became '" + detail + "' (" + world.getPlayerPerson() + ")");
		}

	}
	
	private void battleCommands() {
		Battle currentBattle = currentTile.getBattle();
		Person enemy = currentBattle.getEnemy();
		Person protagonist = world.getPlayerPerson();
		
		int turnsNumber = currentBattle.newTurn();
		if (turnsNumber == Battle.firstTurnNumber) {
			currentBattle.runBeforeBattleFunctions(console);
		} else {
			currentBattle.runOnEachTurnFunctions(console);
		}
		
		protagonist = world.getPlayerPerson();
		
		console.println("You are now fighting with " + enemy.getName() + ".");
		console.println(enemy.getName() + " has " + enemy.getHp() + " hp, while you have " + world.getPlayerPerson().getHp() + " hp.");
		console.println("What would you like to do?");
		
		String line = console.readln();
		
		protagonist.cancelDefend();
		
		if (line.equals(ATTACK_COMMAND)) {
			int damage = enemy.hitBy(protagonist);
			console.println("Your attack causes " + enemy.getName() + " to suffer " + damage + " damage!");
		} else if (line.equals(DEFEND_COMMAND)) {
			protagonist.defend();
		}
		if (enemy.isDead()) {
			console.println("You have beaten " + enemy.getName() + "! Congratulations!");
			currentTile.removeBattle();
			currentBattle.runAfterBattleFunctions(console);
			context = Context.MAP;
			return;
		}
		int damage = protagonist.hitBy(enemy);
		console.println("You are attacked by " + enemy.getName() + " and suffer " + damage + " damage!");
		if (protagonist.isDead()) {
			console.println("You have been beaten by " + enemy.getName() + "! Game over.");
			context = Context.GAME_OVER;
			return;
		}
	}
	
	/**
	 * Until the user enters the "exit" command, this method loops and tries to read
	 * more input from the user. When a command is recognized, it parses the provided arguments
	 * and makes the proper invocation on element of the World.
	 */
	public void start() {
		world = World.getInstance();
		currentTile = world.getTile(0, 0);
		while (true) {
			if (context == Context.MAP) {
				mapCommands();
			} else if (context == Context.BATTLE) {
				battleCommands();
			}
			if (context == Context.GAME_OVER) {
				break;
			}
		}
	}
}
