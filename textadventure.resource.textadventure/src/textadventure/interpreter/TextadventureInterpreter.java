package textadventure.interpreter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Stack;

import org.eclipse.emf.ecore.EObject;

import textadventure.EAction;
import textadventure.EBattle;
import textadventure.EBinding;
import textadventure.ECondition;
import textadventure.EDestination;
import textadventure.EIgnoreItemAbility;
import textadventure.EItem;
import textadventure.EListDescription;
import textadventure.EParticipant;
import textadventure.EPerson;
import textadventure.EProperty;
import textadventure.ERemoveAction;
import textadventure.ETarget;
import textadventure.ETile;
import textadventure.EWhileEffect;
import textadventure.EWorld;
import textadventure.model.Battle;
import textadventure.model.Item;
import textadventure.model.Person;
import textadventure.model.Tile;
import textadventure.model.World;
import textadventure.model.listeners.GetPersonsObserver;
import textadventure.model.listeners.StopUsingObserver;
import textadventure.model.listeners.TACondition;
import textadventure.model.listeners.TAObserver;
import textadventure.model.listeners.TAObserverHelper;
import textadventure.model.listeners.UseOnObserver;
import textadventure.resource.textadventure.util.AbstractTextadventureInterpreter;

/**
 * Process the EMF model to initialize the world and set up the behavior of its elements.
 * The interprete(Stack<EObject> context) method first processes all model elements and then
 * starts the event loop. The model elements are processed by invoking the super implementation
 * of interprete. To process a specific model element, also the super implementation must be invoked.
 * 
 * The interprete_XXX methods process specific model elements. If subsequently referenced
 * model elements must be processed, they should be passed to addObjectsToInterpret(...) followed
 * by an invocation to super.interprete(context).
 * 
 * The context is a stack EMF model entities which are currently being processed. This way, an
 * interprete_XXX method can access the parent entity of the currently processed entity. For this
 * reason, before invoking the super.interprete(context) method, first the current entity should be
 * pushed onto the context stack.
 * 
 * @author bockischcm
 *
 */
public class TextadventureInterpreter extends
		AbstractTextadventureInterpreter<Void, Stack<EObject>> {
	
	TextadventureConsole console;

	@Override
	public Void interprete(Stack<EObject> context) {
		// create a console that can be used to play
		console = new TextadventureConsole();
		
		// this processes the EMF model:
		// create the corresponding World elements (Person, Tile, Item)
		// set up proper observers to realize their behavior
		super.interprete(context);
		
		// now start the event loop to allow playing the game
		new TextadventureEventLoop(console).start();
		
		return null;
	}
	
	@Override
	public Void interprete_textadventure_EWorld(EWorld eWorld, Stack<EObject> context) {
		context.push(eWorld);
		
		// initialize the 2-dimensional array of Tiles in the World 
		org.eclipse.emf.common.util.EList<ETile> eTiles = eWorld.getTiles();
		int width = 0;
		int height = 0;
		for (ETile eTile : eTiles) {
			if (eTile.getX() >= width)
				width = eTile.getX() + 1;
			if (eTile.getY() >= height)
				height = eTile.getY() + 1;
		}
		World.getInstance().setDimension(width, height);
		
		// process all tile specifications
		addObjectsToInterprete(eTiles);
		super.interprete(context);
		
		// process all item specifications
		addObjectsToInterprete(eWorld.getItems());
		super.interprete(context);
		
		// process all person specifications
		addObjectsToInterprete(eWorld.getPersons());
		super.interprete(context);
		
		addObjectsToInterprete(eWorld.getCheckpoint());
		super.interprete(context);
		
		addObjectsToInterprete(eWorld.getBattle());
		super.interprete(context);
		
		addObjectsToInterprete(eWorld.getDestination());
		super.interprete(context);
		
		// set the initial player person on the World as specified in the EMR model
		World.getInstance().setPlayerPerson(Person.getPerson(eWorld.getPlayerPerson().getName()));
		
		context.pop();
		return null;
	}
	
	@Override
	public Void interprete_textadventure_ETile(ETile eTile, Stack<EObject> context) {
		// create a Tile object with the specified properties and add it to the world at the specified coordinates
		Tile tile = new textadventure.model.Tile(eTile.getId(), eTile.getDescription());
		World.getInstance().setTile(tile, eTile.getX(), eTile.getY());
		return null;
	}
	
	@Override
	public Void interprete_textadventure_EItem(EItem eItem, Stack<EObject> context) {
		context.push(eItem);
		
		// create an Item object with the specified properties and add it to the world
		Item item = new Item(eItem.getName());
		Tile.getTile(eItem.getLocation().getId()).addItem(item);
		
		// if there are effects specified for the item, set up the corresponding behavior. 
		addObjectsToInterprete(eItem.getEffects());
		super.interprete(context);

		context.pop();
		return null;
	}
	
	@Override
	public Void interprete_textadventure_EBattle(EBattle eBattle, Stack<EObject> context) {
		
		Battle battle = new Battle(Person.getPerson(eBattle.getEnemy().getName()), eBattle.getLogics());
		Tile.getTile(eBattle.getLocation().getId()).setBattle(battle);
		
		return null;
	}
	
	@Override
	public Void interprete_textadventure_EDestination(EDestination eDestination, Stack<EObject> context) {
		
		Tile.getTile(eDestination.getPoint().getId()).setAsDestination();
		
		return null;
	}
	
	@Override
	public Void interprete_textadventure_EPerson(EPerson ePerson, Stack<EObject> context) {
		context.push(ePerson);

		// create a Person object with the specified properties and add it to the world
		Person person = new Person(ePerson.getName(), 
				ePerson.getHp(), ePerson.getAttack(), ePerson.getDefense());
		Tile.getTile(ePerson.getLocation().getId()).addPerson(person);

		// of there are abilites speficified for this person, set up the corresponding behavior
		addObjectsToInterprete(ePerson.getAbilities());
		super.interprete(context);

		context.pop();
		return null;
	}
	
	@Override
	public Void interprete_textadventure_EWhileEffect(
			final EWhileEffect eWhileEffect, Stack<EObject> context) {
		
		// the item which has this effect
		final EItem eItem = (EItem) context.peek();
		
		// there may be multiple actions associated with this effect
		// first set up observers for all actions
		final List<TAObserver> observerActions = new ArrayList<TAObserver>();
		for (EAction eAction : eWhileEffect.getAction()) {
			if (eAction instanceof ERemoveAction) {
				ERemoveAction eRemoveAction = (ERemoveAction) eAction;

				// first set up observer for removing a value from a property list ...
				GetPersonsObserver observer = setupRemoveFromTileObserver(eItem, eRemoveAction.getFrom(), eRemoveAction.getWhat().getId());
				observer.disable();
				observerActions.add(observer);
			}
			else {
				throw new UnimplementedException(eAction); 
			}
		}
		
		// all observers created from the actions are initially disabled
		// only when the specified condition becomes satisfied, they are enabled and the specified context value is bound
		// set up observers for when the condition starts and ends to be setisfied
		// the observers enable/disable the action observers and set an attribute for the bound value
		setupEffectCondition(eWhileEffect.getCondition(), eWhileEffect.getBinding(), eItem, observerActions);
		
		return null;
	}

	/**
	 * Creates an observer which removes a value from a Tile's list-property.
	 * 
	 * @param eItem the item that has the effect to remove something from a Tile
	 * @param eFrom a description of the list-property from which something should be removed
	 * @param what the name of the observer's attribute that stores the value which should be removed
	 * @return
	 */
	private GetPersonsObserver setupRemoveFromTileObserver(EItem eItem, EListDescription eFrom, final String what) {
		if (eFrom.getTarget() == ETarget.TILE) {
			// ... it's a property of a Tile object
			if (eFrom.getProperty() == EProperty.PERSONS) {
				// ... it's the Persons property
				
				// now define the observer
				GetPersonsObserver observer = new GetPersonsObserver(Item.getItem(eItem.getName())) {
					
					@Override
					public List<Person> notify(Tile tile, List<Person> result) {
						if (!hasAttribute(what))
							return result;
						List<Person> resultList = new ArrayList<Person>();
						for (Person person : result) {
							if (!person.equals(getAttriute(what)))
								resultList.add(person);
						}
						return Collections.unmodifiableList(resultList);
					}
				};
				
				Tile.getPersonsObservers.addObserver(observer);
				return observer;
			}
			else {
				throw new UnimplementedException(eFrom.getProperty());
			}
		}
		else {
			throw new UnimplementedException(eFrom.getTarget());
		}
	}

	/**
	 * Creates two observers that enable respectively disable all the passed observerActions
	 * when a specific conditions becomes satisfied respectively becomes dissatisfied.
	 * 
	 * @param eCondition the condition specification
	 * @param eBinding a specification of which context value to bind when the condition becomes satisfied,
	 * 					and of the attribute name under which the bound value should be stored
	 * @param eItem the item which specified the effect condition
	 * @param observerActions the observers whose enabling/disabling is controlled by the condition
	 */
	private void setupEffectCondition(final ECondition eCondition, final EBinding eBinding,
			final EItem eItem, final List<TAObserver> observerActions) {
		if (eCondition == ECondition.USED) {
			if (eBinding.getParticipant() == EParticipant.ON) {
				// the effect should be enabled, while an item is used on a person
				
				// the condition becomes satisfied when the use(Person) method is invoked
				// on the specified item
				Item.useOnObservers.addObserver(new UseOnObserver(Item.getItem(eItem.getName())) {
					
					@Override
					public void notify(Item item, Person on) {
						// only do something if the item that is currently used
						// really is the item for which we currently set up its behavior
						if (item != Item.getItem(eItem.getName()))
							return;
						
						for (TAObserver observer : observerActions) {
							observer.enable();
							observer.setAttribute(eBinding.getId(), on);
						}
					}
				});

				// the condition becomes dissatisfied when the stopUsing() method is invoked
				// on the specified item
				Item.stopUsingObservers.addObserver(new StopUsingObserver(Item.getItem(eItem.getName())) {
					
					@Override
					public void notify(Item item) {
						// only do something if the item that is currently used
						// really is the item for which we currently set up its behavior
						if (item != Item.getItem(eItem.getName()))
							return;
						
						for (TAObserver observer : observerActions) {
							observer.disable();
							observer.removeAttribute(eBinding.getId());
						}
					}
				});
			}
			else {
				throw new UnimplementedException(eBinding.getParticipant());
			}
		}
		else {
			throw new UnimplementedException(eCondition);
		}
	}
	
	@Override
	public Void interprete_textadventure_EIgnoreItemAbility(
			EIgnoreItemAbility eIgnoreItemAbility, Stack<EObject> context) {
		// The person for which the ability is specified
		final EPerson ePerson = (EPerson) context.peek();
		// The item whose effects should be ignored
		final EItem eIgnoredItem = eIgnoreItemAbility.getIgnoredItem();
		
		// Effects are realized as observers. Therefore, we speficy a condition
		// that prevents observers from being applied if the current player person
		// is the person for which the ability is specified
		TAObserverHelper.addCondition(new TACondition() {
			
			@Override
			public boolean isApplicable(TAObserver observer) {
				Person ignoringPerson = Person.getPerson(ePerson.getName());
				Person currentPerson = World.getInstance().getPlayerPerson();
				Item ignoredItem = Item.getItem(eIgnoredItem.getName());
				
				if (ignoringPerson == currentPerson && observer.getOrigin() == ignoredItem) {
					return false;
				}
				else {
					return true;
				}
			}
		});
		
		return null;
	}
	
}
