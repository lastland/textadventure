package textadventure.interpreter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;

import org.eclipse.ui.console.ConsolePlugin;
import org.eclipse.ui.console.IConsole;
import org.eclipse.ui.console.IConsoleManager;
import org.eclipse.ui.console.IOConsole;

/**
 * Helper class that allows to read/write from/to Eclipse's Console View.
 * @author bockischcm
 *
 */
public class TextadventureConsole {

	private PrintStream out;
	private BufferedReader in;
	private IOConsole console;

	public TextadventureConsole() {
		ConsolePlugin plugin = ConsolePlugin.getDefault();
		IConsoleManager conMan = plugin.getConsoleManager();
		
		IConsole[] existing = conMan.getConsoles();
		for (int i = 0; i < existing.length; i++) {
			if ("Textadventure".equals(existing[i].getName())) {
				console = (IOConsole) existing[i];
				break;
			}
		}
		
		if (console == null) {
			// no console found, so create a new one
			console = new IOConsole("Textadventure", null);
			conMan.addConsoles(new IConsole[] { console });
		}
		
		conMan.showConsoleView(console);
		
		out = new PrintStream(console.newOutputStream());
		in = new BufferedReader(new InputStreamReader(console.getInputStream()));
	}
	
	public void println(Object o) {
		out.println(o);
	}
	
	public void println() {
		out.println();
	}

	public String readln() {
		try {
			return in.readLine();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

}
