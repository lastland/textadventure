package textadventure.interpreter;

import org.eclipse.emf.common.util.Enumerator;
import org.eclipse.emf.ecore.EObject;

public class UnimplementedException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3838131412140620125L;

	public UnimplementedException(EObject eObject) {
		super("Currently no implementation available for: " + eObject + "("  + eObject.getClass() + ")");
	}

	public UnimplementedException(Enumerator eValue) {
		super("Currently no implementation available for: " + eValue + "("  + eValue.getClass() + ")");
	}

}
