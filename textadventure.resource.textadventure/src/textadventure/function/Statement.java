package textadventure.function;

import textadventure.interpreter.TextadventureConsole;

public interface Statement {
	public void run(RunTime runtime, TextadventureConsole console);
}