package textadventure.function;

import java.util.List;
import java.lang.reflect.Method;

import textadventure.interpreter.TextadventureConsole;

public class MethodCallStatement implements Statement {
	
	private String object;
	private String method;
	private List<Object> params;
	
	public MethodCallStatement(String object, String method, List<Object> params) {
		this.object = object;
		this.method = method;
		this.params = params;
	}

	@Override
	public void run(RunTime runtime, TextadventureConsole console) {
		if (runtime.contains(object)) {
			try {
				Object tgt = runtime.getVal(object);
				Method m = tgt.getClass().getMethod(method, params.get(0).getClass());
				m.invoke(tgt, params.get(0));
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			throw new RuntimeException("No such object called " + object + "!");
		}
	}

}
