package textadventure.function;

import textadventure.interpreter.TextadventureConsole;

public class PrintStatement implements Statement {
	
	private String output;
	
	public PrintStatement(String output) {
		this.output = output;
	}

	@Override
	public void run(RunTime t, TextadventureConsole console) {
		console.println(output);
	}
}