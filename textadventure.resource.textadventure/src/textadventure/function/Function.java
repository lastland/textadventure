package textadventure.function;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.emf.common.util.EList;

import textadventure.EFunction;
import textadventure.EMethodCallStatement;
import textadventure.EPrintStatement;
import textadventure.ERemovePlayerStatement;
import textadventure.EStatement;
import textadventure.interpreter.TextadventureConsole;
import textadventure.model.Person;
import textadventure.model.World;

public class Function {
	private LinkedList<String> params;
	private LinkedList<Statement> statements;
	
	public Function(EFunction eFunction) {
		params = new LinkedList<String>();
		statements = new LinkedList<Statement>();
		
		for (String s: eFunction.getParams()) {
			params.addLast(s);
		}
		
		for (EStatement s: eFunction.getBody().getStatements()) {
			if (s instanceof EPrintStatement) {
				statements.addLast(new PrintStatement(((EPrintStatement)s).getOutput()));
			} else if (s instanceof ERemovePlayerStatement) {
				statements.addLast(new RemovePlayerStatement(Person.getPerson(((ERemovePlayerStatement)s).getTarget().getName())));
			} else if (s instanceof EMethodCallStatement) {
				EMethodCallStatement e = (EMethodCallStatement)s;
				statements.addLast(new MethodCallStatement(e.getObject(), e.getMethod(), Arrays.asList(e.getParams().toArray())));
			}
		}
	}
	
	public List<String> getParams() {
		return params;
	}
	
	public void run(RunTime r, TextadventureConsole console) {
		for (Statement s: statements) {
			s.run(r, console);
		}
	}
}
