package textadventure.function;

import java.util.HashMap;

public class RunTime {
	private HashMap<String, Object> memory;
	
	public RunTime() {
		memory = new HashMap<String, Object>();
	}
	
	public void assign(String id, Object val) {
		memory.put(id, val);
	}
	
	public Object getVal(String id) {
		return memory.get(id);
	}
	
	public boolean contains(String id) {
		return memory.containsKey(id);
	}
}