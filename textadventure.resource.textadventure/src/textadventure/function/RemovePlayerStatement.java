package textadventure.function;

import textadventure.interpreter.TextadventureConsole;
import textadventure.model.Person;

public class RemovePlayerStatement implements Statement {
	
	private Person target;
	
	public RemovePlayerStatement(Person target) {
		this.target = target;
	}

	@Override
	public void run(RunTime runtime, TextadventureConsole console) {
		target.die();
		console.println(target.getName() + " is gone.");
	}
}
