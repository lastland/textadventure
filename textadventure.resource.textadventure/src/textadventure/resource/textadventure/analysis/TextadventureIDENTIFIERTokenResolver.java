/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package textadventure.resource.textadventure.analysis;

public class TextadventureIDENTIFIERTokenResolver implements textadventure.resource.textadventure.ITextadventureTokenResolver {
	
	private textadventure.resource.textadventure.analysis.TextadventureDefaultTokenResolver defaultTokenResolver = new textadventure.resource.textadventure.analysis.TextadventureDefaultTokenResolver(true);
	
	public String deResolve(Object value, org.eclipse.emf.ecore.EStructuralFeature feature, org.eclipse.emf.ecore.EObject container) {
		// By default token de-resolving is delegated to the DefaultTokenResolver.
		String result = defaultTokenResolver.deResolve(value, feature, container, null, null, null);
		return result;
	}
	
	public void resolve(String lexem, org.eclipse.emf.ecore.EStructuralFeature feature, textadventure.resource.textadventure.ITextadventureTokenResolveResult result) {
		// By default token resolving is delegated to the DefaultTokenResolver.
		defaultTokenResolver.resolve(lexem, feature, result, null, null, null);
	}
	
	public void setOptions(java.util.Map<?,?> options) {
		defaultTokenResolver.setOptions(options);
	}
	
}
