/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package textadventure.resource.textadventure.analysis;

public class EBattleLocationReferenceResolver implements textadventure.resource.textadventure.ITextadventureReferenceResolver<textadventure.EBattle, textadventure.ETile> {
	
	private textadventure.resource.textadventure.analysis.TextadventureDefaultResolverDelegate<textadventure.EBattle, textadventure.ETile> delegate = new textadventure.resource.textadventure.analysis.TextadventureDefaultResolverDelegate<textadventure.EBattle, textadventure.ETile>();
	
	public void resolve(String identifier, textadventure.EBattle container, org.eclipse.emf.ecore.EReference reference, int position, boolean resolveFuzzy, final textadventure.resource.textadventure.ITextadventureReferenceResolveResult<textadventure.ETile> result) {
		delegate.resolve(identifier, container, reference, position, resolveFuzzy, result);
	}
	
	public String deResolve(textadventure.ETile element, textadventure.EBattle container, org.eclipse.emf.ecore.EReference reference) {
		return delegate.deResolve(element, container, reference);
	}
	
	public void setOptions(java.util.Map<?,?> options) {
		// save options in a field or leave method empty if this resolver does not depend
		// on any option
	}
	
}
