/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package textadventure.resource.textadventure.analysis;

public class ERemoveActionWhatReferenceResolver implements textadventure.resource.textadventure.ITextadventureReferenceResolver<textadventure.ERemoveAction, textadventure.EBinding> {
	
	private textadventure.resource.textadventure.analysis.TextadventureDefaultResolverDelegate<textadventure.ERemoveAction, textadventure.EBinding> delegate = new textadventure.resource.textadventure.analysis.TextadventureDefaultResolverDelegate<textadventure.ERemoveAction, textadventure.EBinding>();
	
	public void resolve(String identifier, textadventure.ERemoveAction container, org.eclipse.emf.ecore.EReference reference, int position, boolean resolveFuzzy, final textadventure.resource.textadventure.ITextadventureReferenceResolveResult<textadventure.EBinding> result) {
		delegate.resolve(identifier, container, reference, position, resolveFuzzy, result);
	}
	
	public String deResolve(textadventure.EBinding element, textadventure.ERemoveAction container, org.eclipse.emf.ecore.EReference reference) {
		return delegate.deResolve(element, container, reference);
	}
	
	public void setOptions(java.util.Map<?,?> options) {
		// save options in a field or leave method empty if this resolver does not depend
		// on any option
	}
	
}
