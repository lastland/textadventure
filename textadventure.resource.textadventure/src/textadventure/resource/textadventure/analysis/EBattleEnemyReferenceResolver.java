/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package textadventure.resource.textadventure.analysis;

public class EBattleEnemyReferenceResolver implements textadventure.resource.textadventure.ITextadventureReferenceResolver<textadventure.EBattle, textadventure.EPerson> {
	
	private textadventure.resource.textadventure.analysis.TextadventureDefaultResolverDelegate<textadventure.EBattle, textadventure.EPerson> delegate = new textadventure.resource.textadventure.analysis.TextadventureDefaultResolverDelegate<textadventure.EBattle, textadventure.EPerson>();
	
	public void resolve(String identifier, textadventure.EBattle container, org.eclipse.emf.ecore.EReference reference, int position, boolean resolveFuzzy, final textadventure.resource.textadventure.ITextadventureReferenceResolveResult<textadventure.EPerson> result) {
		delegate.resolve(identifier, container, reference, position, resolveFuzzy, result);
	}
	
	public String deResolve(textadventure.EPerson element, textadventure.EBattle container, org.eclipse.emf.ecore.EReference reference) {
		return delegate.deResolve(element, container, reference);
	}
	
	public void setOptions(java.util.Map<?,?> options) {
		// save options in a field or leave method empty if this resolver does not depend
		// on any option
	}
	
}
