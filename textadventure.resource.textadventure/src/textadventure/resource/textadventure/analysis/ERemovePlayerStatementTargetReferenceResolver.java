/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package textadventure.resource.textadventure.analysis;

public class ERemovePlayerStatementTargetReferenceResolver implements textadventure.resource.textadventure.ITextadventureReferenceResolver<textadventure.ERemovePlayerStatement, textadventure.EPerson> {
	
	private textadventure.resource.textadventure.analysis.TextadventureDefaultResolverDelegate<textadventure.ERemovePlayerStatement, textadventure.EPerson> delegate = new textadventure.resource.textadventure.analysis.TextadventureDefaultResolverDelegate<textadventure.ERemovePlayerStatement, textadventure.EPerson>();
	
	public void resolve(String identifier, textadventure.ERemovePlayerStatement container, org.eclipse.emf.ecore.EReference reference, int position, boolean resolveFuzzy, final textadventure.resource.textadventure.ITextadventureReferenceResolveResult<textadventure.EPerson> result) {
		delegate.resolve(identifier, container, reference, position, resolveFuzzy, result);
	}
	
	public String deResolve(textadventure.EPerson element, textadventure.ERemovePlayerStatement container, org.eclipse.emf.ecore.EReference reference) {
		return delegate.deResolve(element, container, reference);
	}
	
	public void setOptions(java.util.Map<?,?> options) {
		// save options in a field or leave method empty if this resolver does not depend
		// on any option
	}
	
}
