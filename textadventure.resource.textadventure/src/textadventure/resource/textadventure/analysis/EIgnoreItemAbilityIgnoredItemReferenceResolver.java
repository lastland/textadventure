/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package textadventure.resource.textadventure.analysis;

public class EIgnoreItemAbilityIgnoredItemReferenceResolver implements textadventure.resource.textadventure.ITextadventureReferenceResolver<textadventure.EIgnoreItemAbility, textadventure.EItem> {
	
	private textadventure.resource.textadventure.analysis.TextadventureDefaultResolverDelegate<textadventure.EIgnoreItemAbility, textadventure.EItem> delegate = new textadventure.resource.textadventure.analysis.TextadventureDefaultResolverDelegate<textadventure.EIgnoreItemAbility, textadventure.EItem>();
	
	public void resolve(String identifier, textadventure.EIgnoreItemAbility container, org.eclipse.emf.ecore.EReference reference, int position, boolean resolveFuzzy, final textadventure.resource.textadventure.ITextadventureReferenceResolveResult<textadventure.EItem> result) {
		delegate.resolve(identifier, container, reference, position, resolveFuzzy, result);
	}
	
	public String deResolve(textadventure.EItem element, textadventure.EIgnoreItemAbility container, org.eclipse.emf.ecore.EReference reference) {
		return delegate.deResolve(element, container, reference);
	}
	
	public void setOptions(java.util.Map<?,?> options) {
		// save options in a field or leave method empty if this resolver does not depend
		// on any option
	}
	
}
