package textadventure.model.listeners;

import textadventure.model.Person;
import textadventure.model.Tile;
import textadventure.model.WorldEntity;

public abstract class RemovePersonObserver extends TAObserver {

	public RemovePersonObserver(WorldEntity origin) {
		super(origin);
	}

	public abstract Person notify(Tile tile, Person person);

}
