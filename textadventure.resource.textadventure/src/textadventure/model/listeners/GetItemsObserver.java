package textadventure.model.listeners;

import java.util.List;

import textadventure.model.Item;
import textadventure.model.Tile;
import textadventure.model.WorldEntity;

public abstract class GetItemsObserver extends TAObserver {

	public GetItemsObserver(WorldEntity origin) {
		super(origin);
	}

	public abstract List<Item> notify(Tile tile, List<Item> result);

}
