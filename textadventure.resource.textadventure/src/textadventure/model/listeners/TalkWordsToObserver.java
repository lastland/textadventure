package textadventure.model.listeners;

import textadventure.model.Person;
import textadventure.model.WorldEntity;

public abstract class TalkWordsToObserver extends TAObserver {

	public TalkWordsToObserver(WorldEntity origin) {
		super(origin);
	}

	public abstract void notify(Person person, String words, Person to);
}
