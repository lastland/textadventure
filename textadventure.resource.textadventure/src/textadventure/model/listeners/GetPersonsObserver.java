package textadventure.model.listeners;

import java.util.List;

import textadventure.model.Person;
import textadventure.model.Tile;
import textadventure.model.WorldEntity;

public abstract class GetPersonsObserver extends TAObserver {

	public GetPersonsObserver(WorldEntity origin) {
		super(origin);
	}

	public abstract List<Person> notify(Tile tile, List<Person> result);

}
