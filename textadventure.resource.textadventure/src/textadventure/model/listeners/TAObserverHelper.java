package textadventure.model.listeners;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class TAObserverHelper<O extends TAObserver> implements Iterable<O> {

	private final List<O> observers = new ArrayList<O>();
	private final static List<TACondition> conditions = new ArrayList<TACondition>();
	
	public void addObserver(O observer) {
		observers.add(observer);
	}

	public void removeObserver(O observer) {
		observers.remove(observer);
	}

	public static void addCondition(TACondition condition) {
		conditions.add(condition);
	}

	public static void removeCondition(TACondition condition) {
		conditions.remove(condition);
	}

	@Override
	public Iterator<O> iterator() {
		List<O> enabledObservers = new ArrayList<O>();
		for (O o : observers) {
			if (o.isEnabled() && isApplicable(o))
				enabledObservers.add(o);
		}
		return enabledObservers.iterator();
	}

	private static boolean isApplicable(TAObserver o) {
		for (TACondition condition : conditions) {
			if (!condition.isApplicable(o)) {
				return false;
			}
		}
		return true;
	}
	
	
	
}
