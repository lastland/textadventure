package textadventure.model.listeners;

import java.util.List;

import textadventure.model.Item;
import textadventure.model.Person;
import textadventure.model.WorldEntity;

public abstract class AddToInventoryObserver extends TAObserver {

	public AddToInventoryObserver(WorldEntity origin) {
		super(origin);
	}

	public abstract Item notify(Person person, List<Item> inventory, Item item);
}
