package textadventure.model.listeners;

import java.util.HashMap;
import java.util.Map;

import textadventure.model.WorldEntity;

public abstract class TAObserver {

	private Map<String, Object> attributes = new HashMap<String, Object>();
	
	private boolean isEnabled = true;
	
	private WorldEntity origin;
	
	public TAObserver(WorldEntity origin) {
		this.origin = origin;
	}
	
	public void setAttribute(String key, Object value) {
		attributes.put(key, value);
	}
	
	public void removeAttribute(String key) {
		attributes.remove(key);
	}
	
	public Object getAttriute(String key) {
		return attributes.get(key);
	}
	
	public boolean hasAttribute(String key) {
		return attributes.containsKey(key);
	}
	
	public void enable() {
		this.isEnabled = true;
	}
	
	public void disable() {
		this.isEnabled = false;
	}
	
	public boolean isEnabled() {
		return this.isEnabled;
	}
	
	public WorldEntity getOrigin() {
		return origin;
	}
	
}
