package textadventure.model.listeners;

import textadventure.model.Item;
import textadventure.model.Tile;
import textadventure.model.WorldEntity;

public abstract class AddItemObserver extends TAObserver {

	public AddItemObserver(WorldEntity origin) {
		super(origin);
	}

	public abstract Item notify(Tile tile, Item item);
	
}
