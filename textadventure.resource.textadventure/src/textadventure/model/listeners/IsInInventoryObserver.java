package textadventure.model.listeners;

import java.util.List;

import textadventure.model.Item;
import textadventure.model.Person;
import textadventure.model.WorldEntity;

public abstract class IsInInventoryObserver extends TAObserver {

	public IsInInventoryObserver(WorldEntity origin) {
		super(origin);
	}

	public abstract boolean notify(Person person, List<Item> inventory, Item item, boolean result);
	
}
