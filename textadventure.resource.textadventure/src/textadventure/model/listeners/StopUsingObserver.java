package textadventure.model.listeners;

import textadventure.model.Item;
import textadventure.model.WorldEntity;

public abstract class StopUsingObserver extends TAObserver {

	public StopUsingObserver(WorldEntity origin) {
		super(origin);
	}

	public abstract void notify(Item item);
	
}
