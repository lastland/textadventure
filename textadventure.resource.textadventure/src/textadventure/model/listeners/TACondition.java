package textadventure.model.listeners;

public interface TACondition {
	
	public boolean isApplicable(TAObserver observer);

}
