package textadventure.model.listeners;

import java.util.List;

import textadventure.model.Item;
import textadventure.model.Person;
import textadventure.model.WorldEntity;

public abstract class InspectInventoryObserver extends TAObserver {

	public InspectInventoryObserver(WorldEntity origin) {
		super(origin);
	}

	public abstract List<Item> notify(Person person, List<Item> result);
	
}
