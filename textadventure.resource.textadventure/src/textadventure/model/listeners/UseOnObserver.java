package textadventure.model.listeners;

import textadventure.model.Item;
import textadventure.model.Person;
import textadventure.model.WorldEntity;

public abstract class UseOnObserver extends TAObserver {

	public UseOnObserver(WorldEntity origin) {
		super(origin);
	}

	public abstract void notify(Item item, Person on);
	
}
