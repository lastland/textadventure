package textadventure.model.listeners;

import java.util.List;

import textadventure.model.Item;
import textadventure.model.WorldEntity;

public abstract class UseWithObserver extends TAObserver {

	public UseWithObserver(WorldEntity origin) {
		super(origin);
	}

	public abstract void notify(Item item, List<Item> with);
	
}
