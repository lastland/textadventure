package textadventure.model.listeners;

import textadventure.model.Item;
import textadventure.model.Tile;
import textadventure.model.WorldEntity;

public abstract class RemoveItemObserver extends TAObserver {

	public RemoveItemObserver(WorldEntity origin) {
		super(origin);
	}

	public abstract Item notify(Tile tile, Item item);
	
}
