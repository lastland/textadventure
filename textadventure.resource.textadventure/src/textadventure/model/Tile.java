package textadventure.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import textadventure.model.listeners.AddItemObserver;
import textadventure.model.listeners.AddPersonObserver;
import textadventure.model.listeners.GetItemsObserver;
import textadventure.model.listeners.GetPersonsObserver;
import textadventure.model.listeners.RemoveItemObserver;
import textadventure.model.listeners.RemovePersonObserver;
import textadventure.model.listeners.TAObserverHelper;

public class Tile implements WorldEntity {

	public static final TAObserverHelper<AddItemObserver> addItemObservers = new TAObserverHelper<AddItemObserver>();
	public static final TAObserverHelper<RemoveItemObserver> removeItemObservers = new TAObserverHelper<RemoveItemObserver>();
	public static final TAObserverHelper<GetItemsObserver> getItemsObservers = new TAObserverHelper<GetItemsObserver>();
	public static final TAObserverHelper<AddPersonObserver> addPersonObservers = new TAObserverHelper<AddPersonObserver>();
	public static final TAObserverHelper<RemovePersonObserver> removePersonObservers = new TAObserverHelper<RemovePersonObserver>();
	public static final TAObserverHelper<GetPersonsObserver> getPersonsObservers = new TAObserverHelper<GetPersonsObserver>();

	private Battle battle = null;
	private boolean isDestination = false;
	
	private final String description;
	
	private final List<Item> items = new ArrayList<Item>();
	
	private final List<Person> persons = new ArrayList<Person>();

	private String id;

	private final static Map<String, Tile> name2tile = new HashMap<String, Tile>();

	public static Tile getTile(String id) {
		return name2tile.get(id);
	}
	
	public Tile(String id, String description) {
		super();
		this.id = id;
		name2tile.put(id, this);
		this.description = description;
	}
	
	public void setBattle(Battle battle) {
		this.battle = battle;
	}
	
	public void removeBattle() {
		this.battle = null;
	}
	
	public Battle getBattle() {
		return battle;
	}
	
	public boolean hasBattle() {
		return battle != null;
	}
	
	public boolean isDestination() {
		return isDestination;
	}
	
	public void setAsDestination() {
		isDestination = true;
	}
	
	public void addItem(Item item) {
		for (AddItemObserver observer : addItemObservers) {
			item = observer.notify(this, item);
		}
		if (item != null)
			items.add(item);
	}
	
	public void removeItem(Item item) {
		for (RemoveItemObserver observer : removeItemObservers) {
			item = observer.notify(this, item);
		}
		if (item != null)
			items.remove(item);
	}
	
	public List<Item> getItems() {
		List<Item> result = Collections.unmodifiableList(items);
		for (GetItemsObserver observer : getItemsObservers) {
			result = observer.notify(this, result);
		}
		return result;
	}
	
	public void addPerson(Person person) {
		for (AddPersonObserver observer : addPersonObservers) {
			person = observer.notify(this, person);
		}
		if (person != null)
			persons.add(person);
	}
	
	public void removePerson(Person person) {
		for (RemovePersonObserver observer : removePersonObservers) {
			person = observer.notify(this, person);
		}
		if (person != null)
			persons.remove(person);
	}
	
	public List<Person> getPersons() {
		List<Person> result = Collections.unmodifiableList(persons);
		for (GetPersonsObserver observer : getPersonsObservers) {
			result = observer.notify(this, result);
		}
		return result;
	}
	
	public String getDescription() {
		return description;
	}
	
	public String getId() {
		return id;
	}
	
	@Override
	public String toString() {
		return "Tile : " + getDescription();
	}
}
