package textadventure.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import textadventure.model.listeners.AddToInventoryObserver;
import textadventure.model.listeners.InspectInventoryObserver;
import textadventure.model.listeners.IsInInventoryObserver;
import textadventure.model.listeners.RemoveFromInventoryObserver;
import textadventure.model.listeners.TAObserverHelper;
import textadventure.model.listeners.TalkWordsToObserver;

public class Person implements WorldEntity {
	
	public static final TAObserverHelper<InspectInventoryObserver> inspectInventoryObservers = new TAObserverHelper<InspectInventoryObserver>();
	public static final TAObserverHelper<AddToInventoryObserver> addToInventoryObservers = new TAObserverHelper<AddToInventoryObserver>();
	public static final TAObserverHelper<RemoveFromInventoryObserver> removeFromInventoryObservers = new TAObserverHelper<RemoveFromInventoryObserver>();
	public static final TAObserverHelper<IsInInventoryObserver> isInInventoryObservers = new TAObserverHelper<IsInInventoryObserver>();
	public static final TAObserverHelper<TalkWordsToObserver> talkWordsToObservers = new TAObserverHelper<TalkWordsToObserver>();

	private int hp;
	private int attack;
	private int defense;
	private boolean defending = false;
	
	private final String name;

	private final List<Item> inventory = new ArrayList<Item>();
	
	private final static Map<String, Person> name2person = new HashMap<String, Person>();

	public static Person getPerson(String name) {
		return name2person.get(name);
	}
	
	public Person(String name, int hp, int attack, int defense) {
		super();
		this.name = name;
		name2person.put(name, this);
		this.hp = hp;
		this.attack = attack;
		this.defense = defense;
	}

	public List<Item> inspectInventory() {
		List<Item> result = Collections.unmodifiableList(inventory);
		for (InspectInventoryObserver observer : inspectInventoryObservers) {
			result = observer.notify(this, result);
		}
		return result;
	}
	
	public void addToInventory(Item item) {
		for (AddToInventoryObserver observer : addToInventoryObservers) {
			item = observer.notify(this, Collections.unmodifiableList(inventory), item);
		}
		if (item !=null)
			inventory.add(item);
	}
	
	public void removeFromInventory(Item item) {
		for (RemoveFromInventoryObserver observer : removeFromInventoryObservers) {
			item = observer.notify(this, Collections.unmodifiableList(inventory), item);
		}
		if (item !=null)
		inventory.remove(item);
	}
	
	public boolean isInInventory(Item item) {
		boolean result = inventory.contains(item);
		for (IsInInventoryObserver observer : isInInventoryObservers) {
			result = observer.notify(this, inventory, item, result);
		}
		return result;
	}
	
	public void talk(String words, Person to) {
		for (TalkWordsToObserver observer : talkWordsToObservers) {
			observer.notify(this, words, to);
		}		
	}
	
	public int getHp() {
		return hp;
	}
	
	public int getAttack() {
		return attack;
	}
	
	public int getDefense() {
		return defense;
	}
	
	public void defend() {
		defending = true;
	}
	
	public void cancelDefend() {
		defending = false;
	}
	
	public int hitBy(Person enemy) {
		int damage = (enemy.getAttack() - defense * (defending ? 2 : 1));
		hp -= damage;
		return damage;
	}
	
	public boolean isDead() {
		return hp <= 0;
	}
	
	public void die() {
		name2person.remove(name);
	}
	
	public String getName() {
		return name;
	}
	
	@Override
	public String toString() {
		return "Person : " + getName();
	}
	
	public void become(String name) {
		World.getInstance().setPlayerPerson(Person.getPerson(name));
	}

}
