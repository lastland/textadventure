package textadventure.model;

import java.util.LinkedList;
import java.util.List;

import org.eclipse.emf.common.util.EList;

import textadventure.EBattleCustomization;
import textadventure.function.Function;
import textadventure.function.RunTime;
import textadventure.interpreter.TextadventureConsole;

public class Battle implements WorldEntity {
	
	public static final int firstTurnNumber = 1;
	
	private int turnsNumber = 0;
	
	private final static int beforeBattleVal = 0;
	private final static int afterBattleVal = 1;
	private final static int onEachTurnVal = 2;
	
	private Person enemy;
	
	private LinkedList<Function> beforeBattleFunction;
	private LinkedList<Function> afterBattleFunction;
	private LinkedList<Function> onEachTurnFunction;
	
	public Battle(Person enemy, EList<EBattleCustomization> eList) {
		
		beforeBattleFunction = new LinkedList<Function>();
		afterBattleFunction = new LinkedList<Function>();
		onEachTurnFunction = new LinkedList<Function>();
		
		this.enemy = enemy;
		for (int i = 0; i < eList.size(); i++) {
			EBattleCustomization c = eList.get(i);
 			switch (c.getWhen().getValue()) {
			case beforeBattleVal:
				beforeBattleFunction.addLast(new Function(c.getFunc()));
				break;
			case afterBattleVal:
				afterBattleFunction.addLast(new Function(c.getFunc()));
				break;
			case onEachTurnVal:
				onEachTurnFunction.addLast(new Function(c.getFunc()));
				break;
			}
		}
	}
	
	public void runBeforeBattleFunctions(TextadventureConsole console) {
		for (Function f: beforeBattleFunction) {
			List<String> params = f.getParams();
			RunTime r = new RunTime();
			r.assign(params.get(0), World.getInstance().getPlayerPerson());
			r.assign(params.get(1), enemy);
			f.run(r, console);
		}
	}
	
	public void runAfterBattleFunctions(TextadventureConsole console) {
		for (Function f: afterBattleFunction) {
			List<String> params = f.getParams();
			RunTime r = new RunTime();
			r.assign(params.get(0), World.getInstance().getPlayerPerson());
			r.assign(params.get(1), enemy);
			f.run(r, console);
		}
	}
	
	public void runOnEachTurnFunctions(TextadventureConsole console) {
		for (Function f: onEachTurnFunction) {
			List<String> params = f.getParams();
			RunTime r = new RunTime();
			r.assign(params.get(0), turnsNumber);
			r.assign(params.get(1), World.getInstance().getPlayerPerson());
			r.assign(params.get(2), enemy);
			f.run(r, console);
		}
	}
	
	public int newTurn() {
		return ++turnsNumber;
	}
	
	public Person getEnemy() {
		return enemy;
	}
}