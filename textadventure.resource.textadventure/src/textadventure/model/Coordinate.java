package textadventure.model;

public class Coordinate {
	public final int x, y;

	public Coordinate(int x, int y) {
		super();
		this.x = x;
		this.y = y;
	}

	@Override
	public String toString() {
		return "Coordinate: (" + x + ", " + y + ")";
	}
}