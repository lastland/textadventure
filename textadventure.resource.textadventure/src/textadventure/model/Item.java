package textadventure.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import textadventure.model.listeners.StopUsingObserver;
import textadventure.model.listeners.TAObserverHelper;
import textadventure.model.listeners.UseOnObserver;
import textadventure.model.listeners.UseWithObserver;

public class Item implements WorldEntity {
	
	public static final TAObserverHelper<UseWithObserver> useWithObservers = new TAObserverHelper<UseWithObserver>();
	public static final TAObserverHelper<UseOnObserver> useOnObservers = new TAObserverHelper<UseOnObserver>();
	public static final TAObserverHelper<StopUsingObserver> stopUsingObservers = new TAObserverHelper<StopUsingObserver>();

	
	private final String name;
	
	private final Map<String, Object> properties = new HashMap<String, Object>();
	
	private final static Map<String, Item> name2item = new HashMap<String, Item>();

	public static Item getItem(String name) {
		return name2item.get(name);
	}
	
	public Item(String name) {
		this.name = name;
		name2item.put(name, this);
	}
	
	public void setProperty(String propertyName, Object value) {
		properties.put(propertyName, value);
	}
	
	public Object getProperty(String propertyName) {
		return properties.get(propertyName);
	}
		
	public void use(List<Item> with) {
		for (UseWithObserver observer : useWithObservers)
			observer.notify(this, with);
	}
	
	public void use(Person on) {
		for (UseOnObserver observer : useOnObservers)
			observer.notify(this, on);
	}
	
	public void stopUsing() {
		for (StopUsingObserver observer : stopUsingObservers)
			observer.notify(this);
	}

	public String getName() {
		return name;
	}
	
	@Override
	public String toString() {
		return "Item : " + getName();
	}
}
