/**
 */
package textadventure.tests;

import junit.framework.TestCase;

import textadventure.EAbility;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>EAbility</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class EAbilityTest extends TestCase {

	/**
	 * The fixture for this EAbility test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EAbility fixture = null;

	/**
	 * Constructs a new EAbility test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAbilityTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this EAbility test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(EAbility fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this EAbility test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EAbility getFixture() {
		return fixture;
	}

} //EAbilityTest
