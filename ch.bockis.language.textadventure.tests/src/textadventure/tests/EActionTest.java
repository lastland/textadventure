/**
 */
package textadventure.tests;

import junit.framework.TestCase;

import textadventure.EAction;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>EAction</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class EActionTest extends TestCase {

	/**
	 * The fixture for this EAction test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EAction fixture = null;

	/**
	 * Constructs a new EAction test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EActionTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this EAction test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(EAction fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this EAction test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EAction getFixture() {
		return fixture;
	}

} //EActionTest
