/**
 */
package textadventure.tests;

import junit.textui.TestRunner;

import textadventure.EIgnoreItemAbility;
import textadventure.TextadventureFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>EIgnore Item Ability</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class EIgnoreItemAbilityTest extends EAbilityTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(EIgnoreItemAbilityTest.class);
	}

	/**
	 * Constructs a new EIgnore Item Ability test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EIgnoreItemAbilityTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this EIgnore Item Ability test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EIgnoreItemAbility getFixture() {
		return (EIgnoreItemAbility)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(TextadventureFactory.eINSTANCE.createEIgnoreItemAbility());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //EIgnoreItemAbilityTest
