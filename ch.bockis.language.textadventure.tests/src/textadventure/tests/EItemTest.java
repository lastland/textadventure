/**
 */
package textadventure.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import textadventure.EItem;
import textadventure.TextadventureFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>EItem</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class EItemTest extends TestCase {

	/**
	 * The fixture for this EItem test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EItem fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(EItemTest.class);
	}

	/**
	 * Constructs a new EItem test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EItemTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this EItem test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(EItem fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this EItem test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EItem getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(TextadventureFactory.eINSTANCE.createEItem());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //EItemTest
