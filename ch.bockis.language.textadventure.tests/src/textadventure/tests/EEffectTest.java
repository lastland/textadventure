/**
 */
package textadventure.tests;

import junit.framework.TestCase;

import textadventure.EEffect;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>EEffect</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class EEffectTest extends TestCase {

	/**
	 * The fixture for this EEffect test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EEffect fixture = null;

	/**
	 * Constructs a new EEffect test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEffectTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this EEffect test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(EEffect fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this EEffect test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EEffect getFixture() {
		return fixture;
	}

} //EEffectTest
