/**
 */
package textadventure.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import textadventure.ETile;
import textadventure.TextadventureFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>ETile</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class ETileTest extends TestCase {

	/**
	 * The fixture for this ETile test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ETile fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(ETileTest.class);
	}

	/**
	 * Constructs a new ETile test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ETileTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this ETile test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(ETile fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this ETile test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ETile getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(TextadventureFactory.eINSTANCE.createETile());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //ETileTest
